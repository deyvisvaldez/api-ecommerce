<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/*$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});*/

$factory->define(App\Entities\Item::class, function (Faker\Generator $faker) {

    $name = $faker->name;
    return [
        'uuid' => $faker->uuid,
        'name' => $name,
        'business_code' => $name,
        'slug' => str_slug($name),
        'description' => 'description',
        'published' => true,
        'distributor_published' => true,
        'user_id' => 1,
        'company_id' => 1,
        'category_id' => 1,
        'subcategory_id' => 1,
        'type' => 1,
        'referencial_price' => 23
    ];
});
