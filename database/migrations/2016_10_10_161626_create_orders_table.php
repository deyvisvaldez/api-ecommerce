<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid')->unique();
            $table->string('doc_code')->nullable(); //Número de Boleta, Factura
            $table->string('code')->unique(); // OVE, OPE, OCO
            $table->text('description')->nullable();
            $table->integer('order_type')->unsigned(); //1= venta, 2= pedido, 3= cotizacion, 4= Ingreso, 5= Egreso
            $table->decimal('total')->nullable();
            $table->integer('person_id')->unsigned();
            $table->integer('user_id')->unsigned(); //1= Bot
            $table->integer('status')->unsigned(); //1=pendiente, 2=confirmado, 3=cancelado, 4 = entregado
            $table->string('shipping_mode')->default('Iré a Wiltex'); //destino y nombre de transporte
            $table->decimal('transport_cost', 20, 2)->default(0); //costo de transporte, si el costo es 0 irá a Wiltex
            $table->integer('currency_id')->unsigned(); //ID de la moneda para el pdf publico
            $table->integer('account_id')->default(0);
            $table->integer('company_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
