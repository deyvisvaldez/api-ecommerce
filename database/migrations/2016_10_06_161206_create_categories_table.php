<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('name');
            $table->string('slug');
            $table->string('image')->default('http://wiltex.la/defaults/category.png');
            $table->string('image_path')->nullable();
            $table->string('image_thumb')->default('http://wiltex.la/defaults/category.png');
            $table->string('image_thumb_path')->nullable();
            $table->integer('type')->unsigned(); //1= producto, 2= personalizado, 3=insumos
            $table->text('description');
            $table->boolean('published');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
}
