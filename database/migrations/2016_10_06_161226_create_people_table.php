<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('identity_document')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('avatar')->default('http://wiltex.la/defaults/user.png');
            $table->string('avatar_path')->nullable();
            $table->string('avatar_thumb')->default('http://wiltex.la/defaults/user.png');
            $table->string('avatar_thumb_path')->nullable();
            $table->boolean('gender')->default(true);// 0=femenino, 1=masculino
            $table->date('birthdate')->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('region_id')->unsigned()->nullable();
            $table->integer('person_type')->unsigned();// 1=Natural(DNI), 2=Jurídica(RUC/RUT)
            $table->integer('company_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('people');
    }
}
