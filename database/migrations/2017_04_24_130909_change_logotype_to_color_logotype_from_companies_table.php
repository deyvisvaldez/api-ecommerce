<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeLogotypeToColorLogotypeFromCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->renameColumn('logotype', 'color_logotype');
            $table->renameColumn('logotype_path', 'color_logotype_path');
            $table->renameColumn('logotype_thumb', 'color_logotype_thumb');
            $table->renameColumn('logotype_thumb_path', 'color_logotype_thumb_path');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->renameColumn('color_logotype', 'logotype');
            $table->renameColumn('color_logotype_path', 'logotype_path');
            $table->renameColumn('color_logotype_thumb', 'logotype_thumb');
            $table->renameColumn('color_logotype_thumb_path', 'logotype_thumb_path');
        });
    }
}
