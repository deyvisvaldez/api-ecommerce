<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('company_registration');
            $table->string('business_name')->nullable();
            $table->string('slug');
            $table->string('legal_name');
            $table->string('slogan_title')->nullable();
            $table->string('slogan_subtitle')->nullable();
            $table->string('representative');
            $table->string('dropbox_path')->nullable();
            $table->string('logotype')->default('http://wiltex.la/defaults/company.png');
            $table->string('logotype_path')->nullable();
            $table->string('logotype_thumb')->default('http://wiltex.la/defaults/company.png');
            $table->string('logotype_thumb_path')->nullable();
            $table->text('description')->nullable();
            $table->date('anniversary')->nullable();
            $table->string('facebook')->nullable();
            $table->string('google_plus')->nullable();
            $table->string('youtube')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->string('ga_code')->nullable();
            $table->string('mailchimp_list_id')->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('region_id')->unsigned()->nullable();
            $table->integer('province_id')->unsigned()->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->integer('district_id')->unsigned()->nullable();
            $table->integer('sector_id')->unsigned()->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}
