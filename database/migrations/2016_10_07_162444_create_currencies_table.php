<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('name');
            $table->string('code');
            $table->string('symbol');
            $table->text('description');
            $table->string('flag_image')->nullable();
            $table->string('flag_image_path')->nullable();
            $table->string('flag_image_thumb')->nullable();
            $table->string('flag_image_thumb_path')->nullable();
            $table->integer('default_currency');
            $table->integer('company_id');
            $table->integer('country_id');
            $table->integer('user_id');
            $table->boolean('active');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('currencies');
    }
}
