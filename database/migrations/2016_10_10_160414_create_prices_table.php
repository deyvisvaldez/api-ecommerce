<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('name')->nullable();
            $table->decimal('price', 20, 2);
            $table->integer('type')->default(1);//1= Web, 2= Tienda, 3= Mayor
            $table->integer('currency_id');
            $table->text('content')->nullable();
            $table->decimal('promotional_price')->default(0);
            $table->boolean('available_promotion')->default(false);
            $table->integer('purchase_volumen');
            $table->boolean('active')->default(true);
            $table->integer('user_id');
            $table->integer('presentation_id');
            $table->integer('company_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prices');
    }
}
