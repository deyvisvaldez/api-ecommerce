<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->text('description')->nullable();
            $table->decimal('amount', 20, 2);
            $table->integer('payment_type')->unsigned()->default(1); // 1=Ingreso, 2= Egreso
            $table->integer('payment_way')->unsigned()->default(1); //1=Efectivo, 2=Banco, cuentas
            $table->integer('user_id')->unsigned();
            $table->boolean('status'); //true= pagado, false=pendiente
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments');
    }
}
