<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreAttributesToAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->text('description')->nullable();
            $table->integer('country_id')->unsigned()->default(1);
            $table->integer('region_id')->unsigned()->default(1);
            $table->string('email')->nullable();
            $table->string('address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('country_id');
            $table->dropColumn('region_id');
            $table->dropColumn('email');
            $table->dropColumn('address');
        });
    }
}
