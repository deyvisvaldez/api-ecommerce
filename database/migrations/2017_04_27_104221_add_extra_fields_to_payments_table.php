<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraFieldsToPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->integer('concept_id')->default(1); //Ingreso por venta
            $table->integer('account_id')->default(1); //Cuenta interna - Efectivo
            $table->dropColumn('payment_type');
            $table->dropColumn('payment_way');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn('concept_id');
            $table->dropColumn('account_id');
            $table->integer('payment_type')->unsigned()->default(1);
            $table->integer('payment_way')->unsigned()->default(1);
        });
    }
}
