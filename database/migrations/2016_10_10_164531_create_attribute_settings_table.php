<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('model_id'); //id tupla
            $table->integer('model_type'); //1=business category, 2=category, 3=attributes (model_id: 1=brand, 2=presentation, 3=flavour, 4=size, 5=color, 6=quality, 7=property), 4=sizes, 5=colors
            $table->integer('company_id');
            $table->integer('order');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attribute_settings');
    }
}
