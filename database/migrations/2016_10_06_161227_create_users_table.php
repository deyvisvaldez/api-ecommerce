<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('username');
            $table->string('password');
            $table->integer('role');//1=SuperAdmin, 2=Admin, 3=Vendedor, 4=Distribuidor, 5=Cajero, 6=Logistica, 7=cliente
            $table->boolean('activated');
            $table->string('confirmation_code')->nullable();
            $table->integer('person_id');
            $table->integer('company_id');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
