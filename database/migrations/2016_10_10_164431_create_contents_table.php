<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->text('description');
            $table->string('resource')->nullable();
            $table->string('resource_path')->nullable();
            $table->string('resource_thumb')->nullable();
            $table->string('resource_thumb_path')->nullable();
            $table->integer('model_id'); // id tupla
            $table->integer('model_type'); // 1=company, 2=presentación, 3=sector, 4=post
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contents');
    }
}
