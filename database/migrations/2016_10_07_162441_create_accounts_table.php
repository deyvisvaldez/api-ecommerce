<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('account_name');
            $table->string('account_number');
            $table->integer('bank_id')->unsigned();
            $table->string('account_holder');
            $table->string('document_holder');
            $table->integer('account_type')->unsigned(); //1= Ahorros , 2= Crédito
            $table->boolean('published')->default(true);
            $table->integer('currency_id');
            $table->integer('company_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accounts');
    }
}
