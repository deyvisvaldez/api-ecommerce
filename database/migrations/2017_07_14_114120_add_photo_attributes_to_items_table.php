<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhotoAttributesToItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->string('referencial_photo')->nullable();
            $table->string('referencial_photo_path')->nullable();
            $table->string('referencial_photo_thumb')->nullable();
            $table->string('referencial_photo_thumb_path')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->dropColumn('referencial_photo');
            $table->dropColumn('referencial_photo_path');
            $table->dropColumn('referencial_photo_thumb');
            $table->dropColumn('referencial_photo_thumb_path');
        });
    }
}
