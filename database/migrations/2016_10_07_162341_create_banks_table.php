<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('bank_image')->default('http://wiltex.la/defaults/bank.png');;
            $table->string('bank_image_path')->nullable();
            $table->string('bank_image_thumb')->default('http://wiltex.la/defaults/bank.png');
            $table->string('bank_image_thumb_path')->nullable();
            $table->integer('type'); //1= entidad Bancaria(Banco) 2=Servicio Financiero(WU), 3= Empresa, 4=Caja Municipal, 5=Billetera Online
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
    }
}
