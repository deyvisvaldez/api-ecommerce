<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCelphoneToCellphoneAttributeToCellphonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cellphones', function (Blueprint $table) {
            $table->renameColumn('celphone', 'cellphone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cellphones', function (Blueprint $table) {
            $table->renameColumn('cellphone', 'celphone');
        });
    }
}
