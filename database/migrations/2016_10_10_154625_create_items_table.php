<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid')->unique();
            $table->string('business_code')->nullable();
            $table->string('name');
            $table->string('slug')->unique();
            $table->integer('category_id')->unsigned();
            $table->integer('subcategory_id')->unsigned();
            $table->text('description');
            $table->text('features')->nullable();
            $table->boolean('published');
            $table->integer('user_id')->unsigned();
            $table->integer('type')->unsigned();//1= producto, 2= personalizado, 3=insumos
            $table->string('ga_code')->nullable();
            $table->integer('company_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items');
    }
}
