<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageToSubcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subcategories', function (Blueprint $table) {
            $table->string('image')->nullable();
            $table->string('image_path')->nullable();
            $table->string('image_thumb')->nullable();
            $table->string('image_thumb_path')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subcategories', function (Blueprint $table) {
            $table->dropColumn('image');
            $table->dropColumn('image_path');
            $table->dropColumn('image_thumb');
            $table->dropColumn('image_thumb_path');
        });
    }
}
