<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type'); // 1= Ingreso a Producción (abastecer taller), 2= salida por venta, 3= traslado(salida), 4=traslado(ingreso), 5= ajuste de stock(ingreso), 6= ajuste stock(salida), 7=Devolución, 8=salida por entrega de pedido;
            $table->integer('inventory_id');
            $table->integer('quantity'); // cantidad a mover
            $table->text('description');
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movements');
    }
}
