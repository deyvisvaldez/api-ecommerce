<?php

use Illuminate\Database\Seeder;

//use Uuid;

use App\Entities\Country;
use App\Entities\Region;
use App\Entities\Transport;
use App\Entities\BranchOffice;
use App\Entities\Rule;
use App\Entities\Cost;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->delete();
        DB::table('regions')->delete();

        $country = Country::create([
            'name' => 'Perú'
        ]);

        $region = Region::create([
            'name' => 'Tacna',
            'country_id' => $country->id
        ]);

        $region = Region::create([
            'name' => 'Moquegua',
            'country_id' => $country->id
        ]);

        $country = Country::create([
            'name' => 'Chile'
        ]);

        $region = Region::create([
            'name' => 'Arica',
            'country_id' => $country->id
        ]);

        $region = Region::create([
            'name' => 'Antofagasta',
            'country_id' => $country->id
        ]);
    }
}
