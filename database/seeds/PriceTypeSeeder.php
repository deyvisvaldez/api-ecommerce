<?php

use App\Entities\PriceType;
use Illuminate\Database\Seeder;

class PriceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PriceType::create([
            'name' => 'Precio Web',
            'available' => true
        ]);

        PriceType::create([
            'name' => 'Precio Tienda',
            'available' => false
        ]);

        PriceType::create([
            'name' => 'Precio Distribuidoras',
            'available' => true
        ]);
    }
}
