<?php

use App\Entities\Address;
use App\Entities\AttributeSetting;
use App\Entities\Company;
use App\Entities\Email;
use App\Entities\Location;
use App\Entities\Person;
use App\Entities\PriceRule;
use App\Entities\User;
use App\Entities\Whatsapp;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->delete();
        DB::table('people')->delete();
        DB::table('users')->delete();
        DB::table('locations')->delete();
        DB::table('price_rules')->delete();

        /* ----------
            Wiltex
        */
        $company = Company::create([
            'uuid' => Uuid::generate(4)->string,
            'company_registration' => '123456789',
            'business_name' => 'Company',
            'slug' => 'company',
            'legal_name' => 'E.I.R.L.',
            'slogan_title' => 'Eslogan',
            'slogan_subtitle' => 'Frase',
            'representative' => 'Gerente',
            'anniversary' => '2000-12-01',
            'country_id' => 1,
            'region_id' => 1,
            'province_id' => 1,
            'city_id' => 1,
            'district_id' => 1,
            'sector_id' => 1,
            'active' => true,
            'facebook' => 'https://www.facebook.com/',
            'google_plus' => 'https://plus.google.com',
            'youtube' => ''
        ]);

        $email = Email::create([
            'email' => 'servicios@noveltie.la',
            'main' => true,
            'model_id' => $company->id,
            'model_type' => 2
        ]);

        Whatsapp::create([
            'whatsapp' => '+51 952983869',
            'main' => true,
            'model_id' => $company->id,
            'model_type' => 2
        ]);

        Address::create([
            'address' => 'Buena Vista, Alto de la Alianza',
            'main' => true,
            'model_id' => $company->id,
            'model_type' => 2
        ]);

        // Administradores
        $person = Person::create([
            'uuid' => Uuid::generate(4)->string,
            'identity_document' => '12345678',
            'first_name' => 'Admin',
            'last_name' => 'Admin',
            'gender' => true,
            'birthdate' => '1991-11-03',
            'country_id' => 1, //Peru
            'region_id' => 1, //Tacna
            'person_type' => 1,
            'company_id' => $company->id
        ]);

        //Usuarios asignados
        $user = User::create([
            'uuid' => Uuid::generate(4)->string,
            'username' => 'admin',
            'password' => 'admin',
            'role' => 1, //admin
            'activated' => true,
            'confirmation_code' => 'admin',
            'person_id' => $person->id,
            'company_id' => $company->id, //Empresa Tienda
        ]);

        Location::create([
            'uuid' => Uuid::generate(4)->string,
            'name' => 'Taller',
            'description' => '',
            'address' => '',
            'type' => 2,
            'company_id' => 1,
            'main' => true
        ]);
        Location::create([
            'uuid' => Uuid::generate(4)->string,
            'name' => 'Tienda',
            'description' => '',
            'address' => '',
            'type' => 3,
            'company_id' => 1,
            'main' => false
        ]);

        PriceRule::create([
            'name' => 'Precio U.',
            'quantity' => 1
        ]);
    }
}
