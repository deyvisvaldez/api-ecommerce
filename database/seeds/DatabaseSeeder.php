<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call('CategorySeeder');
        $this->call('AttributeSeeder');
        $this->call('CurrencySeeder');
        $this->call('CompanySeeder');
        $this->call('PriceTypeSeeder');
        $this->call('RegionSeeder');

        $this->call('BanksTableSeeder');
    }
}
