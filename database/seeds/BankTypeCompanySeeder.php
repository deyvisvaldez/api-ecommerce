<?php

use App\Entities\Bank;
use Illuminate\Database\Seeder;

class BankTypeCompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bank::create([
            'name' => 'Efectivo',
            'type' => 3
        ]);
    }
}
