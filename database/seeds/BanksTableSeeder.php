<?php

use Illuminate\Database\Seeder;
use App\Entities\Bank;

class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

//Bancos
        Bank::create([ // R 01
            'name' => 'BCP',
            'bank_image' => 'https://dl.dropboxusercontent.com/s/ioshldw0yqixjn1/bcp.png?dl=0',
            'bank_image_thumb' => 'https://dl.dropboxusercontent.com/s/ioshldw0yqixjn1/bcp.png?dl=0',
            'type' => 1,
            'description' => 'Banco de Crédito del Perú.',
        ]);

        Bank::create([ // R 02
            'name' => 'Interbank',
            'bank_image' => 'https://dl.dropboxusercontent.com/s/dj60y118ziq0g5r/interbank.png?dl=0',
            'bank_image_thumb' => 'https://dl.dropboxusercontent.com/s/dj60y118ziq0g5r/interbank.png?dl=0',
            'type' => 1,
            'description' => 'Banco Interbank',
        ]);

        Bank::create([ // R 03
            'name' => 'Scotiabank',
            'bank_image' => 'https://dl.dropboxusercontent.com/s/1x986mxtcb0byyv/Scotiabank.png?dl=0',
            'bank_image_thumb' => 'https://dl.dropboxusercontent.com/s/1x986mxtcb0byyv/Scotiabank.png?dl=0',
            'type' => 1,
            'description' => 'The Bank of Nova Scotia',
        ]);

        Bank::create([ // R 04
            'name' => 'BBVA',
            'bank_image' => 'https://dl.dropboxusercontent.com/s/s5ktr0m8nf3n9ca/bbva.png?dl=0',
            'bank_image_thumb' => 'https://dl.dropboxusercontent.com/s/s5ktr0m8nf3n9ca/bbva.png?dl=0',
            'type' => 1,
            'description' => 'Grupo BBVA Continental',
        ]);

        Bank::create([ // R 05
            'name' => 'BanBif',
            'bank_image' => 'https://dl.dropboxusercontent.com/s/961gdocgfeyg6y8/banbif.png?dl=0',
            'bank_image_thumb' => 'https://dl.dropboxusercontent.com/s/961gdocgfeyg6y8/banbif.png?dl=0',
            'type' => 1,
            'description' => 'Banco Interamericano de Finanzas',
        ]);

//servicios Financieros
        Bank::create([ // R 06
            'name' => 'Western Union',
            'bank_image' => 'https://dl.dropboxusercontent.com/s/ugvyb0desnl9soa/wu.png?dl=0',
            'bank_image_thumb' => 'https://dl.dropboxusercontent.com/s/ugvyb0desnl9soa/wu.png?dl=0',
            'type' => 2,
            'description' => 'The Western Union Company',
        ]);

        Bank::create([ // R 07
            'name' => 'MoneyGram',
            'bank_image' => 'https://dl.dropboxusercontent.com/s/sl1b99vqr84nbkr/moneygram.png?dl=0',
            'bank_image_thumb' => 'https://dl.dropboxusercontent.com/s/sl1b99vqr84nbkr/moneygram.png?dl=0',
            'type' => 2,
            'description' => 'MoneyGram International Inc.',
        ]);

        Bank::create([ // R 08
            'name' => 'AFEX',
            'bank_image' => 'https://dl.dropboxusercontent.com/s/pwho7g3tfa7dc84/afex.png?dl=0',
            'bank_image_thumb' => 'https://dl.dropboxusercontent.com/s/pwho7g3tfa7dc84/afex.png?dl=0',
            'type' => 2,
            'description' => 'AFEX TRANSFERENCIAS Y CAMBIOS.',
        ]);
    }
}
