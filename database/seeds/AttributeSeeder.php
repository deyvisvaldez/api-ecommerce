<?php

use Illuminate\Database\Seeder;

//use Uuid;

use App\Entities\Size;
use App\Entities\Color;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sizes')->delete();
        DB::table('colors')->delete();

        // Colores
        Color::create([
            'uuid' => Uuid::generate(4)->string,
            'name' => 'Blanco',
            'color_code' => '#FFF',
            'active' => true,
            'order' => 1,
        ]);

        Color::create([
            'uuid' => Uuid::generate(4)->string,
            'name' => 'Rojo Oscuro',
            'color_code' => 'red',
            'active' => true,
            'order' => 2,
        ]);

        Color::create([
            'uuid' => Uuid::generate(4)->string,
            'name' => 'Azul Electrico',
            'color_code' => '#0000a8',
            'active' => true,
            'order' => 3,
        ]);

        Color::create([
            'uuid' => Uuid::generate(4)->string,
            'name' => 'Verde Botella',
            'color_code' => '#006c00',
            'active' => true,
            'order' => 4,
        ]);

        //Tallas
        Size::create(array(
            'uuid' => Uuid::generate(4)->string,
            'name' => 'XS',
            'active' => true,
            'order' => 1,
        ));
        Size::create(array(
            'uuid' => Uuid::generate(4)->string,
            'name' => 'S',
            'active' => true,
            'order' => 2,
        ));
        Size::create(array(
            'uuid' => Uuid::generate(4)->string,
            'name' => 'M',
            'description' => 'Talla M',
            'active' => true,
            'order' => 3,
        ));

        Size::create([
            'uuid' => Uuid::generate(4)->string,
            'name' => 'L',
            'description' => 'Talla L',
            'active' => true,
            'order' => 4,
        ]);

        Size::create([
            'uuid' => Uuid::generate(4)->string,
            'name' => 'XL',
            'description' => 'Talla XL',
            'active' => true,
            'order' => 5,
        ]);

        Size::create([
            'uuid' => Uuid::generate(4)->string,
            'name' => 'XXL',
            'description' => 'Talla XXL',
            'active' => true,
            'order' => 6,
        ]);
    }
}
