<?php

use Illuminate\Database\Seeder;

use App\Entities\Category;
use App\Entities\Subcategory;

class CategoriesPivotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_subcategory')->delete();

        DB::connection('pgsql')->table('category_subcategory')->insert([
            ['category_id' => 2, 'subcategory_id' => 5, 'active' => true],
            ['category_id' => 2, 'subcategory_id' => 7, 'active' => true],
            ['category_id' => 2, 'subcategory_id' => 12, 'active' => true],
            ['category_id' => 2, 'subcategory_id' => 13, 'active' => true],
            ['category_id' => 2, 'subcategory_id' => 14, 'active' => true],
            ['category_id' => 2, 'subcategory_id' => 18, 'active' => true],
            ['category_id' => 2, 'subcategory_id' => 22, 'active' => true]
        ]);
    }
}