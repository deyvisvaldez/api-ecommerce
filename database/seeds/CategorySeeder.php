<?php

use Illuminate\Database\Seeder;

//use Uuid;

use App\Entities\Category;
use App\Entities\Subcategory;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('categories')->delete();
    	DB::table('subcategories')->delete();

        $category1 = Category::create([
            'uuid' => Uuid::generate(4)->string,
            'name' => 'Categoria 1',
            'slug' => 'categoria-1',
            'description' => 'Contamos con muchas novedades',
            'type' => 1,
            'published' => true,
        ]);

        $subcategory1 = Subcategory::create([
            'uuid' => Uuid::generate(4)->string,
            'name' => 'Subcategoria 1',
            'slug' => 'subcategoria-1',
            'description' => 'Poleras',
            //'category_id' => 1, //Educadoras
            'type' => 1,
            'published' => true
        ]);

        $subcategory2 = Subcategory::create([
            'uuid' => Uuid::generate(4)->string,
            'name' => 'Subcategoria 2',
            'slug' => 'subcategoria-2',
            'description' => 'Delantales',
            //'category_id' => 1, //Educadoras
            'type' => 1,
            'published' => true
        ]);


        $category1->subcategories()->attach($subcategory1->id, ['active' => true]);

        $category1->subcategories()->attach($subcategory2->id, ['active' => true]);

    }
}
