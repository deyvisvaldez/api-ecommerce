<?php

use Illuminate\Database\Seeder;

//use Uuid;

use App\Entities\Currency;
use App\Entities\ExchangeRate;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('currencies')->delete();
        DB::table('exchange_rates')->delete();

        Currency::create([
            'uuid' => Uuid::generate(4)->string,
            'name' => 'Nuevo Soles',
            'code' => 'PEN',
            'symbol' => 'S/.',
            'description' => str_random(10).' moneda peruana',
            'default_currency' => 1,
            'user_id' => 1,
            'company_id' => 1,
            'country_id' => 1,
            'active' => true,
            'decimal' =>true,
        ]);

        ExchangeRate::create([
            'uuid' => Uuid::generate(4)->string,
            'sales_exchange' => 1,
            'currency_id' => 1,
            'active' => true,
            'user_id' => 1
        ]);


        Currency::create([
            'uuid' => Uuid::generate(4)->string,
            'name' => 'Peso Chileno',
            'code' => 'CLP',
            'symbol' => '$.',
            'description' => str_random(10).' moneda chilena',
            'default_currency' => 0,
            'user_id' => 1,
            'company_id' => 1,
            'country_id' => 2,
            'active' => true,
            'decimal' =>false,
        ]);

        ExchangeRate::create([
            'uuid' => Uuid::generate(4)->string,
            'sales_exchange' => 210,
            'currency_id' => 2,
            'active' => true,
            'user_id' => 1
        ]);
    }
}
