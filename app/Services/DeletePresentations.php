<?php

namespace App\Services;

use App\Entities\Presentation;

class DeletePresentations
{
    public function execute($presentationIds, $locationId)
    {
        foreach ($presentationIds as $key => $presentationId) {
            $presentation = Presentation::with(['inventories' => function($query) use ($locationId) {
                $query->whereLocationId($locationId);
            }])
            ->with('web_prices')
            ->with('shop_prices')
            ->with('wholesale_prices')
            ->find($presentationId);

            foreach ($presentation->inventories as $key => $inventory) {
                $inventory->delete();
            }

            foreach ($presentation->web_prices as $key => $web_price) {
                $web_price->price_rules()->detach();
                $web_price->delete();
            }

            foreach ($presentation->shop_prices as $key => $shop_price) {
                $shop_price->price_rules()->detach();
                $shop_price->delete();
            }

            foreach ($presentation->wholesale_prices as $key => $wholesale_price) {
                $wholesale_price->price_rules()->detach();
                $wholesale_price->delete();
            }
            $presentation->delete();
        }
        return true;
    }
}