<?php

namespace App\Services;

use App\Uploaders\ImageUploader;

class UploadRecommendationImage
{
    public function execute($image, $extension)
    {
        $dropbox = new ImageUploader();

        $imagePath = '/products/recommendations';
        $dropbox->upload($imagePath, $image, $extension, 50);

        $data['image'] = $dropbox->getDropboxUrl();
        $data['image_path'] = $dropbox->getDropboxPath();

		return $data;
    }
}
