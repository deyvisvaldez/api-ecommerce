<?php

namespace App\Services;

use App\Repositories\MovementRepository;

class RegisterNewMovement
{
    private $movementRepository;

    public function __construct()
    {
        $this->movementRepository = new MovementRepository();
    }

    public function execute($data, $type, $origin, $destiny)
    {
        if (is_null($origin)) {
            $data['origin_destiny'] = $destiny;
        } elseif (is_null($destiny)) {
            $data['origin_destiny'] = $origin;
        } else {
            $data['origin_destiny'] = "$origin -> $destiny";
        }

        $data['type'] = $type;
        $newMovement = $this->movementRepository->create($data);
        return $newMovement;
    }
}
