<?php

namespace App\Services;

use App\Entities\Item;

class SetItemPromotionalPrice
{
    public function execute($itemId)
    {
        $item = Item::with('presentations.prices')->find($itemId);
        $promotion = false;

        foreach ($item->presentations as $key => $presentation) {
            foreach ($presentation->prices as $key => $price) {
                if ($price->available_promotion && $price->promotional_price > 0) {

                    $item->update([
                        'promotional_price' => $price->promotional_price,
                        'available_promotion' => $price->available_promotion
                    ]);

                    $promotion = true;
                    break;
                }
            }
        }

        if (! $promotion) {
            $item->update([
                'promotional_price' => 0,
                'available_promotion' => false
            ]);
        }

        return true;
    }
}