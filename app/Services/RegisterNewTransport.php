<?php

namespace App\Services;

use App\Repositories\TransportRepository;

class RegisterNewTransport
{
    private $transportRepository;

    public function __construct(TransportRepository $transportRepository)
    {
        $this->transportRepository = $transportRepository;
    }

    public function execute($data)
    {
        $this->transportRepository->create($data);
        return true;
    }
}