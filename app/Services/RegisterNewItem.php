<?php

namespace App\Services;

use App\Repositories\ItemRepository;
use App\Repositories\RecommendationRepository;
use App\Services\UploadRecommendationImage;
use App\Uploaders\ImageUploader;
use Uuid;

class RegisterNewItem
{
    private $itemRepository;

    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    public function execute($item, $userId)
    {
        $item['uuid'] = Uuid::generate(4)->string;
        $item['slug'] = str_slug($item['name'], '-');
        $item['published'] = true;
        $item['user_id'] = $userId;
        $item['type'] = 1;
        $item['business_code'] = $item['code'];
        $item['company_id'] = 1;

        if (array_key_exists('measurement', $item)) {
            if ($item['measurement']['extension'] != '') {
                $path = '/products/measurement';
                $uploader = new ImageUploader();
                $uploader->upload($path, $item['measurement']['base64'], $item['measurement']['extension']);
                $item['measurement_table'] = $uploader->getDropboxUrl();
                $item['measurement_table_path'] = $uploader->getDropboxPath();
            }
        }

        $newItem = $this->itemRepository->create($item);

        foreach ($item['recommendations'] as $key => $recommendation) {
            $uploader = new UploadRecommendationImage;
            $data = $uploader->execute($recommendation['url'], $recommendation['extension']);
            $data['item_id'] = $newItem->id;

            $recommendationRepository = new RecommendationRepository;
            $recommendationRepository->create($data);
        }

        return $newItem;
    }
}
