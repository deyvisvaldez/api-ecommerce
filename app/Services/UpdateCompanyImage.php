<?php

namespace App\Services;

use App\Repositories\PhotoRepository;
use App\Uploaders\ImageUploader;

class UpdateCompanyImage
{
    protected $photoRepository;

    public function __construct(PhotoRepository $photoRepository)
    {
        $this->photoRepository = $photoRepository;
    }

    public function execute($data, $photoId)
    {
    	$content = [];
        $photo = $this->photoRepository->find($photoId);

        if ($data['extension'] != '') {
            $dropbox = new ImageUploader();
            $imagePath = '/companies/images';
            $dropbox->upload($imagePath, $data['base64'], $data['extension']);

            $content['title'] = $data['title'];
            $content['description'] = $data['description'];
            $content['resource'] = $dropbox->getDropboxUrl();
            $content['resource_path'] = $dropbox->getDropboxPath();
            $content['resource_thumb'] = $dropbox->getDropboxUrl();
            $content['resource_thumb_path'] = $dropbox->getDropboxPath();

            $dropbox->delete($photo->resource_path, $photo->resource);
        }

		$this->photoRepository->update($photo, $content);
        return true;
    }
}
