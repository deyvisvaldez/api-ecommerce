<?php

namespace App\Services;

use App\Repositories\RegionRepository;

class RegisterNewRegion
{
    private $regionRepository;

    public function __construct(RegionRepository $regionRepository)
    {
        $this->regionRepository = $regionRepository;
    }

    public function execute($data)
    {
        $this->regionRepository->create($data);
        return true;
    }
}