<?php

namespace App\Services;

use App\Entities\Cellphone;

class UpdateCellphone
{
    public function execute($data, $personId)
    {
        foreach ($data['cellphones'] as $key => $cellphone) {
            if ($cellphone['id'] > 0) {
                // Update
                $oneCellphone = Cellphone::find($cellphone['id']);
                $cellphone['main'] = false;
                if ($cellphone['id'] == $data['checked']) {
                    $cellphone['main'] = true;
                }
                $oneCellphone = $oneCellphone->update($cellphone);
            } else {
                //Create
                $cellphone['model_id'] = $personId;
                $cellphone['model_type'] = 1;
                $cellphone['main'] = false;
                if ($cellphone['id'] == $data['checked']) {
                    $cellphone['main'] = true;
                }
                $oneCellphone = Cellphone::create($cellphone);
            }
        }

        return true;
    }
}