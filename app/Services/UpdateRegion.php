<?php

namespace App\Services;

use App\Repositories\RegionRepository;

class Updateregion
{
    private $regionRepository;

    public function __construct(RegionRepository $regionRepository)
    {
        $this->regionRepository = $regionRepository;
    }

    public function execute($data, $regionId)
    {
        $region = $this->regionRepository->find($regionId);
        $this->regionRepository->update($region, $data);
        return true;
    }
}