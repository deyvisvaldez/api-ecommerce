<?php

namespace App\Services;

use App\Entities\Content;
use App\Entities\Inventory;
use App\Entities\Item;
use App\Entities\Location;
use App\Entities\Presentation;
use App\Entities\Price;
use App\Repositories\InventoryRepository;
use App\Repositories\ItemRepository;
use App\Services\RegisterNewMovement;
use Uuid;

class RegisterNewInventories
{
    /*protected $itemRepository;

    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }*/

    public function execute($data, $itemId, $userId, $production)
    {
        $lastPresentation = 0;

        $sizes = [];
        $colors = [];

        foreach ($data['inventories'] as $presentation ) {
            $sizes[] = $presentation['size_id'];
            $colors[] = $presentation['color_id'];
        }

        $item = Item::with('presentations.color')->find($itemId);
        $itemColors = new GetItemColors();
        $itemColors = $itemColors->execute($item);

        foreach ($data['inventories'] as $presentationAttributes ) {

            foreach ($itemColors as $key => $color) {

                $existence = Presentation::whereItemId($itemId)->whereSizeId($presentationAttributes['size_id'])->whereColorId($color['id'])->first();

                if (! $existence) {

                    $basePresentation = Presentation::with('photos')->with('web_prices.price_rules')
                    ->with('shop_prices.price_rules')->with('wholesale_prices.price_rules')
                    ->whereItemId($itemId)->whereColorId($color['id'])->first();

                    //dd($basePresentation->toArray());

                    $presentation = Presentation::create([
                        'uuid' => Uuid::generate(4)->string,
                        'item_id' => $itemId,
                        'size_id' => $presentationAttributes['size_id'],
                        'color_id' => $color['id'],
                        'published' => true,
                        'available' => true
                    ]);

                    $lastPresentation = $presentation;

                    foreach ($basePresentation->photos as $key => $photo) {
                        Content::create([
                            'uuid' => $photo->uuid,
                            'description' => $photo->description,
                            'resource' => $photo->resource,
                            'resource_path' => $photo->resource_path,
                            'resource_thumb' => $photo->resource_thumb,
                            'resource_thumb_path' => $photo->resource_thumb_path,
                            'model_id' => $presentation->id,
                            'model_type' => $photo->model_type,
                        ]);
                    }

                    foreach ($basePresentation->web_prices as $key => $webPrice) {
                        $referencialPrice = 0;
                        if ($webPrice->price_rules[0]->quantity == 1) {
                            $referencialPrice = $item->referencial_price;
                        }

                        $price = Price::create([
                            'uuid' => Uuid::generate(4)->string,
                            'type' => $webPrice->type,
                            'price' => $referencialPrice,
                            'currency_id' => $webPrice->currency_id,
                            'promotional_price' => 0,
                            'available_promotion' => false,
                            'active' => true,
                            'user_id' => $userId,
                            'presentation_id' => $presentation->id,
                            'company_id' => 1,
                        ]);

                        $price->price_rules()->attach($webPrice->price_rules[0]->id);
                    }

                    foreach ($basePresentation->shop_prices as $key => $shopPrice) {
                        $referencialPrice = 0;
                        if ($shopPrice->price_rules[0]->quantity == 1) {
                            $referencialPrice = $item->referencial_price;
                        }

                        $price = Price::create([
                            'uuid' => Uuid::generate(4)->string,
                            'type' => $shopPrice->type,
                            'price' => $referencialPrice,
                            'currency_id' => $shopPrice->currency_id,
                            'promotional_price' => 0,
                            'available_promotion' => false,
                            'active' => true,
                            'user_id' => $userId,
                            'presentation_id' => $presentation->id,
                            'company_id' => 1,
                        ]);

                        $price->price_rules()->attach($shopPrice->price_rules[0]->id);
                    }

                    foreach ($basePresentation->wholesale_prices as $key => $wholesalePrice) {
                        $referencialPrice = 0;
                        if ($wholesalePrice->price_rules[0]->quantity == 1) {
                            $referencialPrice = $item->referencial_price;
                        }

                        $price = Price::create([
                            'uuid' => Uuid::generate(4)->string,
                            'type' => $wholesalePrice->type,
                            'price' => $referencialPrice,
                            'currency_id' => $wholesalePrice->currency_id,
                            'promotional_price' => 0,
                            'available_promotion' => false,
                            'active' => true,
                            'user_id' => $userId,
                            'presentation_id' => $presentation->id,
                            'company_id' => 1,
                        ]);

                        $price->price_rules()->attach($wholesalePrice->price_rules[0]->id);
                    }

                    $flag = false;
                    $availableStock = 0;

                    foreach ($data['inventories'] as $key => $combination) {
                        if ($presentation->color_id == $combination['color_id'] && $presentation->size_id == $combination['size_id']) {
                            $availableStock = $data['specifications']['available_stock'];
                            $flag = true;
                            break;
                        }
                    }

                    $inventory = Inventory::create([
                        'uuid' => Uuid::generate(4)->string,
                        'location_id' => $data['location_id'],
                        'available_stock' => $availableStock,
                        'min_stock' => $data['specifications']['min_stock'],
                        'user_id' => $userId,
                        'company_id' => 1,
                        'presentation_id' => $presentation->id
                    ]);

                    if ($production && $flag) {
                        // Movement
                        $movement = new RegisterNewMovement();

                        /**
                         * In RegisterNewMovement, execute method passes as $type parameter
                         * 1= Ingreso a Producción (de taller a almacen)
                         */
                        $data['quantity'] = $data['specifications']['available_stock'];
                        $data['description'] = "Inventariado";
                        $data['user_id'] = $userId;
                        $data['inventory_id'] = $inventory->id;
                        $movement->execute($data, 1, null, $inventory->location->name);
                    }
                }
            }
        }

        return $lastPresentation;
    }
}