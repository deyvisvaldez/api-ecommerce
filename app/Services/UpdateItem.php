<?php

namespace App\Services;

use App\Repositories\ItemRepository;
use App\Repositories\RecommendationRepository;
use App\Services\UploadRecommendationImage;
use App\Uploaders\ImageUploader;

class UpdateItem
{
    private $itemRepository;

    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    public function execute($data, $itemId, $userId)
    {
        $item = $this->itemRepository->find($itemId);

        if (array_key_exists('code', $data)) {
            $data['business_code'] = $data['code'];
        }

        if (array_key_exists('name', $data)) {
            $data['slug'] = str_slug($data['name'], '-');
        }

        if (array_key_exists('measurement', $data)) {
            if ($data['measurement']['extension'] != '') {
                $path = '/products/measurement';
                $uploader = new ImageUploader();
                $uploader->upload($path, $item['table']['base64'], $item['table']['extension']);
                $data['measurement_table'] = $uploader->getDropboxUrl();
                $data['measurement_table_path'] = $uploader->getDropboxPath();

                $uploader->delete($item->measurement_table, $item->measurement_table_path);
            }
        }

        $data['user_id'] = $userId;
        //dd($data);
        $this->itemRepository->update($item, $data);

        if (array_key_exists('recommendations', $data)) {
            foreach ($data['recommendations'] as $key => $recommendation) {
                if ($recommendation['id'] < 1) {
                    $uploader = new UploadRecommendationImage;
                    $recm = $uploader->execute($recommendation['url'], $recommendation['extension']);
                    $recm['item_id'] = $item->id;

                    $recommendationRepository = new RecommendationRepository;
                    $recommendationRepository->create($recm);
                }
            }
        }

        if (array_key_exists('published', $data)) {
            foreach ($item->presentations as $key => $presentation) {
                $presentation->published = ! (boolean) $presentation->published;
                $presentation->save();
            }
        }

        return $item;
    }
}
