<?php

namespace App\Services;

use Uuid;
use App\Entities\Inventory;
use App\Entities\Price;
use App\Entities\Content;
use App\Repositories\LocationRepository;

class RegisterNewLocation
{
    private $locationRepository;

    public function __construct(LocationRepository $locationRepository)
    {
        $this->locationRepository = $locationRepository;
    }

    public function execute($data, $userId)
    {
        $data['uuid'] = Uuid::generate(4)->string;
        $data['company_id'] = 1;
        $location = $this->locationRepository->create($data);

        return true;
    }
}