<?php

namespace App\Services;

use App\Entities\Person;
use App\Entities\User;
use App\Uploaders\ImageUploader;
use Uuid;

class CreateNewUser
{
    public function execute($data)
    {

        if ($data['avatar']['extension'] != '') {
            $uploader = new ImageUploader();

            $path = '/users/image';
            $thumbPath = '/users/thumb';

            $image = $data['avatar'];

            $uploader->upload($path, $image['base64'], $image['extension']);
            $person['avatar'] = $uploader->getDropboxUrl();
            $person['avatar_path'] = $uploader->getDropboxPath();

            $uploader->upload($thumbPath, $image['base64'], $image['extension'], 100);
            $person['avatar_thumb'] = $uploader->getDropboxUrl();
            $person['avatar_thumb_path'] = $uploader->getDropboxPath();
        } else {
            unset($data['avatar']);
        }

        $existingPerson = Person::whereIdentityDocument($data['identity_document'])->first();

        $person['first_name'] = $data['first_name'];
        $person['last_name'] = $data['last_name'];
        $person['gender'] = $data['gender'];
        $person['birthdate'] = $data['birthdate'];
        $person['country_id'] = $data['country_id'];
        $person['region_id'] = $data['region_id'];

        if ($existingPerson) {
            $existingPerson->update($person);
            $person = $existingPerson;
        } else {
            $person['uuid'] = Uuid::generate(4)->string;
            $person['company_id'] = 1;
            $person['person_type'] = 1;
            $person['identity_document'] = $data['identity_document'];

            $person = Person::create($person);
        }

        /**
         * Create User
         */
        $user['uuid'] = Uuid::generate(4)->string;
        $user['activated'] = true;
        $user['username'] = $data['identity_document'];
        $user['password'] = $data['password'];
        $user['role'] = $data['role'];
        $user['company_id'] = 1;

        $person->createUser($user);

        /**
         * Create Email
         */
        $email['email'] = $data['email'];
        $email['model_type'] = 1;
        $email['main'] = true;

        $person->addEmail($email);

        /**
         * Create Phone
         */
        $phone['phone'] = $data['phone'];
        $phone['model_type'] = 1;
        $phone['main'] = true;

        $person->addPhone($phone);

        /**
         * Create Cellphone
         */
        $cellphone['cellphone'] = $data['cellphone'];
        $cellphone['model_type'] = 1;
        $cellphone['main'] = true;

        $person->addCellphone($cellphone);

        /**
         * Create Address
         */
        $address['address'] = $data['address'];
        $address['model_type'] = 1;
        $address['main'] = true;

        $person->addAddress($address);

        /**
         * Create Whatsapp
         */
        $whatsapp['whatsapp'] = $data['whatsapp'];
        $whatsapp['model_type'] = 1;
        $whatsapp['main'] = true;

        $person->addWhatsapp($whatsapp);

        return $person;
    }
}