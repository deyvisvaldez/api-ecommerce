<?php

namespace App\Services;

use App\Repositories\SubcategoryRepository;
use App\Uploaders\ImageUploader;
use DB;
use Uuid;

class UpdateSubcategory
{
    private $subcategoryRepository;

    public function __construct(SubcategoryRepository $subcategoryRepository)
    {
        $this->subcategoryRepository = $subcategoryRepository;
    }

    public function execute($data, $subcategoryId)
    {
        $subcategory = $this->subcategoryRepository->find($subcategoryId);
        $data['slug'] = str_slug($data['name'], '-');

        $image = $data['image'];
        unset($data['image']);
        if ($image['extension'] != '') {
            $path = '/subcategories/image';
            $thumbPath = '/subcategories/image_thumb';
            $dropbox = new ImageUploader();

            $dropbox->upload($path, $image['base64'], $image['extension']);
            $data['image'] = $dropbox->getDropboxUrl();
            $data['image_path'] = $dropbox->getDropboxPath();

            $dropbox->upload($thumbPath, $image['base64'], $image['extension'], 100);
            $data['image_thumb'] = $dropbox->getDropboxUrl();
            $data['image_thumb_path'] = $dropbox->getDropboxPath();
        }
        $newsubcategory = $this->subcategoryRepository->update($subcategory, $data);

        DB::table('category_subcategory')
            ->where('id', $data['category_subcategory_id'])
            ->update([
                'order' => $data['order'],
                'active' => $data['active']
            ]);

        return $newsubcategory;
    }
}
