<?php

namespace App\Services;

use Uuid;
use App\Entities\Account;
use App\Repositories\AccountRepository;

class UpdateCompanyBankAccount
{
    private $accountRepository;

    public function __construct(AccountRepository $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }

    public function execute($data, $id)
    {
        $account = $this->accountRepository->find($id);
        if (array_key_exists('account_name', $data)) {
            $data['account_name'] = ($data['account_type'] == 1) ? 'Cuenta de Ahorros' : 'Cuenta de Crédito';
        }
        $this->accountRepository->update($account, $data);
        return $account;
    }
}

