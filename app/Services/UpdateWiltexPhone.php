<?php

namespace App\Services;

use App\Entities\Phone;

class UpdateWiltexPhone
{
    public function execute($data)
    {
        foreach ($data['phones'] as $key => $phone) {
            if ($phone['id'] > 0) {
                // Update
                $onePhone = Phone::find($phone['id']);
                $phone['main'] = false;
                if ($phone['id'] == $data['checked']) {
                    $phone['main'] = true;
                }
                $onePhone = $onePhone->update($phone);
            } else {
                //Create
                $phone['model_id'] = 1; //Wiltex
                $phone['model_type'] = 2; //Company
                $phone['main'] = false;
                if ($phone['id'] == $data['checked']) {
                    $phone['main'] = true;
                }
                $onePhone = Phone::create($phone);
            }
        }

        return true;
    }
}