<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Collection;

class GetItemColors
{
    public function execute($item)
    {
        $colors = [];

        foreach ($item->presentations as $key => $presentation) {
            if ($presentation->color) {
                $colors[] = [
                    'id' => (int) $presentation->color->id,
                    'color' => $presentation->color->name,
                    'code' => $presentation->color->color_code,
                    'order' => (int) $presentation->color->order
                ];
            }
        }

        $colors = array_values(array_unique($colors, SORT_REGULAR));

        return $colors;
    }
}