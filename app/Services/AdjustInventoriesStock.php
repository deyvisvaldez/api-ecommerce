<?php

namespace App\Services;

use App\Repositories\InventoryRepository;
use App\Services\RegisterNewMovement;

class AdjustInventoriesStock
{
    private $inventoryRepository;

    public function __construct(InventoryRepository $inventoryRepository)
    {
        $this->inventoryRepository = $inventoryRepository;
    }

    public function execute($presentationIds, $locationId, $userId)
    {
        foreach ($presentationIds as $key => $presentationId) {
            $inventory = $this->inventoryRepository->byLocation($presentationId['id'], $locationId);

            if ($presentationId['add']) {
                $newStock = (int)$inventory->available_stock + (int)$presentationId['quantity'];
            } else {
                $newStock = (int)$inventory->available_stock - (int)$presentationId['quantity'];
            }

            $inventory->available_stock = $newStock;
            $inventory->save();

            $movement = new RegisterNewMovement();

            /**
             * In RegisterNewMovement, execute method passes as $type parameter
             * 5= Ajuste de stock (Ingreso)
             * 6= ajuste de stock (Salida)
             */
            $data['quantity'] = $presentationId['quantity'];
            $data['user_id'] = $userId;
            $data['inventory_id'] = $inventory->id;
            if ($presentationId['add']) {
                $data['description'] = 'Ajuste de stock - Aumento';
                $movement->execute($data, 5, $inventory->location->name, null);
            } else {
                $data['description'] = 'Ajuste de stock - Salida';
                $movement->execute($data, 6, $inventory->location->name, null);
            }
        }

        return true;
    }
}
