<?php

namespace App\Services;

use App\Entities\Presentation;

class UpdatePresentationPrices
{
    public function execute($itemId, $colors, $sizes, $type, $rule, $price)
    {
        foreach ($colors as $key => $color) {
            foreach ($sizes as $key => $size) {
                $presentation = Presentation::whereItemId($itemId)->whereColorId($color['id'])->whereSizeId($size['id']);

                switch ($type) {
                    case 1:
                        $presentation = $presentation->with(['web_prices' => function ($query) use ($rule){
                            $query->with(['price_rules' => function($query) use ($rule) {
                                $query->whereQuantity($rule);
                            }])
                            ->whereHas('price_rules', function ($query) use ($rule) {
                                $query->whereQuantity($rule);
                            });
                        }])
                        ->first();

                        /**
                         * Update Price
                         */

                        $presentation->web_prices[0]->price = $price;
                        $presentation->web_prices[0]->save();

                        break;

                    case 2:
                        $presentation = $presentation->with(['shop_prices' => function ($query) use ($rule){
                            $query->with(['price_rules' => function($query) use ($rule) {
                                $query->whereQuantity($rule);
                            }])
                            ->whereHas('price_rules', function ($query) use ($rule) {
                                $query->whereQuantity($rule);
                            });
                        }])
                        ->first();

                        /**
                         * Update Price
                         */

                        $presentation->shop_prices[0]->price = $price;
                        $presentation->shop_prices[0]->save();

                        break;

                    case 3:
                        $presentation = $presentation->with(['wholesale_prices' => function ($query) use ($rule){
                            $query->with(['price_rules' => function($query) use ($rule) {
                                $query->whereQuantity($rule);
                            }])
                            ->whereHas('price_rules', function ($query) use ($rule) {
                                $query->whereQuantity($rule);
                            });
                        }])
                        ->first();

                        /**
                         * Update Price
                         */

                        $presentation->wholesale_prices[0]->price = $price;
                        $presentation->wholesale_prices[0]->save();

                        break;
                }
            }
        }

        return true;
    }
}
