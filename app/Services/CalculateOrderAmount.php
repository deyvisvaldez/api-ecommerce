<?php

namespace App\Services;

use App\Entities\Presentation;
use App\Entities\PriceRule;
use App\Repositories\PriceRuleRepository;

class CalculateOrderAmount
{
    public function __construct()
    {
        $this->priceRuleRepository = new PriceRuleRepository;
    }
    /**
     * @param  array  $presentations presentations id and quantity
     * @param  int    $orderType     1 = Venta, 2 = Pedido, 3 = Cotización
     * @param  int    $priceTypeId     1 = Web, 2 = Shop, 3 = Wholesale
     * @return float                Total Amount
     */
    public function execute(array $presentations, $orderType, $priceTypeId)
    {
        $total = 0;

        foreach ($presentations as $key => $presentation) {

            switch ($priceTypeId) {
                case 1:
                    $ruleQuantity = $this->priceRuleRepository->getRuleQuantity($presentation['quantity'], $presentation['presentation_id'], $priceTypeId);
                    $priceType = 'web_prices';
                    break;

                case 2:
                    $ruleQuantity = $this->priceRuleRepository->getRuleQuantity($presentation['quantity'], $presentation['presentation_id'], $priceTypeId);
                    $priceType = 'shop_prices';
                    break;

                case 3:
                    $ruleQuantity = $this->priceRuleRepository->getRuleQuantity($presentation['quantity'], $presentation['presentation_id'], $priceTypeId);
                    $priceType = 'wholesale_prices';
                    break;
            }

            $pre = Presentation::with([$priceType => function ($query) use ($ruleQuantity) {
                $query->with(['price_rules' => function($query) use ($ruleQuantity) {
                    $query->whereQuantity($ruleQuantity);
                }])
                ->whereHas('price_rules', function ($query) use ($ruleQuantity) {
                    $query->whereQuantity($ruleQuantity);
                });
            }])
            ->find($presentation['presentation_id']);

            $price = $pre->{$priceType}[0]->price;

            $total = $total + ((int) $presentation['quantity'] * $price);
        }
        return $total;
    }
}