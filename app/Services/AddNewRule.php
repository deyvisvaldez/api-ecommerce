<?php

namespace App\Services;

use App\Repositories\RuleRepository;

class AddNewRule
{
    private $ruleRepository;

    public function __construct(RuleRepository $ruleRepository)
    {
        $this->ruleRepository = $ruleRepository;
    }

    public function execute($data)
    {
        $data['unit_value'] = $data['rule'];
        $this->ruleRepository->create($data);
        return true;
    }
}