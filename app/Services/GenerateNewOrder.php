<?php

namespace App\Services;

use App\Entities\Location;
use App\Repositories\OrderRepository;
use App\Repositories\PresentationRepository;
use App\Repositories\PriceRuleRepository;
use App\Repositories\RegionRepository;
use App\Repositories\TransportRepository;
use Uuid;

class GenerateNewOrder
{
    private $orderRepository;
    private $regionRepository;
    private $presentationRepository;

    function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->regionRepository = new RegionRepository();
        $this->presentationRepository = new PresentationRepository();
        $this->priceRuleRepository = new PriceRuleRepository();
    }

    public function execute($data, $account, $code, $total, $personId, $items, $currencyId, $priceTypeId, $userId)
    {
        if ($data['flag']) {
            //Modificar esto
            $region = $this->regionRepository->withCountry($data['region_id']);
            //$data['shipping_address'] = $region->name.'-'.$region->country->name;
            $data['shipping_address'] = $data['address'];

            $region = $this->regionRepository->withTransport($data['region_id'], $data['transport_id']);
            $data['transport_cost'] = $region->transports[0]->pivot->price;
        }

        $location = Location::whereMain(true)->first();

        $data['uuid'] = Uuid::generate(4)->string;
        $data['order_type'] = 2;
        $data['company_id'] = 1;
        $data['status'] = 1;
        $data['code'] = $code;
        $data['total'] = $total;
        $data['person_id'] = $personId;
        if ($userId) {
            $data['user_id'] = $userId;
            $data['origin'] = 3;
        } else {
            $data['user_id'] = 1;
            $data['origin'] = 1;
        }
        $data['currency_id'] = $currencyId;
        $data['account_id'] = $account;
        $data['location_id'] = $location->id;

        $order = $this->orderRepository->create($data);

        foreach ($items as $key => $presentation) {
            $ruleQuantity = $this->priceRuleRepository->getRuleQuantity($presentation['quantity'], $presentation['presentation_id'], $priceTypeId);

            switch ($priceTypeId) {
                case 1:
                    $price = $this->presentationRepository->getPrice($presentation['presentation_id'], 'web_prices', $ruleQuantity);
                    break;

                case 3:
                    $price = $this->presentationRepository->getPrice($presentation['presentation_id'], 'wholesale_prices', $ruleQuantity);
                    break;
            }

            $order->presentations()->attach($presentation['presentation_id'], ['price' => $price , 'quantity' => $presentation['quantity']]);
        }

        return $order;
    }
}
