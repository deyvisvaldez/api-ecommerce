<?php

namespace App\Services;

use App\Repositories\PhotoRepository;
use App\Uploaders\ImageUploader;
use Uuid;

class SaveCompanyImage
{
    protected $photoRepository;

    public function __construct(PhotoRepository $photoRepository)
    {
        $this->photoRepository = $photoRepository;
    }

    public function execute($data)
    {
    	$content = [];
    	$image = $data['base64'];
    	$extension = $data['extension'];

    	$dropbox = new ImageUploader();

        $imagePath = '/companies/images';

        $dropbox->upload($imagePath, $image, $extension);

        $content['uuid'] = Uuid::generate(4)->string;
        $content['title'] = $data['title'];
        $content['description'] = $data['description'];
		$content['resource'] = $dropbox->getDropboxUrl();
		$content['resource_path'] = $dropbox->getDropboxPath();
        $content['resource_thumb'] = $dropbox->getDropboxUrl();
        $content['resource_thumb_path'] = $dropbox->getDropboxPath();
        $content['model_id'] = 1;
        $content['model_type'] = 1;
        $content['published'] = true;

		$this->photoRepository->create($content);
    }
}
