<?php

namespace App\Services;

use App\Repositories\PresentationRepository;

class DeleteSameColorPresentations
{
    private $presentationRepository;

    public function __construct(PresentationRepository $presentationRepository)
    {
        $this->presentationRepository = $presentationRepository;
    }

    public function execute($itemId, $colorId)
    {
        $presentations = $this->presentationRepository->findSameColorWithPrices($itemId, $colorId);

        foreach ($presentations as $key => $presentation) {
            foreach ($presentation->web_prices as $key => $web_price) {
                $web_price->price_rules()->detach();
                $web_price->delete();
            }

            foreach ($presentation->shop_prices as $key => $shop_price) {
                $shop_price->price_rules()->detach();
                $shop_price->delete();
            }

            foreach ($presentation->wholesale_prices as $key => $wholesale_price) {
                $wholesale_price->price_rules()->detach();
                $wholesale_price->delete();
            }
            $presentation->delete();
        }
        return true;
    }
}