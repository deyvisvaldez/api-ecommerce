<?php

namespace App\Services;

use App\Repositories\RegionRepository;

class RegisterRegion
{
    private $regionRepository;

    public function __construct(RegionRepository $regionRepository)
    {
        $this->regionRepository = $regionRepository;
    }

    public function execute($data)
    {
        $region = $this->regionRepository->create($data);
        return $region;
    }
}