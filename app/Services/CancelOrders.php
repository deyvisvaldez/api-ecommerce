<?php

namespace App\Services;

use App\Repositories\OrderRepository;
use App\Services\RegisterNewMovement;

class CancelOrders
{
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function execute($orderCode, $userId)
    {
        $order = $this->orderRepository->byCode($orderCode);
        $order->status = 3;
        $order->save();

        /*if ($order->order_type == 1) {
            $order->load('inventories');
            $order->load('payments');
            //dd($order->toArray());

            foreach ($order->inventories as $key => $inventory) {
                $newQuantity = (int) $inventory->pivot->quantity + (int) $inventory->available_stock;
                $inventory->available_stock = $newQuantity;
                $inventory->save();

                $data = [
                    'inventory_id' => $inventory->id,
                    'quantity' => $inventory->pivot->quantity,
                    'description' => 'Devolución por orden cancelada',
                    'user_id' => $userId
                ];
                $movement = new RegisterNewMovement();
                $movement->execute($data, 7, null, $inventory->location->name);
            }

            //cancelar payments type egreso / true
            foreach ($order->payments as $key => $payment) {
                $payment->payment_type = 2;
                $payment->save();
            }
        }*/

        return true;
    }
}