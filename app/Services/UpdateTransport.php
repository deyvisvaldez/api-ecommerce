<?php

namespace App\Services;

use App\Repositories\TransportRepository;

class UpdateTransport
{
    private $transportRepository;

    public function __construct(TransportRepository $transportRepository)
    {
        $this->transportRepository = $transportRepository;
    }

    public function execute($transportId, $data)
    {
        $transport = $this->transportRepository->find($transportId);
        $this->transportRepository->update($transport, $data);
        return true;
    }
}