<?php

namespace App\Services;

use App\Entities\Whatsapp;

class UpdateWhatsapp
{
    public function execute($data, $personId)
    {
        foreach ($data['whatsapps'] as $key => $whatsapp) {
            if ($whatsapp['id'] > 0) {
                // Update
                $oneWhatsapp = Whatsapp::find($whatsapp['id']);
                $whatsapp['main'] = false;
                if ($whatsapp['id'] == $data['checked']) {
                    $whatsapp['main'] = true;
                }
                $oneWhatsapp = $oneWhatsapp->update($whatsapp);
            } else {
                //Create
                $whatsapp['model_id'] = $personId;
                $whatsapp['model_type'] = 1;
                $whatsapp['main'] = false;
                if ($whatsapp['id'] == $data['checked']) {
                    $whatsapp['main'] = true;
                }
                $oneWhatsapp = Whatsapp::create($whatsapp);
            }
        }

        return true;
    }
}