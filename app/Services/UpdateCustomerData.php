<?php

namespace App\Services;

use Uuid;
use App\Repositories\PersonRepository;
use App\Repositories\EmailRepository;
use App\Repositories\WhatsappRepository;
use App\Repositories\AddressRepository;
use App\Repositories\RegionRepository;
use App\Services\RegisterRegion;

class UpdateCustomerData
{
    private $personRepository;
    private $emailRepository;
    private $addressRepository;

    public function __construct(PersonRepository $personRepository)
    {
        $this->personRepository = $personRepository;
        $this->emailRepository = new EmailRepository();
        $this->whatsappRepository = new WhatsappRepository();
        $this->addressRepository = new AddressRepository();
    }

    public function execute($customerId, $data)
    {
        if ($data['region_id'] < 1) {
            $useCase = new RegisterRegion(new RegionRepository);
            $region = $useCase->execute([
                'name' => $data['region_name'],
                'country_id' => $data['country_id']
            ]);
            $data['region_id'] = $region->id;
        }

        /**
         * Customer data.
         */
        $customer = $this->personRepository->find($customerId);
        $this->personRepository->update($customer, $data);

        $data['model_id'] = $customer->id;
        $data['model_type'] = 1;

        /**
         * Email data.
         */
        $count = $this->emailRepository->personEmailQuantity($customer->id);
        if (! $count) {
            $data['main'] = true;
        }
        $email = $this->emailRepository->personEmailExists($customer->id, $data['email']);

        if (! $email) {
            $email = $this->emailRepository->create($data);
        }

        if (array_key_exists('main', $data)) {
            unset($data['main']);
        }

        /**
         * Whatsapp data.
         */
        $count = $this->whatsappRepository->personWhatsappQuantity($customer->id);
        if (! $count) {
            $data['main'] = true;
        }

        $whatsapp = $this->whatsappRepository->personWhatsappExists($customer->id, $data['whatsapp']);
        if (! $whatsapp) {
            $whatsapp = $this->whatsappRepository->create($data);
        }

        if (array_key_exists('main', $data)) {
            unset($data['main']);
        }

        /**
         * Address data.
         */
        $count = $this->addressRepository->personAddressQuantity($customer->id);
        if (! $count) {
            $data['main'] = true;
        }

        $address = $this->addressRepository->personAddressExists($customer->id, $data['address']);
        if (! $address) {
            $address = $this->addressRepository->create($data);
        }

        if (array_key_exists('main', $data)) {
            unset($data['main']);
        }

        return $customer;
    }
}