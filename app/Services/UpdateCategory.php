<?php

namespace App\Services;

use Uuid;
use App\Repositories\CategoryRepository;
use App\Uploaders\ImageUploader;

class UpdateCategory
{
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function execute($data, $categoryId)
    {
        $path = '/categories/image';
        $thumbPath = '/categories/image_thumb';

        if ($data['extension'] != '') {
            $dropbox = new ImageUploader();

            $dropbox->upload($path, $data['img'], $data['extension']);
            $data['image'] = $dropbox->getDropboxUrl();
            $data['image_path'] = $dropbox->getDropboxPath();

            $dropbox->upload($thumbPath, $data['img'], $data['extension'], 100);
            $data['image_thumb'] = $dropbox->getDropboxUrl();
            $data['image_thumb_path'] = $dropbox->getDropboxPath();
        }

        $category = $this->categoryRepository->find($categoryId);
        $data['slug'] = str_slug($data['name'], '-');
        $newCategory = $this->categoryRepository->update($category, $data);

        return $newCategory;
    }
}
