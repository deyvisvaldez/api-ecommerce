<?php

namespace App\Services;

use App\Entities\Inventory;
use App\Entities\Item;
use App\Entities\Location;
use App\Entities\Presentation;
use App\Entities\Price;
use App\Entities\PriceRule;
use App\Entities\PriceType;
use Uuid;

class NewPresentationsBasedOnColor
{
    public function execute($sizes, $itemId, $colorId, $priceType, $userId)
    {
        $location = Location::whereMain(true)->first();
        $item = Item::find($itemId);
        $priceTypes = PriceType::all();
        $rule = PriceRule::first();
        $currentPriceType = $priceType;

        $presentationsFlag = false;

        switch ($priceType) {
            case 1:
                $item = Item::with('presentations.web_prices.price_rules')->find($itemId);

                if (count($item->presentations)) {
                    $presentationsFlag = true;

                    $priceRuleIds = [];
                    foreach ($item->presentations[0]->web_prices as $key => $web_price) {
                        $priceRuleIds[] = $web_price->price_rules[0]->id;
                    }
                    $priceRuleIds;
                }
                break;

            case 2:
                $item = Item::with('presentations.shop_prices.price_rules')->find($itemId);
                if (count($item->presentations)) {
                    $presentationsFlag = true;

                    $priceRuleIds = [];
                    foreach ($item->presentations[0]->shop_prices as $key => $shop_price) {
                        $priceRuleIds[] = $shop_price->price_rules[0]->id;
                    }
                    $priceRuleIds;
                }
                break;

            case 3:
                $item = Item::with('presentations.wholesale_prices.price_rules')->find($itemId);
                if (count($item->presentations)) {
                    $presentationsFlag = true;

                    $priceRuleIds = [];
                    foreach ($item->presentations[0]->wholesale_prices as $key => $wholesale_price) {
                        $priceRuleIds[] = $wholesale_price->price_rules[0]->id;
                    }
                    $priceRuleIds;
                }
                break;
        }

        if ($presentationsFlag) {
            $priceRules = PriceRule::whereIn('id', $priceRuleIds)->get();

            foreach ($sizes as $key => $size) {
                $data = [
                    'uuid' => Uuid::generate(4)->string,
                    'bar_code' => '',
                    'item_id' => $itemId,
                    'size_id' => $size['id'],
                    'color_id' => $colorId,
                    'published' => true,
                    'available' => true
                ];
                $pre = Presentation::create($data);

                $inv = Inventory::create([
                    'uuid' => Uuid::generate(4)->string,
                    'location_id' => $location->id,
                    'available_stock' => 0,
                    'min_stock' => 6,
                    'user_id' => $userId,
                    'company_id' => 1,
                    'presentation_id' => $pre->id
                ]);

                foreach ($priceRules as $key => $priceRule) {

                    if ($priceRule->id == 1) {
                        foreach ($priceTypes as $key => $priceType) {
                            $price = Price::create([
                                'uuid' => Uuid::generate(4)->string,
                                'name' => '',
                                'price' => $item->referencial_price,
                                'type' => $priceType->id,
                                'currency_id' => 1,
                                'promotional_price' => 0,
                                'available_promotion' => false,
                                'active' => true,
                                'user_id' => $userId,
                                'presentation_id' => $pre->id,
                                'company_id' => 1,
                            ]);

                            $price->price_rules()->attach($priceRule->id);
                        }
                    } else {
                        $price = Price::create([
                            'uuid' => Uuid::generate(4)->string,
                            'name' => '',
                            'price' => 0,
                            'type' => $currentPriceType,
                            'currency_id' => 1,
                            'promotional_price' => 0,
                            'available_promotion' => false,
                            'active' => true,
                            'user_id' => $userId,
                            'presentation_id' => $pre->id,
                            'company_id' => 1,
                        ]);

                        $price->price_rules()->attach($priceRule->id);
                    }
                }
            }
        } else {
            foreach ($sizes as $key => $size) {
                $data = [
                    'uuid' => Uuid::generate(4)->string,
                    'bar_code' => '',
                    'item_id' => $itemId,
                    'size_id' => $size['id'],
                    'color_id' => $colorId,
                    'published' => true,
                    'available' => true
                ];
                $pre = Presentation::create($data);

                $inv = Inventory::create([
                    'uuid' => Uuid::generate(4)->string,
                    'location_id' => $location->id,
                    'available_stock' => 0,
                    'min_stock' => 6,
                    'user_id' => $userId,
                    'company_id' => 1,
                    'presentation_id' => $pre->id
                ]);

                foreach ($priceTypes as $key => $priceType) {
                    $price = Price::create([
                        'uuid' => Uuid::generate(4)->string,
                        'name' => '',
                        'price' => $item->referencial_price,
                        'type' => $priceType->id,
                        'currency_id' => 1,
                        'promotional_price' => 0,
                        'available_promotion' => false,
                        'active' => true,
                        'user_id' => $userId,
                        'presentation_id' => $pre->id,
                        'company_id' => 1,
                    ]);
                    $price->price_rules()->attach($rule->id);
                }
            }
        }
        return true;
    }
}
