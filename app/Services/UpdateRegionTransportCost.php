<?php

namespace App\Services;

use App\Repositories\CostRepository;
use App\Repositories\BranchOfficeRepository;

class UpdateRegionTransportCost
{
    private $branchOfficeRepository;

    public function __construct(BranchOfficeRepository $branchOfficeRepository)
    {
        $this->branchOfficeRepository = $branchOfficeRepository;
    }

    public function execute($data)
    {
        $branchOffices = $this->branchOfficeRepository->officesRules($data['region_id'], $data['transport_id'], $data['rule_id']);

        foreach ($branchOffices as $key => $branchOffice) {
            foreach ($branchOffice->costs as $key => $cost) {
                $cost->cost = $data['cost'];
                $cost->save();
            }
        }
        return true;
    }
}