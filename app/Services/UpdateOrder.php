<?php

namespace App\Services;

use App\Entitties\BranchOffice;
use App\Repositories\BranchOfficeRepository;
use App\Repositories\ExtraCostRepository;
use App\Repositories\InventoryRepository;
use App\Repositories\OrderRepository;
use App\Repositories\PaymentRepository;
use App\Repositories\PresentationRepository;
use App\Repositories\PriceRuleRepository;
use App\Repositories\RegionRepository;
use App\Repositories\TransportRepository;
use App\Services\AddOrderExtraCost;
use App\Services\CalculateUpdateOrderCode;
use App\Services\CalculateUpdatedTotal;
use App\Services\RegisterNewMovement;
use Uuid;

class UpdateOrder
{
    private $orderRepository;
    private $regionRepository;
    private $inventoryRepository;
    private $presentationRepository;
    private $branchOfficeRepository;

    function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->regionRepository = new RegionRepository();
        $this->inventoryRepository = new InventoryRepository();
        $this->presentationRepository = new PresentationRepository();
        $this->branchOfficeRepository = new BranchOfficeRepository();
        $this->priceRuleRepository = new PriceRuleRepository();
    }

    public function execute($data, $orderId, $userId)
    {
        $oldOrder = $this->orderRepository->find($orderId);

        $newCode = new CalculateUpdatedOrderCode($this->orderRepository);
        $newCode = $newCode->execute($oldOrder->code);
        $newTotal = new CalculateUpdatedTotal($this->inventoryRepository);
        $newTotal = $newTotal->execute($oldOrder->presentations, $data['cart'], $oldOrder->origin);
        $totalAmount = $newTotal['amount'];
        $totalQuantity = $newTotal['quantity'];

        /**
         * Transport Cost
         * @var [type]
         */
        /*if ($data['costs']['regionId'] != 0 && $data['costs']['transportId'] != 0) {
            $branchOffice = $this->branchOfficeRepository->ruleCost($data['costs']['regionId'], $data['costs']['transportId'], $totalQuantity);
            $transportCost = (float) $branchOffice->costs[0]->cost;
            $totalAmount = $totalAmount + $transportCost;
        }*/

        $newOrder = [
            'uuid' => Uuid::generate(4)->string,
            'code' => $newCode,
            'description' => 'Orden actualizada',
            'order_type'=> $oldOrder->order_type,
            'total' => $totalAmount,
            'person_id' => $oldOrder->person_id,
            'user_id' => $userId,
            'status' => $oldOrder->status,
            'shipping_address' => $oldOrder->shipping_address,
            'account_id' => $oldOrder->account_id,
            'currency_id' =>$oldOrder->currency_id,
            'company_id' => $oldOrder->company_id,
            'location_id' => $oldOrder->location_id,
            'origin' => $oldOrder->origin
        ];

        $order = $this->orderRepository->create($newOrder);

        foreach ($data['cart'] as $key => $newInventory) {
            if ($newInventory['priceFlag']) {
                $price = (float) $newInventory['customPrice'];
            } else {
                $flag = true;
                foreach ($oldOrder->presentations as $key => $oldPresentation) {
                    if ($oldPresentation->id == $newPresentation['inventoryId']) {
                        $price = (float) $oldPresentation->pivot->price;
                        $flag = false;
                        break;
                    }
                }

                if ($flag) {
                    $ruleQuantity = $this->priceRuleRepository->getMinRuleQuantity($newPresentation['inventoryId'], $origin);

                    switch ($origin) {
                        case 1:
                            $price = $this->presentationRepository->getPrice($newPresentation['presentation_id'], 'web_prices', $ruleQuantity);
                            break;

                        case 2:
                            $price = $this->presentationRepository->getPrice($newPresentation['presentation_id'], 'shop_prices', $ruleQuantity);
                            break;

                        case 3:
                            $price = $this->presentationRepository->getPrice($newPresentation['presentation_id'], 'wholesale_prices', $ruleQuantity);
                            break;
                    }
                }
            }
            $order->inventories()->attach($newInventory['inventoryId'], ['price' => $price , 'quantity' => $newInventory['quantity']]);
        }

        /**
         * Save Extra Cost
         */
        /*if ($data['costs']['regionId'] != 0 && $data['costs']['transportId'] != 0) {
            $useCase = new AddOrderExtraCost(new ExtraCostRepository());
            $useCase->execute($order->id, $data['costs']['regionId'], $data['costs']['transportId'], $transportCost);
        }*/

        /**
         * Cancel Old Order
         */

        $oldOrder->status = 3;
        $oldOrder->save();

        return $newCode;
    }
}
