<?php

namespace App\Services;

use App\Entities\Phone;

class UpdatePhone
{
    public function execute($data, $personId)
    {
        foreach ($data['phones'] as $key => $phone) {
            if ($phone['id'] > 0) {
                // Update
                $onePhone = Phone::find($phone['id']);
                $phone['main'] = false;
                if ($phone['id'] == $data['checked']) {
                    $phone['main'] = true;
                }
                $onePhone = $onePhone->update($phone);
            } else {
                //Create
                $phone['model_id'] = $personId;
                $phone['model_type'] = 1;
                $phone['main'] = false;
                if ($phone['id'] == $data['checked']) {
                    $phone['main'] = true;
                }
                $onePhone = Phone::create($phone);
            }
        }

        return true;
    }
}