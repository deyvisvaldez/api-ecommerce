<?php

namespace App\Services;

use Uuid;
use App\Repositories\SizeRepository;

class CreateSize
{
    private $sizeRepository;

    public function __construct(SizeRepository $sizeRepository)
    {
        $this->sizeRepository = $sizeRepository;
    }

    public function execute($data)
    {
        $data['uuid'] = Uuid::generate(4)->string;
        $data['active'] = true;
        $this->sizeRepository->create($data);
        return true;
    }
}