<?php

namespace App\Services;

use App\Entities\Bank;
use App\Uploaders\ImageUploader;

class AddBank
{
    public function execute($data)
    {
        if ($data['logotype']['extension'] != '') {
            $uploader = new ImageUploader();
            $path = 'banks';
            $uploader->upload($path, $data['logotype']['base64'], $data['logotype']['extension']);
            $data['bank_image'] = $uploader->getDropboxUrl();
            $data['bank_image_path'] = $uploader->getDropboxPath();

            $uploader->upload($path, $data['logotype']['base64'], $data['logotype']['extension'], 100);
            $data['bank_image_thumb'] = $uploader->getDropboxUrl();
            $data['bank_image_thumb_path'] = $uploader->getDropboxPath();
        }

        Bank::create($data);
        return true;
    }
}