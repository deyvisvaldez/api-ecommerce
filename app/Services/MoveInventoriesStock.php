<?php

namespace App\Services;

use App\Entities\Inventory;
use App\Exceptions\DestinationNotReceivedException;
use App\Repositories\InventoryRepository;
use App\Services\RegisterNewMovement;
use Uuid;

class MoveInventoriesStock
{
    private $inventoryRepository;

    public function __construct(InventoryRepository $inventoryRepository)
    {
        $this->inventoryRepository = $inventoryRepository;
    }

    public function execute($presentationIds, $description, $origin, $userId)
    {
        foreach ($presentationIds as $key => $presentationId) {
            if (! $presentationId['destination']) {
                throw new DestinationNotReceivedException('No se ha seleccionado un destino.', 1);
            }
        }

        foreach ($presentationIds as $key => $presentationId) {
            $originInventory = $this->inventoryRepository->byLocation($presentationId['id'], $origin);

            $destinationInventory = $this->inventoryRepository->byLocation($presentationId['id'], $presentationId['destination']);

            $data['user_id'] = $userId;
            $data['quantity'] = $presentationId['quantity'];
            $data['description'] = $description;

            if ($destinationInventory) {
                $newDestinationStock = (int) $destinationInventory->available_stock + (int) $presentationId['quantity'];
                $destinationInventory->available_stock = $newDestinationStock;
                $destinationInventory->save();

                //$movement = new RegisterNewMovement();

                /**
                 * In RegisterNewMovement, execute method passes as $type parameter
                 * 3 = traslado(salida),
                 * 4 = traslado(ingreso)
                 */

                /*$data['inventory_id'] = $originInventory->id;
                $movement->execute($data, 3, $originInventory->location->name, $destinationInventory->location->name);

                $data['inventory_id'] = $destinationInventory->id;
                $movement->execute($data, 4, $originInventory->location->name, $destinationInventory->location->name);*/

            } else {

                $destinationInventory = Inventory::create([
                    'uuid' => Uuid::generate(4)->string,
                    'location_id' => $presentationId['destination'],
                    'available_stock' => $presentationId['quantity'],
                    'min_stock' => $originInventory->min_stock,
                    'user_id' => $userId,
                    'company_id' => $originInventory->company_id,
                    'published' => true,
                    'available' => true,
                    'presentation_id' => $originInventory->id
                ]);

                $destinationInventory->load('location');

                //$destiny = $this->locationRepository->find($inventoryId['destination']);

                //$movement = new RegisterNewMovement();

                /**
                 * In RegisterNewMovement, execute method passes as $type parameter
                 * 3 = traslado(salida),
                 * 4 = traslado(ingreso)
                 */
                /*$data['inventory_id'] = $originInventory->id;
                $movement->execute($data, 3, $originInventory->location->name, $destiny->name);

                $data['inventory_id'] = $newInventory->id;
                $movement->execute($data, 4, $originInventory->location->name, $destiny->name);*/

            }

            $movement = new RegisterNewMovement();

            /**
             * In RegisterNewMovement, execute method passes as $type parameter
             * 3 = traslado(salida),
             * 4 = traslado(ingreso)
             */

            $data['inventory_id'] = $originInventory->id;
            $movement->execute($data, 3, $originInventory->location->name, $destinationInventory->location->name);

            $data['inventory_id'] = $destinationInventory->id;
            $movement->execute($data, 4, $originInventory->location->name, $destinationInventory->location->name);

            // Discount stock
            $newOriginStock = (int) $originInventory->available_stock - (int) $presentationId['quantity'];
            $originInventory->available_stock = $newOriginStock;
            $originInventory->save();
        }

        return true;
    }
}