<?php

namespace App\Services;

use App\Repositories\OrderRepository;

class CalculateUpdatedOrderCode
{
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function execute($orderCode)
    {
        $oldOrder = explode('_', $orderCode);
        $count = $this->orderRepository->orderQuantity($oldOrder[0]);
        $newCode = $oldOrder[0].'_'.$count;
        return $newCode;
    }
}