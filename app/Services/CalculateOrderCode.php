<?php

namespace App\Services;

use App\Repositories\OrderRepository;

class CalculateOrderCode
{
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function execute($orderType)
    {
        $count = $this->orderRepository->orderTypeQuantity($orderType);
        $count++;

        switch ($orderType) {
            case 1:
                $code = "OVE-$count";
                break;

            case 2:
                $code = "OPE-$count";
                break;

            case 3:
                $code = "OCO-$count";
                break;
        }

        return $code;
    }
}