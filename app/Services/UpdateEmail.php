<?php

namespace App\Services;

use App\Entities\Email;

class UpdateEmail
{
    public function execute($data, $personId)
    {
        foreach ($data['emails'] as $key => $email) {
            if ($email['id'] > 0) {
                // Update
                $oneEmail = Email::find($email['id']);
                $email['main'] = false;
                if ($email['id'] == $data['checked']) {
                    $email['main'] = true;
                }
                $oneEmail = $oneEmail->update($email);
            } else {
                //Create
                $email['model_id'] = $personId;
                $email['model_type'] = 1;
                $email['main'] = false;
                if ($email['id'] == $data['checked']) {
                    $email['main'] = true;
                }
                $oneEmail = Email::create($email);
            }
        }

        return true;
    }
}