<?php

namespace App\Services;

use Uuid;
use App\Entities\Account;

class AddCompanyBankAccount
{
    public function execute($data)
    {
        $data['uuid'] = Uuid::generate(4)->string;
        $data['company_id'] = 1;
        $data['published'] = true;
        $data['account_name'] = ($data['account_type'] == 1) ? 'Cuenta de Ahorros' : 'Cuenta de Crédito';
        $account = Account::create($data);

        return $account;
    }
}

