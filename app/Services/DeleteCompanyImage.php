<?php

namespace App\Services;

use App\Repositories\PhotoRepository;
use App\Uploaders\ImageUploader;

class DeleteCompanyImage
{
    protected $photoRepository;

    public function __construct(PhotoRepository $photoRepository)
    {
        $this->photoRepository = $photoRepository;
    }

    public function execute($photoId)
    {
        $photo = $this->photoRepository->find($photoId);

        //Para saber si es empty or null two equals (=) or empty()
        if ($photo->resource != null) {
            $dropbox = new ImageUploader();
            $dropbox->delete($photo->resource_path, $photo->resource);
        }

		$this->photoRepository->delete($photoId);
        return true;
    }
}
