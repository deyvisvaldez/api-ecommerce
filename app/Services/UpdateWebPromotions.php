<?php

namespace App\Services;

use App\Repositories\PresentationRepository;

class UpdateWebPromotions
{
    private $presentationRepository;

    public function __construct(PresentationRepository $presentationRepository)
    {
        $this->presentationRepository = $presentationRepository;
    }

    public function execute($presentationIds, $webPrices)
    {
        foreach ($presentationIds as $key => $presentationId) {
            $presentation = $this->presentationRepository->withPrices($presentationId['id'], $webPrices, 1);
            $presentation->{$webPrices}[0]->promotional_price = $presentationId['promotional_price'];
            $presentation->{$webPrices}[0]->available_promotion = true;
            $presentation->{$webPrices}[0]->save();
        }
        return true;
    }
}