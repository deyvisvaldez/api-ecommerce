<?php

namespace App\Services;

use Uuid;
use App\Repositories\ImageRepository;
use App\Uploaders\ImageUploader;

class UploadPresentationPhoto
{
    public function execute($data, $presentations)
    {
        $dropbox = new ImageUploader();

        $imagePath = '/products/image';
        $dropbox->upload($imagePath, $data['url'], $data['extension']);
        $data['resource'] = $dropbox->getDropboxUrl();
        $data['resource_path'] = $dropbox->getDropboxPath();

        $thumbPath = '/products/image_thumb';
        $dropbox->upload($thumbPath, $data['url'], $data['extension'], 350);
        $data['resource_thumb'] = $dropbox->getDropboxUrl();
        $data['resource_thumb_path'] = $dropbox->getDropboxPath();

        $data['uuid'] = Uuid::generate(4)->string;

        foreach ($presentations as $key => $presentation) {
            $data['model_id'] = $presentation->id;
            $data['description'] = '';
            $data['model_type'] = 2;

            $imageRepository = new ImageRepository();
            $imageRepository->create($data);
        }

        return $data['uuid'];
    }
}