<?php

namespace App\Services;

use App\Entities\Color;
use App\Entities\Inventory;
use App\Repositories\ImageRepository;
use App\Uploaders\ImageUploader;
use Uuid;

class UploadReferencialPhoto
{
    public function execute($item, $photo)
    {
        $dropbox = new ImageUploader();

        $imagePath = '/products/image';
        $dropbox->upload($imagePath, $photo['base64'], $photo['extension']);
        $data['referencial_photo'] = $dropbox->getDropboxUrl();
        $data['referencial_photo_path'] = $dropbox->getDropboxPath();

        $thumbPath = '/products/image_thumb';
        $dropbox->upload($thumbPath, $photo['base64'], $photo['extension'], 350);
        $data['referencial_photo_thumb'] = $dropbox->getDropboxUrl();
        $data['referencial_photo_thumb_path'] = $dropbox->getDropboxPath();

        $item->update($data);

        return true;
    }
}