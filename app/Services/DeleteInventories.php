<?php

namespace App\Services;

use App\Repositories\InventoryRepository;

class DeleteInventories
{
    private $inventoryRepository;

    public function __construct(InventoryRepository $inventoryRepository)
    {
        $this->inventoryRepository = $inventoryRepository;
    }

    public function execute($presentationIds, $locationId)
    {
        foreach ($presentationIds as $key => $presentationId) {
            $inventory = $this->inventoryRepository->byLocation($presentationId, $locationId);
            if ($inventory) {
                $inventory->delete();
            }
        }
        return true;
    }
}