<?php

namespace App\Services;

use App\Repositories\ColorRepository;

class UpdateColor
{
    private $colorRepository;

    public function __construct(ColorRepository $colorRepository)
    {
        $this->colorRepository = $colorRepository;
    }

    public function execute($data, $colorId)
    {
        $color = $this->colorRepository->find($colorId);
        $this->colorRepository->update($color, $data);
        return true;
    }
}