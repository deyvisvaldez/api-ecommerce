<?php

namespace App\Services;

use App\Repositories\ImageRepository;
use App\Uploaders\ImageUploader;

class DeleteImages
{
    private $imageRepository;

    public function __construct(ImageRepository $imageRepository)
    {
        $this->imageRepository = $imageRepository;
    }

    public function execute($uuid)
    {
        $images = $this->imageRepository->findByUuid($uuid);

        $dropbox = new ImageUploader();

        foreach ($images as $key => $image) {
            $deleteImage = $dropbox->delete($image->resource_path, $image->resource);
            $deleteImageThumb = $dropbox->delete($image->resource_thumb_path, $image->resource_thumb);

            $this->imageRepository->delete($image);
        }
        return true;
    }
}