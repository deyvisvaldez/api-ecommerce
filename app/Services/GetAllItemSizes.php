<?php

namespace App\Services;

class GetAllItemSizes
{
    public function execute($items)
    {
        $sizes = [];

        foreach ($items as $key => $item) {
           foreach ($item->presentations as $key => $presentation) {
               if ($presentation->size) {
                   $sizes[$presentation->size->order] = [
                        'id' => (int) $presentation->size->id,
                        'size' => $presentation->size->name,
                        'order' => (int) $presentation->size->order
                   ];
               }
           }
        }

        ksort($sizes);
        $sizes = array_values($sizes);

        return $sizes;
    }
}