<?php

namespace App\Services;

use App\Entities\Order;
use App\Exceptions\NotEnoughAvailableStockException;
use App\Services\RegisterNewMovement;

class DeliverOrder
{
    public function execute($orderCode, $locationId, $userId)
    {
        $order = Order::with(['presentations.inventory' => function($query) use ($locationId) {
            $query->with('location')
            ->whereLocationId($locationId);
        }])
        ->whereCode($orderCode)
        ->first();

        $noStock = false;
        foreach ($order->presentations as $key => $presentation) {
            if ((int) $presentation->inventory->available_stock === 0) {
                $noStock = true;
                break;
            }
        }

        if ($noStock) {
            throw new NotEnoughAvailableStockException("Not enough available stock .", 1);
        }

        $order->status = 4;// Set order status to "Entregado"
        $order->save();

        // Discount Stock
        foreach ($order->presentations as $key => $presentation) {
            // Decrease stock
            $quantity = (int) $presentation->pivot->quantity;
            $presentation->inventory->available_stock = (int) $presentation->inventory->available_stock - $quantity;
            $presentation->inventory->save();

            /**
             * Register Movements
             * 8: Salida por entrega de pedido;
             */
            $movement = [
                'inventory_id' => $presentation->inventory->id,
                'quantity' => $quantity,
                'description' => 'Entrega de Pedido',
                'user_id' => $userId
            ];
            $useCase = new RegisterNewMovement();
            $useCase->execute($movement, 8, $presentation->inventory->location->name, null);
        }

        return true;
    }
}