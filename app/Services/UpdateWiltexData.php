<?php

namespace App\Services;

use App\Repositories\CompanyRepository;
use App\Uploaders\ImageUploader;

class UpdateWiltexData
{
    private $companyRepository;

    public function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

    public function execute($data)
    {
        $company = $this->companyRepository->first();

        $data['slug'] = str_slug($data['business_name'], '-');

        $uploader = new ImageUploader();

        if ($data['icon']['extension'] != '') {
            $iconPath = $company->dropbox_path.'/icon';
            $uploader->upload($iconPath, $data['icon']['base64'], $data['icon']['extension']);
            $data['icon'] = $uploader->getDropboxUrl();
            $data['icon_path'] = $uploader->getDropboxPath();
            $uploader->delete($company->icon_path, $company->icon);
        } else {
            unset($data['icon']);
        }

        if ($data['color_logotype']['extension'] != '') {
            $colorPath = $company->dropbox_path.'/color_logotype';
            $uploader->upload($colorPath, $data['color_logotype']['base64'], $data['color_logotype']['extension']);
            $data['color_logotype'] = $uploader->getDropboxUrl();
            $data['color_logotype_path'] = $uploader->getDropboxPath();
            $uploader->delete($company->color_logotype_path, $company->color_logotype);
        } else {
            unset($data['color_logotype']);
        }

        if ($data['white_logotype']['extension'] != '') {
            $whitePath = $company->dropbox_path.'/white_logotype';
            $uploader->upload($whitePath, $data['white_logotype']['base64'], $data['white_logotype']['extension']);
            $data['white_logotype'] = $uploader->getDropboxUrl();
            $data['white_logotype_path'] = $uploader->getDropboxPath();
            $uploader->delete($company->white_logotype_path, $company->white_logotype);
        } else {
            unset($data['white_logotype']);
        }

        $company = $this->companyRepository->update($company, $data);

        return $company;
    }
}