<?php

namespace App\Services;

use App\Repositories\PresentationRepository;

class DeleteSameSizePresentations
{
    private $presentationRepository;

    public function __construct(PresentationRepository $presentationRepository)
    {
        $this->presentationRepository = $presentationRepository;
    }

    public function execute($itemId, $sizeId)
    {
        $presentations = $this->presentationRepository->findSameSizeWithPrices($itemId, $sizeId);

        foreach ($presentations as $key => $presentation) {
            foreach ($presentation->web_prices as $key => $web_price) {
                $web_price->price_rules()->detach();
                $web_price->delete();
            }

            foreach ($presentation->shop_prices as $key => $shop_price) {
                $shop_price->price_rules()->detach();
                $shop_price->delete();
            }

            foreach ($presentation->wholesale_prices as $key => $wholesale_price) {
                $wholesale_price->price_rules()->detach();
                $wholesale_price->delete();
            }
            $presentation->delete();
        }
        return true;
    }
}