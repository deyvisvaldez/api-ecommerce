<?php

namespace App\Services;

use App\Entities\Cellphone;

class UpdateWiltexCellphone
{
    public function execute($data)
    {
        foreach ($data['cellphones'] as $key => $cellphone) {
            if ($cellphone['id'] > 0) {
                // Update
                $oneCellphone = Cellphone::find($cellphone['id']);
                $cellphone['main'] = false;
                if ($cellphone['id'] == $data['checked']) {
                    $cellphone['main'] = true;
                }
                $oneCellphone = $oneCellphone->update($cellphone);
            } else {
                //Create
                $cellphone['model_id'] = 1; //Wiltex
                $cellphone['model_type'] = 2; //Company
                $cellphone['main'] = false;
                if ($cellphone['id'] == $data['checked']) {
                    $cellphone['main'] = true;
                }
                $oneCellphone = Cellphone::create($cellphone);
            }
        }

        return true;
    }
}