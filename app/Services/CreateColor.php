<?php

namespace App\Services;

use Uuid;
use App\Repositories\ColorRepository;

class CreateColor
{
    private $colorRepository;

    public function __construct(ColorRepository $colorRepository)
    {
        $this->colorRepository = $colorRepository;
    }

    public function execute($data)
    {
        $data['uuid'] = Uuid::generate(4)->string;
        $data['active'] = true;
        $this->colorRepository->create($data);
        return true;
    }
}