ç<?php

namespace App\Services;

use App\Repositories\CompanyRepository;
use App\Utils\MailchimpNewsletter;

class UpdateCompanyData
{
    private $companyRepository;

    public function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

    public function execute($companyId, $data)
    {
        $company = $this->companyRepository->find($companyId);

        $mainEmail = $this->companyRepository->getMainEmail($companyId);
        $mainPhone = $this->companyRepository->getMainPhone($companyId);
        $companyData = $company->toArray();
        $companyData['email'] = $mainEmail;
        $companyData['phone'] = $mainPhone;

        /** Crear Nueva Lista de suscribción */
        $newsletter = new MailchimpNewsletter();
        $list = $newsletter->createList($companyData); //$list->id o $list['id'];

        $data['mailchimp_list_id'] = $list['id'];

        $company = $this->companyRepository->update($company, $data);

        return $company;
    }
}