<?php

namespace App\Services;

use App\Repositories\PresentationRepository;
use App\Repositories\PriceRuleRepository;

class CalculateUpdatedTotal
{
    private $presentationRepository;
    private $priceRuleRepository;

    public function __construct(PresentationRepository $presentationRepository)
    {
        $this->presentationRepository = $presentationRepository;
        $this->priceRuleRepository = new PriceRuleRepository();
    }

    public function execute($oldPresentations, $newPresentations, $origin)
    {
        $amount = 0;
        $quantity = 0;

        foreach ($newPresentations as $key => $newPresentation) {
            $quantity = $quantity + (int) $newPresentation['quantity'];

            if ($newPresentation['priceFlag']) {
                $amount = (float) $amount + ((float) $newPresentation['customPrice'] * (int) $newPresentation['quantity']);
            } else {
                $flag = true;
                foreach ($oldPresentations as $key => $oldPresentation) {
                    if ($oldPresentation->id == $newPresentation['inventoryId']) {
                        $amount = (float) $amount + ((float) $oldPresentation->pivot->price * (int) $newPresentation['quantity']);
                        $flag = false;
                        break;
                    }
                }

                if ($flag) {
                    $ruleQuantity = $this->priceRuleRepository->getMinRuleQuantity($newPresentation['inventoryId'], $origin);

                    switch ($origin) {
                        case 1:
                            $price = $this->presentationRepository->getPrice($newPresentation['presentation_id'], 'web_prices', $ruleQuantity);
                            break;

                        case 2:
                            $price = $this->presentationRepository->getPrice($newPresentation['presentation_id'], 'shop_prices', $ruleQuantity);
                            break;

                        case 3:
                            $price = $this->presentationRepository->getPrice($newPresentation['presentation_id'], 'wholesale_prices', $ruleQuantity);
                            break;
                    }

                    $amount = (float) $amount + ((float) $price * (int) $newPresentation['quantity']);
                }
            }
        }
        return [
            'amount' => $amount,
            'quantity' => $quantity
        ];
    }
}