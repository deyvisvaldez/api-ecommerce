<?php

namespace App\Services;

class GetPresentationSizes
{
    public function execute($presentations)
    {
        $sizes = [];

        foreach ($presentations as $key => $presentation) {
            if ($presentation->size) {
                $sizes[$presentation->size->order] = [
                    'id' => (int) $presentation->size->id,
                    'size' => $presentation->size->name,
                    'order' => (int) $presentation->size->order
                ];
            }
        }

        ksort($sizes);
        $sizes = array_values($sizes);

        return $sizes;
    }
}