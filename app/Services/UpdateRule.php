<?php

namespace App\Services;

use App\Repositories\RuleRepository;

class UpdateRule
{
    private $ruleRepository;

    public function __construct(RuleRepository $ruleRepository)
    {
        $this->ruleRepository = $ruleRepository;
    }

    public function execute($data, $ruleId)
    {
        $data['unit_value'] = $data['rule'];
        $rule = $this->ruleRepository->find($ruleId);
        $this->ruleRepository->update($rule, $data);
        return true;
    }
}