<?php

namespace App\Services;

use App\Entities\Item;

class DeleteItem
{
    public function execute($itemId)
    {
        $item = Item::with('presentations')->find($itemId);

        foreach ($item->presentations as $key => $presentation) {
            $presentation->delete();
        }

        $item->update(['name' => $item->id]);
        $item->update(['slug' => $item->id]);
        $item->delete();
        return $item;
    }
}
