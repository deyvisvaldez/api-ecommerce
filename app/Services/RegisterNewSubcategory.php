<?php

namespace App\Services;

use App\Repositories\CategoryRepository;
use App\Repositories\SubcategoryRepository;
use App\Uploaders\ImageUploader;
use Uuid;

class RegisterNewSubcategory
{
    private $subcategoryRepository;
    private $categoryRepository;

    public function __construct(SubcategoryRepository $subcategoryRepository)
    {
        $this->subcategoryRepository = $subcategoryRepository;
        $this->categoryRepository = new CategoryRepository;
    }

    public function execute($data, $categoryId)
    {
        $category = $this->categoryRepository->find($categoryId);

        if ($data['subcategory_id']) {
            $subcategory = $this->subcategoryRepository->find($data['subcategory_id']);
        } else {
            $image = $data['image'];
            unset($data['image']);
            if ($image['extension'] != '') {
                $path = '/subcategories/image';
                $thumbPath = '/subcategories/image_thumb';
                $dropbox = new ImageUploader();

                $dropbox->upload($path, $image['base64'], $image['extension']);
                $data['image'] = $dropbox->getDropboxUrl();
                $data['image_path'] = $dropbox->getDropboxPath();

                $dropbox->upload($thumbPath, $image['base64'], $image['extension'], 100);
                $data['image_thumb'] = $dropbox->getDropboxUrl();
                $data['image_thumb_path'] = $dropbox->getDropboxPath();
            }

            $data['uuid'] = Uuid::generate(4)->string;
            $data['slug'] = str_slug($data['name'], '-');
            $data['published'] = true;
            $data['type'] = 1; //products
            $subcategory = $this->subcategoryRepository->create($data);
        }

        $category->subcategories()->attach($subcategory->id, ['active' => true]);

        return $subcategory;
    }
}
