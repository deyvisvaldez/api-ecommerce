<?php

namespace App\Services;

use App\Repositories\PresentationRepository;

class UpdatePresentationsUnitaryPrices
{
    private $presentationRepository;

    public function __construct(PresentationRepository $presentationRepository)
    {
        $this->presentationRepository = $presentationRepository;
    }

    public function execute($presentationIds, $allColors)
    {
        foreach ($presentationIds as $key => $presentationId) {
            $pre = $this->presentationRepository->findWithUnitaryPrices($presentationId['id']);

            if ($allColors) {
                $pres = $this->presentationRepository->findSameSize($pre->item_id, $pre->size_id);
                foreach ($pres as $key => $pre) {
                    $pre->web_prices[0]->price = $presentationId['web_price'];
                    $pre->web_prices[0]->save();
                    $pre->shop_prices[0]->price = $presentationId['shop_price'];
                    $pre->shop_prices[0]->save();
                    $pre->wholesale_prices[0]->price = $presentationId['wholesale_price'];
                    $pre->wholesale_prices[0]->save();
                }
            } else {
                $pre->web_prices[0]->price = $presentationId['web_price'];
                $pre->web_prices[0]->save();
                $pre->shop_prices[0]->price = $presentationId['shop_price'];
                $pre->shop_prices[0]->save();
                $pre->wholesale_prices[0]->price = $presentationId['wholesale_price'];
                $pre->wholesale_prices[0]->save();
            }
        }

        return true;
    }
}