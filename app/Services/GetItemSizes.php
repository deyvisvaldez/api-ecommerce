<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Collection;

class GetItemSizes
{
    public function execute($item)
    {
        $sizes = [];

        foreach ($item->presentations as $key => $presentation) {
            if ($presentation->size) {
                $sizes[] = [
                    'id' => (int) $presentation->size->id,
                    'size' => $presentation->size->name,
                    'order' => (int) $presentation->size->order
                ];
            }
        }

        $sizes = array_values(array_unique($sizes, SORT_REGULAR));

        return $sizes;
    }
}