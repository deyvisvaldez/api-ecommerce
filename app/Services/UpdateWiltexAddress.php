<?php

namespace App\Services;

use App\Entities\Address;

class UpdateWiltexAddress
{
    public function execute($data)
    {
        foreach ($data['addresses'] as $key => $address) {
            if ($address['id'] > 0) {
                // Update
                $oneAddress = Address::find($address['id']);
                $address['main'] = false;
                if ($address['id'] == $data['checked']) {
                    $address['main'] = true;
                }
                $oneAddress = $oneAddress->update($address);
            } else {
                //Create
                $address['model_id'] = 1; //Wiltex
                $address['model_type'] = 2; //Company
                $address['main'] = false;
                if ($address['id'] == $data['checked']) {
                    $address['main'] = true;
                }
                $oneAddress = Address::create($address);
            }
        }

        return true;
    }
}