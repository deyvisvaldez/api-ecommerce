<?php

namespace App\Services;

use App\Entities\Address;

class UpdateAddress
{
    public function execute($data, $personId)
    {
        foreach ($data['addresses'] as $key => $address) {
            if ($address['id'] > 0) {
                // Update
                $oneAddress = Address::find($address['id']);
                $address['main'] = false;
                if ($address['id'] == $data['checked']) {
                    $address['main'] = true;
                }
                $oneAddress = $oneAddress->update($address);
            } else {
                //Create
                $address['model_id'] = $personId;
                $address['model_type'] = 1;
                $address['main'] = false;
                if ($address['id'] == $data['checked']) {
                    $address['main'] = true;
                }
                $oneAddress = Address::create($address);
            }
        }

        return true;
    }
}