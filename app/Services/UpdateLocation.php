<?php

namespace App\Services;

use App\Repositories\LocationRepository;

class UpdateLocation
{
    private $locationRepository;

    public function __construct(LocationRepository $locationRepository)
    {
        $this->locationRepository = $locationRepository;
    }

    public function execute($locationId, $data)
    {
        $location = $this->locationRepository->find($locationId);
        $this->locationRepository->update($location, $data);
        return true;
    }
}