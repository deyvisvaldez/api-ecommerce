<?php

namespace App\Services;

use App\Repositories\CostRepository;
use App\Repositories\BranchOfficeRepository;

class SetRegionTransportCost
{
    private $branchOfficeRepository;

    public function __construct(BranchOfficeRepository $branchOfficeRepository)
    {
        $this->branchOfficeRepository = $branchOfficeRepository;
    }

    public function execute($data)
    {
        $branchOffices = $this->branchOfficeRepository->offices($data['region_id'], $data['transport_id']);

        foreach ($branchOffices as $key => $branchOffice) {
            $costRepository = new costRepository();
            $data['region_transport_id'] = $branchOffice->id;
            $cost = $costRepository->create($data);
        }
        return true;
    }
}