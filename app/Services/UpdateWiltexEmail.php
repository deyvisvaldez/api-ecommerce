<?php

namespace App\Services;

use App\Entities\Email;

class UpdateWiltexEmail
{
    public function execute($data)
    {
        foreach ($data['emails'] as $key => $email) {
            if ($email['id'] > 0) {
                // Update
                $oneEmail = Email::find($email['id']);
                $email['main'] = false;
                if ($email['id'] == $data['checked']) {
                    $email['main'] = true;
                }
                $oneEmail = $oneEmail->update($email);
            } else {
                //Create
                $email['model_id'] = 1; //Wiltex
                $email['model_type'] = 2; //Company
                $email['main'] = false;
                if ($email['id'] == $data['checked']) {
                    $email['main'] = true;
                }
                $oneEmail = Email::create($email);
            }
        }

        return true;
    }
}