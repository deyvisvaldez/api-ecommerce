<?php

namespace App\Services;

use App\Entities\Whatsapp;

class UpdateWiltexWhatsapp
{
    public function execute($data)
    {
        foreach ($data['whatsapps'] as $key => $whatsapp) {
            if ($whatsapp['id'] > 0) {
                // Update
                $oneWhatsapp = Whatsapp::find($whatsapp['id']);
                $whatsapp['main'] = false;
                if ($whatsapp['id'] == $data['checked']) {
                    $whatsapp['main'] = true;
                }
                $oneWhatsapp = $oneWhatsapp->update($whatsapp);
            } else {
                //Create
                $whatsapp['model_id'] = 1; //Wiltex
                $whatsapp['model_type'] = 2; //Company
                $whatsapp['main'] = false;
                if ($whatsapp['id'] == $data['checked']) {
                    $whatsapp['main'] = true;
                }
                $oneWhatsapp = Whatsapp::create($whatsapp);
            }
        }

        return true;
    }
}