<?php

namespace App\Services;

use App\Repositories\CountryRepository;

class RegisterNewCountry
{
    private $countryRepository;

    public function __construct(CountryRepository $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    public function execute($data)
    {
        $this->countryRepository->create($data);
        return true;
    }
}