<?php

namespace App\Services;

use App\Entities\Person;
use App\Entities\User;
use App\Uploaders\ImageUploader;
use Uuid;

class UpdateUser
{
    public function execute($data, $personId)
    {
        $person = Person::with('user')->find($personId);

        if ($data['avatar']['extension'] != '') {
            $uploader = new ImageUploader();

            $path = '/users/image';
            $thumbPath = '/users/thumb';

            $image = $data['avatar'];

            $uploader->upload($path, $image['base64'], $image['extension']);
            $data['avatar'] = $uploader->getDropboxUrl();
            $data['avatar_path'] = $uploader->getDropboxPath();
            $uploader->delete($person->avatar_path, $person->avatar);

            $uploader->upload($thumbPath, $image['base64'], $image['extension']);
            $data['avatar'] = $uploader->getDropboxUrl();
            $data['avatar_path'] = $uploader->getDropboxPath();
            $uploader->delete($person->avatar_path, $person->avatar);
        } else {
            unset($data['avatar']);
        }

        $person->user->username = $data['identity_document'];
        $person->user->role = $data['role'];
        $person->user->save();
        $person->update($data);

        return $person;
    }
}