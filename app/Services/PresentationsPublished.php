<?php

namespace App\Services;

use App\Repositories\PresentationRepository;

class PresentationsPublished
{
    private $presentationRepository;

    public function __construct(PresentationRepository $presentationRepository)
    {
        $this->presentationRepository = $presentationRepository;
    }

    public function execute($presentationIds, $published)
    {
        foreach ($presentationIds as $key => $presentationId) {
            $presentation = $this->presentationRepository->find($presentationId);
            $presentation->published = $published;
            $presentation->save();
        }

        return true;
    }
}