<?php

namespace App\Services;

use App\Repositories\PresentationRepository;
use App\Entities\Price;

class DisableWebPromotions
{
    private $presentationRepository;

    public function __construct(PresentationRepository $presentationRepository)
    {
        $this->presentationRepository = $presentationRepository;
    }

    public function execute($presentationIds, $webPrices)
    {
        foreach ($presentationIds as $key => $presentationId) {
            $presentation = $this->presentationRepository->withPrices($presentationId, $webPrices, 1);
            $presentation->{$webPrices}[0]->promotional_price = 0;
            $presentation->{$webPrices}[0]->available_promotion = false;
            $presentation->{$webPrices}[0]->save();
        }
        return true;


    }
}