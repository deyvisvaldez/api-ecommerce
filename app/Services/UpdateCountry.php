<?php

namespace App\Services;

use App\Repositories\CountryRepository;

class UpdateCountry
{
    private $countryRepository;

    public function __construct(CountryRepository $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    public function execute($data, $countryId)
    {
        $country = $this->countryRepository->find($countryId);
        $this->countryRepository->update($country, $data);
        return true;
    }
}