<?php

namespace App\Services;

use App\Repositories\InventoryRepository;
use App\Services\RegisterNewMovement;

class InventoriesProductionInput
{
    private $inventoryRepository;

    public function __construct(InventoryRepository $inventoryRepository)
    {
        $this->inventoryRepository = $inventoryRepository;
    }

    public function execute($presentationIds, $description, $locationId, $userId)
    {
        foreach ($presentationIds as $key => $presentationId) {
            $inv = $this->inventoryRepository->byLocation($presentationId['id'], $locationId);

            $newStock = (int)$inv->available_stock + (int)$presentationId['quantity'];

            $inv->available_stock = $newStock;
            $inv->save();

            $movement = new RegisterNewMovement();

            /**
             * In RegisterNewMovement, execute method passes as $type parameter
             * 1= Ingreso a Producción (de taller a almacen)
             */
            $data['quantity'] = $presentationId['quantity'];
            $data['description'] = $description;
            $data['user_id'] = $userId;
            $data['inventory_id'] = $inv->id;
            $movement->execute($data, 1, null, $inv->location->name);
        }

        return true;
    }
}
