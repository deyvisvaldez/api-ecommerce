<?php

namespace App\Services;

use Mail;

use App\Entities\Person;
use App\Entities\Payment;
use App\Entities\Email;
use App\Repositories\OrderRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\PersonRepository;
use App\Mail\WebOrderEmail;

class ConfirmOrders
{
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->companyRepository = new CompanyRepository;
        $this->personRepository = new PersonRepository;
    }

    public function execute($orderCode, $message, $rawEmail, $currencyId, $userId)
    {
        $order = $this->orderRepository->byCode($orderCode);
        $order->status = 2;
        $order->currency_id = $currencyId;
        $order->save();

        /**
         * Register Payments
         */
        $payment = [
            'order_id' => $order->id,
            'concept_id' => 1,
            'account_id' => 1,
            'user_id' => $userId,
            'description' => 'Pago 1',
            'amount' => $order->total,
            'status' => false
        ];
        $newPayment = Payment::create($payment);

        /**
         * Send Email
         */
        $person = Person::where('id', $order->person->id)->first();

        $email = Email::whereModelId($order->person_id)->whereModelType(1)->whereEmail($rawEmail)->first();

        if (! $email) {
            $email = $this->emailRepository->create($data);
        }

        $company = $this->companyRepository->completeData($order->company_id);

        $person = $this->personRepository->emailData($order->person_id);

        foreach ($person->emails as $key => $email) {
            $emailData = [
                'fullName' => $person->first_name.' '.$person->last_name,
                'emisor' => $company->email->email,
                //'receptor' => $person->email->email,
                'receptor' => $email,
                'uuid' => $order->uuid,
                'text' => $message
            ];
            Mail::to($emailData['receptor'])->send(new WebOrderEmail($emailData));
        }

    }
}