<?php

namespace App\Services;

use App\Repositories\SizeRepository;

class UpdateSize
{
    private $sizeRepository;

    public function __construct(SizeRepository $sizeRepository)
    {
        $this->sizeRepository = $sizeRepository;
    }

    public function execute($data, $sizeId)
    {
        $size = $this->sizeRepository->find($sizeId);
        $this->sizeRepository->update($size, $data);
        return true;
    }
}