<?php

namespace App\Repositories;

use App\Entities\Photo;

class PhotoRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Photo;
    }

    public function companyPhotos()
    {
        return $this->model->whereModelId(1)->whereModelType(1)->get();
    }
}