<?php

namespace App\Repositories;

use App\Entities\Inventory;

class InventoryRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Inventory;
    }

    public function sameItemColorStock($itemId, $colorId)
    {
        return $this->model
        ->whereHas('presentation', function($query) use ($itemId, $colorId) {
            $query->whereItemId($itemId)->whereColorId($colorId);
        })
        ->sum('available_stock');
    }

    public function sameItemSizeStock($itemId, $sizeId)
    {
        return $this->model
        ->whereHas('presentation', function($query) use ($itemId, $sizeId) {
            $query->whereItemId($itemId)->whereSizeId($sizeId);
        })
        ->sum('available_stock');
    }

    public function byLocation($presentationId, $locationId)
    {
        return $this->model->with('location')
        ->wherePresentationId($presentationId)
        ->whereLocationId($locationId)
        ->first();
    }

    public function filterLocation($presentationId, $locationId)
    {
        $inventories = $this->model->with('location')
        ->wherePresentationId($presentationId);

        if ($locationId) {
            $inventories = $inventories->where('location_id',  '!=', $locationId);
        }

        return $inventories->get();
    }

    public function withMovements($presentationId, $locationId, $date)
    {
        return $this->model
        ->with(['movements' => function ($query) use ($date) {
            //$query->whereRaw("to_char(created_at, 'YYYY-MM-DD') LIKE '%$date%'");
            $query->where('created_at', 'LIKE', "%$date%");
        }])
        ->with('movements.inventory.location')
        ->with('movements.user.person')
        ->whereHas('movements', function ($query) use ($date) {
            //$query->whereRaw("to_char(created_at, 'YYYY-MM-DD') LIKE '%$date%'");
            $query->where('created_at', 'LIKE', "%$date%");
        })
        ->wherePresentationId($presentationId)
        ->whereLocationId($locationId)
        ->first();
    }

    public function search($locationId)
    {
        return $this->model
        ->with(['presentation' => function($query) {
            $query->with('item')->with('color')->with('size')
            ->with(['web_prices' => function($query) {
                $query->with('price_rules')
                ->where('price', '>', 0);
            }])
            ->with(['shop_prices' => function($query) {
                $query->with('price_rules')
                ->where('price', '>', 0);
            }])
            ->with(['wholesale_prices' => function($query) {
                $query->with('price_rules')
                ->where('price', '>', 0);
            }]);
        }])
        ->whereLocationId($locationId)
        ->whereHas('presentation.item', function($query){
            $query->whereType(1);
        })
        ->get();
    }
}