<?php

namespace App\Repositories;

use App\Entities\Address;

class AddressRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Address;
    }

    public function personAddressQuantity($personId)
    {
        return $this->model->whereModelId($personId)->whereModelType(1)->count();
    }

    public function personAddressExists($personId, $address)
    {
        return $this->model->whereModelId($personId)->whereModelType(1)->whereAddress($address)->first();
    }
}