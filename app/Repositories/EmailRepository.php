<?php

namespace App\Repositories;

use App\Entities\Email;

class EmailRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Email;
    }

    public function personEmailQuantity($personId)
    {
        return $this->model->whereModelId($personId)->whereModelType(1)->count();
    }

    public function personEmailExists($personId, $email)
    {
        return $this->model->whereModelId($personId)->whereModelType(1)->whereEmail($email)->first();
    }
}