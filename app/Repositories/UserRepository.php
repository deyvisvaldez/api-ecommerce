<?php

namespace App\Repositories;

use JWTAuth;

use App\Entities\User;
use App\Http\Requests;
use App\Http\Controllers\Response;

use Tymon\JWTAuth\Exceptions\JWTException;

class UserRepository extends AbstractRepository
{
    public function getModel()
    {
        return new User;
    }

    public function findByPersonId($personId)
    {
        return $this->model->wherePersonId($personId)->first();
    }

    public function completeData($userId)
    {
        return $this->model->with(['person' => function($query){
            $query/*->with(['emails' => function($query){
                $query->whereMain(true);
            }])*/
            ->with('email')
            ->with('address')
            ->with('whatsapp');
        }])
        ->find($userId);

        //return $this->model->with('person.email')->with('person.address')->with('person.whatsapp')->find($userId);
    }
}
