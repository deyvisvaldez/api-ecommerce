<?php

namespace App\Repositories;

use App\Entities\Order;
use App\Entities\OrderInventory;

use Carbon\Carbon;

class OrderRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Order;
    }

    public function byCode($orderCode)
    {
        return $this->model->whereCode($orderCode)->first();
    }

    public function toPdfReport($orderUuid)
    {
        return $this->model
        ->with('presentations.item')
        ->with('presentations.size')
        ->with('presentations.color')
        ->whereUuid($orderUuid)
        ->first();
    }

    public function orderTypeQuantity($orderType)
    {
        return $this->model->withTrashed()->whereOrderType($orderType)->count();
    }

    public function all($filters)
    {
        $orders = $this->model->with('location')->with('person.region')->with('person.user')->with('presentations');

        if (array_key_exists('status', $filters)) {
            $orders = $orders->whereStatus($filters['status']);
        }

        $orders = $orders->orderBy('id', 'desc')->get();

        return $orders;
    }

    public function findByCode($orderCode, $locationId = 0)
    {
        return $this->model
        ->with(['presentations' => function($query) use ($locationId) {
            if ($locationId) {
                $query->with(['inventory' => function($query) use ($locationId) {
                    $query->whereLocationId($locationId);
                }]);
            }
            $query->with('color')->with('size')->with('item');
        }])
        ->with('extra_cost')
        ->with('person.whatsapp')
        ->with('person.email')
        ->with('person.address')
        ->with('person.region')
        ->with(['payments' => function($query) {
            $query->whereStatus(true);
        }])
        ->whereCode($orderCode)
        ->first();
    }

    public function orderQuantity($code)
    {
        return $this->model->where('code', 'ilike', "$code%")->count();
    }


    /**
     * Wiltex
     */

    public function findToUpdate($orderId)
    {
        return $this->model->with('inventories')->get();
    }

    public function findByUuid($orderUuid)
    {
        return $this->model->whereUuid($orderUuid)->first();
    }

    public function getOrdersQuantity($companyId)
    {
        $quantity = $this->model->whereOrderType(1)->whereCompanyId($companyId)->count();

        if ($quantity) {
            $o = $this->model->whereOrderType(1)->whereCompanyId($companyId)->orderBy('id', 'desc')->first();
            $number = explode("-", $o->code);
            $number = (int)$number[1];
            return $number + 1;
        }

        return $quantity + 1;
    }

    public function generateLot($personIdentityDocument)
    {
        $now = Carbon::now('America/Lima');
        $date = $now->toDateTimeString();
        $lot = "$personIdentityDocument $date";
        return $lot;
    }

    /*public function getEmailData($orderId)
    {
        $order = $this->model
        ->with(['order_inventories' => function($query){
            $query->with(['inventory' => function($query){
                $query->with(['item' => function($query){
                    $query->select('id', 'name');
                }])
                ->with(['brand' => function ($query) {
                    $query->select(['id', 'name as value']);
                }])
                ->with(['presentation' => function ($query) {
                    $query->select(['id', 'name as value']);
                }])
                ->with(['flavour' => function ($query) {
                    $query->select(['id', 'name as value']);
                }])
                ->with(['size' => function ($query) {
                    $query->select(['id', 'name as value']);
                }])
                ->with(['color' => function ($query) {
                    $query->select(['id', 'name as value']);
                }])
                ->with(['quality' => function ($query) {
                    $query->select(['id', 'name as value']);
                }])
                ->with(['property' => function ($query) {
                    $query->select(['id', 'name as value']);
                }])
                ->select('id', 'item_id', 'brand_id', 'presentation_id', 'flavour_id', 'size_id', 'color_id', 'quality_id', 'property_id');
            }])
            ->select('inventory_id', 'quantity', 'price', 'order_id');
        }])
        ->select('id', 'total', 'created_at')
        ->find($orderId);

        $formattedOrder = [
            'total' => $order->total,
            'date' => $order->created_at->format('d/m/Y'),
            'time' => $order->created_at->format('H:i'),
            'items' => [],
            'itemsName' => [],
            'itemAttributes' => []
        ];

        $items = [];
        $itemsName = [];
        $itemAttributes = [];
        foreach ($order->order_inventories as $order_inventory) {
            $item = [
                'price' => (float)$order_inventory->price,
                'quantity' => $order_inventory->quantity,
                'subtotal' => (float)$order_inventory->price * (int)$order_inventory->quantity,
                'item' => $order_inventory->inventory->item->name,
                'attributes' => []
            ];
            $itemsName[] = $order_inventory->inventory->item->name;

            $attributes = [];

            if ($order_inventory->inventory->brand) {
                $attributes['Marca'] = $order_inventory->inventory->brand->value;
                $itemAttributes[] = 'Marca';
            }

            if ($order_inventory->inventory->presentation) {
                $attributes['Presentación'] = $order_inventory->inventory->presentation->value;
                $itemAttributes[] = 'Presentación';
            }

            if ($order_inventory->inventory->flavour) {
                $attributes['Sabor'] = $order_inventory->inventory->flavour->value;
                $itemAttributes[] = 'Sabor';
            }

            if ($order_inventory->inventory->size) {
                $attributes['Tamaño'] = $order_inventory->inventory->size->value;
                $itemAttributes[] = 'Tamaño';
            }

            if ($order_inventory->inventory->color) {
                $attributes['Color'] = $order_inventory->inventory->color->value;
                $itemAttributes[] = 'Color';
            }

            if ($order_inventory->inventory->quality) {
                $attributes['Calidad'] = $order_inventory->inventory->quality->value;
                $itemAttributes[] = 'Calidad';
            }

            if ($order_inventory->inventory->property) {
                $attributes['Propiedad'] = $order_inventory->inventory->property->value;
                $itemAttributes[] = 'Propiedad';
            }

            $item['attributes'] = $attributes;

            $items[] = $item;
        }

        $quantityNames = array_count_values($itemsName);

        $formattedOrder['items'] = $items;
        $formattedOrder['itemsName'] = $quantityNames;
        $formattedOrder['itemAttributes'] = array_unique($itemAttributes);

        return $formattedOrder;
    }*/
}