<?php

namespace App\Repositories;

use App\Entities\PriceRule;

class PriceRuleRepository extends AbstractRepository
{
    public function getModel()
    {
        return new PriceRule;
    }

    public function getRuleQuantity($quantity, $presentationId, $priceType)
    {
        $priceRule = $this->model
        ->whereHas('prices', function($query) use ($presentationId, $priceType) {
            $query->whereType($priceType)
            ->where('price', '>', 0)
            ->whereHas('presentation', function($query) use ($presentationId) {
                $query->whereId($presentationId);
            });
        })
        ->where('quantity', '<=', $quantity)
        ->orderBy('quantity', 'desc')
        ->first();
        return (int) $priceRule->quantity;
    }

    public function getMinRuleQuantity($presentationId, $priceType)
    {
        $priceRule = $this->model
        ->whereHas('prices', function($query) use ($presentationId, $priceType) {
            $query->whereType($priceType)
            ->where('price', '>', 0)
            ->whereHas('presentation', function($query) use ($presentationId) {
                $query->whereId($presentationId);
            });
        })
        ->orderBy('quantity', 'asc')
        ->first();
        return (int) $priceRule->quantity;
    }
}