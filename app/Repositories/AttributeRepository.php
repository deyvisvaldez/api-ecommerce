<?php

namespace ComproenTacna\Repositories;

use App\Entities\Category;
use App\Entities\CategorySetting;
/*use App\Entities\Brand;
use App\Entities\Presentation;
use App\Entities\Flavour;
use App\Entities\Size;
use App\Entities\Color;
use App\Entities\Quality;
use App\Entities\Property;*/

class AttributeRepository
{
    public function getAllAttributes()
    {
        $attributes = [
            [
                'id' => '1',
                'title' => 'Marca',
                'type' => 'brand'
            ],
            [
                'id' => '2',
                'title' => 'Presentación',
                'type' => 'presentation'
            ],
            [
                'id' => '3',
                'title' => 'Sabor',
                'type' => 'flavour'
            ],
            [
                'id' => '4',
                'title' => 'Tamaño',
                'type' => 'size'
            ],
            [
                'id' => '5',
                'title' => 'Color',
                'type' => 'color'
            ],
            [
                'id' => '6',
                'title' => 'Calidad',
                'type' => 'quality'
            ],
            [
                'id' => '7',
                'title' => 'Propiedad',
                'type' => 'property'
            ]
        ];

        return $attributes;
    }

    public function attributesByCategory($categoryId)
    {
        $categories = Category::with(['category_settings' => function($query){
            $query->select(['category_id', 'categor_id']);
        }])
        ->select(['id', 'name', 'slug', 'image'])
        ->whereBusinessCategoryId($categoryId)
        ->wherePublished(true)
        ->get();

        $formattedCategories = [];

        foreach ($categories as $key => $category) {
            $cat = [
                'id' => $category->id,
                'name' => $category->name,
                'slug' => $category->slug,
                'image' => $category->image,
                'attributes' => []
            ];

            $attributes = [];
            foreach ($category->category_settings as $key => $setting) {
                switch ($setting->attribute_id) {
                    case '1':
                        $attributes[] = [
                            'id' => '1',
                            'title' => 'Marca',
                            'type' => 'brand',
                            'style_type' => 'text'
                        ];
                        break;

                    case '2':
                        $attributes[] = [
                            'id' => '2',
                            'title' => 'Presentación',
                            'type' => 'presentation',
                            'style_type' => 'text'
                        ];
                        break;

                    case '3':
                        $attributes[] = [
                            'id' => '3',
                            'title' => 'Sabor',
                            'type' => 'flavour',
                            'style_type' => 'text'
                        ];
                        break;

                    case '4':
                        $attributes[] = [
                            'id' => '4',
                            'title' => 'Tamaño',
                            'type' => 'size',
                            'style_type' => 'text'
                        ];
                        break;

                    case '5':
                        $attributes[] = [
                            'id' => '5',
                            'title' => 'Color',
                            'type' => 'color',
                            'style_type' => 'color'
                        ];
                        break;

                    case '6':
                        $attributes[] = [
                            'id' => '6',
                            'title' => 'Calidad',
                            'type' => 'quality',
                            'style_type' => 'text'
                        ];
                        break;

                    case '7':
                        $attributes[] = [
                            'id' => '7',
                            'title' => 'Propiedad',
                            'type' => 'property',
                            'style_type' => 'text'
                        ];
                        break;
                }
            }

            $cat['attributes'] = $attributes;

            $formattedCategories[] = $cat;
        }

        return $formattedCategories;
    }
}