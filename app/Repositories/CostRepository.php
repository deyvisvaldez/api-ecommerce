<?php

namespace App\Repositories;

use App\Entities\Cost;

class CostRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Cost;
    }
}