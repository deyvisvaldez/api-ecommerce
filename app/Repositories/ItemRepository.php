<?php

namespace App\Repositories;

use App\Entities\AttributeSetting;
use App\Entities\Color;
use App\Entities\Item;
use App\Entities\Price;
use Carbon\Carbon;
use DB;

class ItemRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Item;
    }

    public function lastWeekItems($tag, $query, $categoryId, $subcategoryId)
    {
        $lastWeek = Carbon::today()->subWeeks(1);

        $items = $this->model
        ->with('category')
        ->with('subcategory')
        ->with(['presentations' => function ($query) use ($tag) {
            $query->with(['photos' => function ($query) {
                $query->inRandomOrder();
            }])
            ->wherePublished(true)
            ->inRandomOrder();

            if ($tag) {
                $query->whereHas('tags', function($query) use ($tag) {
                    $query->where('tags.id', $tag);
                });
            }
        }])
        ->where('created_at', '>=', $lastWeek)
        ->where('referencial_price', '>', 1)
        ->whereType(1)
        ->whereHas('category', function ($query) {
            $query->wherePublished(true);
        })
        ->whereHas('presentations', function ($query) {
            $query->wherePublished(true);
        })
        ->whereHas('presentations.inventories.location', function($query){
            $query->whereMain(true);
        })
        ->whereHas('presentations.web_prices', function($query){
            $query->where('price', '>', 1);
        });

        if ($tag) {
            $items = $items->whereHas('presentations.tags', function($query) use ($tag) {
                $query->where('tags.id', $tag);
            });
        }

        if ($categoryId) {
            $items = $items->whereCategoryId($categoryId);
        }

        if ($subcategoryId) {
            $items = $items->whereSubcategoryId($subcategoryId);
        }

        if ($query) {
            $items = $items->where('name', 'like', "%$query%");
        }

        $items = $items->inRandomOrder()->get();

        $formattedItems = [];
        foreach ($items as $key => $item) {

            if (count($item->presentations)) {
                $fmtItems = [
                    'id' => (int) $item->id,
                    'name' => $item->name,
                    'slug' => $item->slug,
                    'price' => (float) $item->referencial_price,
                    'firstImage' => asset('defaults/item.png'),
                    'secondImage' => '',
                    'promotion' => [
                        'flag' => (boolean) $item->available_promotion,
                        'price' => (float) $item->promotional_price
                    ],
                    'categorySlug' => $item->category->slug,
                    'subcategoryName' => $item->subcategory->name,
                    'subcategorySlug' => $item->subcategory->slug,
                ];

                $flag = true;

                $photos = [];
                foreach ($item->presentations as $key => $presentation) {
                    foreach ($presentation->photos as $key => $photo) {
                        $photos[] = $photo->resource_thumb;
                    }
                }
                $photos = array_values(array_unique($photos, SORT_REGULAR));

                $count = count($photos);
                if ($count) {
                    $fmtItems['firstImage'] = $photos[0];
                    if ($count > 1) {
                        $fmtItems['secondImage'] = $photos[1];
                    }
                }

                $formattedItems[] = $fmtItems;
            }
        }

        return $formattedItems;
    }

    public function remainCatalog($ids, $tag, $query, $categoryId, $subcategoryId)
    {
        $items = $this->model
        ->with('category')
        ->with('subcategory')
        ->with(['presentations' => function ($query) use ($tag) {
            $query->with(['photos' => function ($query) {
                $query->inRandomOrder();
            }])
            ->wherePublished(true)
            ->inRandomOrder();

            if ($tag) {
                $query->whereHas('tags', function($query) use ($filters) {
                    $query->where('tags.id', $tag);
                });
            }
        }])
        ->where('referencial_price', '>', 1)
        ->whereType(1)
        ->whereHas('category', function ($query) {
            $query->wherePublished(true);
        })
        ->whereHas('presentations', function ($query) {
            $query->wherePublished(true);
        })
        ->whereHas('presentations.inventories.location', function($query){
            $query->whereMain(true);
        })
        ->whereHas('presentations.web_prices', function($query){
            $query->where('price', '>', 1);
        });

        if ($tag) {
            $items = $items->whereHas('presentations.tags', function($query) use ($tag) {
                $query->where('tags.id', $tag);
            });
        }

        if ($categoryId) {
            $items = $items->whereCategoryId($categoryId);
        }

        if ($subcategoryId) {
            $items = $items->whereSubcategoryId($subcategoryId);
        }

        if ($query) {
            $items = $items->where('name', 'like', "%$query%");
        }

        if (count($ids)) {
            $items = $items->whereNotIn('id', $ids);
        }

        $items = $items->inRandomOrder()->get();

        $formattedItems = [];
        foreach ($items as $key => $item) {

            if (count($item->presentations)) {
                $fmtItems = [
                    'id' => (int) $item->id,
                    'name' => $item->name,
                    'slug' => $item->slug,
                    'price' => (float) $item->referencial_price,
                    'firstImage' => asset('defaults/item.png'),
                    'secondImage' => '',
                    'promotion' => [
                        'flag' => (boolean) $item->available_promotion,
                        'price' => (float) $item->promotional_price
                    ],
                    'categorySlug' => $item->category->slug,
                    'subcategoryName' => $item->subcategory->name,
                    'subcategorySlug' => $item->subcategory->slug,
                ];

                $flag = true;

                $photos = [];
                foreach ($item->presentations as $key => $presentation) {
                    foreach ($presentation->photos as $key => $photo) {
                        $photos[] = $photo->resource_thumb;
                    }
                }
                $photos = array_values(array_unique($photos, SORT_REGULAR));

                $count = count($photos);
                if ($count) {
                    $fmtItems['firstImage'] = $photos[0];
                    if ($count > 1) {
                        $fmtItems['secondImage'] = $photos[1];
                    }
                }

                $formattedItems[] = $fmtItems;
            }
        }

        return $formattedItems;
    }

    public function last($last = 10)
    {
        return $this->model
        ->with(['presentations' => function ($query) {
            $query->with('color')
            ->with('tags')
            ->with(['photos' => function($query) {
                $query->inRandomOrder();
            }])
            ->wherePublished(true)
            ->inRandomOrder();
        }])
        ->with('category')
        ->with('subcategory')
        ->whereType(1)
        ->where('referencial_price', '>', 1)
        ->whereHas('presentations', function ($query) {
            $query->wherePublished(true);
        })
        ->whereHas('presentations.inventories.location', function($query){
            $query->whereMain(true);
        })
        ->take($last)
        ->orderBy('id', 'desc')
        ->inRandomOrder()
        ->get();
    }

    public function webCatalog($filters, $pagination = 0)
    {
        $items = $this->model
        ->with(['presentations' => function ($query) use ($filters) {
            $query->with('color')
            ->with('tags')
            ->with(['photos' => function($query) {
                $query->inRandomOrder();
            }]);

            if (array_key_exists('tag', $filters)) {
                if ($filters['tag']) {
                    $query->whereHas('tags', function($query) use ($filters) {
                        $query->where('tags.id', $filters['tag']);
                    });
                }
            }

            $query->wherePublished(true)
            ->inRandomOrder();
        }])
        ->with('category')
        ->with('subcategory')
        ->whereType(1)
        ->where('referencial_price', '>', 1)
        ->whereHas('presentations', function ($query) {
            $query->wherePublished(true);
        })
        ->whereHas('presentations.inventories.location', function($query){
            $query->whereMain(true);
        });

        if (array_key_exists('tag', $filters)) {
            if ($filters['tag']) {
                $items = $items->whereHas('presentations.tags', function($query) use ($filters) {
                    $query->where('tags.id', $filters['tag']);
                });
            }
        }

        if (array_key_exists('categoryId', $filters)) {
            if ($filters['categoryId']) {
                $items = $items->whereCategoryId($filters['categoryId']);
            } else {
                $items = $items->whereHas('category', function ($query) {
                    $query->wherePublished(true);
                });
            }
        }

        if (array_key_exists('subcategoryId', $filters)) {
            if ($filters['subcategoryId']) {
                $items = $items->whereSubcategoryId($filters['subcategoryId']);
            }
        }

        if (array_key_exists('query', $filters)) {
            $q = $filters['query'];
            $items = $items->where('name', 'like', "%$q%");
        }

        if (array_key_exists('last', $filters)) {
            if ($filters['last']) {
                $items = $items->take($filters['last']);
            }
        }

        if ($pagination) {
            return $items = $items->inRandomOrder()->paginate($pagination);
        }

        return $items = $items->inRandomOrder()->get();
    }

    public function distributorCatalog($filters)
    {
        $items = DB::table('items')
        ->select('items.id', 'items.name', 'items.slug', 'prices.currency_id', 'presentations.color_id', /*'contents.resource_thumb', */'colors.color_code', 'colors.name as color')
        ->join('presentations', 'presentations.item_id', 'items.id')
        //->join('contents', 'contents.model_id', 'presentations.id')
        ->join('prices', 'prices.presentation_id', 'presentations.id')
        ->join('colors', 'colors.id', 'presentations.color_id')
        ->join('inventories', 'inventories.presentation_id', 'presentations.id')
        ->join('locations', 'locations.id', 'inventories.location_id')
        ->where('locations.main', true)
        ->where('items.distributor_published', true)
        ->whereNull('items.deleted_at')
        ->whereNull('presentations.deleted_at')
        //->whereNull('contents.deleted_at')
        ->whereNull('prices.deleted_at')
        ->whereNull('inventories.deleted_at')
        ->where('items.type', 1)
        /*->where('contents.model_type', 2)*/;

        if ($filters['categoryId']) {
            $items = $items->where('items.category_id', $filters['categoryId']);
        }

        if ($filters['subcategoryId']) {
            $items = $items->where('items.subcategory_id', $filters['subcategoryId']);
        }

        if (array_key_exists('query', $filters)) {
            $name = $filters['query'];
            $items = $items->where('items.name', 'like', "%$name%");
        }

        $items = $items->groupBy('items.id', 'items.name', 'items.slug', 'presentations.color_id', 'prices.currency_id', /*'contents.resource_thumb', */'colors.color_code', 'colors.name')
        ->orderBy('presentations.color_id')
        ->get();

        return $items;
    }

    public function admin($filters)
    {
        $items = $this->model
        ->with(['presentations' => function ($query) {
            $query->with(['photos' => function ($query) {
                $query->inRandomOrder();
            }])
            ->inRandomOrder();
        }])
        ->whereType(1);

        if (array_key_exists('categoryId', $filters)) {
            if ($filters['categoryId']) {
                $items = $items->whereCategoryId($filters['categoryId']);
            }
        }

        if (array_key_exists('subcategoryId', $filters)) {
            if ($filters['subcategoryId']) {
                $items = $items->whereSubcategoryId($filters['subcategoryId']);
            }
        }

        if (array_key_exists('q', $filters)) {
            $text = $filters['q'];
            $items = $items->where('name', 'like', "%$text%");
        }

        return $items = $items->orderBy('id', 'desc')->get();
    }

    public function find($itemId)
    {
        return $this->model->with('presentations')->find($itemId);
    }

    public function findBySlug($slug)
    {
        return $this->model->with('recommendations')->whereSlug($slug)->whereType(1)->first();
    }

    public function generalMatrix($filters, $paginate=0)
    {
        $items = $this->model
        ->with(['presentations' => function ($query) use ($filters, $paginate) {
            $query->with(['size' => function ($query) {
                $query->orderBy('order');
            }])
            ->with(['color' => function ($query) {
                $query->orderBy('order');
            }])
            ->with(['web_prices' => function ($query) {
                $query->with(['price_rules' => function($query) {
                    //$query->where('name', 'like', '%Precio Unitario%');
                    $query->whereQuantity(1);
                }])
                ->whereHas('price_rules', function ($query) {
                    //$query->where('name', 'like', '%Precio Unitario%');
                    $query->whereQuantity(1);
                });
            }])
            ->with(['photos' => function ($query) {
                $query->inRandomOrder();
            }]);

            // Si pagina, no consultar los inventarios, para sólo conseguri la tallas
            if ($paginate) {
                $query->with(['inventories' => function($query) use ($filters) {
                    if ($filters['locationId']) {
                        $query->whereLocationId($filters['locationId']);
                    }
                }]);
            }

            if ($filters['colorId']) {
                $query->whereColorId($filters['colorId']);
            }
        }])
        ->whereType(1)
        ->whereHas('presentations');

        if ($filters['categoryId']) {
            $items = $items->whereCategoryId($filters['categoryId']);
        }

        if ($filters['subcategoryId']) {
            $items = $items->whereSubcategoryId($filters['subcategoryId']);
        }

        if ($paginate) {
            $items = $items->paginate($paginate);
        } else {
            $items = $items->get();
        }

        return $items;
    }

    public function presentationProfile($presentations, $locationId)
    {
        // Con precios unitarios
        return $this->model
        ->with(['presentations' => function ($query) use ($presentations, $locationId) {
            $query->with('color')->with('size')
            ->with(['web_prices' => function ($query) {
                $query->with(['price_rules' => function($query) {
                    $query->whereQuantity(1);
                }])
                ->whereHas('price_rules', function ($query) {
                    $query->whereQuantity(1);
                });
            }])
            ->with(['shop_prices' => function ($query) {
                $query->with(['price_rules' => function($query) {
                    $query->whereQuantity(1);
                }])
                ->whereHas('price_rules', function ($query) {
                    $query->whereQuantity(1);
                });
            }])
            ->with(['wholesale_prices' => function ($query) {
                $query->with(['price_rules' => function($query) {
                    $query->whereQuantity(1);
                }])
                ->whereHas('price_rules', function ($query) {
                    $query->whereQuantity(1);
                });
            }])
            ->with(['inventories' => function($query) use ($locationId) {
                $query->whereLocationId($locationId);
            }])
            ->whereIn('id', $presentations);
        }])
        ->whereHas('presentations', function ($query) use ($presentations) {
            $query->whereIn('id', $presentations);
        })
        ->whereType(1)
        ->get();
    }

    public function profile($itemSlug, $priceType)
    {
        $item = $this->model
        ->with('recommendations')
        ->with('subcategory')
        ->with(['presentations' => function ($query) use ($priceType){
            $query->with(['size' => function ($query) {
                $query->orderBy('order');
            }])
            ->with(['color' => function ($query) {
                $query->orderBy('order');
            }])
            ->with([$priceType => function($query) {
                $query->with('price_rules')
                ->where('price', '>', 1);
            }])
            ->with(['photos' => function ($query) {
                $query->inRandomOrder();
            }])
            ->with(['inventories' => function ($query) {
                $query->whereHas('location', function($query){
                    $query->whereMain(true);
                });
            }])
            ->with('tags')
            ->wherePublished(true)
            ->whereHas('inventories.location', function($query){
                $query->whereMain(true);
            })
            ->whereHas($priceType, function($query){
                $query->where('price', '>', 1);
            });
        }])
        ->whereSlug($itemSlug)
        ->whereHas('presentations', function($query) {
            $query->wherePublished(true);
        })
        ->whereType(1)
        ->firstOrFail();

        return $item;
    }

    //Revisar
    public function findWithStock($presentations)
    {
        return $this->model
        ->with(['presentations' => function ($query) use ($presentations) {
            $query->with('color')->with('size')
            ->with(['inventories' => function($query) {
                $query->with('location')
                ->where('available_stock', '>', 0);
            }])
            ->whereIn('id', $presentations);
        }])
        ->whereHas('presentations', function ($query) use ($presentations) {
            $query->whereIn('id', $presentations);
        })
        ->whereType(1)
        ->get();
    }

    public function profileByLocation($itemSlug, $locationId)
    {
        $item = $this->model
        ->with(['presentations' => function ($query) use ($locationId) {
            $query->with('size')->with('color')
            ->with(['web_prices' => function ($query) {
                $query->with(['price_rules' => function($query) {
                    $query->whereQuantity(1);
                }])
                ->whereHas('price_rules', function ($query) {
                    $query->whereQuantity(1);
                });
            }])
            ->with(['photos' => function ($query) {
                $query->inRandomOrder();
            }]);

            if ($locationId) {
                $query->with(['inventories' => function($query) use ($locationId) {
                    $query->whereLocationId($locationId);
                }])
                ->has('inventories');
            } else {
                $query->with('inventories');
            }
        }])
        ->with('subcategory')
        ->whereSlug($itemSlug)
        ->firstOrFail();

        return $item;
    }

    public function findWithColors($itemSlug)
    {
        return $this->model->with('presentations.color')->whereSlug($itemSlug)->whereType(1)->first();
    }


    /**
     * Wiltex
     */

    public function all()
    {
        return $this->model->with('recommendations')->whereType(1)->get();
    }

    public function posSearch($locationId)
    {
        return $this->model->with('recommendations')
        ->whereHas('inventories', function($query) use ($locationId) {
            $query->whereLocationId($locationId);
        })
        ->whereType(1)
        ->get();
    }

    public function byLocation($locationId)
    {
        return $this->model
        ->with(['inventories' => function($query) use ($locationId) {
            $query->with('color')
            ->whereLocationId($locationId);
        }])
        ->whereHas('inventories', function($query) use ($locationId) {
            $query->whereLocationId($locationId);
        })
        ->whereType(1)
        ->get();
    }

    public function byColorAndLocation($colorId, $locationId)
    {
        return $this->model
        ->with(['inventories' => function($query) use ($colorId, $locationId) {
            $query->whereColorId($colorId)
            ->whereLocationId($locationId);
        }])
        ->whereHas('inventories', function($query) use ($colorId, $locationId) {
            $query->whereColorId($colorId)
            ->whereLocationId($locationId);
        })
        ->whereType(1)
        ->get();
    }



    public function sharedItem($slug)
    {
        $item = $this->model
        ->with(['inventories' => function ($query) {
            $query->with(['photos' => function ($query) {
                $query->select(['id', 'model_id', 'resource', 'resource_thumb'])
                ->inRandomOrder();
            }])
            ->select(['id', 'item_id'])
            ->wherePublished(true);
        }])
        ->select(['id', 'name', 'description'])
        ->whereSlug($itemSlug)
        ->whereType(1)
        ->firstOrFail();
        return $item;
    }

    public function catalog()
    {
        return $items = $this->model
        ->with(['inventories' => function ($query) {
            $query->with(['photos' => function ($query) {
                $query->select(['id', 'model_id', 'resource', 'resource_thumb'])
                ->inRandomOrder();
            }])
            ->with(['category' => function ($query) {
                $query->select(['id', 'slug']);
            }])
            ->with(['subcategory' => function ($query) {
                $query->select(['id', 'name', 'slug']);
            }])
            ->with(['color' => function ($query) {
                $query->select(['id', 'name', 'color_code']);
            }])
            ->with(['prices' => function ($query) {
                $query->select(['id', 'inventory_id', 'name', 'price', 'available_promotion', 'promotional_price']);
            }])
            ->select(['id', 'item_id', 'category_id', 'subcategory_id', 'color_id'])
            ->wherePublished(true)
            ->whereHas('photos')
            ->whereHas('location', function($query){
                $query->whereMain(true);
            })
            //->whereLocationId(3) //Sólo en demo.wiltex.la
            ->inRandomOrder();
        }])
        ->select(['id', 'company_id', 'name', 'slug', 'business_code', 'published'])
        ->whereHas('inventories', function ($query) {
            $query->wherePublished(true)/*
            ->whereLocationId(3)*/; //Sólo en demo.wiltex.la
        })
        ->whereHas('inventories.location', function($query){
            $query->whereMain(true);
        })
        ->whereHas('inventories.photos')
        //->whereIn('category_id', [2, 5]) //Sólo en demo.wiltex.la
        ->whereType(1)
        ->get();
    }

    public function related($subcategoryId, $itemId)
    {
        return $items = $this->model
        ->with(['presentations' => function ($query) {
            $query->with('color')
            ->with('tags')
            ->with('random_photos')
            ->with(['web_prices' => function ($query) {
                $query->whereAvailablePromotion(true)
                ->whereHas('price_rules', function($query){
                    $query->whereQuantity(1);
                });
            }])
            ->with(['random_price' => function ($query) {
                $query->whereHas('price_rules', function($query){
                    $query->whereQuantity(1);
                });
            }]);
            /*->whereHas('location', function($query){
                $query->whereMain(true);
            })*/

            $query->wherePublished(true)
            ->inRandomOrder();
        }])
        ->with('category')
        ->with('subcategory')
        ->whereType(1)
        ->whereHas('presentations', function ($query) {
            $query->wherePublished(true);
        })
        ->whereHas('presentations.inventories.location', function($query){
            $query->whereMain(true);
        })
        ->whereSubcategoryId($subcategoryId)
        ->where('id', '!=', $itemId)
        ->inRandomOrder()
        ->take(8)
        ->get();
    }

    public function profilePos($itemId, $locationId)
    {
        $item = $this->model
        ->with(['inventories' => function ($query) use ($locationId){
            $query->with(['size' => function ($query) {
                $query->select(['id', 'name', 'order'])
                ->orderBy('order');
            }])
            ->with(['color' => function ($query) {
                $query->select(['id', 'name', 'color_code', 'order'])
                ->orderBy('order');
            }])
            ->with(['prices' => function ($query) {
                $query->select(['id', 'inventory_id', 'name', 'price', 'available_promotion', 'promotional_price', 'currency_id']);
            }])
            ->with(['shop_price' => function ($query) {
                $query->select(['id', 'inventory_id', 'name', 'price', 'available_promotion', 'promotional_price', 'currency_id']);
            }])
            ->with(['subcategory' => function ($query) {
                $query->select(['id', 'name']);
            }])
            ->with(['photos' => function ($query) {
                $query->select(['id', 'model_id', 'resource', 'resource_thumb'])
                ->inRandomOrder();
            }])
            ->select(['id', 'item_id', 'size_id', 'color_id', 'subcategory_id', 'located', 'available_stock'])
            ->whereLocationId($locationId)
            /*->wherePublished(true)*/;
        }])
        ->select(['id', 'company_id', 'name', 'slug', 'description', 'features'])
        ->whereHas('inventories', function($query) use ($locationId){
            $query->/*wherePublished(true)
            ->*/whereLocationId($locationId);
        })
        ->find($itemId);

        return $item;
    }

    public function lastItem($locationId)
    {
        $item = $this->model
        ->with(['inventories' => function ($query) use ($locationId){
            $query->with(['size' => function ($query) {
                $query->select(['id', 'name', 'order'])
                ->orderBy('order');
            }])
            ->with(['color' => function ($query) {
                $query->select(['id', 'name', 'color_code', 'order'])
                ->orderBy('order');
            }])
            ->with(['prices' => function ($query) {
                $query->select(['id', 'inventory_id', 'name', 'price', 'available_promotion', 'promotional_price', 'currency_id']);
            }])
            ->with(['shop_price' => function ($query) {
                $query->select(['id', 'inventory_id', 'name', 'price', 'available_promotion', 'promotional_price', 'currency_id']);
            }])
            ->with(['subcategory' => function ($query) {
                $query->select(['id', 'name']);
            }])
            ->with(['photos' => function ($query) {
                $query->select(['id', 'model_id', 'resource', 'resource_thumb'])
                ->inRandomOrder();
            }])
            ->select(['id', 'item_id', 'size_id', 'color_id', 'subcategory_id', 'located', 'available_stock'])
            ->whereLocationId($locationId)
            ->wherePublished(true);
        }])
        ->select(['id', 'company_id', 'name', 'slug', 'description', 'features'])
        ->whereHas('inventories', function($query) use ($locationId){
            $query->wherePublished(true)
            ->whereLocationId($locationId);
        })
        ->whereType(1)
        ->orderBy('id', 'DESC')
        ->first();

        return $item;
    }




    public function findWithPrices($inventories)
    {
        return $this->model
        ->with(['inventories' => function ($query) use ($inventories) {
            $query->with('web_price')->with('shop_price')->with('wholesale_price')->with('color')->with('size')
            ->whereIn('id', $inventories);
        }])
        ->whereHas('inventories', function ($query) use ($inventories) {
            $query->whereIn('id', $inventories);
        })
        ->whereType(1)
        ->get();
    }

    private function array_sort_by_column(&$arr, $col, $dir = SORT_ASC)
    {
        $sort_col = array();
        foreach ($arr as $key => $row) {
            $sort_col[$key] = $row[$col];
        }

        array_multisort($sort_col, $dir, $arr);
    }

    private function find_option_name($options, $field, $value)
    {
       foreach($options as $key => $option)
       {
          if ( $option[$field] == $value )
            return $key;
       }
       return false;
    }
}
