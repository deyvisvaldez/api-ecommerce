<?php

namespace App\Repositories;

use App\Entities\Video;

class VideoRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Video;
    }

    public function companyVideos()
    {
        return $this->model->whereModelId(1)->whereModelType(1)->get();
    }
}