<?php

namespace App\Repositories;

use App\Entities\Presentation;

class PresentationRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Presentation;
    }

    public function findWithUnitaryPrices($presentationId)
    {
        return $this->model
        ->with(['web_prices' => function ($query) {
            $query->with(['price_rules' => function($query) {
                $query->whereQuantity(1);
            }])
            ->whereHas('price_rules', function ($query) {
                $query->whereQuantity(1);
            });
        }])
        ->with(['shop_prices' => function ($query) {
            $query->with(['price_rules' => function($query) {
                $query->whereQuantity(1);
            }])
            ->whereHas('price_rules', function ($query) {
                $query->whereQuantity(1);
            });
        }])
        ->with(['wholesale_prices' => function ($query) {
            $query->with(['price_rules' => function($query) {
                $query->whereQuantity(1);
            }])
            ->whereHas('price_rules', function ($query) {
                $query->whereQuantity(1);
            });
        }])
        ->find($presentationId);
    }

    public function findSameSize($itemId, $sizeId)
    {
        return $this->model
        ->with(['web_prices' => function ($query) {
            $query->with(['price_rules' => function($query) {
                $query->whereQuantity(1);
            }])
            ->whereHas('price_rules', function ($query) {
                $query->whereQuantity(1);
            });
        }])
        ->with(['shop_prices' => function ($query) {
            $query->with(['price_rules' => function($query) {
                $query->whereQuantity(1);
            }])
            ->whereHas('price_rules', function ($query) {
                $query->whereQuantity(1);
            });
        }])
        ->with(['wholesale_prices' => function ($query) {
            $query->with(['price_rules' => function($query) {
                $query->whereQuantity(1);
            }])
            ->whereHas('price_rules', function ($query) {
                $query->whereQuantity(1);
            });
        }])
        ->whereItemId($itemId)
        ->whereSizeId($sizeId)
        ->get();
    }

    public function findSameSizeWithPrices($itemId, $sizeId)
    {
        return $this->model
        ->with('web_prices.price_rules')
        ->with('shop_prices.price_rules')
        ->with('wholesale_prices.price_rules')
        ->whereItemId($itemId)
        ->whereSizeId($sizeId)
        ->get();
    }

    public function findSameColorWithPrices($itemId, $colorId)
    {
        return $this->model
        ->with('web_prices.price_rules')
        ->with('shop_prices.price_rules')
        ->with('wholesale_prices.price_rules')
        ->whereItemId($itemId)
        ->whereColorId($colorId)
        ->get();
    }

    public function itemPresentations($itemId, $priceType)
    {
        $item = $this->model
        ->with('color')
        ->with('size');

        switch ($priceType) {
            case 1:
                $item = $item->with('web_prices.price_rules');
                break;

            case 2:
                $item = $item->with('shop_prices.price_rules');
                break;

            case 3:
                $item = $item->with('wholesale_prices.price_rules');
                break;
        }

        $item = $item->whereItemId($itemId)
        ->get();

        return $item;
    }

    public function photosByColor($itemId, $colorId)
    {
        return $this->model->whereItemId($itemId)->whereColorId($colorId)
        ->with(['photos' => function($query) {
            $query->orderBy('id', 'desc');
        }])
        ->first();
    }

    public function sameItemAndColor($itemId, $colorId)
    {
        return $this->model->whereItemId($itemId)->whereColorId($colorId)->get();
    }

    public function findByAttributes($itemId, $colorId, $sizeId)
    {
        return $this->model->whereItemId($itemId)->whereColorId($colorId)->whereSizeId($sizeId)->first();
    }

    public function cart($ids)
    {
        return $this->model
        ->with(['photos' => function ($query) {
            $query->inRandomOrder();
        }])
        ->with(['web_prices' => function($query) {
            $query->where('price', '>', 0);
        }])
        ->with(['shop_prices' => function($query) {
            $query->where('price', '>', 0);
        }])
        ->with(['wholesale_prices' => function($query) {
            $query->where('price', '>', 0);
        }])
        ->with('color')
        ->with('size')
        ->with('item')
        ->findMany($ids);
    }

    public function getPrice($presentationId, $priceType, $ruleQuantity)
    {
        $presentation = $this->model->with([$priceType => function ($query) use ($ruleQuantity) {
            $query->with(['price_rules' => function($query) use ($ruleQuantity) {
                $query->whereQuantity($ruleQuantity);
            }])
            ->whereHas('price_rules', function ($query) use ($ruleQuantity) {
                $query->whereQuantity($ruleQuantity);
            });
        }])
        ->find($presentationId);

        return (float) $presentation->{$priceType}[0]->price;
    }

    public function withPrices($presentationId, $priceType, $ruleQuantity)
    {
        return $this->model->with([$priceType => function ($query) use ($ruleQuantity) {
            $query->with(['price_rules' => function($query) use ($ruleQuantity) {
                $query->whereQuantity($ruleQuantity);
            }])
            ->whereHas('price_rules', function ($query) use ($ruleQuantity) {
                $query->whereQuantity($ruleQuantity);
            });
        }])
        ->find($presentationId);
    }

    public function presentationTags($itemId)
    {
        return $this->model->with('tags')->whereItemId($itemId)->get();
    }

    public function sameColorStock($itemId, $colorId)
    {
        return $this->model->with('color')->with('item')->with('inventories')->whereItemId($itemId)->whereColorId($colorId)->get();
    }

    public function search($locationId)
    {
        return $this->model->with('item')->with('color')->with('size')
        ->with('web_prices')->with('shop_prices')->with('wholesale_prices')
        ->with('inventories')
        ->whereHas('inventories', function($query) {
            $query->whereLocationId($locationId);
        })
        ->whereHas('item', function($query){
            $query->whereType(1);
        })
        ->get();
    }
}