<?php

namespace App\Repositories;

use App\Entities\ExtraCost;

class ExtraCostRepository extends AbstractRepository
{
    public function getModel()
    {
        return new ExtraCost;
    }
}