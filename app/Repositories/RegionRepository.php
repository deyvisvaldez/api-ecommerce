<?php

namespace App\Repositories;

use App\Entities\Region;

class RegionRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Region;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function destinations()
    {
        return $this->model
        ->with('branch_offices.transport')
        ->with('branch_offices.costs.rule')
        ->whereHas('branch_offices.costs')
        ->get();
    }

    public function find($regionId)
    {
        return $this->model->with('branch_offices.transport')->find($regionId);
    }

    public function withCountry($regionId)
    {
        return $this->model->with('country')->find($regionId);
    }

    public function withTransport($regionId, $transportId)
    {
        return $this->model->with(['transports' => function($query) use ($transportId) {
            $query->find($transportId);
        }])
        ->find($regionId);
    }

    public function getWithPrices($countryId, $transportId)
    {
        /*$regions = $this->model
        ->with(['costs' => function($query) use ($transportId) {
            $query->with('rule');
            if ($transportId) {
                $query->where('region_transport.transport_id', $transportId);
            }
        }]);*/

        $regions = $this->model
        ->with(['branch_office' => function($query) use ($transportId) {
            $query->with('costs.rule');
            if ($transportId) {
                $query->whereTransportId($transportId);
            }
        }]);

        if ($transportId) {
            $regions->whereHas('branch_office', function($query) use ($transportId) {
                $query->whereTransportId($transportId);
            });
        }

        if ($countryId) {
            $regions = $regions->whereCountryId($countryId);
        }

        $regions = $regions->get();

        return $regions;


    }
}