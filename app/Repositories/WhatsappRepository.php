<?php

namespace App\Repositories;

use App\Entities\Whatsapp;

class WhatsappRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Whatsapp;
    }

    public function personWhatsappQuantity($personId)
    {
        return $this->model->whereModelId($personId)->whereModelType(1)->count();
    }

    public function personWhatsappExists($personId, $whatsapp)
    {
        return $this->model->whereModelId($personId)->whereModelType(1)->whereWhatsapp($whatsapp)->first();
    }
}