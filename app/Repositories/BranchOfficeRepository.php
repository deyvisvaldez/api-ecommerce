<?php

namespace App\Repositories;

use App\Entities\BranchOffice;

class BranchOfficeRepository extends AbstractRepository
{
    public function getModel()
    {
        return new BranchOffice;
    }

    public function ruleCost($regionId, $transportId, $quantity)
    {
        return $this->model
        ->with(['costs' => function($query) use ($quantity) {
            $query->join('rules', 'costs.rule_id', 'rules.id')
            ->whereHas('rule', function($query) use ($quantity){
                $query->where('unit_value', '<=', $quantity);
            })
            ->orderBy('rules.unit_value', 'desc');
        }])
        ->whereRegionId($regionId)
        ->whereTransportId($transportId)
        ->first();
    }

    public function offices($regionId, $transportId)
    {
        return $this->model->whereRegionId($regionId)->whereTransportId($transportId)->get();
    }

    public function officesRules($regionId, $transportId, $ruleId)
    {
        return $this->model
        ->with(['costs' => function($query) use ($ruleId) {
            $query->whereRuleId($ruleId);
        }])
        ->whereRegionId($regionId)
        ->whereTransportId($transportId)
        ->get();
    }
}