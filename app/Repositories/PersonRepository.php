<?php

namespace App\Repositories;

use App\Entities\Person;
use App\Entities\Whatsapp;
use App\Entities\Email;

class PersonRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Person;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function phones($personId)
    {
        return $this->model->with('phones')->find($personId);
    }

    public function cellphones($personId)
    {
        return $this->model->with('cellphones')->find($personId);
    }

    public function whatsapps($personId)
    {
        return $this->model->with('whatsapps')->find($personId);
    }

    public function addresses($personId)
    {
        return $this->model->with('addresses')->find($personId);
    }

    public function emails($personId)
    {
        return $this->model->with('emails')->find($personId);
    }

    public function users()
    {
        return $this->model->with('user')->with('email')->with('cellphone')
        ->with('region')->with('country')->has('user')->get();
    }

    public function withUser($personId)
    {
        return $this->model->with('user')->find($personId);
    }

    public function completeData($personId)
    {
        return $this->model->with('email')->with('address')->with('region')->with('whatsapp')->find($personId);
    }

    public function emailData($personId)
    {
        return $this->model->with('emails')->with('address')->with('region')->with('whatsapp')->find($personId);
    }

    public function findByIdentityDocument($identityDocument)
    {
        return $this->model->with('email')->with('address')->with('whatsapp')
        ->whereIdentityDocument($identityDocument)->first();
    }

    public function findPersonWhatsapp($personId, $whatsapp)
    {
        $whatsapp = Whatsapp::whereModelId($personId)->whereModelType(1)->whereWhatsapp($whatsapp)->first();

        return $whatsapp;
    }

    public function findPersonEmail($personId, $email)
    {
        $email = Email::whereModelId($personId)->whereModelType(1)->whereEmail($email)->first();

        return $email;
    }

    public function getPersonEmails($personId)
    {
        $person = $this->model->find($personId);

        $whatsapps = Whatsapp::whereModelId($person->id)->whereModelType(1)->whereWhatsapp($whatsapp)->first();

        return $whatsapp;
    }

    public function getEmailCustomerData($personId)
    {
        $customer = $this->model
        ->with(['emails' => function($query){
            $query->select('model_id', 'email', 'main')
            ->whereModelType(1);
        }])
        ->with(['whatsapps' => function($query){
            $query->select('model_id', 'whatsapp')
            ->whereModelType(1);
        }])
        ->with(['country' => function($query){
            $query->select('id', 'name');
        }])
        ->with(['region' => function($query){
            $query->select('id', 'name');
        }])
        ->select('id', 'first_name', 'last_name', 'country_id', 'region_id')
        ->find($personId);

        $formattedCustomer = [
            'firstName' => $customer->first_name,
            'lastName' => $customer->last_name,
            'country' => $customer->country->name,
            'region' => $customer->region->name,
            'emails' => [],
            'whatsapps' => []
        ];

        $emails = [];
        foreach ($customer->emails as $email) {
            $emails[] = $email->email;
        }
        $formattedCustomer['emails'] = $emails;

        $whatsapps = [];
        foreach ($customer->whatsapps as $whatsapp) {
            $whatsapps[] = $whatsapp->whatsapp;
        }
        $formattedCustomer['whatsapps'] = $whatsapps;

        return $formattedCustomer;
    }

    public function getMainEmail($personId)
    {
        $person = $this->model
        ->with(['emails' => function($query){
                    $query->select('model_id', 'email', 'main')
                    ->whereMain(true)
                    ->whereModelType(1);
                }])
        ->select('id')
        ->find($personId);

        return $person->emails[0]->email;
    }
}