<?php

namespace App\Repositories;

use App\Entities\Currency;

class CurrencyRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Currency;
    }

    public function all()
    {
        return $currencies = $this->model
        ->with(['exchange_rate' => function($query){
            $query->whereActive(true);
        }])
        ->get();
    }
}