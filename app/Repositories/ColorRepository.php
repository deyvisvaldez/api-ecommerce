<?php

namespace App\Repositories;

use App\Entities\Color;

class ColorRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Color;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function allOrder()
    {
        return $this->model->orderBy('order')->get();
    }

    public function findByName($name)
    {
        return $this->model->whereName($name)->first();
    }

    public function findExistence($name, $sizeId)
    {
        return $this->model->whereName($name)->where('id', '!=', $sizeId)->first();
    }
}
