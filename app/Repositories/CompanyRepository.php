<?php

namespace App\Repositories;

use App\Entities\Company;
use App\Entities\Person;
use App\Entities\User;
use App\Entities\Whatsapp;
use App\Entities\Email;

class CompanyRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Company;
    }

    public function first()
    {
        return $this->model
        ->with('addresses')
        ->with('whatsapps')
        ->with('cellphones')
        ->with('phones')
        ->with('emails')
        ->with('address')
        ->with('whatsapp')
        ->with('cellphone')
        ->with('phone')
        ->with('email')
        ->with('photos')
        ->with('users.person')
        /*->whereHas('users', function ($query) {
            $query->whereActive(true);
        })*/
        ->first();
    }

    public function findByIdentity($companyRegistration)
    {
        $company = $this->model->whereCompanyRegistration($companyRegistration)->first();
        return $company;
    }

    public function completeData($companyId)
    {
        return $this->model->with('email')->with('address')->with('whatsapp')->find($companyId);
    }

    public function getEmailData($companyId)
    {
        $company = $this->model
        ->with(['manager' => function($query){
            $query->select('first_name', 'last_name', 'company_id');
        }])
        ->with(['whatsapps' => function($query){
            $query->select('model_id', 'whatsapp')
            ->whereModelType(2);
        }])
        ->with(['phones' => function($query){
            $query->select('model_id', 'phone')
            ->whereModelType(2);
        }])
        ->with(['emails' => function($query){
            $query->select('model_id', 'email', 'main')
            ->whereModelType(2);
        }])
        ->with(['addresses' => function($query){
            $query->select('model_id', 'address')
            ->whereModelType(2);
        }])
        ->with(['billing_cards' => function($query){
            $query->select('account_name', 'account_number', 'bank_image_thumb', 'company_id');
        }])
        ->select('id', 'business_name', 'logotype', 'company_registration')
        ->find($companyId);

        $formattedCompany = [
            'name' => $company->business_name,
            'ruc' => $company->company_registration,
            'logotype' => $company->logotype,
            'addresses' => [],
            'emails' => [],
            'whatsapps' => [],
            'phones' => [],
            'billing_cards' => $company->billing_cards
        ];

        $whatsapps = [];
        foreach ($company->whatsapps as $whatsapp) {
            $whatsapps[] = $whatsapp->whatsapp;
        }
        $formattedCompany['whatsapps'] = $whatsapps;

        $phones = [];
        foreach ($company->phones as $phone) {
            $phones[] = $phone->phone;
        }
        $formattedCompany['phones'] = $phones;

        $emails = [];
        foreach ($company->emails as $email) {
            $emails[] = $email->email;
        }
        $formattedCompany['emails'] = $emails;

        $addresses = [];
        foreach ($company->addresses as $address) {
            $addresses[] = $address->address;
        }
        $formattedCompany['addresses'] = $addresses;

        return $formattedCompany;
    }

    public function getMainEmail($companyId)
    {
        $company = $this->model
        ->with(['emails' => function($query){
                    $query->select('model_id', 'email', 'main')
                    ->whereMain(true)
                    ->whereModelType(2);
                }])
        ->select('id')
        ->find($companyId);

        return $company->emails[0]->email;
    }

    public function getMainPhone($companyId)
    {
        $company = $this->model
        ->with(['phones' => function($query){
                    $query->select('model_id', 'phone', 'main')
                    ->whereMain(true)
                    ->whereModelType(2);
                }])
        ->select('id')
        ->find($companyId);

        return $company->phones[0]->phone;
    }

    public function findListId($companyId)
    {
        $company = $this->model->select(['mailchimp_list_id'])->find($companyId);
        if ($company) {
            return $company->mailchimp_list_id;
        }
        return false;
    }

    public function photos()
    {
        return $this->model->with('photos')->first();
    }
}