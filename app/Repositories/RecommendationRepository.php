<?php

namespace App\Repositories;

use App\Entities\Recommendation;

class RecommendationRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Recommendation;
    }

    public function itemRecommendations($itemId)
    {
        return $this->model->whereItemId($itemId)->get();
    }
}
