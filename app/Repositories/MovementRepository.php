<?php

namespace App\Repositories;

use App\Entities\Movement;

class MovementRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Movement;
    }

    public function all()
    {
        return $this->model->all();
    }
}