<?php

namespace App\Repositories;

use App\Entities\Country;

class CountryRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Country;
    }

    public function all()
    {
        return $this->model->with('regions')->get();
    }
}