<?php

namespace App\Repositories;

use App\Entities\Content;

class ImageRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Content;
    }

    public function findByUuid($uuid)
    {
        return $this->model->whereUuid($uuid)->get();
    }
}