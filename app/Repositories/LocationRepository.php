<?php

namespace App\Repositories;

use App\Entities\Location;

class LocationRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Location;
    }

    public function all()
    {
        return $this->model->all();
    }
}