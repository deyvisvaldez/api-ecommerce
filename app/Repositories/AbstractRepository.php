<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

abstract class AbstractRepository
{
    const PAGINATE = true;

    //public $filters = [];

    protected $model;

    public function __construct()
    {
        $this->model = $this->getModel();
    }

    abstract public function getModel();

    public function newModel()
    {
        return $this->getModel();
    }

    public function findBySlug($slug)
    {
        $model = $this->model->whereSlug($slug)->firstOrFail();
        return $model;
    }

    public function lists()
    {
        return $this->model->select(['id', 'name'])->get();
    }



    public function find($id)
    {
        try {
            $model = $this->model->findOrFail($id);
            return $model;
        } catch (ModelNotFoundException $e) {
            throw new NotFoundHttpException;
        }
    }

    public function create(array $data)
    {
        $model = $this->model->create($data);
        return $model;
    }

    public function first()
    {
        $model = $this->model->first();
        return $model;
    }

    public function update($model, array $data)
    {
        $model = $model->update($data);
        return $model;
    }

    public function delete($model)
    {
        if (is_numeric($model))
        {
            $model = $this->find($model);
        }
        $model->delete();
        return $model;
    }
}
