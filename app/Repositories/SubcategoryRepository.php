<?php

namespace App\Repositories;

use App\Entities\Subcategory;

class SubcategoryRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Subcategory;
    }

    public function adminFind($subcategoryId)
    {
        return $this->model->with('categories')->find($subcategoryId);
    }

    public function all()
    {
        return $this->model->get();
    }

    public function findBySlug($slug)
    {
        return $this->model->whereSlug($slug)->first();
    }
}
