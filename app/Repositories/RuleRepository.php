<?php

namespace App\Repositories;

use App\Entities\Rule;

class RuleRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Rule;
    }

    public function all()
    {
        return $this->model->all();
    }
}
