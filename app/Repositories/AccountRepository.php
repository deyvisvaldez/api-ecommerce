<?php

namespace App\Repositories;

use App\Entities\Account;

class AccountRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Account;
    }

    public function all()
    {
        return $this->model
        ->with('bank')
        ->with('country')
        ->with('region')
        ->with('currency')
        //->wherePublished(true)
        /*->whereHas('bank', function ($query) {
            $query->whereType(2);
        })*/
        //->whereBankAccount(true)
        ->orderBy('bank_id')
        ->get();
    }

    public function landing()
    {
        return $this->model
        ->with('bank')
        ->with('country')
        ->with('region')
        ->with('currency')
        ->wherePublished(true)
        ->whereHas('bank', function ($query) {
            $query->where('type', '!=', 3);
        })
        ->orderBy('bank_id')
        ->get();
    }
}