<?php

namespace App\Repositories;

use App\Entities\Transport;

class TransportRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Transport;
    }

    public function all()
    {
        return $this->model->all();
    }
}
