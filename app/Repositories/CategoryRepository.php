<?php

namespace App\Repositories;

use App\Entities\Category;

class CategoryRepository extends AbstractRepository
{
    public function getModel()
    {
        return new Category;
    }

    public function publicMenu()
    {
        return $categories = $this->model
        ->with('subcategories')
        ->with(['items' => function ($query) {
            $query->with('presentations.photos')
            ->with('presentations.web_prices')
            /*->whereHas('presentations.web_prices', function ($query) {
                $query->whereAvailablePromotion(true)
                ->whereHas('price_rules', function($query){
                    $query->where('name', 'like', '%Precio Unitario%');
                });
            })*/
            ->inRandomOrder();
        }])
        ->wherePublished(true)
        ->whereType(1)
        ->whereHas('items.presentations')
        ->get();
    }

    /**
     * Wiltex
     */

    public function all()
    {
        return $this->model->with('subcategories')->get();
    }

    public function adminMenu()
    {
        return $this->model
        ->with(['subcategories' => function($query){
            $query->orderBy('category_subcategory.order', 'asc');
        }])
        ->whereType(1)
        ->orderBy('order', 'asc')
        ->get();
    }

    public function findBySlug($slug)
    {
        return $this->model->whereSlug($slug)->first();
    }

    public function catalog()
    {
        return $categories = $this->model
        ->with(['items' => function ($query) {
            $query->with(['inventories' => function ($query) {
                $query->with(['photos' => function ($query) {
                    $query->select(['id', 'model_id', 'resource', 'resource_thumb'])
                    ->inRandomOrder();
                }])
                ->with(['category' => function ($query) {
                    $query->select(['id', 'slug']);
                }])
                ->with(['subcategory' => function ($query) {
                    $query->select(['id', 'name', 'slug']);
                }])
                ->with(['color' => function ($query) {
                    $query->select(['id', 'name', 'color_code']);
                }])
                ->with(['prices' => function ($query) {
                    $query->select(['id', 'inventory_id', 'name', 'price', 'available_promotion', 'promotional_price']);
                }])
                ->select(['inventories.id', 'item_id', 'category_id', 'subcategory_id', 'color_id'])
                ->wherePublished(true)
                //->whereLocationId(2) //Sólo en demo.wiltex.la
                ->inRandomOrder();
            }])
            ->select(['id', 'company_id', 'name', 'slug', 'business_code', 'published', 'category_id'])
            ->whereHas('inventories', function ($query) {
                $query->wherePublished(true);
                //->whereLocationId(2); //Sólo en demo.wiltex.la
            })
            ->get();
        }])
        ->select(['id', 'name', 'slug', 'description', 'image'])
        ->whereHas('items.inventories', function ($query) {
            $query->wherePublished(true);
            //->whereLocationId(2); //Sólo en demo.wiltex.la
        })
        //->whereIn('id', [2, 5])
        ->get();
}



    public function findSubcategory($categoryId, $subcategoryId)
    {
        return $this->model
        ->with(['subcategories' => function($query) use ($subcategoryId){
            $query->where('subcategories.id', $subcategoryId);
        }])
        ->find($categoryId);
    }

    public function getCompanyBusinessCategories($companyId)
    {
        $businessCategories = $this->model
        ->select(['id', 'name', 'slug', 'image'])
        ->wherePublished(true)
        ->whereCompanyId($companyId)
        ->get();

        return $businessCategories;
    }
}