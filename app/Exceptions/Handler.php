<?php

namespace App\Exceptions;

use App\Exceptions\DestinationNotReceivedException;
use App\Exceptions\PaymentNotApprovedException;
use App\Exceptions\PaymentTransactionFailedException;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $errorMessage = str_replace("\n", "", $exception->getMessage());

        if ($exception instanceof NotFoundHttpException) {
            return response()->view('index', [], 404);
        }

        if ($exception instanceof DestinationNotReceivedException) {
            return response()->json([
                'status' => 'error',
                'message' => $errorMessage
            ], 400);
        }

        if ($exception instanceof PaymentNotApprovedException) {
            return response()->json([
                'status' => 'error',
                'message' => $errorMessage
            ], 400);
        }

        if ($exception instanceof PaymentTransactionFailedException) {
            return response()->json([
                'status' => 'error',
                'message' => $errorMessage
            ], 500);
        }

        if ($exception instanceof NoAvailableStockException) {
            return response()->json([
                'status' => 'error',
                'message' => $errorMessage
            ], 500);
        }

        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('login');
    }
}
