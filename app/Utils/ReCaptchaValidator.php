<?php

namespace App\Utils;

use Request;

class ReCaptchaValidator
{
    public function validate($gRecaptchaResponse)
    {
        $secretKey = env('RECAPTCHA_SECRET');
        $remoteIp = Request::ip();

        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secretKey&response=$gRecaptchaResponse&remoteip=$remoteIp");

        $aux = json_decode($response, true);

        if ($aux['success']) {
            return true;
        }

        return false;
    }
}