<?php

namespace App\Utils;

use \DrewM\MailChimp\MailChimp;

class MailchimpNewsletter
{
    protected $mailchimp;

    public function __construct()
    {
        $this->mailchimp = new MailChimp(env('MAILCHIMP_API_KEY'));
    }

    public function createList($data)
    {
        $json = [
            'name' => 'Lista de '.$data['legal_name'],
            'contact' => [
                'company' => $data['legal_name'],
                'address1' => '',
                'address2' => '',
                'city' => '',
                'state' => '',
                'zip' => '',
                'country' => '',
                'phone' => $data['phone']
            ],
            'permission_reminder' => 'Usted esta recibiendo este email porque esta suscrito a la lista de '.$data['legal_name'],
            'campaign_defaults' => [
                'from_name' => $data['representative'],
                'from_email' => $data['email'],
                'subject' => 'Newsletter',
                'language' => "es",
            ],
            'email_type_option' => true
        ];

        $result = $this->mailchimp->post('lists', $json);

        return $result;
    }

    public function addEmailToList($data, $listId, $confirmation)
    {
        $subscriberHash = $this->mailchimp->subscriberHash($data['email']);
        $url = "/lists/$listId/members/$subscriberHash";
        $member = $this->mailchimp->get($url);

        if ( $member['status'] == 404 || $member['status'] == 'pending') {
            $this->mailchimp->post("/lists/$listId/members", [
                'email_address' => $data['email'],
                'status'        => 'subscribed',
                'merge_fields' => [
                    'FNAME' => $data['firstname'],
                    'LNAME' => $data['lastname']
                ]
            ]);

            return true;
        }

        return false;

        /*try {
            $this->mailchimp
                ->lists
                ->subscribe(
                    $this->listId,
                    ['email' => $email]
                );
        } catch (\Mailchimp_List_AlreadySubscribed $e) {
            // do something
        } catch (\Mailchimp_Error $e) {
            // do something
        }*/
    }
}