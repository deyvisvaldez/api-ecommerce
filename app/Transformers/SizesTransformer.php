<?php

namespace App\Transformers;

class SizesTransformer extends AbstractTransformer
{
    public function transform($sizes)
    {
        $formattedSizes = [];

        foreach ($sizes as $key => $size) {
            $s = [
                'id' => (int) $size->id,
                'name' => $size->name,
                'isSelected' => false,
                'order' => (int) $size->order
            ];

            $formattedSizes[] = $s;
        }

        return [
            'data' => $formattedSizes
        ];
    }
}