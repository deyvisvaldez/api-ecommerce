<?php

namespace App\Transformers;

class OrderReportTransformer
{
    public function transform($order, $person)
    {
        //return $order;
        $formattedPerson = [
            'cellphone' => ($person->whatsapp) ? $person->whatsapp->whatsapp : '',
            'email' => ($person->email) ? $person->email->email : '',
            'name' => $person->first_name.' '.$person->last_name,
            'region' => $person->region->name,
            'address' => ($person->address) ? $person->address->address : ''
        ];

        switch ($order->order_type) {
            case 1:
                $operation = 'Venta';
                break;

            case 2:
                $operation = 'Pedido';
                break;

            case 3:
                $operation = 'Cotización';
                break;
        }

        switch ($order->status) {
            case 1:
                $status = 'Pendiente';
                break;

            case 2:
                $status = 'Confirmado';
                break;

            case 3:
                $status = 'Cancelado';
                break;
        }

        switch ($order->shipping_id) {
            case 1:
                $shipping = 'Recoger en tienda';
                break;

            case 2:
                $shipping = 'Envio a domicilio';
                break;

            default:
                $shipping = 'Método no seleccionado';
                break;
        }

        $formattedOrder = [
            'date' => $order->created_at->format('Y-m-d'),
            'code' => $order->code,
            'status' => $status,
            'operation' => $operation,
            'currencyId' => (int) $order->currency_id,
            'shipping' => $shipping
        ];

        $items = [];
        $presentations = [];

        foreach ($order->presentations as $key => $presentation) {
            $items[] = [
                'id' => (int) $presentation->item->id,
                'name' => $presentation->item->name,
                //'description' => $presentation->item->description,
                'inventories' => []
            ];


            $presentations[] = [
                'itemId' => (int) $presentation->item->id,
                'colorName' => $presentation->color->name,
                'sizeName' => $presentation->size->name,
                'price' => (float) $presentation->pivot->price,
                'quantity' => (int) $presentation->pivot->quantity,
                //'currencyId' => (int) $presentation->web_prices[0]->currency_id
                'currencyId' => 1
                //'subtotal' => (float) $presentation->pivot->price * (int) $presentation->pivot->quantity
            ];
        }

        $items = array_values(array_unique($items, SORT_REGULAR));

        foreach ($items as $key => &$item) {
            foreach ($presentations as $key => $presentation) {
                if ($item['id'] === $presentation['itemId']) {
                    $item['inventories'][] = $presentation;
                }
            }
        }

        $formattedItems = $items;

        return $data = [
            'order' => $formattedOrder,
            'person' => $formattedPerson,
            'items' => $formattedItems
        ];
    }
}