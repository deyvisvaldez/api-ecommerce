<?php

namespace App\Transformers;

class RegionTransformer extends AbstractTransformer
{
    public function transform($region)
    {
        $formattedRegion = [
            'id' => (int) $region->id,
            'countryId' => (int) $region->country->id,
            'name' => $region->name
        ];


        return [
            'data' => $formattedRegion
        ];
    }
}