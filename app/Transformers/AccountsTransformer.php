<?php

namespace App\Transformers;

class AccountsTransformer extends AbstractTransformer
{
    public function transform($accounts)
    {
        $formattedAccounts = [];

        foreach ($accounts as $key => $account) {

            if ($account->bank->type = 2) {
                $type = '';
            } else {
                $type = ($account->account_type == '1') ? 'Cuenta de Ahorros en '.$account->currency->name : 'Cuenta de Crédito '.$account->currency->name;
            }
            $region = ($account->region) ? $account->region->name : '';
            $country = ($account->country) ? $account->country->name : '';

            $a = [
                'id' => (int) $account->id,
                'logoUrl' => $account->bank->bank_image_thumb,
                'currencyId' => (int) $account->currency_id,
                'type' => $type,
                'propietary' => $account->account_holder,
                'propietaryDocument' => $account->document_holder,
                'number' => $account->account_number,
                'region' => $region.' '.$country
            ];

            $formattedAccounts[] = $a;
        }

        return [
            'data' => $formattedAccounts
        ];
    }
}