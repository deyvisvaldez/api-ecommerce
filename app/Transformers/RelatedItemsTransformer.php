<?php

namespace App\Transformers;

use App\Transformers\AbstractTransformer;

class RelatedItemsTransformer extends AbstractTransformer
{
    public function transform($items)
    {
        $formattedItems = [];

        foreach ($items as $key => $item) {

            if (count($item->presentations)) {
                $fmtItems = [
                    'id' => (int) $item->id,
                    'name' => $item->name,
                    'slug' => $item->slug,
                    'price' => (float) $item->presentations[0]->random_price->price,
                    'firstImage' => asset('defaults/item.png'),
                    'secondImage' => '',
                    'promotion' => [
                        'flag' => false,
                        'price' => 0
                    ],
                    'categorySlug' => $item->category->slug,
                    'subcategoryName' => $item->subcategory->name,
                    'subcategorySlug' => $item->subcategory->slug,
                    'tags' => []
                ];

                $flag = true;

                foreach ($item->presentations as $key => $presentation) {

                    if ($flag) {
                        if (count($presentation->web_prices)) {
                            $fmtItems['promotion']['flag'] = true;
                            $fmtItems['promotion']['price'] = (float) $presentation->web_prices[0]->promotional_price;
                            $flag = false;
                        }
                    }

                    /*if ($presentation->color) {
                        $colors[] = [
                            'id' => (int) $presentation->color->id,
                            'name' => $presentation->color->name,
                            'code' => $presentation->color->color_code,
                            'mainImage' => '',
                            'secondaryImage' => '',
                            'itemId' => (int) $item->id
                        ];
                    }*/

                    /*if (count($presentation->photos)) {
                        foreach ($presentation->photos as $key => $photo) {
                            $photos[] = [
                                'colorId' => (int) $presentation->color->id,
                                'image' => $photo->resource_thumb,
                                'itemId' => (int) $item->id
                            ];
                        }
                    }*/
                }

                $photos = [];
                foreach ($item->presentations as $key => $presentation) {
                    foreach ($presentation->random_photos as $key => $photo) {
                        $photos[] = $photo->resource_thumb;
                    }
                }
                $photos = array_values(array_unique($photos, SORT_REGULAR));

                $count = count($photos);
                if ($count) {
                    $fmtItems['firstImage'] = $photos[0];
                    if ($count > 1) {
                        $fmtItems['secondImage'] = $photos[1];
                    }
                }

                $formattedItems[] = $fmtItems;
            }
        }

        return [
            'data' => $formattedItems
        ];
    }
}

