<?php

namespace App\Transformers;

use App\Transformers\AbstractTransformer;

class WebCatalogTransformer extends AbstractTransformer
{
    public function transform($paginateData)
    {
        //return $paginateData;
        $paginateData = $paginateData->toArray();
        $items = $paginateData['data'];

        $formattedItems = [];

        foreach ($items as $key => $item) {

            if (count($item['presentations'])) {
                $fmtItems = [
                    'id' => (int) $item['id'],
                    'name' => $item['name'],
                    'slug' => $item['slug'],
                    'price' => (float) $item['referencial_price'],
                    'firstImage' => asset('defaults/item.png'),
                    'secondImage' => '',
                    'promotion' => [
                        'flag' => (boolean) $item['available_promotion'],
                        'price' => (float) $item['promotional_price']
                    ],
                    'categorySlug' => $item['category']['slug'],
                    'subcategoryName' => $item['subcategory']['name'],
                    'subcategorySlug' => $item['subcategory']['slug'],
                ];

                $flag = true;

                $photos = [];
                foreach ($item['presentations'] as $key => $presentation) {
                    foreach ($presentation['photos'] as $key => $photo) {
                        $photos[] = $photo['resource_thumb'];
                    }
                }
                $photos = array_values(array_unique($photos, SORT_REGULAR));

                $count = count($photos);
                if ($count) {
                    $fmtItems['firstImage'] = $photos[0];
                    if ($count > 1) {
                        $fmtItems['secondImage'] = $photos[1];
                    }
                }

                $formattedItems[] = $fmtItems;
            }
        }

        $paginateData['data'] = $formattedItems;

        return $paginateData;
    }
}

