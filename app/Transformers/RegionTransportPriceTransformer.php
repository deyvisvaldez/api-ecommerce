<?php

namespace App\Transformers;

class RegionTransportPriceTransformer
{
    public function transform($regions, $rules)
    {
        //return $regions;
        $formattedRules = [];

        foreach ($rules as $key => $rule) {
            $r = [
                'id' => (int) $rule->id,
                'quantity' => $rule->unit_value
            ];
            $formattedRules[] = $r;
        }

        $formattedRegions = [];

        foreach ($regions as $key => $region) {
            $rg = [
                'id' => (int) $region->id,
                'name' => $region->name,
                'prices' => []
            ];

            /*foreach ($region->costs as $key => $cost) {
                $c = [
                    'price' => $cost->cost,
                    'id' => $cost->id,
                    'regionTransportId' => $cost->region_transport_id,
                    'unityId' => $cost->rule->id
                ];
                $rg['prices'][] = $c;
            }*/
            if ($region->branch_office) {
                foreach ($rules as $key => $rule) {
                    $flag = true;
                    foreach ($region->branch_office->costs as $key => $cost) {
                        if ($cost->rule->id === $rule->id) {
                            $c = [
                                'price' => (float) $cost->cost,
                                'id' => (int) $cost->id,
                                'unityId' => (int) $cost->rule->id
                            ];
                            $rg['prices'][] = $c;
                            $flag = false;
                            break;
                        }
                    }

                    if ($flag) {
                        $c = [
                            'price' => 0,
                            'id' => 0,
                            'unityId' => (int) $rule->id
                        ];
                        $rg['prices'][] = $c;
                    }
                }
            } else {
                foreach ($rules as $key => $rule) {
                    $c = [
                        'price' => 0,
                        'id' => 0,
                        'unityId' => (int) $rule->id
                    ];
                    $rg['prices'][] = $c;
                }
            }

            $formattedRegions[] = $rg;
        }

        return [
            'data' => [
                'rules' => $formattedRules,
                'regions' => $formattedRegions,
            ]
        ];
    }
}
