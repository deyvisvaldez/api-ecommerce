<?php

namespace App\Transformers;

use App\Entities\Presentation;

class DistributorCatalogTransformer extends AbstractTransformer
{
    public function transform($items)
    {
        //return $items;
        $formattedItems = [];

        $rawItems = [];
        $photos = [];
        $colors = [];

        foreach ($items as $key => $item) {
            $presentation = Presentation::with('random_photos')->whereItemId($item->id)->whereColorId($item->color_id)->first();
            //return $presentation;

            $rawItems[] = [
                'id' => (int) $item->id,
                'name' => $item->name,
                'slug' => $item->slug,
                'currencyId' => (int) $item->currency_id,
                'colorId' => (int) $item->color_id,
                'primaryImage' => count($presentation->random_photos) ? $presentation->random_photos[0]->resource_thumb : asset('defaults/product.png'),
                'secondaryImage' => array_key_exists(1, $presentation->random_photos) ? $presentation->random_photos[1]->resource_thumb : ''
            ];

            /*$photos[] = [
                'image' => $item->resource_thumb,
                'colorId' => (int) $item->color_id,
                'itemId' => (int) $item->id
            ];*/

            $colors[] = [
                'name' => $item->color,
                'code' => $item->color_code,
                'id' => (int) $item->color_id
            ];
        }

        $rawItems = array_values(array_unique($rawItems, SORT_REGULAR));
        $photos = array_values(array_unique($photos, SORT_REGULAR));
        $colors = array_values(array_unique($colors, SORT_REGULAR));

        /*$formattedItems['items'] = $rawItems;
        $formattedItems['photos'] = $photos;*/

        foreach ($rawItems as $key => &$rawItem) {

            $fmtItem = [
                'name' => $rawItem['name'],
                'slug' => $rawItem['slug'],
                'currencyId' => (int) $rawItem['currencyId'],
                'colorId' => (int) $rawItem['colorId'],
                'primaryImage' => $rawItem['primaryImage'],
                'secondaryImage' => $rawItem['secondaryImage']
            ];

            $flag = true;
            $count = 0;

            /*foreach ($photos as $key => &$photo) {
                if ($rawItem['colorId'] === $photo['colorId'] && $rawItem['id'] === $photo['itemId']) {
                    if ($count === 0) {
                        $fmtItem['primaryImage'] = $photo['image'];
                        $count++;
                        $flag = false;
                    }
                    if ($count === 1) {
                        $fmtItem['secondaryImage'] = $photo['image'];
                        break;
                    }
                }
            }

            if ($flag) {
                $fmtItem['primaryImage'] = asset('defaults/product.png');
            }*/

            $formattedItems[] = $fmtItem;
        }

        return [
            'data' => [
                'items' => $formattedItems,
                'colors' => $colors
            ]
        ];

    }
}

