<?php

namespace App\Transformers;

class PhotoTransformer
{
    public function transform($photo)
    {
        $p = [
            'base64' => $photo->resource_thumb,
            'extension' => '',
            'title' => $photo->title,
            'description' => $photo->description
        ];

        return [
            'data' => $p
        ];
    }
}