<?php

namespace App\Transformers;

class PresentationPhotosTransformer extends AbstractTransformer
{
    public function transform($images)
    {
        //return $images;
        $formattedImages = [];

        foreach ($images as $key => $image) {
            $i = [
                'id' => $image->uuid,
                'url' => $image->resource_thumb,
                'success' => true
            ];

            $formattedImages[] = $i;
        }

        return [
            'data' => $formattedImages
        ];
    }
}
