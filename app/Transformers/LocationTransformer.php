<?php

namespace App\Transformers;

class LocationTransformer extends AbstractTransformer
{
    public function transform($location)
    {
        $formattedLocation = [
            'id' => (int) $location->id,
            'name' => $location->name,
            'type' => (int) $location->type,
            'main' => (boolean) $location->main
        ];

        return [
            'data' => $formattedLocation
        ];
    }
}