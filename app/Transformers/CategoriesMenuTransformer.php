<?php

namespace App\Transformers;

use App\Entities\Item;

class CategoriesMenuTransformer extends AbstractTransformer
{
    public function transform($categories)
    {
        //return $categories;
        $formattedCategories = [];

        foreach ($categories as $key => $category) {
            $bc = [
                'id' => $category->id,
                'name' => $category->name,
                'imageUrl' => $category->image,
                'slug' => $category->slug,
                'subcategories' => [],
                'item' => []
            ];

            foreach ($category->subcategories as $key => $subcategory) {
                $c = [
                    'id' => $subcategory->id,
                    'slug' => $subcategory->slug,
                    'name' => $subcategory->name,
                ];

                $bc['subcategories'][] = $c;
            }

            /*if (count($category->items)) {
                $item = [
                    'name' => $category->items[0]->name,
                    'slug' => $category->items[0]->slug,
                    'imageUrl' => asset('defaults/product.png'),
                    'price' => 0
                ];

                foreach ($category->items[0]->inventories as $key => $inventory) {
                    if ($inventory->web_price->available_promotion) {
                        $item['price'] = $inventory->web_price->promotional_price;
                        if (count($inventory->photos)) {
                            $item['imageUrl'] = $inventory->photos[0]->resource;
                        }
                    }
                }
            } else {
                $rawItem = Item::with(['inventories' => function ($query) {
                    $query->with('web_price')
                    ->with('photos')
                    ->inRandomOrder();
                }])
                ->has('inventories.photos')
                ->whereCategoryId($category->id)
                ->inRandomOrder()
                ->first();

                $item = [
                    'name' => $rawItem->name,
                    'slug' => $rawItem->slug,
                    'imageUrl' => (count($rawItem->inventories[0]->photos)) ? $rawItem->inventories[0]->photos[0]->resource : asset('defaults/product.png'),
                    'price' => $rawItem->inventories[0]->web_price->price,
                ];
            }*/


            //$bc['item'] = $item;

            $formattedCategories[] = $bc;
        }

        return [
            'data' => $formattedCategories
        ];
    }
}