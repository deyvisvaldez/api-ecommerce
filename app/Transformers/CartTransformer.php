<?php

namespace App\Transformers;

class CartTransformer extends AbstractTransformer
{
    public function transform($inventories)
    {
        $formattedInventories = [];

        foreach ($inventories as $key => $inventory) {

            $inventory = [
                'inventoryId' => (int) $inventory->id,
                'itemId' => (int) $inventory->item->id,
                'itemName' => $inventory->item->name,
                'itemSlug' => $inventory->item->slug,
                'imageUrl' => (count($inventory->photos)) ? $inventory->photos[0]->resource_thumb : asset('defaults/product.png'),
                'colorCode' => $inventory->color->color_code,
                'colorName' => $inventory->color->name,
                'size' => [
                    'size' => $inventory->size->name,
                    'price' => (float) $inventory->web_price->price,
                    'currencyId' => (int) $inventory->web_price->currency_id,
                    'promotion' => [
                        'flag' => (boolean) $inventory->web_price->available_promotion,
                        'price' => ($inventory->web_price->available_promotion) ? (float) $inventory->shop_price->promotional_price : 0
                    ]
                ]
            ];

            $formattedInventories[] = $inventory;
        }

        return $formattedInventories;
    }
}