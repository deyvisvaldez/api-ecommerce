<?php

namespace App\Transformers;

class CurrenciesTransformer extends AbstractTransformer
{
    public function transform($currencies)
    {
        $formattedCurrencies = [];
        foreach ($currencies as $currency) {
            $formatted = [
                'id' => (int) $currency->id,
                'code' => $currency->code,
                'symbol' => $currency->symbol,
                'name' => $currency->name,
                'default' => (boolean) $currency->default_currency,
                'exchangeRate' => (float) $currency->exchange_rate->sales_exchange,
                'decimal' => (boolean) $currency->decimal
            ];

            $formattedCurrencies[] = $formatted;
        }

        return [
            'data' => $formattedCurrencies
        ];
    }
}