<?php

namespace App\Transformers;

class CompanyVideosTransformer
{
    public function transform($videos)
    {
        $p = [];
        foreach ($videos as $key => $video) {
            $vid = explode('=', $video->url);

            $p[] = [
                'id' => (int) $video->id,
                'title' => $video->title,
                'urlId' => $vid[1],
                'status' => (boolean) $video->published
            ];
        }

        return [
            'data' => $p
        ];
    }
}