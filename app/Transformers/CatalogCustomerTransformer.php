<?php

namespace App\Transformers;

class CatalogCustomerTransformer extends AbstractTransformer
{
    public function transform($person)
    {
        $formattedPerson = [
            'firstName' => $person->first_name,
            'lastName' => $person->last_name,
            'email' => ($person->email) ? $person->email->email : null,
            'whatsapp' => ($person->whatsapp) ? $person->whatsapp->whatsapp : null,
            'address' => ($person->address) ? $person->address->address : null,
            'countryId' => $person->country_id,
            'regionId' => $person->region_id
        ];

        return [
            'data' => $formattedPerson
        ];
    }
}