<?php

namespace App\Transformers;

class RegionDestinationsTransformer extends AbstractTransformer
{
    public function transform($region)
    {
        //return $region;
        $formattedDestinartions = [];

        if (count($region->branch_offices)) {
            foreach ($region->branch_offices as $key => $branch_office) {
                $d = [
                    'id' => (int) $branch_office->id,
                    'transportId' => (int) $branch_office->transport_id,
                    'address' => $branch_office->address,
                    'duration' => (int) $branch_office->duration
                ];
                $formattedDestinartions[] = $d;
            }
        }

        return [
            'data' => $formattedDestinartions
        ];
    }
}