<?php

namespace App\Transformers;

class AccountTransformer extends AbstractTransformer
{
    public function transform($account)
    {
        $formattedAccounts = [
            'accountNumber' => $account->account_number,
            'bankId' => $account->bank_id,
            'accountHolder' => $account->account_holder,
            'documentHolder' => $account->document_holder,
            'accountType' => $account->account_type,
            'currencyId' => $account->currency_id,
            'description' => $account->description,
            'countryId' => $account->country_id,
            'regionId' => $account->region_id,
            'address' => $account->address,
            'email' => $account->email
        ];

        return [
            'data' => $formattedAccounts
        ];
    }
}