<?php

namespace App\Transformers;

class ModalPresentationsTransformer
{
    public function transform($presentations, $priceType)
    {
        //return $presentations;
        $formattedPresentations = [];

        $pres = [];
        $colors = [];
        $sizes = [];
        $rules = [];

        foreach ($presentations as $key => $presentation) {

            $colors[] = [
                'id' => (int) $presentation->color_id,
                'name' => $presentation->color->name,
            ];

            $sizes[] = [
                'id' => (int) $presentation->size_id,
                'name' => $presentation->size->name,
            ];

            $pre = [
                'color' => $presentation->color->name,
                'colorId' => (int) $presentation->color_id,
                'size' => $presentation->size->name,
                'sizeId' => (int) $presentation->size_id,
                'available' => (boolean) $presentation->available,
                'prices' => []
            ];

            switch ($priceType) {
                case 1:
                    foreach ($presentation->web_prices as $key => $web_price) {
                        $pri = [
                            'id' => (int) $web_price->id,
                            'price' => (float) $web_price->price,
                            'promotionalPrice' => (float) $web_price->promotional_price,
                            'availablePromotion' => (boolean) $web_price->available_promotion,
                            'rule' => (int) $web_price->price_rules[0]->quantity
                        ];

                        $rules[] = [
                            'id' => (int) $web_price->price_rules[0]->id,
                            'name' => $web_price->price_rules[0]->name,
                            'quantity' => (int) $web_price->price_rules[0]->quantity,
                        ];

                        $pre['prices'][] = $pri;
                    }

                    break;

                case 2:
                    foreach ($presentation->shop_prices as $key => $shop_price) {
                        $pri = [
                            'id' => (int) $shop_price->id,
                            'price' => (float) $shop_price->price,
                            'promotionalPrice' => (float) $shop_price->promotional_price,
                            'availablePromotion' => (boolean) $shop_price->available_promotion,
                            'rule' => (int) $shop_price->price_rules[0]->quantity
                        ];

                        $rules[] = [
                            'id' => (int) $shop_price->price_rules[0]->id,
                            'name' => $shop_price->price_rules[0]->name,
                            'quantity' => (int) $shop_price->price_rules[0]->quantity,
                        ];

                        $pre['prices'][] = $pri;
                    }

                    break;

                case 3:
                    foreach ($presentation->wholesale_prices as $key => $wholesale_price) {
                        $pri = [
                            'id' => (int) $wholesale_price->id,
                            'price' => (float) $wholesale_price->price,
                            'promotionalPrice' => (float) $wholesale_price->promotional_price,
                            'availablePromotion' => (boolean) $wholesale_price->available_promotion,
                            'rule' => (int) $wholesale_price->price_rules[0]->quantity
                        ];

                        $rules[] = [
                            'id' => (int) $wholesale_price->price_rules[0]->id,
                            'name' => $wholesale_price->price_rules[0]->name,
                            'quantity' => (int) $wholesale_price->price_rules[0]->quantity,
                        ];

                        $pre['prices'][] = $pri;
                    }

                    break;
            }
            $pres[] = $pre;
        }

        $colors = array_values(array_unique($colors, SORT_REGULAR));
        $sizes = array_values(array_unique($sizes, SORT_REGULAR));
        $rules = array_values(array_unique($rules, SORT_REGULAR));
        //$prices = array_values(array_unique($prices, SORT_REGULAR));

        return [
            'data' => [
                'inventories' => $pres,
                'colors' => $colors,
                'sizes' => $sizes,
                'rules' => $rules
            ]
        ];
    }
}