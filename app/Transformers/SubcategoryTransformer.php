<?php

namespace App\Transformers;

class SubcategoryTransformer extends AbstractTransformer
{
    public function transform($subcategory)
    {
        $formattedSubcategory = [
            'id' => (int) $subcategory->id,
            'description' => $subcategory->description,
            'name' => $subcategory->name,
            'slug' => $subcategory->slug
        ];

        return [
            'data' => $formattedSubcategory
        ];
    }
}