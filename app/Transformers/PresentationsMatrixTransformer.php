<?php

namespace App\Transformers;

class PresentationsMatrixTransformer
{
    public function transform($items, $allSizes, $locations = false)
    {
        // Convetir en Array para facilitar manipulación de la paginación
        $data = $items->toArray();
        $items = $data['data'];

        //return $items;
        $formattedInventories = [];
        foreach ($items as $key => $item) {

            $photos = [];
            $colors = [];
            $sizes = [];

            foreach ($item['presentations'] as $key => $presentation) {

                if (count($presentation['inventories'])) {
                    if (count($presentation['photos'])) {
                        $photos[] = [
                            'imageUrl' => $presentation['photos'][0]['resource_thumb'],
                            'colorId' => (int) $presentation['color']['id']
                        ];
                    }

                    //if ($presentation['color']) {
                        $colors[] = [
                            'id' => (int) $presentation['color']['id'],
                            'name' => $presentation['color']['name'],
                            'code' => $presentation['color']['color_code'],
                        ];
                    //}

                    //if ($presentation['size']) {
                        if ($locations) {
                            $stock = 0;
                            foreach ($presentation['inventories'] as $key => $inventory) {
                                $stock = $stock + (int) $inventory['available_stock'];
                            }
                        } else {
                            $stock = (int) $presentation['inventories'][0]['available_stock'];
                        }

                        $sizes[] = [
                            'sizeId' => (int) $presentation['size']['id'],
                            'size' => $presentation['size']['name'],
                            'presentationId' => (int) $presentation['id'],
                            //'stock' => (int) $presentation['inventories'][0]['available_stock'],
                            'stock' => $stock,
                            'colorId' => ($presentation['color']) ? (int) $presentation['color']['id'] : 0,
                            'promotion' => (float) $presentation['web_prices'][0]['available_promotion'],
                            'minStock' => (int) $presentation['inventories'][0]['min_stock'],
                            'published' => (float) $presentation['published'],
                            'order' => (int) $presentation['size']['order']
                        ];
                    //}
                }
            }

            $colors = array_values(array_unique($colors, SORT_REGULAR));

            foreach ($colors as $key => $color) {
                $c = [
                    'itemCode' => $item['business_code'],
                    'itemName' => $item['name'],
                    'itemSlug' => $item['slug'],
                    'itemId' => (int) $item['id'],
                    'id' => $color['id'],
                    'code' => $color['code'],
                    'name' => $color['name'],
                    'imageUrl' => asset('defaults/product.png'),
                    'inventories' => []
                ];

                foreach ($photos as $key => $photo) {
                    if ($photo['colorId'] === $color['id']) {
                        $c['imageUrl'] = $photo['imageUrl'];
                        break;
                    }
                }


                foreach ($allSizes as $key => $oneSize) {

                    $flag = true;

                    foreach ($sizes as $key => $size) {

                        if ($color['id'] === $size['colorId']) {

                            if ($oneSize['id'] === $size['sizeId']) {
                                $s = [
                                    'id' => $size['presentationId'],
                                    'colorId' => $size['colorId'],
                                    'stock' => $size['stock'],
                                    'sizeId' => $size['sizeId'],
                                    'state' => 'created',
                                    'isSelected' => false,
                                    'promotion' => $size['promotion'],
                                    'minStock' => $size['minStock'],
                                    'published' => $size['published'],
                                    'order' => $size['order']
                                ];
                                $c['inventories'][] = $s;

                                $flag = false;
                            }
                        }
                    }

                    if ($flag) {
                        $s = [
                            'id' => '',
                            'colorId' => $color['id'],
                            'stock' => 0,
                            'sizeId' => $oneSize['id'],
                            'state' => 'notCreated',
                            'barCode' => '',
                            'isSelected' => false,
                            'promotion' => false,
                            'published' => false,
                            'order' => $oneSize['order']
                        ];

                        $c['inventories'][] = $s;
                    }
                }

                $formattedInventories[] = $c;
            }
        }

        $data['data'] = [
                'itemColors' => $formattedInventories,
                'itemSizes' => $allSizes,
                'excelUrl' => ''
        ];
        return $data;
    }
}
