<?php

namespace App\Transformers;

class VideoTransformer
{
    public function transform($video)
    {
        $p = [
            'name' => $video->title,
            'url' => $video->url
        ];

        return [
            'data' => $p
        ];
    }
}