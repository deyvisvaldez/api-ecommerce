<?php

namespace App\Transformers;

class CompanyPhotosTransformer
{
    public function transform($photos)
    {
        $p = [];
        foreach ($photos as $key => $photo) {
            $p[] = [
                'id' => (int) $photo->id,
                'title' => strip_tags($photo->title),
                'url' => $photo->resource_thumb,
                'status' => (boolean) $photo->published
            ];
        }

        return [
            'data' => $p
        ];
    }
}