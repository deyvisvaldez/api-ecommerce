<?php

namespace App\Transformers;

class WiltexCellphoneTransformer extends AbstractTransformer
{
    public function transform($cellphones)
    {
        $formattedCellphones = [];
        $main = 0;
        foreach ($cellphones as $key => $cellphone) {
            $e = [
                'id' => (int) $cellphone->id,
                'cellphone' => $cellphone->cellphone,
                'main' => (boolean) $cellphone->main
            ];

            $formattedCellphones[] = $e;

            if ($cellphone->main) {
                $main = (int) $cellphone->id;
            }
        }

        return [
            'data' => [
                'cellphones' => $formattedCellphones,
                'checked' => $main
            ]
        ];
    }
}