<?php

namespace App\Transformers;

class UserTransformer
{
    public function transform($person)
    {
        $user = [
            'identityDocument' => $person->identity_document,
            'firstName' => $person->first_name,
            'lastName' => $person->last_name,
            'gender' => (boolean) $person->gender,
            'birthdate' => $person->birthdate,
            'countryId' => (int) $person->country_id,
            'regionId' => (int) $person->region_id,
            'address' => ($person->address) ? $person->address->address : '',
            'cellphone' => ($person->cellphone) ? $person->cellphone->cellphone : '',
            'phone' => ($person->phone) ? $person->phone->phone : '',
            'whatsapp' => ($person->whatsapp) ? $person->whatsapp->whatsapp : '',
            'email' => ($person->email) ? $person->email->email : '',
            'role' =>  (int) $person->user->role,
            'password' => '',
            'avatar' => [
                'base64' => $person->avatar,
                'extension' => ''
            ]
        ];

        return [
            'data' => $user
        ];
    }
}
