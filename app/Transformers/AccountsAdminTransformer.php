<?php

namespace App\Transformers;

class AccountsAdminTransformer extends AbstractTransformer
{
    public function transform($accounts)
    {
        $formattedAccounts = [];

        foreach ($accounts as $key => $account) {

            $type = ($account->account_type == 1) ? 'Cuenta de Ahorros' : 'Cuenta de Crédito';

            $a = [
                'id' => (int) $account->id,
                'imageUrl' => ($account->bank) ? $account->bank->bank_image_thumb : asset('defaults/bank.png'),
                'currency' => ($account->currency) ? $account->currency->name : '',
                'type' => $type,
                'number' => $account->account_number,
                'published' => $account->published,
            ];

            $formattedAccounts[] = $a;
        }

        return [
            'data' => $formattedAccounts
        ];
    }
}