<?php

namespace App\Transformers;

class LocationsTransformer extends AbstractTransformer
{
    public function transform($locations)
    {
        $formattedLocations = [];

        foreach ($locations as $key => $location) {
            $s = [
                'id' => (int) $location->id,
                'name' => $location->name,
                'type' => $location->type,
                'imageUrl' => ($location->type == 3) ? asset('defaults/store.png') : asset('defaults/workshop.png'),
                'main' => $location->main
            ];

            $formattedLocations[] = $s;
        }

        return [
            'data' => $formattedLocations
        ];
    }
}