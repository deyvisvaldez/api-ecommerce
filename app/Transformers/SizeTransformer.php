<?php

namespace App\Transformers;

class SizeTransformer extends AbstractTransformer
{
    public function transform($size)
    {
        $formattedSize = [
            'name' => $size->name,
            'order' => (int) $size->order
        ];

        return [
            'data' => $formattedSize
        ];
    }
}