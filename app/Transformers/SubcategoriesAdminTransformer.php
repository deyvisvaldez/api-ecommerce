<?php

namespace App\Transformers;

class SubcategoriesAdminTransformer extends AbstractTransformer
{
    public function transform($subcategories)
    {
        $formattedSubcategories = [];

        foreach ($subcategories as $key => $subcategory) {
            $bc = [
                'id' => (int) $subcategory->id,
                'name' => $subcategory->name,
                'description' => $subcategory->description,
                'image' => [
                    'base64' => $subcategory->image_thumb,
                    'extension' => ''
                ]
            ];

            $formattedSubcategories[] = $bc;
        }

        return [
            'data' => $formattedSubcategories
        ];
    }
}