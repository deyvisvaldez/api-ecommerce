<?php

namespace App\Transformers;

class AdminItemsTransformer extends AbstractTransformer
{
    public function transform($data)
    {
        //return $data;
        $formattedItems = [];

        foreach ($data as $key => $item) {

            if (count($item->presentations)) {
                $images = [];
                foreach ($item->presentations as $key => $presentation) {
                    foreach ($presentation->photos as $key => $photo) {
                        $images[] = $photo->resource_thumb;
                    }
                }
                if (count($images)) {
                    $image = $images[0];
                } else {
                    $image = asset('defaults/product.png');
                }
            } else {
                $image = asset('defaults/product.png');
            }

            $fmtItems = [
                'id' => (int) $item->id,
                'slug' => $item->slug,
                'name' => $item->name,
                'imageUrl' => $image,
                'published' => (boolean) $item->published,
                'distributorPublished' => $item->distributor_published,
                'code' => $item->business_code,
                'inventoriesFlag' => (count($item->presentations)) ? true : false
            ];

            $formattedItems[] = $fmtItems;
        }

        return [
            'data' => $formattedItems
        ];

    }
}

