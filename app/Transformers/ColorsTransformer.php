<?php

namespace App\Transformers;

class ColorsTransformer extends AbstractTransformer
{
    public function transform($colors)
    {
        $formattedColors = [];

        foreach ($colors as $key => $color) {
            $c = [
                'id' => (int) $color->id,
                'code' => $color->color_code,
                'name' => $color->name,
                'isSelected' => false,
                'order' => (int) $color->order
            ];

            $formattedColors[] = $c;
        }

        return [
            'data' => $formattedColors
        ];
    }
}