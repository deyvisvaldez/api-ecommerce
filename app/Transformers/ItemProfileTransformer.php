<?php

namespace App\Transformers;

class ItemProfileTransformer
{
    public function transform($item, $priceType)
    {
        //return $item;
        $formattedItem = [
            'id' => (int) $item->id,
            'name' => $item->name,
            'slug' => $item->slug,
            'subcategoryName' => $item->subcategory->name,
            'description' => $item->description,
            'features' => $item->features,
            'recommendations' => [],
            'features' => '',
            'promotion' => [
                'flag' => false,
                'colorId' => 0,
                'sizeId' => 0,
            ],
            'colors' => [],
            //'sizes' => [],
            //'prices' => [],
            //'attributes' => []
        ];

        foreach ($item->recommendations as $key => $recommendation) {
            $formattedItem['recommendations'][] = $recommendation->image;
        }

        $attributes = [];

        $sizes = [];
        $colors = [];
        $prices = [];
        $photos = [];
        $tags = [];

        $flag = true;

        foreach ($item->presentations as $key => $presentation) {

            if ($flag) {
                if ($presentation->web_prices[0]->available_promotion) {
                    $formattedItem['promotion']['flag'] = true;
                    $formattedItem['promotion']['colorId'] = (int) $presentation->color_id;
                    $formattedItem['promotion']['sizeId'] = (int) $presentation->size_id;
                    $flag = false;
                }
            }

            if (count($presentation->photos)) {
                foreach ($presentation->photos as $key => $photo) {
                    $photos[] = [
                        'url' => $photo->resource,
                        'urlThumb' => $photo->resource_thumb,
                        'colorId' => (int) $presentation->color->id
                    ];
                }
            }

            if ($presentation->color) {
                $colors[] = [
                    'id' => (int) $presentation->color->id,
                    'name' => $presentation->color->name,
                    'code' => $presentation->color->color_code,
                    'order' => $presentation->color->order
                ];
            }

            if ($presentation->size) {
                $sizes[] = [
                    'id' => (int) $presentation->size->id,
                    'size' => $presentation->size->name,
                    'presentationId' => (int) $presentation->id,
                    'colorId' => (int) $presentation->color->id,
                    'order' => (int) $presentation->size->order,
                    'stock' => (int) $presentation->inventories[0]->available_stock,
                    'available' => (boolean) $presentation->available
                ];
            }

            // Begin prices array
            $p  = [
                'presentationId' => (int) $presentation->id,
                'prices' => [],
                'tags' => []
            ];

            foreach ($presentation->{$priceType} as $key => $price) {
                $p['prices'][$price->price_rules[0]->quantity] = [
                    'price' => (float) $price->price,
                    'currencyId' => (int) $price->currency_id,
                    'ruleId' => (int) $price->price_rules[0]->id,
                    'ruleQuantity' => (int) $price->price_rules[0]->quantity,
                    'promotion' => [
                        'flag' => (boolean) $price->available_promotion,
                        'price' => ($price->available_promotion) ? (float) $price->promotional_price : 0
                    ]
                ];
            }
            krsort($p['prices']);
            $p['prices'] = array_values($p['prices']);

            foreach ($presentation->tags as $key => $tag) {
                $p['tags'][$tag->order] = [
                //$p['tags'][] = [
                    'id' => $tag->id,
                    'name' => $tag->name,
                ];
            }
            ksort($p['tags']);
            $p['tags'] = array_values($p['tags']);

            // Add prices and tags to prices array
            $prices[] = $p;
        }

        $colors = array_values(array_unique($colors, SORT_REGULAR));
        $photos = array_values(array_unique($photos, SORT_REGULAR));
        $sizes = array_values(array_unique($sizes, SORT_REGULAR));


        /*$formattedItem['colors'] = $colors;
        $formattedItem['sizes'] = $sizes;
        $formattedItem['prices'] = $prices;
        $formattedItem['photos'] = $photos;
        dd($formattedItem);*/

        foreach ($colors as $key => $color) {

            $c = [
                'id' => $color['id'],
                'code' => $color['code'],
                'name' => $color['name'],
                'images' => [],
                'sizes' => []
            ];

            foreach ($photos as $key => $photo) {

                if ($photo['colorId'] === $color['id']) {
                    $i = [
                        //'id' => $photo['id'],
                        'id' => $key + 2,
                        'url' => $photo['url'],
                        'urlThumb' => $photo['urlThumb']
                    ];

                    $c['images'][] = $i;
                    //$c['images'][0] = $i;
                }
            }

            if (! count($c['images'])) {
                $c['images'][] = [
                    'id' => 1,
                    'url' => asset('defaults/product.png'),
                    'urlThumb' => asset('defaults/product.png')
                ];
            }

            foreach ($sizes as $key => $size) {

                if ($color['id'] === $size['colorId']) {

                    $s['id'] = $size['id'];
                    $s['size'] = $size['size'];
                    $s['stock'] = $size['stock'];
                    $s['available'] = $size['available'];

                    foreach ($prices as $key => $price) {

                        if ($size['presentationId'] === $price['presentationId']) {
                            $s['quantity'] = 0;
                            $s['presentationId'] = $price['presentationId'];
                            $s['prices'] = $price['prices'];
                            $s['tags'] = $price['tags'];
                        }
                    }

                    $c['sizes'][$size['order']] = $s;
                    //$c['sizes'][] = $s;
                }
            }

            ksort($c['sizes']);
            $c['sizes'] = array_values($c['sizes']);

            $formattedItem['colors'][$color['order']] = $c;
            //$formattedItem['colors'][] = $c;
        }

        ksort($formattedItem['colors']);
        $formattedItem['colors'] = array_values($formattedItem['colors']);

        return [
            'data' => $formattedItem
        ];

   }
}
