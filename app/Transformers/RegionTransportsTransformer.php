<?php

namespace App\Transformers;

class RegionTransportsTransformer extends AbstractTransformer
{
    public function transform($regions)
    {
        //return $regions;
        $formattedRegions = [];

        foreach ($regions as $key => $region) {

            $r = [
                'id' => (int) $region->id,
                'name' => $region->name,
                'transports' => []
            ];

            $transports = [];
            $branch_offices = [];
            $costs = [];

            foreach ($region->branch_offices as $key => $branch_office) {
                $transports[] = [
                    'id' => (int) $branch_office->transport->id,
                    'name' => $branch_office->transport->name,
                    'message' => $branch_office->transport->description,
                    'addresses' => [],
                    'prices' => []
                ];

                $branch_offices[] = [
                    'transportId' => (int) $branch_office->transport->id,
                    'address' => $branch_office->address,
                    'days' => $branch_office->duration,
                ];

                foreach ($branch_office->costs as $key => $cost) {
                    $costs[] = [
                        //'id' => (int) $cost->id,
                        'transportId' => (int) $branch_office->transport->id,
                        'rule' => (int) $cost->rule->unit_value,
                        'price' => (float) $cost->cost
                    ];
                }
            }
            $transports = array_values(array_unique($transports, SORT_REGULAR));
            $costs = array_values(array_unique($costs, SORT_REGULAR));

            $temps = [];
            foreach ($transports as $key => $transport) {
                foreach ($branch_offices as $key => $branch_office) {
                    if ($transport['id'] === $branch_office['transportId']) {
                        unset($branch_office['transportId']);
                        $transport['addresses'][] = $branch_office;
                    }
                }

                foreach ($costs as $key => $cost) {
                    if ($transport['id'] === $cost['transportId']) {
                        unset($cost['transportId']);
                        $cost['id'] = $key;
                        $transport['prices'][$cost['rule']] = $cost;
                    }
                }
                ksort($transport['prices']);
                $transport['prices'] = array_values($transport['prices']);

                if (count($transport['prices'])) {
                    $temps[] = $transport;
                }
            }
            $r['transports'] = $temps;

            $formattedRegions[] = $r;
        }

        return [
            'data' => $formattedRegions
        ];
    }
}