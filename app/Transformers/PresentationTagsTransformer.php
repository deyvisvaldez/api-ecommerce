<?php

namespace App\Transformers;

class PresentationTagsTransformer
{
    public function transform($presentations)
    {
        $formattedPresentations = [];

        $pres = [];
        $colors = [];
        $sizes = [];
        $rules = [];

        foreach ($presentations as $key => $presentation) {
            $pre = [
                'id' => (int) $presentation->id,
                'color' => $presentation->color->name,
                'colorId' => (int) $presentation->color_id,
                'size' => $presentation->size->name,
                'sizeId' => (int) $presentation->size_id,
                'available' => (boolean) $presentation->available,
                'tags' => []
            ];

            foreach ($presentation->tags as $key => $tag) {
                $t = [
                'id' => (int) $tag->id,
                'name' => $tag->name,
                ];
                $pre['tags'][] = $t;
            }

            $pres[] = $pre;
        }

        return [
            'data' => $pres
        ];
    }
}