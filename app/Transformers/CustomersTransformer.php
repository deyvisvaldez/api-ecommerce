<?php

namespace App\Transformers;

class CustomersTransformer extends AbstractTransformer
{
    public function transform($people)
    {
        $formattedPeople = [];

        foreach ($people as $key => $person) {
            $p = [
                'id' => (int) $person->id,
                'fullName' => $person->first_name.' '.$person->last_name
            ];

            $formattedPeople[] = $p;
        }

        return [
            'data' => $formattedPeople
        ];
    }
}