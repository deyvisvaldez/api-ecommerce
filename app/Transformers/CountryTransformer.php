<?php

namespace App\Transformers;

class CountryTransformer extends AbstractTransformer
{
    public function transform($country)
    {
        $formattedCountry = [
            'name' => $country->name,
        ];

        return [
            'data' => $formattedCountry
        ];
    }
}