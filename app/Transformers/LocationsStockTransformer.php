<?php

namespace App\Transformers;

class LocationsStockTransformer extends AbstractTransformer
{
    public function transform($inventories)
    {
        //return $inventories;
        $formattedInventories = [];

        foreach ($inventories as $key => $inventory) {
            $inv = [
                'location' => $inventory->location->name,
                'stock' => (int) $inventory->available_stock,
            ];
            $formattedInventories[] = $inv;
        }


        return [
            'data' => $formattedInventories
        ];
    }
}