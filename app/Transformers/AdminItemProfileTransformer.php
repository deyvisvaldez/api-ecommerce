<?php

namespace App\Transformers;

class AdminItemProfileTransformer extends AbstractTransformer
{
    public function transform($item)
    {
        $formattedItem = [
            'id' => (int) $item->id,
            'name' => $item->name,
            'slug' => $item->slug,
            'code' => $item->business_code,
            'description' => $item->description,
            'referencialPrice' => (float) $item->referencial_price,
            'features' => $item->features,
            'categoryId' => (int)$item->category_id,
            'subcategoryId' => (int)$item->subcategory_id,
            'recommendations' => [],
            'measurement' => [
                'base64' => $item->measurement,
                'extension' => ''
            ]
        ];

        foreach ($item->recommendations as $key => $recommendation) {
            $formattedItem['recommendations'][] = [
                'id' => $recommendation->id,
                'url' => $recommendation->image
            ];
        }

        return [
            'data' => $formattedItem
        ];

   }
}
