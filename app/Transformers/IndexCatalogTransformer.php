<?php

namespace App\Transformers;

use App\Transformers\AbstractTransformer;

class IndexCatalogTransformer extends AbstractTransformer
{
    public function transform($items)
    {
        //return $items;
        $formattedItems = [];

        foreach ($items as $key => $item) {

            if (count($item->presentations)) {
                $fmtItems = [
                    'id' => (int) $item->id,
                    'name' => $item->name,
                    'slug' => $item->slug,
                    'price' => $item->presentations[0]->random_price->price,
                    'promotion' => [
                        'flag' => false,
                        'price' => 0
                    ],
                    'categorySlug' => $item->category->slug,
                    'subcategoryName' => $item->subcategory->name,
                    'subcategorySlug' => $item->subcategory->slug,
                    'colors' => [],
                ];

                $flag = true;
                $colors = [];
                $photos = [];

                foreach ($item->presentations as $key => $presentation) {

                    if ($flag) {
                        if (count($presentation->web_prices)) {
                            $fmtItems['promotion']['flag'] = true;
                            $fmtItems['promotion']['price'] = (float) $presentation->web_prices[0]->promotional_price;
                            $flag = false;
                        }
                    }

                    if ($presentation->color) {
                        $colors[] = [
                            'id' => (int) $presentation->color->id,
                            'name' => $presentation->color->name,
                            'code' => $presentation->color->color_code,
                            'mainImage' => '',
                            'secondaryImage' => '',
                            'itemId' => (int) $item->id
                        ];
                    }

                    if (count($presentation->photos)) {
                        foreach ($presentation->photos as $key => $photo) {
                            $photos[] = [
                                'colorId' => (int) $presentation->color->id,
                                'image' => $photo->resource_thumb,
                                'itemId' => (int) $item->id
                            ];
                        }
                    }
                }

                $colors = array_values(array_unique($colors, SORT_REGULAR));

                foreach ($colors as $key => &$color) {
                    $flag = true;
                    $count = 0;
                    foreach ($photos as $key => $photo) {
                        if ($color['id'] === $photo['colorId'] && $color['itemId'] === $photo['itemId']) {
                            if ($count == 0) {
                                $color['mainImage'] = $photo['image'];
                                $count = 1;
                                $flag = false;
                            } else  {
                                $color['secondaryImage'] = $photo['image'];
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        if ($item->referencial_photo_thumb) {
                            $color['mainImage'] = $item->referencial_photo_thumb;
                        } else {
                            $color['mainImage'] = asset('defaults/product.png');
                        }

                    }
                    unset($color['itemId']);
                }

                $fmtItems['colors'] = $colors;

                $formattedItems[] = $fmtItems;
            }
        }

        return [
            'data' => $formattedItems
        ];

    }
}

