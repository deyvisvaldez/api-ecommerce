<?php

namespace App\Transformers;

abstract class AbstractTransformer
{
    abstract public function transform($data);
}
