<?php

namespace App\Transformers;

class WiltexPhoneTransformer extends AbstractTransformer
{
    public function transform($phones)
    {
        $formattedPhones = [];
        $main = 0;
        foreach ($phones as $key => $phone) {
            $e = [
                'id' => (int) $phone->id,
                'phone' => $phone->phone,
                'main' => (boolean) $phone->main
            ];

            $formattedPhones[] = $e;

            if ($phone->main) {
                $main = (int) $phone->id;
            }
        }

        return [
            'data' => [
                'phones' => $formattedPhones,
                'checked' => $main
            ]
        ];
    }
}