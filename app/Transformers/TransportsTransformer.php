<?php

namespace App\Transformers;

class TransportsTransformer extends AbstractTransformer
{
    public function transform($transports)
    {
        $formattedTransports = [];

        foreach ($transports as $key => $transport) {
            $s = [
                'id' => (int) $transport->id,
                'name' => $transport->name
            ];

            $formattedTransports[] = $s;
        }

        return [
            'data' => $formattedTransports
        ];
    }
}