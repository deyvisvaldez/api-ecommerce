<?php

namespace App\Transformers;

use App\Repositories\PriceRuleRepository;

class UpdateSearchTransformer
{
    public function __construct()
    {
        $this->priceRuleRepository = new PriceRuleRepository();
    }

    public function transform($inventories, $origin)
    {
        //return $inventories;
        $formattedInventories = [];

        foreach ($inventories as $key => $inventory) {

            // reset() returns the value of the first array element, or FALSE if the array is empty.
            switch ($origin) {
                case 1:
                    $prices = [];
                    foreach ($inventory->presentation->web_prices as $key => $webPrice) {
                        $prices[$webPrice->price_rules[0]->quantity] = (float) $webPrice->price;
                    }
                    $price = reset($prices);
                    break;
                case 2:
                    $prices = [];
                    foreach ($inventory->presentation->shop_prices as $key => $shopPrice) {
                        $prices[$shopPrice->price_rules[0]->quantity] = (float) $shopPrice->price;
                    }
                    $price = reset($prices);
                    break;
                case 3:
                    $prices = [];
                    foreach ($inventory->presentation->wholesale_prices as $key => $wholesalePrice) {
                        $prices[$wholesalePrice->price_rules[0]->quantity] = (float) $wholesalePrice->price;
                    }
                    $price = reset($prices);
                    break;
            }

            if ($price) {
                $i = [
                    'id' => (int) $inventory->id,
                    'name' => "{$inventory->presentation->item->name} {$inventory->presentation->color->name} {$inventory->presentation->size->name}",
                    'itemName' => $inventory->presentation->item->name,
                    'colorName' => $inventory->presentation->color->name,
                    'size' => $inventory->presentation->size->name,
                    'colorCode' => $inventory->presentation->color->color_code,
                    'price' => (float) $price,
                    'stock' => (int) $inventory->available_stock
                ];
                $formattedInventories[] = $i;
            }
        }

        return [
            'data' => $formattedInventories
        ];
    }
}