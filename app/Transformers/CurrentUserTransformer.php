<?php

namespace App\Transformers;

class CurrentUserTransformer extends AbstractTransformer
{
    public function transform($user)
    {
        $formattedUser = [
            'identityDocument' => $user->person->identity_document,
            'imageUrl' => $user->person->avatar_thumb,
            'firstName' => $user->person->first_name,
            'lastName' => $user->person->last_name,
            'address' => ($user->person->address) ? $user->person->address->address : '',
            'role' => $user->role,
            'email' => ($user->person->email) ? $user->person->email->email : '',
            'whatsapp' => ($user->person->whatsapp) ? $user->person->whatsapp->whatsapp : '',
            'countryId' => $user->person->country_id,
            'regionId' => $user->person->region_id
        ];

        return [
            'data' => $formattedUser
        ];
    }
}