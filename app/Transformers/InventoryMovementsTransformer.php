<?php

namespace App\Transformers;

class InventoryMovementsTransformer extends AbstractTransformer
{
    public function transform($movements)
    {
        //return $movements;
        $formattedMovements = [];

        foreach ($movements as $key => $movement) {
            switch ($movement->type) {
                case 1:
                    $operation = 'Ingreso a Producción';
                    $quantity = "+ {$movement->quantity}";
                    break;

                case 2:
                    $operation = 'Salida por Venta';
                    $quantity = "- {$movement->quantity}";
                    break;

                case 3:
                    $operation = 'Traslado (Salida)';
                    $quantity = "- {$movement->quantity}";
                    break;

                case 4:
                    $operation = 'Traslado (Ingreso)';
                    $quantity = "+ {$movement->quantity}";
                    break;

                case 5:
                    $operation = 'Ajuste de Stock (Ingreso)';
                    $quantity = "+ {$movement->quantity}";
                    break;

                case 6:
                    $operation = 'Ajuste de Stock (Salida)';
                    $quantity = "- {$movement->quantity}";
                    break;

                case 7:
                    $operation = 'Devolución';
                    $quantity = "+ {$movement->quantity}";
                    break;

                case 8:
                    $operation = 'Entrega de Pedido (Salida)';
                    $quantity = "- {$movement->quantity}";
                    break;
            }

            $m = [
                'date' => $movement->created_at->format('d/m/Y g:i A'),
                'user' => $movement->user->person->first_name.' '.$movement->user->person->last_name,
                //'location' => $movement->inventory->location->name,
                'location' => $movement->origin_destiny,
                'quantity' => $quantity,
                'operation' => $operation,
                'reason' => $movement->description
            ];

            $formattedMovements[] = $m;
        }

        return [
            'data' => $formattedMovements
        ];
    }
}