<?php

namespace App\Transformers;

class TransportTransformer extends AbstractTransformer
{
    public function transform($transport)
    {
        $formattedTransport = [
            'id' => (int) $transport->id,
            'name' => $transport->name
        ];

        return [
            'data' => $formattedTransport
        ];
    }
}