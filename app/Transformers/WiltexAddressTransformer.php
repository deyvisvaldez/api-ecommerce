<?php

namespace App\Transformers;

class WiltexAddressTransformer extends AbstractTransformer
{
    public function transform($addresses)
    {
        $formattedAddresses = [];
        $main = 0;
        foreach ($addresses as $key => $address) {
            $e = [
                'id' => (int) $address->id,
                'address' => $address->address,
                'main' => false
            ];

            $formattedAddresses[] = $e;

            if ($address->main) {
                $main = (int) $address->id;
            }
        }

        return [
            'data' => [
                'addresses' => $formattedAddresses,
                'checked' => $main
            ]
        ];
    }
}