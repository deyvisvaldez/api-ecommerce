<?php

namespace App\Transformers;

class OrderTransformer extends AbstractTransformer
{
    public function transform($order)
    {
        switch ($order->status) {
            case 1:
                $status = 'Pendiente';
                break;

            case 2:
                $status = 'Confirmado';
                break;

            case 3:
                $status = 'Cancelado';
                break;

            case 4:
                $status = 'Entregado';
                break;
        }

        switch ($order->shipping_id) {
            case 1:
                $shipping = 'Recoger en tienda';
                break;

            case 2:
                $shipping = 'Envio a domicilio';
                break;

            default:
                $shipping = 'Método no seleccionado';
                break;
        }

        $amount = 0;
        foreach ($order->payments as $key => $payment) {
            $amount = (float) $amount + (float) $payment->amount;
        }

        if ($order->extra_cost) {
            $cost = [
                'regionId' => $order->extra_cost->region_id,
                'transportId' => $order->extra_cost->transport_id,
                'cost' => $order->extra_cost->cost
            ];
        } else {
            $cost = [
                'regionId' => 0,
                'transportId' => 0,
                'cost' => 0
            ];
        }

        $formattedOrder = [
            'costs' => $cost,
            'order' => [
                'id' => (int) $order->id,
                'date' => $order->created_at->format('d/m/Y'),
                'code' => $order->code,
                'pdfUrl' => route('public.order', ['uuid' => $order->uuid]),
                'status' => $status,
                'currencyId' => 1,
                'statusId' => (int) $order->status,
                'locationId' => (int) $order->location_id,
                'amountPaid' => (float) $amount,
                'origin' => (int) $order->origin,
                'type' => (int) $order->order_type,
                'shipping' => $shipping,
                'person' => [
                    'name' => $order->person->first_name.' '.$order->person->last_name,
                    'cellphone' => ($order->person->address) ? $order->person->whatsapp->whatsapp : '',
                    'email' => ($order->person->email) ? $order->person->email->email : '',
                    'address' => ($order->person->address) ? $order->person->address->address : '',
                    'region' => ($order->person->region) ? $order->person->region->name : ''
                ]
            ]
        ];

        return [
            'data' => $formattedOrder
        ];
    }
}