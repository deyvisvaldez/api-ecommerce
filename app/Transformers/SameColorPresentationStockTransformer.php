<?php

namespace App\Transformers;

class SameColorPresentationStockTransformer
{
    public function transform($presentations, $sizes, $locations)
    {
        //return $presentations;
        $formattedLocations = [];
        $generalFlag = false;

        foreach ($locations as $key => $location) {
            $l = [
                'name' => $location->name,
                'inventories' => []
            ];

            $invs = [];
            foreach ($sizes as $key => $size) {
                $flag = true;
                foreach ($presentations as $key => $presentation) {
                    foreach ($presentation->inventories as $key => $inventory) {
                        if ($inventory->location_id == $location->id && $presentation->size_id == $size['id']) {
                            $invs[$size['order']] = [
                                'id' => (int) $inventory->id,
                                'stock' => (int) $inventory->available_stock,
                                'order' => (int) $size['order']
                            ];
                            if ($inventory->available_stock) {
                                $generalFlag = true;
                            }
                            $flag = false;
                            break;
                        }
                    }
                }
                if ($flag) {
                    $invs[$size['order']] = [
                        'id' => 0,
                        'stock' => 0,
                        'order' => (int) $size['order']
                    ];
                }
            }
            ksort($invs);
            $invs = array_values($invs);
            $l['inventories'] = $invs;
            $formattedLocations[] = $l;
        }

        $sortSizes = [];
        foreach ($sizes as $key => $size) {
            $sortSizes[$size['order']] = $size;
        }
        ksort($sortSizes);
        $sortSizes = array_values($sortSizes);

        return [
            'sizes' => $sortSizes,
            'locations' => $formattedLocations,
            'flag' => $generalFlag,
            'colorName' => $presentations[0]->color->name,
            'colorCode' => $presentations[0]->color->color_code,
            'itemName' => $presentations[0]->item->name
        ];
    }
}