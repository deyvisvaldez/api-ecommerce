<?php

namespace App\Transformers;

class UsersTransformer
{
    public function transform($people)
    {
        $users = [];

        foreach ($people as $key => $person) {

            switch ($person->user->role) {
                case 1:
                    $role = 'Super Admin';
                    break;

                case 2:
                    $role = 'Admin';
                    break;

                case 3:
                    $role = 'Vendedor';
                    break;

                case 4:
                    $role = 'Distribuidor';
                    break;

                case 5:
                    $role = 'Cajero';
                    break;

                case 6:
                    $role = 'Logística';
                    break;

                case 7:
                    $role = 'Cliente';
                    break;
            }

            $u = [
                'id' => (int) $person->id,
                'photoUrl' => $person->avatar,
                'lastName' => $person->last_name,
                'firstName' => $person->first_name,
                'email' => ($person->email) ? $person->email->email : '',
                'region' => ($person->region) ? $person->region->name : '',
                'cellphone' => ($person->cellphone) ? $person->cellphone->cellphone : '',
                'country' => ($person->country) ? $person->country->name : '',
                'documentNumber' => $person->identity_document,
                'role' => $role,
                'status' => $person->user->activated
            ];

            $users[] = $u;
        }

        return [
            'data' => $users
        ];
    }
}