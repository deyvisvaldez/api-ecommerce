<?php

namespace App\Transformers;

class ColorTransformer extends AbstractTransformer
{
    public function transform($color)
    {
        $formattedColor = [
            'name' => $color->name,
            'order' => (int) $color->order,
            'colorCode' => [
                'hex' => $color->color_code,
            ]
        ];

        return [
            'data' => $formattedColor
        ];
    }
}