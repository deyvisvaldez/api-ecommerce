<?php

namespace App\Transformers;

class RegionsTransformer extends AbstractTransformer
{
    public function transform($regions)
    {
        $formattedRegions = [];

        foreach ($regions as $key => $region) {
            $r = [
                'id' => (int) $region->id,
                'name' => $region->name
            ];

            $formattedRegions[] = $r;
        }

        return [
            'data' => $formattedRegions
        ];
    }
}