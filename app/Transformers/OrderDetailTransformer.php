<?php

namespace App\Transformers;

class OrderDetailTransformer extends AbstractTransformer
{
    public function transform($order)
    {
        $formattedOrder = [];

        foreach ($order->presentations as $key => $presentation) {

            // En realidad debería ser "presentationId"
            $i = [
                'inventoryId' => (int) $presentation->id,
                'stock' => (int) $presentation->inventory->available_stock,
                'name' => $presentation->item->name,
                'colorName' => $presentation->color->name,
                'sizeName' => $presentation->size->name,
                'quantity' => (int) $presentation->pivot->quantity,
                'price' => (float) $presentation->pivot->price,
                'currencyId' => (int) $presentation->web_price->currency_id,
                'customPrice' => (float) $presentation->pivot->price,
                'priceFlag' => false,
                'showStocks' => false,
                'locationId' => $order->location_id
            ];
            $formattedOrder[] = $i;
        }

        return [
            'data' => $formattedOrder
        ];
    }
}