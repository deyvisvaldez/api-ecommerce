<?php

namespace App\Transformers;

class OrdersTransformer extends AbstractTransformer
{
    public function transform($orders)
    {
        //return $orders;
        $formattedOrders = [];

        foreach ($orders as $key => $order) {

            $quantity = 0;
            foreach ($order->presentations as $key => $presentation) {
                $quantity = $quantity + (int)$presentation->pivot->quantity;
            }

            switch ($order->user->role) {
                case 1:
                    if ($order->order_type == 2) {
                        $location = 'Web - '.$order->location->name;
                    } else {
                        $location = 'POS - '.$order->location->name;
                    }
                    break;

                case 4:
                    $location = 'Distribuidor - '.$order->location->name;
                    break;
            }

            //$quantity = count($order->presentations);
            $o = [
                'code' => $order->code,
                'quantity' => ($quantity === 1) ? $quantity.' producto' : $quantity.' productos',
                'clientName' => $order->person->first_name.' '.$order->person->last_name,
                'clientCellphone' => ($order->person->whatsapp) ? $order->person->whatsapp->whatsapp : '',
                'total' => (float) $order->total,
                'currencyId' => (int) $order->currency_id,
                'status' => (int) $order->status,
                'date' => $order->created_at->format('d/m/Y'),
                'regionId' => ($order->person->region) ? (int) $order->person->region->id : 0,
                'regionName' => ($order->person->region) ? $order->person->region->name : '',
                'type' => (int) $order->order_type,
                'locationId' => (int) $order->location_id,
                'location' => $location
            ];

            $formattedOrders[] = $o;
        }

        return [
            'data' => $formattedOrders
        ];
    }
}