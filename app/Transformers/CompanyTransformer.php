<?php

namespace App\Transformers;

class CompanyTransformer extends AbstractTransformer
{
    public function transform($company)
    {
        $formattedCompany = [
            'name' => $company->business_name,
            'legalName' => $company->legal_name,
            'logoUrl' => $company->color_logotype,
            'whiteLogoUrl' => $company->white_logotype,
            'slogan' => $company->slogan_title,
            'address' => ($company->address) ? $company->address->address : '',
            'facebookPage' => $company->facebook,
            'gplusPage' => $company->google_plus,
            'youtubePage' => $company->youtube,
            'returnPolicies' => $company->return_policies,
            'shippingPolicies' => $company->shipping_policies,
            'team' => [],
            'videos' => [],
            'images' => [],
            'cellphones' => [],
            'imageConstruction' => 'https://dl.dropboxusercontent.com/s/rb7iwkmhoy44ngj/IMAGEN_LOGIN.png',
            'cellphone' => ($company->cellphone) ? $company->cellphone->cellphone : '',
            'email' => ($company->email) ? $company->email->email : '',
            'phone' => ($company->phone) ? $company->phone->phone : '',
            'whatsapp' => ($company->whatsapp) ? $company->whatsapp->whatsapp : '',
            'emails' => []
        ];

        foreach ($company->users as $key => $user) {
            $formattedCompany['team'][] = [
                'name' => $user->person->first_name.' '.$user->person->last_name,
                'photoUrl' => $user->person->avatar_thumb
            ];
        }

        foreach ($company->photos as $key => $photo) {
            $formattedCompany['images'][] = $photo->resource;
        }

        foreach ($company->emails as $key => $email) {
            $formattedCompany['emails'][] = $email->email;
        }

        foreach ($company->whatsapps as $key => $whatsapp) {
            $formattedCompany['cellphones'][] = $whatsapp->whatsapp;
        }

        return [
            'data' => $formattedCompany
        ];
    }
}
