<?php

namespace App\Transformers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class WeekCatalogTransformer
{
    public function transform($lastWeekItems, $remainItems)
    {

        $catalog = array_merge($lastWeekItems, $remainItems);

        //Get current page form url e.g. &page=6
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        //Create a new Laravel collection from the array data
        $collection = new Collection($catalog);

        //Define how many items we want to be visible in each page
        $perPage = 12;

        //Slice the collection to get the items to display in current page
        //$currentPageSearchResults = $collection->slice($currentPage * $perPage, $perPage)->all();
        $currentPageSearchResults = $collection->slice(($currentPage - 1) * $perPage, $perPage)->all();

        //Create our paginator and pass it to the view
        $paginatedSearchResults= new LengthAwarePaginator($currentPageSearchResults, count($collection), $perPage);

        $result = $paginatedSearchResults->toArray();
        $data = $result['data'];
        unset($result['data']);
        $data = array_values($data);
        $result['data'] = $data;
        return $result;
    }
}
