<?php

namespace App\Transformers;

use App\Entities\Color;
use App\Entities\Size;

class PresentationsProfileTransformer extends AbstractTransformer
{
    public function transform($items)
    {
        $colors = [];
        $sizes = [];
        foreach ($items as $key => $item) {
           foreach ($item->presentations as $key => $presentation) {
               if ($presentation->color) {
                   $colors[$presentation->color->order] = [
                        'id' => (int) $presentation->color->id,
                        'name' => $presentation->color->name,
                        //'order' => (int) $presentation->color->order
                   ];
               }
               if ($presentation->size) {
                   $sizes[$presentation->size->order] = [
                        'id' => (int) $presentation->size->id,
                        'name' => $presentation->size->name,
                        //'order' => (int) $presentation->size->order
                   ];
               }
           }
        }

        ksort($colors);
        ksort($sizes);

        //$a = [$colors, $sizes];
        //dd($a);

        $formattedItems = [];

        foreach ($items as $key => $item) {
            $i = [
                'name' => $item->name,
                'inventories' => []
            ];

            foreach ($colors as $key => $color) {
                $temps = [];
                foreach ($item->presentations as $key => $presentation) {
                    if ($color['name'] == $presentation->color->name) {
                        $temps[] = [
                            'id' => (int) $presentation->id,
                            'colorName' => $presentation->color->name,
                            'colorCode' => $presentation->color->color_code,
                            'sizeName' => $presentation->size->name,
                            'stock' => (int) $presentation->inventories[0]->available_stock,
                            'destination' => 0,
                            'quantity' => 1,
                            'flag' => true,
                            'promotionalPrice' => (count($presentation->web_prices)) ? (float) $presentation->web_prices[0]->promotional_price : 0,
                            'webPrice' => (count($presentation->web_prices)) ? (float) $presentation->web_prices[0]->price : 0,
                            'shopPrice' => (count($presentation->shop_prices)) ? (float) $presentation->shop_prices[0]->price : 0,
                            'wholesalePrice' => (count($presentation->wholesale_prices)) ? (float) $presentation->wholesale_prices[0]->price : 0
                        ];
                    }
                }

                foreach ($sizes as $key => $size) {
                    foreach ($temps as $key => $temp) {
                        if ($size['name'] == $temp['sizeName']) {
                            $i['inventories'][] = $temp;
                            break;
                        }
                    }
                }
            }

            $formattedItems[] = $i;
        }

        return [
            'data' => $formattedItems
        ];
    }
}
