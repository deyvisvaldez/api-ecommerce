<?php

namespace App\Transformers;

class GeneralCartTransformer
{
    public function transform($presentations, $priceType)
    {
        //return $presentations;
        $formattedPresentations = [];

        foreach ($presentations as $key => $presentation) {

            $pre = [
                'presentationId' => (int) $presentation->id,
                'itemId' => (int) $presentation->item->id,
                'itemName' => $presentation->item->name,
                'itemSlug' => $presentation->item->slug,
                'imageUrl' => (count($presentation->photos)) ? $presentation->photos[0]->resource_thumb : asset('defaults/product.png'),
                'colorCode' => $presentation->color->color_code,
                'colorName' => $presentation->color->name,
                'size' => $presentation->size->name,
                'prices' => []
            ];

            foreach ($presentation->{$priceType} as $key => $price) {
                $wP = [
                    'ruleId' => (int) $price->price_rules[0]->id,
                    'ruleQuantity' => (int) $price->price_rules[0]->quantity,
                    'price' => (float) $price->price,
                    'currencyId' => (int) $price->currency_id,
                    'promotion' => [
                        'flag' => (boolean) $price->available_promotion,
                        'price' => ($price->available_promotion) ? (float) $price->promotional_price : 0
                    ]
                ];
                $pre['prices'][$price->price_rules[0]->quantity] = $wP;
            }

            krsort($pre['prices']);
            $pre['prices'] = array_values($pre['prices']);

            $formattedPresentations[] = $pre;
        }

        return [
            'data' => $formattedPresentations
        ];
    }
}
