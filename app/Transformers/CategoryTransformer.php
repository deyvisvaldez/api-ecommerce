<?php

namespace App\Transformers;

class CategoryTransformer extends AbstractTransformer
{
    public function transform($category)
    {
        $formattedCategory = [
            'id' => (int) $category->id,
            'img' => $category->image_thumb,
            'description' => $category->description,
            'name' => $category->name,
            'slug' => $category->slug,
            'order' => $category->order,
            'published' => (boolean) $category->published,
            'extension' => ''
        ];

        return [
            'data' => $formattedCategory
        ];
    }
}