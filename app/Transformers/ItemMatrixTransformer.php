<?php

namespace App\Transformers;

class ItemMatrixTransformer
{
    public function transform($item, $allSizes, $allColors, $locations = false)
    {
        foreach ($allSizes as $key => $size) {
            $sizeIds[] = $size['id'];
        }

        $formattedInventories = [];

        $photos = [];
        $colors = [];
        $sizes = [];

        foreach ($item->presentations as $key => $presentation) {

            if (count($presentation->inventories)) {
                if (count($presentation->photos)) {
                    $photos[] = [
                        'imageUrl' => $presentation->photos[0]->resource_thumb,
                        'colorId' => (int) $presentation->color->id
                    ];
                }

                $colors[] = [
                    'id' => (int) $presentation->color->id,
                    'name' => $presentation->color->name,
                    'code' => $presentation->color->color_code,
                ];

                if ($locations) {
                    $stock = 0;
                    foreach ($presentation->inventories as $key => $inventory) {
                        $stock = $stock + (int) $inventory->available_stock;
                    }
                } else {
                    $stock = (int) $presentation->inventories[0]->available_stock;
                }

                $sizes[] = [
                    'sizeId' => (int) $presentation->size->id,
                    'size' => $presentation->size->name,
                    'presentationId' => (int) $presentation->id,
                    'stock' => $stock,
                    'colorId' => (int) $presentation->color->id,
                    'promotion' => (float) $presentation->web_prices[0]->available_promotion,
                    'minStock' => $presentation->inventories[0]->min_stock,
                    'published' => $presentation->published,
                    'order' => $presentation->size->order
                ];
            }
        }

        $colors = array_values(array_unique($colors, SORT_REGULAR));

        foreach ($allColors as $key => $oneColor) {

            $colorFlag = true;

            foreach ($colors as $key => $color) {

                if ($oneColor['id'] === $color['id']) {
                    $c = [
                        'itemId' => (int) $item->id,
                        'id' => $color['id'],
                        'code' => $color['code'],
                        'name' => $color['name'],
                        'imageUrl' => asset('defaults/product.png'),
                        'inventories' => []
                    ];

                    foreach ($photos as $key => $photo) {
                        if ($photo['colorId'] === $color['id']) {
                            $c['imageUrl'] = $photo['imageUrl'];
                            break;
                        }
                    }

                    foreach ($allSizes as $key => $oneSize) {

                        $flag = true;

                        foreach ($sizes as $key => $size) {

                            if ($color['id'] === $size['colorId']) {

                                if ($oneSize['id'] === $size['sizeId']) {
                                    $s = [
                                        'id' => $size['presentationId'],
                                        'colorId' => $size['colorId'],
                                        'stock' => $size['stock'],
                                        'sizeId' => $size['sizeId'],
                                        'state' => 'created',
                                        'isSelected' => false,
                                        'promotion' => $size['promotion'],
                                        'minStock' => $size['minStock'],
                                        'published' => $size['published'],
                                        'showStocks' => false,
                                        'order' => $size['order']
                                    ];
                                    $c['inventories'][] = $s;

                                    $flag = false;
                                }
                            }
                        }

                        if ($flag) {
                            $s = [
                                'id' => '',
                                'colorId' => $color['id'],
                                'stock' => 0,
                                'sizeId' => $oneSize['id'],
                                'state' => 'notCreated',
                                'isSelected' => false,
                                'published' => false,
                                'promotion' => false,
                                'showStocks' => false,
                                'order' => $oneSize['order']
                            ];

                            $c['inventories'][] = $s;
                        }
                    }

                    $formattedInventories[] = $c;

                    $colorFlag = false;
                    break;
                }

            }

            if ($colorFlag) {
                $c = [
                    'itemId' => (int) $item->id,
                    'id' => $oneColor['id'],
                    'code' => $oneColor['code'],
                    'name' => $oneColor['color'],
                    'imageUrl' => asset('defaults/product.png'),
                    'inventories' => []
                ];

                foreach ($photos as $key => $photo) {
                    if ($photo['colorId'] === $oneColor['id']) {
                        $c['imageUrl'] = $photo['imageUrl'];
                        break;
                    }
                }

                foreach ($allSizes as $key => $oneSize) {
                    $s = [
                        'id' => '',
                        'colorId' => $oneColor['id'],
                        'stock' => 0,
                        'sizeId' => $oneSize['id'],
                        'state' => 'notCreated',
                        'isSelected' => false,
                        'published' => false,
                        'promotion' => false,
                        'showStocks' => false,
                        'order' => $oneSize['order']
                    ];

                    $c['inventories'][] = $s;
                }

                $formattedInventories[] = $c;
            }
        }

        return [
            'data' => [
                'itemColors' => $formattedInventories,
                'itemSizes' => $allSizes,
                'excelUrl' => ''
            ]
        ];
    }
}
