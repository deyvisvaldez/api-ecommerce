<?php

namespace App\Transformers;

class BanksTransformer extends AbstractTransformer
{
    public function transform($banks)
    {
        $formattedBanks = [];

        foreach ($banks as $key => $bank) {
            $b = [
                'id' => (int) $bank->id,
                'name' => $bank->name,
                'imageUrl' => $bank->bank_image
            ];

            $formattedBanks[] = $b;
        }

        return [
            'data' => $formattedBanks
        ];
    }
}