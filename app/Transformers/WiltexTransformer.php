<?php

namespace App\Transformers;

class WiltexTransformer extends AbstractTransformer
{
    public function transform($company)
    {
        $formattedCompany = [
            'businessName' => $company->business_name,
            'legalName' => $company->legal_name,
            'sloganTitle' => $company->slogan_title,
            'sloganSubtitle' => $company->slogan_subtitle,
            'representative' => $company->representative,
            'whoWeAre' => $company->who_we_are,
            'whatWeDo' => $company->what_we_do,
            'proposal' => $company->proposal,
            'anniversary' => $company->anniversary,
            'schedules' => $company->schedules,
            'facebook' => $company->facebook,
            'googlePlus' => $company->google_plus,
            'youtube' => $company->youtube,
            'linkedin' => $company->linkedin,
            'twitter' => $company->twitter,
            'website' => $company->website,
            'returnPolicies' => $company->return_policies,
            'shippingPolicies' => $company->shipping_policies,
            'countryId' =>(int)  $company->country_id,
            'regionId' => (int) $company->region_id,
            'cellphone' => ($company->cellphone) ?  $company->cellphone->cellphone : '',
            'phone' => ($company->phone) ? $company->phone->phone : '',
            'whatsapp' => ($company->whatsapp) ? $company->whatsapp->whatsapp : '',
            'email' => ($company->email) ? $company->email->email : '',
            'address' => ($company->address) ? $company->address->address : '',
            'icon' => [
                'base64' => $company->icon,
                'extension' => ''
            ],
            'colorLogotype' => [
                'base64' => $company->color_logotype,
                'extension' => ''
            ],
            'whiteLogotype' => [
                'base64' => $company->white_logotype,
                'extension' => ''
            ]

        ];

        return [
            'data' => $formattedCompany
        ];
    }
}
