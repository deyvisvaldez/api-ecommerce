<?php

namespace App\Transformers;

class WiltexEmailTransformer extends AbstractTransformer
{
    public function transform($emails)
    {
        $formattedEmails = [];
        $main = 0;
        foreach ($emails as $key => $email) {
            $e = [
                'id' => (int) $email->id,
                'email' => $email->email,
                'main' => (boolean) $email->main
            ];

            $formattedEmails[] = $e;

            if ($email->main) {
                $main = (int) $email->id;
            }
        }

        return [
            'data' => [
                'emails' => $formattedEmails,
                'checked' => $main
            ]
        ];
    }
}