<?php

namespace App\Transformers;

class WiltexWhatsappTransformer extends AbstractTransformer
{
    public function transform($whatsapps)
    {
        $formattedWhatsapps = [];
        $main = 0;
        foreach ($whatsapps as $key => $whatsapp) {
            $e = [
                'id' => (int) $whatsapp->id,
                'whatsapp' => $whatsapp->whatsapp,
                'main' => (boolean) $whatsapp->main
            ];

            $formattedWhatsapps[] = $e;

            if ($whatsapp->main) {
                $main = (int) $whatsapp->id;
            }
        }

        return [
            'data' => [
                'whatsapps' => $formattedWhatsapps,
                'checked' => $main
            ]
        ];
    }
}