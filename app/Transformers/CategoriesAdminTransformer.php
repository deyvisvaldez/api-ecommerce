<?php

namespace App\Transformers;

class CategoriesAdminTransformer extends AbstractTransformer
{
    public function transform($categories)
    {
        $formattedCategories = [];

        foreach ($categories as $key => $category) {
            $bc = [
                'id' => (int) $category->id,
                'name' => $category->name,
                'imageUrl' => $category->image,
                'slug' => $category->slug,
                'order' => (int) $category->order,
                'published' => (int) $category->published,
                'subcategories' => [],
            ];

            foreach ($category->subcategories as $key => $subcategory) {
                $c = [
                    'id' => (int) $subcategory->id,
                    'slug' => $subcategory->slug,
                    'name' => $subcategory->name,
                    'categorySubcategoryId' => $subcategory->pivot->id,
                    'order' => (int) $subcategory->pivot->order
                ];

                $bc['subcategories'][] = $c;
            }

            $formattedCategories[] = $bc;
        }

        return [
            'data' => $formattedCategories
        ];
    }
}