<?php

namespace App\Transformers;

class EditSubcategoryTransformer extends AbstractTransformer
{
    public function transform($subcategories)
    {
        //return $subcategory = $subcategory->toArray();
        $formattedSubcategory = [
            //'id' => (int) $subcategory->id,
            'name' => $subcategories[0]->name,
            'description' => $subcategories[0]->description,
            'categorySubcategoryId' => (int) $subcategories[0]->pivot->id,
            'order' => (int) $subcategories[0]->pivot->order,
            'active' => (boolean) $subcategories[0]->pivot->active,
            'image' => [
                'base64' => $subcategories[0]->image_thumb,
                'extension' => ''
            ]
        ];

        return [
            'data' => $formattedSubcategory
        ];
    }
}