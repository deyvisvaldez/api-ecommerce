<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\CompanyRepository;
use App\Services\UpdateWiltexAddress;
use App\Services\UpdateWiltexCellphone;
use App\Services\UpdateWiltexData;
use App\Services\UpdateWiltexEmail;
use App\Services\UpdateWiltexPhone;
use App\Services\UpdateWiltexWhatsapp;
use App\Transformers\WiltexAddressTransformer;
use App\Transformers\WiltexCellphoneTransformer;
use App\Transformers\WiltexEmailTransformer;
use App\Transformers\WiltexPhoneTransformer;
use App\Transformers\WiltexTransformer;
use App\Transformers\WiltexWhatsappTransformer;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    protected $companyRepository;

    public function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

    public function showFirst()
    {
        $company = $this->companyRepository->first();
        $data = new WiltexTransformer();
        return response()->json($data->transform($company), 200);
    }

    public function updateFirst(Request $request)
    {
        $data = $request->all();
        $useCase = new UpdateWiltexData($this->companyRepository);
        $useCase->execute($data);
        return response()->json(['success' => true], 200);
    }

    public function showEmail(Request $request)
    {
        $company = $this->companyRepository->first();
        $data = new WiltexEmailTransformer();
        return response()->json($data->transform($company->emails), 200);
    }

    public function updateEmail(Request $request)
    {
        $data = $request->all();
        $useCase = new UpdateWiltexEmail();
        $useCase->execute($data);
        return response()->json(['success' => true], 200);
    }

    public function showAddress(Request $request)
    {
        $company = $this->companyRepository->first();
        $data = new WiltexAddressTransformer();
        return response()->json($data->transform($company->addresses), 200);
    }

    public function updateAddress(Request $request)
    {
        $data = $request->all();
        $useCase = new UpdateWiltexAddress();
        $useCase->execute($data);
        return response()->json(['success' => true], 200);
    }

    public function showPhone(Request $request)
    {
        $company = $this->companyRepository->first();
        $data = new WiltexPhoneTransformer();
        return response()->json($data->transform($company->phones), 200);
    }

    public function updatePhone(Request $request)
    {
        $data = $request->all();
        $useCase = new UpdateWiltexPhone();
        $useCase->execute($data);
        return response()->json(['success' => true], 200);
    }

    public function showCellphone(Request $request)
    {
        $company = $this->companyRepository->first();
        $data = new WiltexCellphoneTransformer();
        return response()->json($data->transform($company->cellphones), 200);
    }

    public function updateCellphone(Request $request)
    {
        $data = $request->all();
        $useCase = new UpdateWiltexCellphone();
        $useCase->execute($data);
        return response()->json(['success' => true], 200);
    }

    public function showWhatsapp(Request $request)
    {
        $company = $this->companyRepository->first();
        $data = new WiltexWhatsappTransformer();
        return response()->json($data->transform($company->whatsapps), 200);
    }

    public function updateWhatsapp(Request $request)
    {
        $data = $request->all();
        $useCase = new UpdateWiltexWhatsapp();
        $useCase->execute($data);
        return response()->json(['success' => true], 200);
    }
}
