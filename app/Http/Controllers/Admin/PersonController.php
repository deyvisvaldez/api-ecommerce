<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\PersonRepository;
use App\Services\CreateNewUser;
use App\Services\UpdateAddress;
use App\Services\UpdateCellphone;
use App\Services\UpdateEmail;
use App\Services\UpdatePhone;
use App\Services\UpdateUser;
use App\Services\UpdateWhatsapp;
use App\Transformers\UserTransformer;
use App\Transformers\UsersTransformer;
use Illuminate\Http\Request;
use Uuid;

class PersonController extends Controller
{
    protected $personRepository;

    public function __construct(PersonRepository $personRepository)
    {
        $this->personRepository = $personRepository;
    }

    public function index()
    {
        $people = $this->personRepository->users();
        $data = new UsersTransformer();
        return response()->json($data->transform($people), 200);
    }

    public function show($personId)
    {
        $person = $this->personRepository->withUser($personId);
        $data = new UserTransformer();
        return response()->json($data->transform($person), 200);
    }

    public function getPhones($personId)
    {
        $person = $this->personRepository->phones($personId);
        $phones = [];
        $checked = 0;
        foreach ($person->phones as $key => $phone) {
            $phones[] = [
                'id' => $phone->id,
                'phone' => $phone->phone
            ];
            if ($phone->main) {
                $checked = $phone->id;
            }
        }
        return response()->json(['data' => ['phones' => $phones, 'checked' => $checked]], 200);
    }

    public function getCellphones($personId)
    {
        $person = $this->personRepository->cellphones($personId);
        $cellphones = [];
        $checked = 0;
        foreach ($person->cellphones as $key => $cellphone) {
            $cellphones[] = [
                'id' => $cellphone->id,
                'cellphone' => $cellphone->cellphone
            ];
            if ($cellphone->main) {
                $checked = $cellphone->id;
            }
        }
        return response()->json(['data' => ['cellphones' => $cellphones, 'checked' => $checked]], 200);
    }

    public function getEmails($personId)
    {
        $person = $this->personRepository->emails($personId);
        $emails = [];
        $checked = 0;
        foreach ($person->emails as $key => $email) {
            $emails[] = [
                'id' => $email->id,
                'email' => $email->email
            ];

            if ($email->main) {
                $checked = $email->id;
            }
        }
        return response()->json(['data' => ['emails' => $emails, 'checked' => $checked]], 200);
    }

    public function getAddresses($personId)
    {
        $person = $this->personRepository->addresses($personId);
        $addresses = [];
        $checked = 0;
        foreach ($person->addresses as $key => $address) {
            $addresses[] = [
                'id' => $address->id,
                'address' => $address->address
            ];

            if ($address->main) {
                $checked = $address->id;
            }
        }
        return response()->json(['data' => ['addresses' => $addresses, 'checked' => $checked]], 200);
    }

    public function getWhatsapps($personId)
    {
        $person = $this->personRepository->whatsapps($personId);
        $emails = [];
        $checked = 0;
        foreach ($person->whatsapps as $key => $whatsapp) {
            $whatsapps[] = [
                'id' => $whatsapp->id,
                'whatsapp' => $whatsapp->whatsapp
            ];

            if ($whatsapp->main) {
                $checked = $whatsapp->id;
            }
        }
        return response()->json(['data' => ['whatsapps' => $whatsapps, 'checked' => $checked]], 200);
    }

    public function store(RegisterUserRequest $request)
    {
        $data = $request->all();
        $useCase = new CreateNewUser();
        $useCase->execute($data);
        return response()->json(['success' => true], 201);
    }

    public function update(UpdateUserRequest $request, $personId)
    {
        $data = $request->all();
        $useCase = new UpdateUser();
        $useCase->execute($data, $personId);
        return response()->json(['success' => true], 200);
    }

    public function publish($personId)
    {
        $person = $this->personRepository->withUser($personId);
        $person->user->activated = ! $person->user->activated;
        $person->user->save();
        return response()->json(['success' => true], 200);
    }

    public function updateEmail(Request $request, $personId)
    {
        $data = $request->all();
        $useCase = new UpdateEmail();
        $useCase->execute($data, $personId);
        return response()->json(['success' => true], 200);
    }

    public function updatePhone(Request $request, $personId)
    {
        $data = $request->all();
        $useCase = new UpdatePhone();
        $useCase->execute($data, $personId);
        return response()->json(['success' => true], 200);
    }

    public function updateCellphone(Request $request, $personId)
    {
        $data = $request->all();
        $useCase = new UpdateCellphone();
        $useCase->execute($data, $personId);
        return response()->json(['success' => true], 200);
    }

    public function updateAddress(Request $request, $personId)
    {
        $data = $request->all();
        $useCase = new UpdateAddress();
        $useCase->execute($data, $personId);
        return response()->json(['success' => true], 200);
    }

    public function updateWhatsapp(Request $request, $personId)
    {
        $data = $request->all();
        $useCase = new UpdateWhatsapp();
        $useCase->execute($data, $personId);
        return response()->json(['success' => true], 200);
    }
}
