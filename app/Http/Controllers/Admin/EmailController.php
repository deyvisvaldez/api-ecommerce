<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Email;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    public function delete($id)
    {
        $email = Email::find($id);
        $email->delete();
        return response()->json(['success' => true], 200);
    }
}
