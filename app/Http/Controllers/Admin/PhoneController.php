<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Phone;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PhoneController extends Controller
{
    public function delete($id)
    {
        $phone = Phone::find($id);
        $phone->delete();
        return response()->json(['success' => true], 200);
    }
}
