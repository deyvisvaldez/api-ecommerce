<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Company;
use App\Entities\Email;
use App\Entities\Person;
use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Mail\ResetPasswordEmail;
use App\Repositories\UserRepository;
use App\Transformers\CurrentUserTransformer;
use Illuminate\Http\Request;
use Mail;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function CurrentUser(Request $request)
    {
        $userId = $request->user()->id;
        $user = $this->userRepository->completeData($userId);
        $data = new CurrentUserTransformer();
        return response()->json($data->transform($user), 200);
    }

    public function update(Request $request, $personId)
    {
        $data = $request->all();
        $person = Person::with('user')->find($personId);

        if ($request->has('password')) {
            $person->user->password = $request->password;
            $person->user->save();
        }

        $person->update($data);

        return response()->json(['success' => true], 200);
    }

    public function password(Request $request, $personId)
    {
        $user = $this->userRepository->findByPersonId($personId);
        $user->password = $request->input('password');
        $user->save();
        return response()->json(['success' => true], 200);
    }

    public function publish(Request $request, $personId)
    {
        $user = $this->userRepository->findByPersonId($personId);
        $user->activated = ! $user->activated;
        $user->save();
        return response()->json(['success' => true], 200);
    }
}
