<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\TransportRepository;
use App\Services\RegisterNewTransport;
use App\Services\UpdateTransport;
use App\Transformers\TransportTransformer;
use Illuminate\Http\Request;

class TransportController extends Controller
{
    protected $transportRepository;

    public function __construct(TransportRepository $transportRepository)
    {
        $this->transportRepository = $transportRepository;
    }

    public function show($transportId)
    {
        $transport = $this->transportRepository->find($transportId);
        $data = new TransportTransformer();
        return response()->json($data->transform($transport), 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $useCase = new RegisterNewTransport($this->transportRepository);
        $useCase->execute($data);
        return response()->json(['success' => true], 201);
    }

    public function update(Request $request, $transportId)
    {
        $data = $request->all();
        $useCase = new UpdateTransport($this->transportRepository);
        $useCase->execute($transportId, $data);
        return response()->json(['success' => true], 200);
    }
}
