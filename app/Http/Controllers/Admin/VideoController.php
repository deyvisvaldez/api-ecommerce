<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Video;
use App\Http\Controllers\Controller;
use App\Repositories\VideoRepository;
use App\Transformers\CompanyVideosTransformer;
use App\Transformers\VideoTransformer;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    protected $videoRepository;

    public function __construct(VideoRepository $videoRepository)
    {
        $this->videoRepository = $videoRepository;
    }

    public function companyVideos()
    {
        $videos = $this->videoRepository->companyVideos();
        $data = new CompanyVideosTransformer();
        return response()->json($data->transform($videos), 200);
    }

    public function show($videoId)
    {
        $video = $this->videoRepository->find($videoId);
        $data = new VideoTransformer();
        return response()->json($data->transform($video), 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['title'] = $data['name'];
        $data['model_id'] = 1;
        $data['model_type'] = 1;
        $data['published'] = true;

        Video::create($data);

        return response()->json(["success" => true], 200);
    }

    public function update(Request $request, $videoId)
    {
        $data = $request->all();
        $data['title'] = $data['name'];

        $video = Video::find($videoId);
        $video->update($data);

        return response()->json(['success' => true], 200);
    }

     public function publish($videoId)
    {
        $video = Video::find($videoId);

        $video->published = ! (boolean) $video->published;
        $video->save();

        return response()->json(['success' => true], 200);
    }

    public function delete($videoId)
    {
        $video = Video::find($videoId);
        $video->delete();

        return response()->json(['success' => true],200);
    }
}
