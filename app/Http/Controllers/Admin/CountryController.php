<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\CountryRepository;
use App\Services\RegisterNewCountry;
use App\Services\UpdateCountry;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    protected $countryRepository;

    public function __construct(CountryRepository $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    public function show($countryId)
    {
        $country = $this->countryRepository->find($countryId);
        $data = new CountryTransformer();
        return response()->json($data->transform($country), 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $useCase = new RegisterNewCountry($this->countryRepository);
        $useCase->execute($data);
        return response()->json(['success' => true], 201);
    }

    public function update(Request $request, $countryId)
    {
        $data = $request->all();
        $useCase = new UpdateCountry($this->countryRepository);
        $useCase->execute($data, $countryId);
        return response()->json(['success' => true], 200);
    }
}
