<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Size;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\ItemRepository;
use App\Repositories\PresentationRepository;
use App\Services\DeleteItem;
use App\Services\GetItemColors;
use App\Services\GetItemSizes;
use App\Services\NewPresentationsBasedOnColor;
use App\Services\NewPresentationsBasedOnSize;
use App\Services\RegisterNewItem;
use App\Services\UpdateItem;
use App\Services\UploadReferencialPhoto;
use App\Transformers\AdminItemProfileTransformer;
use App\Transformers\AdminItemsTransformer;
use App\Transformers\ItemMatrixTransformer;
use App\Transformers\ModalPresentationsTransformer;
use App\Transformers\PresentationTagsTransformer;
use Illuminate\Http\Request;
use DB;

class ItemController extends Controller
{
    protected $itemRepository;
    protected $presentationRepository;

    public function __construct(ItemRepository $itemRepository, PresentationRepository $presentationRepository)
    {
        $this->itemRepository = $itemRepository;
        $this->presentationRepository = $presentationRepository;
    }

    public function index(Request $request)
    {
        $filters = $request->all();
        $items = $this->itemRepository->admin($filters);
        $data = new AdminItemsTransformer();
        return response()->json($data->transform($items), 200);
    }

    public function show($itemSlug)
    {
        $item = $this->itemRepository->findBySlug($itemSlug);
        $data = new AdminItemProfileTransformer();
        return response()->json($data->transform($item), 200);
    }

    public function store(Request $request)
    {
        $userId = $request->user()->id;
        $data = $request->all();

        $itemSlug = '';
        //DB::transaction(function () use ($data, $userId, $itemSlug){

            $useCase = new RegisterNewItem($this->itemRepository);
            $item = $useCase->execute($data, $userId);

            $itemSlug = $item->slug;

            if (count($data['photos'])) {
                $upload = new UploadReferencialPhoto();
                $upload->execute($item, $data['photos'][0]);
            }

        //});

        return response()->json(['success' => true, 'slug' => $itemSlug], 201);
    }

    public function modalInventories(Request $request, $itemId)
    {
        $priceType = $request->type;
        $presentations = $this->presentationRepository->itemPresentations($itemId, $priceType);
        $data = new ModalPresentationsTransformer();
        return response()->json($data->transform($presentations, $priceType), 200);
    }

    public function storeModalColor(Request $request, $itemId, $colorId)
    {
        $sizes = $request->sizes;
        $priceType = $request->type;
        if (! count($sizes)) {
            return response()->json(['success' => true, 'colorId' => $colorId], 200);
        }
        $userId = $request->user()->id;
        $useCase = new NewPresentationsBasedOnColor();
        $useCase->execute($sizes, $itemId, $colorId, $priceType, $userId);
        return response()->json(['success' => true], 200);
    }

    public function storeModalSize(Request $request, $itemId, $sizeId)
    {
        $colors = $request->colors;
        $priceType = $request->type;
        if (! count($colors)) {
            return response()->json(['success' => true, 'sizeId' => $sizeId], 200);
        }
        $userId = $request->user()->id;
        $useCase = new NewPresentationsBasedOnSize();
        $useCase->execute($colors, $itemId, $sizeId, $priceType, $userId);
        return response()->json(['success' => true], 200);
    }

    public function update(Request $request, $itemId)
    {
        $userId = $request->user()->id;
        $data = $request->all();
        $useCase = new UpdateItem($this->itemRepository);
        $item = $useCase->execute($data, $itemId, $userId);
        return response()->json(['success' => true], 200);
    }

    public function updateInvetoriesAvailability(Request $request, $itemId)
    {
        $colorId = $request->color_id;
        $sizeId = $request->size_id;

        $presentation = $this->presentationRepository->findByAttributes($itemId, $colorId, $sizeId);
        $presentation->available = ! $presentation->available;
        $presentation->save();

        return response()->json(['success' => true], 200);
    }

    public function delete($itemId)
    {
        $useCase = new DeleteItem();
        $useCase->execute($itemId);
        return response()->json(['success' => true], 200);
    }

    public function inventories(Request $request, $itemSlug)
    {
        $locationId = $request->input('locationId');
        $size = $request->input('size');

        $item = $this->itemRepository->profileByLocation($itemSlug, $locationId);

        $itemColors = $this->itemRepository->findWithColors($itemSlug);

        if ($size == 'all') {
            $sizes = Size::orderBy('order', 'asc')->get();
            foreach ($sizes as $key => $size) {
                $allSizes[] = [
                    'id' => (int) $size->id,
                    'size' => $size->name,
                    'order' => (int) $size->order
                ];
            }
        } else {
            $sizes = new GetItemSizes();
            $allSizes = $sizes->execute($item);
        }

        $colors = new GetItemColors();
        $allColors = $colors->execute($itemColors);

        $data = new ItemMatrixTransformer();
        if ($locationId) {
            return response()->json($data->transform($item, $allSizes, $allColors), 200);
        }
        return response()->json($data->transform($item, $allSizes, $allColors, true), 200);
    }

    public function getTags($itemId)
    {
        $presentations = $this->presentationRepository->presentationTags($itemId);
        $data = new PresentationTagsTransformer();
        return response()->json($data->transform($presentations), 200);
    }
}