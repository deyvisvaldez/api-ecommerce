<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\AccountRepository;
use App\Services\AddCompanyBankAccount;
use App\Services\UpdateCompanyBankAccount;
use App\Transformers\AccountsAdminTransformer;
use App\Transformers\AccountsTransformer;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    protected $accountRepository;

    public function __construct(AccountRepository $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }

    public function adminIndex()
    {
        $accounts = $this->accountRepository->all();
        $data = new AccountsAdminTransformer();

        return response()->json($data->transform($accounts), 200);
    }

    public function show($accountId)
    {
        $account = $this->accountRepository->find($accountId);
        $data = new AccountTransformer();
        return response()->json($data->transform($account), 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $userCase = new AddCompanyBankAccount();
        $userCase->execute($data);
        return response()->json(['success' => true], 201);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $userCase = new UpdateCompanyBankAccount($this->accountRepository);
        $userCase->execute($data, $id);
        return response()->json(['success' => true], 200);
    }

    public function publish(Request $request, $id)
    {
        $data = $request->all();
        $userCase = new UpdateCompanyBankAccount($this->accountRepository);
        $userCase->execute($data, $id);
        return response()->json(['success' => true], 200);
    }

    public function destroy($id)
    {
        $this->accountRepository->delete($id);
        return response()->json(['success' => true], 200);
    }
}
