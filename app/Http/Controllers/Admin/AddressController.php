<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Address;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function delete($id)
    {
        $address = Address::find($id);
        $address->delete();
        return response()->json(['success' => true], 200);
    }
}
