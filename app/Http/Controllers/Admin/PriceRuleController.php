<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Item;
use App\Entities\Price;
use App\Entities\PriceRule;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Uuid;

class PriceRuleController extends Controller
{
    public function index(Request $request, $itemId)
    {
        $priceType = $request->type;

        switch ($priceType) {
            case 1:
                $item = Item::with('presentations.web_prices.price_rules')->find($itemId);
                $priceRuleIds = [];
                foreach ($item->presentations[0]->web_prices as $key => $web_price) {
                    $priceRuleIds[] = $web_price->price_rules[0]->id;
                }
                $priceRuleIds;
                break;

            case 2:
                $item = Item::with('presentations.shop_prices.price_rules')->find($itemId);
                $priceRuleIds = [];
                foreach ($item->presentations[0]->shop_prices as $key => $shop_price) {
                    $priceRuleIds[] = $shop_price->price_rules[0]->id;
                }
                $priceRuleIds;
                break;

            case 3:
                $item = Item::with('presentations.wholesale_prices.price_rules')->find($itemId);
                $priceRuleIds = [];
                foreach ($item->presentations[0]->wholesale_prices as $key => $wholesale_price) {
                    $priceRuleIds[] = $wholesale_price->price_rules[0]->id;
                }
                $priceRuleIds;
                break;
        }

        $priceRules = PriceRule::whereNotIn('id', $priceRuleIds)->orderBy('quantity', 'asc')->get();

        $pRs = [];
        foreach ($priceRules as $key => $priceRule) {
            $pRs[] = [
                'id' => (int) $priceRule->id,
                'name' => $priceRule->name,
                'quantity' => (int) $priceRule->quantity
            ];
        }
        return response()->json(['data' => $pRs], 200);
    }

    public function show($priceRuleId)
    {
        $priceRule = PriceRule::find($priceRuleId);
        return response()->json([
            'data' => [
                'name' => $priceRule->name,
                'quantity' => (int) $priceRule->quantity
            ]
        ], 200);
    }

    public function store(Request $request, $itemId)
    {
        $data = $request->all();
        $item = Item::with('presentations')->find($itemId);

        if ($data['price_rule_id'] > 0) {
            $priceRule = PriceRule::find($data['price_rule_id']);
        } else {
            $count = PriceRule::whereQuantity($data['quantity'])->count();

            if ($count) {
                return response()->json(['success' => false, 'message' => 'Ya existe una regla con esa cantidad'], 400);
            } else {
                $priceRule = PriceRule::create([
                    'name' => $data['name'],
                    'quantity' => $data['quantity']
                ]);
            }
        }

        if ($priceRule->quantity === 1) {
            $referencialPrice = $item->referencial_price;
        } else {
            $referencialPrice = 0;
        }

        foreach ($item->presentations as $key => $presentation) {

            $price = Price::create([
                'uuid' => Uuid::generate(4)->string,
                'name' => 'Precio Web',
                'price' => $referencialPrice,
                'type' => $data['type'],
                'currency_id' => 1,
                'promotional_price' => 0,
                'available_promotion' => false,
                'active' => true,
                'user_id' => 1,
                'presentation_id' => $presentation->id,
                'company_id' => 1,
            ]);

            $price->price_rules()->attach($priceRule->id);
        }
        return response()->json(['success' => true], 201);
    }

    public function update(Request $request, $priceRuleId)
    {
        $data = $request->all();
        $otherRule = PriceRule::where('id', '!=', $priceRuleId)->whereQuantity($data['quantity'])->count();
        if ($otherRule) {
            return response()->json(['success' => false, 'message' => 'Ya existe una regla con esa cantidad'], 400);
        }
        $price = PriceRule::find($priceRuleId);
        $price->update($data);

        return response()->json(['success' => true], 200);
    }
}
