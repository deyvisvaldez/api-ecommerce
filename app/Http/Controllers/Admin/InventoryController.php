<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\InventoryRepository;
use App\Repositories\PresentationRepository;
use App\Services\AdjustInventoriesStock;
use App\Services\DeleteInventories;
use App\Services\InventoriesProductionInput;
use App\Services\MoveInventoriesStock;
use App\Services\RegisterNewInventories;
use App\Transformers\InventoryMovementsTransformer;
use App\Transformers\LocationsStockTransformer;
use App\Transformers\PresentationImagesTransformer;
use App\Transformers\UpdateSearchTransformer;
use Illuminate\Http\Request;

class InventoryController extends Controller
{
    protected $itemRepository;
    protected $presentationRepository;

    public function __construct(PresentationRepository $presentationRepository, InventoryRepository $inventoryRepository)
    {
        $this->inventoryRepository = $inventoryRepository;
        $this->presentationRepository = $presentationRepository;
    }

    public function images($itemId, $colorId)
    {
        $inventory = $this->presentationRepository->firstImages($itemId, $colorId);
        $data = new PresentationImagesTransformer();
        return response()->json($data->transform($inventory->photos), 200);
    }

    public function stockAdjustment(Request $request)
    {
        $presentationIds = $request->inventories;
        $locationId = $request->location_id;
        $userId = $request->user()->id;
        $useCase = new AdjustInventoriesStock($this->inventoryRepository);
        $useCase->execute($presentationIds, $locationId, $userId);

        return response()->json(['success' => true], 200);
    }

    public function movements(Request $request, $presentationId)
    {
        $date = $request->date;
        $locationId = $request->location_id;
        $inventory = $this->inventoryRepository->withMovements($presentationId, $locationId, $date);
        $data = new InventoryMovementsTransformer();
        if ($inventory) {
            return response()->json($data->transform($inventory->movements), 200);
        }
        return response()->json(['data' => []], 200);
    }

    public function otherStocks(Request $request, $presentationId)
    {
        $locationId = $request->input('locationId');
        $inventories = $this->inventoryRepository->filterLocation($presentationId, $locationId);

        $data = new LocationsStockTransformer();
        return response()->json($data->transform($inventories), 200);
    }

    public function stockMovement(Request $request)
    {
        $presentationIds = $request->inventories;
        $description = $request->description;
        $origin = $request->location_id;
        $userId = $request->user()->id;

        $useCase = new MoveInventoriesStock($this->inventoryRepository);
        $useCase->execute($presentationIds, $description, $origin, $userId);

        return response()->json(['success' => true], 201);
    }

    public function delete(Request $request)
    {
        $presentationIds = $request->inventories;
        $locationId = $request->location_id;

        $useCase = new DeleteInventories($this->inventoryRepository);
        $useCase->execute($presentationIds, $locationId);

        return response()->json(['success' => true], 200);
    }

    public function production(Request $request)
    {
        $userId = $request->user()->id;
        $presentationIds = $request->inventories;
        $description = $request->description;
        $locationId = $request->location_id;

        $useCase = new InventoriesProductionInput($this->inventoryRepository);
        $useCase->execute($presentationIds, $description, $locationId, $userId);

        return response()->json(['success' => true], 200);
    }

    public function searchUpdate(Request $request)
    {
        $locationId = $request->input('locationId');
        $origin = $request->input('origin');
        $inventory = $this->inventoryRepository->search($locationId);
        $data = new UpdateSearchTransformer();
        return response()->json($data->transform($inventory, $origin), 200);
    }

    public function store(Request $request, $itemId)
    {
        $data = $request->all();
        $userId = $request->user()->id;
        $useCase = new RegisterNewInventories(/*$this->itemRepository*/);
        $inventory = $useCase->execute($data, $itemId, $userId, true);

        return response()->json(['success' => true], 201);
    }
}