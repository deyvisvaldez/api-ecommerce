<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\OrderRepository;
use App\Services\CancelOrders;
use App\Services\ConfirmOrders;
use App\Services\DeliverOrder;
use App\Transformers\OrderDetailTransformer;
use App\Transformers\OrderTransformer;
use App\Transformers\OrdersTransformer;
use Illuminate\Http\Request;
use Mail;

class OrderController extends Controller
{
    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function index(Request $request)
    {
        $filters = $request->all();
        $orders = $this->orderRepository->all($filters);
        $data = new OrdersTransformer();
        return response()->json($data->transform($orders), 200);
    }

    public function show($orderCode)
    {
        $order = $this->orderRepository->findByCode($orderCode);
        $data = new OrderTransformer();
        return response()->json($data->transform($order), 200);
    }

    public function orderDetail(Request $request, $orderCode)
    {
        $locationId = $request->location_id;
        $locationId = 1;
        $order = $this->orderRepository->findByCode($orderCode, $locationId);
        $data = new OrderDetailTransformer();
        return response()->json($data->transform($order), 200);
    }

    public function cancel(Request $request, $orderCode)
    {
        $userId = $request->user()->id;

        $useCase = new CancelOrders($this->orderRepository);
        $useCase->execute($orderCode, $userId);

        return response()->json(['success' => true], 200);
    }

    public function confirm(Request $request, $orderCode)
    {
        $userId = $request->user()->id;
        $message = $request->input('message');
        $newEmail = $request->input('email');
        $currencyId = $request->input('currency_id');

        $useCase = new ConfirmOrders($this->orderRepository);
        $useCase->execute($orderCode, (string)$message, $newEmail, $currencyId, $userId);

        return response()->json(['success' => true], 200);
    }

    public function deliver(Request $request, $orderCode)
    {
        $userId = $request->user()->id;
        $locationId = $request->location_id;

        $useCase = new DeliverOrder();
        $useCase->execute($orderCode, $locationId, $userId);

        return response()->json(['success' => true], 200);
    }

    public function updateOrder(Request $request, $orderId)
    {
        $data = $request->all();
        $userId = $request->user()->id;
        $useCase = new UpdateOrder($this->orderRepository);
        $orderCode = $useCase->execute($data, $orderId, $userId);
        return response()->json(['success' => true, 'orderCode' => $orderCode], 200);
    }
}
