<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Price;
use App\Http\Controllers\Controller;
use App\Services\SetItemPromotionalPrice;
use Illuminate\Http\Request;
use Uuid;

class PriceController extends Controller
{
    public function show($priceId)
    {
        $price = Price::find($priceId);
        $fP = [
            'price' => (float) $price->price,
            'promotionalPrice' => (float) $price->promotional_price,
            'availablePromotion' => (boolean) $price->available_promotion
        ];
        return response()->json(['data' => $fP], 200);
    }

    public function update(Request $request, $priceId)
    {
        $data = $request->all();
        $price = Price::find($priceId);
        $price->update($data);

        $useCase = new SetItemPromotionalPrice();
        $useCase->execute($price->presentation->item_id);

        return response()->json(['succes' => true], 200);
    }
}
