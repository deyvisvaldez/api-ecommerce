<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\CategoryRepository;
use App\Services\RegisterNewCategory;
use App\Services\UpdateCategory;
use App\Transformers\CategoriesAdminTransformer;
use App\Transformers\CategoryTransformer;
use App\Transformers\EditSubcategoryTransformer;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function admin()
    {
        $categories = $this->categoryRepository->adminMenu();
        $data = new CategoriesAdminTransformer();
        return response()->json($data->transform($categories), 200);
    }

    public function show($categoryId)
    {
        $category = $this->categoryRepository->find($categoryId);
        $data = new CategoryTransformer();
        return response()->json($data->transform($category), 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $useCase = new RegisterNewCategory($this->categoryRepository);
        $useCase->execute($data);
        return response()->json(['success' => true], 201);
    }

    public function showSubcategory($categoryId, $subcategoryId)
    {
        $category = $this->categoryRepository->findSubcategory($categoryId, $subcategoryId);
        $data = new EditSubcategoryTransformer();
        return response()->json($data->transform($category->subcategories), 200);
    }

    public function update(Request $request, $categoryId)
    {
        $data = $request->all();
        $useCase = new UpdateCategory($this->categoryRepository);
        $useCase->execute($data, $categoryId);
        return response()->json(['success' => true], 200);
    }
}
