<?php

namespace App\Http\Controllers\Admin;

use App\Entities\PriceType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Uuid;

class PriceTypeController extends Controller
{
    public function index()
    {
        $priceTypes = PriceType::whereAvailable(true)->get();
        $pTs = [];
        foreach ($priceTypes as $key => $priceType) {
            $pTs[] = [
                'id' => (int) $priceType->id,
                'name' => $priceType->name,
            ];
        }
        return response()->json(['data' => $pTs], 200);
    }


}
