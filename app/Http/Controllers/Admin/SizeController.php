<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\SizeRepository;
use App\Services\CreateSize;
use App\Services\UpdateSize;
use Illuminate\Http\Request;

class SizeController extends Controller
{
    protected $sizeRepository;

    public function __construct(SizeRepository $sizeRepository)
    {
        $this->sizeRepository = $sizeRepository;
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $size = $this->sizeRepository->findByName($data['name']);
        if ($size) {
            return response()->json(['success' => false, 'message' => 'Esta Talla ya existe.'], 400);
        }
        $useCase = new CreateSize($this->sizeRepository);
        $useCase->execute($data);

        return response()->json(['success' => true], 201);
    }

    public function update(Request $request, $sizeId)
    {
        $data = $request->all();
        $size = $this->sizeRepository->findExistence($data['name'], $sizeId);
        if ($size) {
            return response()->json(['success' => false, 'message' => 'Esta Talla ya existe.'], 400);
        }
        $useCase = new UpdateSize($this->sizeRepository);
        $useCase->execute($data, $sizeId);

        return response()->json(['success' => true], 200);
    }
}
