<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\SubcategoryRepository;
use App\Services\RegisterNewSubcategory;
use App\Services\UpdateSubcategory;
use App\Transformers\SubcategoriesAdminTransformer;
use App\Transformers\SubcategoryTransformer;
use Illuminate\Http\Request;

class SubcategoryController extends Controller
{
    protected $subcategoryRepository;

    public function __construct(SubcategoryRepository $subcategoryRepository)
    {
        $this->subcategoryRepository = $subcategoryRepository;
    }

    public function index()
    {
        $subcategories = $this->subcategoryRepository->all();
        $data = new SubcategoriesAdminTransformer();
        return response()->json($data->transform($subcategories), 200);
    }

    public function show($slug)
    {
        $subcategory = $this->subcategoryRepository->findBySlug($slug);
        $data = new SubcategoryTransformer();
        return response()->json($data->transform($subcategory), 200);
    }

    public function store(Request $request, $categoryId)
    {
        $data = $request->all();
        $useCase = new RegisterNewSubcategory($this->subcategoryRepository);
        $useCase->execute($data, $categoryId);
        return response()->json(['success' => true], 201);
    }

    public function update(Request $request, $subcategoryId)
    {
        $data = $request->all();
        $useCase = new UpdateSubcategory($this->subcategoryRepository);
        $useCase->execute($data, $subcategoryId);
        return response()->json(['success' => true], 200);
    }
}