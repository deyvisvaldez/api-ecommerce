<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Whatsapp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WhatsappController extends Controller
{
    public function delete($id)
    {
        $whatsapp = Whatsapp::find($id);
        $whatsapp->delete();
        return response()->json(['success' => true], 200);
    }
}
