<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\ColorRepository;
use App\Services\CreateColor;
use App\Services\UpdateColor;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    protected $colorRepository;

    public function __construct(ColorRepository $colorRepository)
    {
        $this->colorRepository = $colorRepository;
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $color = $this->colorRepository->findByName($data['name']);
        if ($color) {
            return response()->json(['success' => false, 'message' => 'Esta Color ya existe.'], 400);
        }

        $useCase = new CreateColor($this->colorRepository);
        $useCase->execute($data);

        return response()->json(['success' => true], 201);
    }

    public function update(Request $request, $colorId)
    {
        $data = $request->all();
        $color = $this->colorRepository->findExistence($data['name'], $colorId);
        if ($color) {
            return response()->json(['success' => false, 'message' => 'Este Color ya existe.'], 400);
        }

        $useCase = new UpdateColor($this->colorRepository);
        $useCase->execute($data, $colorId);

        return response()->json(['success' => true], 200);
    }
}
