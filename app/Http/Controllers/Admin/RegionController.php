<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\BranchOfficeRepository;
use App\Repositories\RegionRepository;
use App\Repositories\RuleRepository;
use App\Services\RegisterNewRegion;
use App\Services\RegisterRegionDestinations;
use App\Services\SetRegionTransportCost;
use App\Transformers\RegionDestinationsTransformer;
use App\Transformers\RegionTransportPriceTransformer;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    protected $regionRepository;
    protected $ruleRepository;
    protected $branchOfficeRepository;

    public function __construct(RegionRepository $regionRepository, RuleRepository $ruleRepository, BranchOfficeRepository $branchOfficeRepository)
    {
        $this->regionRepository = $regionRepository;
        $this->ruleRepository = $ruleRepository;
        $this->branchOfficeRepository = $branchOfficeRepository;
    }

    public function prices(Request $request)
    {
        $countryId = $request->input('countryId');
        $transportId = $request->input('transportId');
        $regions = $this->regionRepository->getWithPrices($countryId, $transportId);
        $rules = $this->ruleRepository->all();

        $data = new RegionTransportPriceTransformer();
        return response()->json($data->transform($regions, $rules), 200);
    }

    public function storePrices(Request $request)
    {
        $data = $request->all();
        $useCase = new SetRegionTransportCost($this->branchOfficeRepository);
        $useCase->execute($data);
        return response()->json(['success' => true], 201);
    }

    public function updatePrices(Request $request)
    {
        $data = $request->all();
        $useCase = new SetRegionTransportCost($this->branchOfficeRepository);
        $useCase->execute($data);
        return response()->json(['success' => true], 201);
    }

    public function destinations($regionId)
    {
        $region = $this->regionRepository->find($regionId);
        $data = new RegionDestinationsTransformer();
        return response()->json($data->transform($region), 200);
    }

    public function storeDestinations(Request $request, $regionId)
    {
        $data = $request->input('branchOffices');
        $useCase = new RegisterRegionDestinations($this->regionRepository);
        $useCase->execute($regionId, $data);
        return response()->json(['succes' => true], 201);
    }

    public function deleteDestinations($destinationId)
    {
        $this->branchOfficeRepository->delete($destinationId);
        return response()->json(['succes' => true], 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $useCase = new RegisterNewRegion($this->regionRepository);
        $useCase->execute($data);
        return response()->json(['success' => true], 201);
    }

    public function update(Request $request, $regionId)
    {
        $data = $request->all();
        $useCase = new UpdateRegion($this->regionRepository);
        $useCase->execute($data, $regionId);
        return response()->json(['success' => true], 200);
    }
}
