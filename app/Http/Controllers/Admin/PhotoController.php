<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\PhotoRepository;
use App\Services\DeleteCompanyImage;
use App\Services\SaveCompanyImage;
use App\Services\UpdateCompanyImage;
use App\Transformers\CompanyPhotosTransformer;
use App\Transformers\PhotoTransformer;
use Illuminate\Http\Request;

class PhotoController extends Controller
{
    protected $photoRepository;

    public function __construct(PhotoRepository $photoRepository)
    {
        $this->photoRepository = $photoRepository;
    }

    public function companyPhotos()
    {
        $photos = $this->photoRepository->companyPhotos();
        $data = new CompanyPhotosTransformer();
        return response()->json($data->transform($photos), 200);
    }

    public function show($photoId)
    {
        $photo = $this->photoRepository->find($photoId);
        $data = new PhotoTransformer();
        return response()->json($data->transform($photo), 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $recorder = new SaveCompanyImage($this->photoRepository);
        $recorder->execute($data);

        return response()->json(["success" => true], 200);
    }

    public function update(Request $request, $photoId)
    {
        $data = $request->all();

        $recorder = new UpdateCompanyImage($this->photoRepository);
        $recorder->execute($data, $photoId);

        return response()->json(['success' => true], 200);
    }

     public function publish($photoId)
    {
        $photo = $this->photoRepository->find($photoId);

        $photo->published = ! (boolean) $photo->published;
        $photo->save();

        return response()->json(['success' => true], 200);
    }

    public function delete($photoId)
    {
        $deleter = new DeleteCompanyImage($this->photoRepository);
        $deleter->execute($photoId);

        return response()->json(['success' => true],200);
    }
}
