<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\ImageRepository;
use App\Services\DeleteImages;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    protected $imageRepository;

    public function __construct(ImageRepository $imageRepository)
    {
        $this->imageRepository = $imageRepository;
    }

    public function delete($uuid)
    {
        $useCase = new DeleteImages($this->imageRepository);
        $useCase->execute($uuid);

        return response()->json(['success' => true], 200);
    }
}
