<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\LocationRepository;
use App\Services\RegisterNewLocation;
use App\Services\UpdateLocation;
use App\Transformers\LocationTransformer;
use App\Transformers\LocationsTransformer;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    protected $imageRepository;

    public function __construct(LocationRepository $locationRepository)
    {
        $this->locationRepository = $locationRepository;
    }

    public function index()
    {
        $locations = $this->locationRepository->all();
        $data = new LocationsTransformer();
        return response()->json($data->transform($locations), 200);
    }

    public function user()
    {
        $locations = $this->locationRepository->all();
        $data = new LocationsTransformer();
        return response()->json($data->transform($locations), 200);
    }

    public function show($locationId)
    {
        $location = $this->locationRepository->find($locationId);
        $data = new LocationTransformer();
        return response()->json($data->transform($location), 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $userId = $request->user()->id;
        $useCase = new RegisterNewLocation($this->locationRepository);
        $useCase->execute($data, $userId);
        return response()->json(['success' => true], 201);
    }

    public function update(Request $request, $locationId)
    {
        $data = $request->all();
        $locations = $this->locationRepository->all();
        if ($data['main']) {
            foreach ($locations as $key => $location) {
                $location->main = false;
                $location->save();
            }
        }
        $useCase = new UpdateLocation($this->locationRepository);
        $useCase->execute($locationId, $data);
        return response()->json(['success' => true], 200);
    }
}
