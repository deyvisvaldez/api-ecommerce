<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Tag;
use App\Http\Controllers\Controller;
//use App\Repositories\TagRepository;
use Illuminate\Http\Request;

class TagController extends Controller
{
    protected $tagRepository;

    /*public function __construct(TagRepository $tagRepository)
    {
        $this->tagRepository = $tagRepository;
    }*/

    public function index()
    {
        $tags = Tag::orderBy('order', 'asc')->get();
        $fT = [];
        foreach ($tags as $key => $tag) {
            $fT[] = [
                'id' => (int) $tag->id,
                'name' => $tag->name,
                'order' => (int) $tag->order
            ];
        }
        return response()->json(['data' => $fT], 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($data['name']);
        $tag = Tag::whereName($data['name'])->first();
        if ($tag) {
            return response()->json(['success' => false, 'message' => 'Esta etiqueta ya existe.'], 400);
        }

        Tag::create($data);

        return response()->json(['success' => true], 201);
    }

    public function show($tagId)
    {
        $tag = Tag::find($tagId);
        $fT = [
            'name' => $tag->name,
            'order' => (int) $tag->order,
            'published' => (boolean) $tag->published
        ];
        return response()->json(['data' => $fT], 200);
    }

    public function update(Request $request, $tagId)
    {
        $data = $request->all();
        $tag = Tag::where('id', '!=', $tagId)->whereName($data['name'])->first();
        if ($tag) {
            return response()->json(['success' => false, 'message' => 'Este tag ya existe.'], 400);
        }

        $tag = Tag::find($tagId);
        $data['slug'] = str_slug($data['name']);
        $tag->update($data);

        return response()->json(['success' => true], 200);
    }

    public function destroy($tagId)
    {
        $tag = Tag::find($tagId);
        $tag->delete();
        return response()->json(['success' => true], 200);
    }
}
