<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\InventoryRepository;
use App\Repositories\ItemRepository;
use App\Repositories\LocationRepository;
use App\Repositories\PresentationRepository;
use App\Services\AdjustInventoriesStock;
use App\Services\DeletePresentations;
use App\Services\DeleteSameColorPresentations;
use App\Services\DeleteSameSizePresentations;
use App\Services\DisableWebPromotions;
use App\Services\GetAllItemSizes;
use App\Services\GetPresentationSizes;
use App\Services\PresentationsPublished;
use App\Services\UpdatePresentationPrices;
use App\Services\UpdatePresentationsUnitaryPrices;
use App\Services\UpdateWebPromotions;
use App\Services\UploadPresentationPhoto;
use App\Transformers\InventoryMovementsTransformer;
use App\Transformers\PresentationPhotosTransformer;
use App\Transformers\PresentationsMatrixTransformer;
use App\Transformers\PresentationsProfileTransformer;
use App\Transformers\SameColorPresentationStockTransformer;
use Illuminate\Http\Request;

class PresentationController extends Controller
{
    protected $itemRepository;
    protected $presentationRepository;
    protected $inventoryRepository;
    protected $locationRepository;

    public function __construct(ItemRepository $itemRepository, PresentationRepository $presentationRepository, InventoryRepository $inventoryRepository, LocationRepository $locationRepository)
    {
        $this->itemRepository = $itemRepository;
        $this->presentationRepository = $presentationRepository;
        $this->inventoryRepository = $inventoryRepository;
        $this->locationRepository = $locationRepository;
    }

    public function index(Request $request)
    {
        $filters = $request->all();
        $size = $request->input('size');

        $items = $this->itemRepository->generalMatrix($filters);

        $sizes = new GetAllItemSizes();
        $allSizes = $sizes->execute($items);

        $items = $this->itemRepository->generalMatrix($filters, 5);
        $data = new PresentationsMatrixTransformer();
        if ($filters['locationId']) {
            return response()->json($data->transform($items, $allSizes), 200);
        }
        return response()->json($data->transform($items, $allSizes, true), 200);
    }

    public function profile(Request $request)
    {
        $presentationIds = $request->input('inventories');
        $locationId = $request->location_id;
        $items = $this->itemRepository->presentationProfile($presentationIds, $locationId);

        $data = new PresentationsProfileTransformer();
        return response()->json($data->transform($items), 200);
    }

    public function getPhotos($itemId, $colorId)
    {
        $presentation = $this->presentationRepository->photosByColor($itemId, $colorId);
        $data = new PresentationPhotosTransformer();
        return response()->json($data->transform($presentation->photos), 200);
    }

    public function uploadPhotos(Request $request, $itemId, $colorId)
    {
        $photo = $request->all();
        $presentations = $this->presentationRepository->sameItemAndColor($itemId, $colorId);

        $useCase = new UploadPresentationPhoto();
        $uuid = $useCase->execute($photo, $presentations);

        return response()->json(['success' => true, 'uuid' => $uuid], 201);
    }

    public function updatePrices(Request $request, $itemId)
    {
        $colors = $request->colors;
        $sizes = $request->sizes;
        $type = $request->type;
        $rule = $request->rule;
        $price = $request->price;
        $useCase = new UpdatePresentationPrices();
        $useCase->execute($itemId, $colors, $sizes, $type, $rule, $price);
        return response()->json(['success' => true]);
    }

    public function updateMatrixPrices(Request $request)
    {
        $presentationIds = $request->input('inventories');
        $allColors = $request->input('all');

        $useCase = new UpdatePresentationsUnitaryPrices($this->presentationRepository);
        $useCase->execute($presentationIds, $allColors);

        return response()->json(['success' => true], 200);
    }

    public function delete(Request $request)
    {
        $presentationIds = $request->inventories;
        $locationId = $request->location_id;

        $useCase = new DeletePresentations();
        $useCase->execute($presentationIds, $locationId);

        return response()->json(['success' => true], 200);
    }

    public function deleteSameColor($itemId, $colorId)
    {
        $quantity = $this->inventoryRepository->sameItemColorStock($itemId, $colorId);
        if ($quantity) {
            return response()->json(['success' => false, 'message' => 'Existen Inventarios con Stock'], 400);
        }
        $useCase = new DeleteSameColorPresentations($this->presentationRepository);
        $useCase->execute($itemId, $colorId);
        return response()->json(['success' => true], 200);
    }

    public function deleteSameSize($itemId, $sizeId)
    {
        $quantity = $this->inventoryRepository->sameItemSizeStock($itemId, $sizeId);
        if ($quantity) {
            return response()->json(['success' => false, 'message' => 'Existen Inventarios con Stock'], 400);
        }
        $useCase = new DeleteSameSizePresentations($this->presentationRepository);
        $useCase->execute($itemId, $sizeId);
        return response()->json(['success' => true], 200);
    }

    public function updateWebPromotion(Request $request)
    {
        $presentationIds = $request->input('inventories');

        $useCase = new UpdateWebPromotions($this->presentationRepository);
        $useCase->execute($presentationIds, 'web_prices');

        return response()->json(['success' => true], 200);
    }

    public function disableWebPromotion(Request $request)
    {
        $presentationIds = $request->input('inventories');

        $useCase = new DisableWebPromotions($this->presentationRepository);
        $useCase->execute($presentationIds, 'web_prices');

        return response()->json(['success' => true], 200);
    }

    public function published(Request $request)
    {
        $presentationIds = $request->input('inventories');
        $published = $request->input('published');

        $useCase = new PresentationsPublished($this->presentationRepository);
        $useCase->execute($presentationIds, $published);

        return response()->json(['success' => true], 200);
    }

    public function addTag(Request $request, $presentationId)
    {
        $tagId = $request->tag_id;
        $presentation = $this->presentationRepository->find($presentationId);
        $presentation->tags()->attach($tagId);

        return response()->json(['success' => true], 200);
    }

    public function removeTag($presentationId, $tagId)
    {
        $presentation = $this->presentationRepository->find($presentationId);
        $presentation->tags()->detach($tagId);

        return response()->json(['success' => true], 200);
    }

    public function stocks($itemId, $colorId)
    {
        $presentations = $this->presentationRepository->sameColorStock($itemId, $colorId);
        $sizes = new GetPresentationSizes();
        $sizes = $sizes->execute($presentations);
        $locations = $this->locationRepository->all();

        $data = new SameColorPresentationStockTransformer();
        return response()->json($data->transform($presentations, $sizes, $locations), 200);
    }
}