<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Cellphone;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CellphoneController extends Controller
{
    public function delete($id)
    {
        $cellphone = Cellphone::find($id);
        $cellphone->delete();
        return response()->json(['success' => true], 200);
    }
}
