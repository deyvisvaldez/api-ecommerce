<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Repositories\SizeRepository;
use App\Transformers\SizeTransformer;
use App\Transformers\SizesTransformer;
use Illuminate\Http\Request;

class SizeController extends Controller
{
    protected $sizeRepository;

    public function __construct(SizeRepository $sizeRepository)
    {
        $this->sizeRepository = $sizeRepository;
    }

    public function index()
    {
        $sizes = $this->sizeRepository->allOrder();
        $data = new SizesTransformer();

        return response()->json($data->transform($sizes), 200);
    }

    public function show($sizeId)
    {
        $size = $this->sizeRepository->find($sizeId);
        $data = new SizeTransformer();
        return response()->json($data->transform($size), 200);
    }
}
