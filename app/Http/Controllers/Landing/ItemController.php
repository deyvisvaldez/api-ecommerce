<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\ItemRepository;
use App\Repositories\PresentationRepository;
use App\Transformers\DistributorCatalogTransformer;
use App\Transformers\ItemProfileTransformer;
use App\Transformers\LastItemsTransformer;
use App\Transformers\RelatedItemsTransformer;
use App\Transformers\WebCatalogTransformer;
use App\Transformers\WeekCatalogTransformer;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    protected $itemRepository;
    protected $presentationRepository;

    public function __construct(ItemRepository $itemRepository, PresentationRepository $presentationRepository)
    {
        $this->itemRepository = $itemRepository;
        $this->presentationRepository = $presentationRepository;
    }

    public function index(Request $request)
    {
        $tag = $request->input('tag');
        $query = $request->input('query');
        $categoryId = $request->input('categoryId');
        $subcategoryId = $request->input('subcategoryId');
        //$subcategoryId = $request->subcategoryId;

        $lastWeekItems = $this->itemRepository->lastWeekItems($tag, $query, $categoryId, $subcategoryId);

        $ids = [];
        foreach ($lastWeekItems as $key => $item) {
            $ids[] = $item['id'];
        }

        $remainItems = $this->itemRepository->remainCatalog($ids, $tag, $query, $categoryId, $subcategoryId);

        $data = new WeekCatalogTransformer();
        return response()->json($data->transform($lastWeekItems, $remainItems), 200);
    }

    /*public function index(Request $request)
    {
        $filters = $request->all();
        $items = $this->itemRepository->webCatalog($filters, 12);
        $data = new WebCatalogTransformer();
        return response()->json($data->transform($items), 200);
    }*/

    public function last()
    {
        $items = $this->itemRepository->last(12);
        $data = new LastItemsTransformer();
        return response()->json($data->transform($items), 200);
    }

    public function show($itemSlug)
    {
        $item = $this->itemRepository->profile($itemSlug, 'web_prices');
        $data = new ItemProfileTransformer();
        return response()->json($data->transform($item, 'web_prices'), 200);
    }

    public function distributorIndex(Request $request)
    {
        $filters = $request->all();
        $items = $this->itemRepository->distributorCatalog($filters);
        $data = new DistributorCatalogTransformer();
        return response()->json($data->transform($items), 200);
    }

    public function showDistributor($itemSlug)
    {
        $item = $this->itemRepository->profile($itemSlug, 'wholesale_prices');
        $data = new ItemProfileTransformer();
        return response()->json($data->transform($item, 'wholesale_prices'), 200);
    }

    public function related($itemSlug)
    {
        $item = $this->itemRepository->findBySlug($itemSlug);
        $items = $this->itemRepository->related($item->subcategory_id, $item->id);
        $data = new RelatedItemsTransformer();
        return response()->json($data->transform($items), 200);
    }
}