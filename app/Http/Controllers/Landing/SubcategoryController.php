<?php

namespace App\Http\Controllers\Landing;

use App\Entities\Subcategory;
use App\Http\Controllers\Controller;
use App\Repositories\SubcategoryRepository;
use App\Transformers\SubcategoriesAdminTransformer;
use Illuminate\Http\Request;

class SubcategoryController extends Controller
{
    protected $subcategoryRepository;

    public function __construct(SubcategoryRepository $subcategoryRepository)
    {
        $this->subcategoryRepository = $subcategoryRepository;
    }

    public function collections()
    {
        $subcategories = Subcategory::with('categories')->wherePublished(true)->get();

        $colls = [];
        foreach ($subcategories as $key => $subcategory) {
            $colls[] = [
                'categoryName' => $subcategory->categories[0]->name,
                'categorySlug' => $subcategory->categories[0]->slug,
                'subcategorySlug' => $subcategory->slug,
                'subcategoryName' => $subcategory->name,
                'imageUrl' => ($subcategory->image) ? $subcategory->image : asset('defaults/product.png')
            ];
        }
        return response()->json(['data' => $colls], 200);
    }
}