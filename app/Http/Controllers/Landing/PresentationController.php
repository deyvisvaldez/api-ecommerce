<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\PresentationRepository;
use App\Transformers\GeneralCartTransformer;
use Illuminate\Http\Request;

class PresentationController extends Controller
{
    protected $presentationRepository;

    public function __construct(PresentationRepository $presentationRepository)
    {
        $this->presentationRepository = $presentationRepository;
    }

    public function distributorCart(Request $request)
    {
        $ids = $request->input('q');
        $presentations = $this->presentationRepository->cart($ids);
        $data = new GeneralCartTransformer();
        return response()->json($data->transform($presentations, 'wholesale_prices'), 200);
    }

    public function webCart(Request $request)
    {
        $ids = $request->input('q');
        $presentations = $this->presentationRepository->cart($ids);
        $data = new GeneralCartTransformer();
        return response()->json($data->transform($presentations, 'web_prices'), 200);
    }
}