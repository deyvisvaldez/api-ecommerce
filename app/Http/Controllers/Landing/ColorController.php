<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Repositories\ColorRepository;
use App\Repositories\ItemRepository;
use App\Transformers\ColorTransformer;
use App\Transformers\ColorsTransformer;
use App\Transformers\ItemsColorTransformer;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    protected $colorRepository;
    protected $itemRepository;

    public function __construct(ColorRepository $colorRepository, ItemRepository $itemRepository)
    {
        $this->colorRepository = $colorRepository;
        $this->itemRepository = $itemRepository;
    }

    public function index(Request $request)
    {
        $locationId = $request->input('locationId');
        if ($locationId) {
            $items = $this->itemRepository->byLocation($locationId);
            $data = new ItemsColorTransformer();
            return response()->json($data->transform($items), 200);
        } else {
            $colors = $this->colorRepository->allOrder();
            $data = new ColorsTransformer();
            return response()->json($data->transform($colors), 200);
        }
    }

    public function show($colorId)
    {
        $color = $this->colorRepository->find($colorId);
        $data = new ColorTransformer();
        return response()->json($data->transform($color), 200);
    }
}
