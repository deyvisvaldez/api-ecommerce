<?php

namespace App\Http\Controllers\Landing;

use App\Entities\Company;
use App\Entities\Email;
use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Mail\ResetPasswordEmail;
use Illuminate\Http\Request;
use Mail;

class UserController extends Controller
{
    public function resetPassword(Request $request)
    {
        $rawEmail = $request->input('email');
        $email = Email::with('person.user')
            ->whereHas('person.user')
            ->whereEmail($rawEmail)
            ->whereModelType(1)
            ->whereMain(true)
            ->first();

        if (! $email) {
            return response()->json(['success' => false, 'message' => 'This email is not registered'], 404);
        }

        $company = Company::with('email')->with('address')->with('whatsapp')->first();

        if ($email->person) {
            $new_password = User::getRandomPassword();

            $email->person->user->password = $new_password;
            $email->person->user->save();

            $emailData = [
                'fullName' => $email->person->first_name.' '.$email->person->last_name,
                'emisor' => $company->email->email,
                'receptor' => $rawEmail,
                'new_password' => $new_password
            ];
            Mail::to($emailData['receptor'])->send(new ResetPasswordEmail($emailData));

            return response()->json(['success' => true, 'message' => 'There is not a user person asociate to this user'], 200);
        }

        return response()->json(['success' => false], 404);
    }
}
