<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Repositories\AccountRepository;
use App\Transformers\AccountsTransformer;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    protected $accountRepository;

    public function __construct(AccountRepository $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }

    public function index()
    {
        $accounts = $this->accountRepository->landing();
        $data = new AccountsTransformer();
        return response()->json($data->transform($accounts), 200);
    }
}
