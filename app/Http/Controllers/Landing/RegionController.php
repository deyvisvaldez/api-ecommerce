<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Repositories\RegionRepository;
use App\Transformers\RegionTransformer;
use App\Transformers\RegionTransportsTransformer;
use App\Transformers\RegionsTransformer;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    protected $regionRepository;

    public function __construct(RegionRepository $regionRepository)
    {
        $this->regionRepository = $regionRepository;
    }

    public function index()
    {
        $regions = $this->regionRepository->destinations();
        $data = new RegionTransportsTransformer();
        return response()->json($data->transform($regions), 200);
    }

    public function all()
    {
        $regions = $this->regionRepository->all();
        $data = new RegionsTransformer();
        return response()->json($data->transform($regions), 200);
    }

    public function show($regionId)
    {
        $region = $this->regionRepository->find($regionId);
        $data = new RegionTransformer();
        return response()->json($data->transform($region), 200);
    }
}
