<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Repositories\CurrencyRepository;
use App\Transformers\CurrenciesTransformer;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    protected $currencyRepository;

    public function __construct(CurrencyRepository $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
    }

    public function index()
    {
        $currencies = $this->currencyRepository->all();
        $data = new CurrenciesTransformer();
        return response()->json($data->transform($currencies), 200);
    }
}
