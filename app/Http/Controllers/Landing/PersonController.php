<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Repositories\PersonRepository;
use App\Transformers\CatalogCustomerTransformer;
use Illuminate\Http\Request;
use Uuid;

class PersonController extends Controller
{
    protected $personRepository;

    public function __construct(PersonRepository $personRepository)
    {
        $this->personRepository = $personRepository;
    }

    public function show($identityDocument)
    {
        $person = $this->personRepository->findByIdentityDocument($identityDocument);
        $data = new CatalogCustomerTransformer();
        return response()->json($data->transform($person), 200);
    }
}
