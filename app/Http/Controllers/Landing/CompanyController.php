<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Repositories\CompanyRepository;
use App\Transformers\CompanyTransformer;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    protected $companyRepository;

    public function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

    public function first()
    {
        $company = $this->companyRepository->first();
        $data = new CompanyTransformer();
        return response()->json($data->transform($company), 200);
    }
}
