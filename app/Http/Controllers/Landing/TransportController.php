<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Repositories\TransportRepository;
use App\Transformers\TransportsTransformer;
use Illuminate\Http\Request;

class TransportController extends Controller
{
    protected $transportRepository;

    public function __construct(TransportRepository $transportRepository)
    {
        $this->transportRepository = $transportRepository;
    }

    public function index()
    {
        $transports = $this->transportRepository->all();
        $data = new TransportsTransformer();
        return response()->json($data->transform($transports), 200);
    }
}

