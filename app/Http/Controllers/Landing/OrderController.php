<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Mail\KelvinOrderEmail;
use App\Repositories\CompanyRepository;
use App\Repositories\OrderRepository;
use App\Repositories\PersonRepository;
use App\Services\CalculateOrderAmount;
use App\Services\CalculateOrderCode;
use App\Services\GenerateNewOrder;
use App\Services\RegisterNewCustomer;
use App\Services\UpdateCustomerData;
use App\Transformers\OrderReportTransformer;
use Illuminate\Http\Request;
use Mail;

class OrderController extends Controller
{
    protected $orderRepository;
    protected $personRepository;
    protected $companyRepository;

    public function __construct(OrderRepository $orderRepository, PersonRepository $personRepository, CompanyRepository $companyRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->personRepository = $personRepository;
        $this->companyRepository = $companyRepository;
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $userId = null;
        if ($request->user()) {
            $userId = $request->user()->id;
        }

        $person = $this->personRepository->findByIdentityDocument($data['person']['identity_document']);

        if ($person) {
            $p = new UpdateCustomerData($this->personRepository);
            $person = $p->execute($person->id, $data['person']);
        }
        if (! $person) {
            $p = new RegisterNewCustomer($this->personRepository);
            $person = $p->execute($data['person']);
        }

        /**
         * In CalculateOrderCode and CalculateOrderAmount execute passes as parameter
         * 1 = Venta
         * 2 = Pedido
         * 3 = Cotización
         */
        $code = new CalculateOrderCode($this->orderRepository);
        $code = $code->execute(2);
        /**
         * In CalculateOrderAmount execute passes as parameter
         * 1 = Web
         * 2 = Shop
         * 3 = Wholesale
         */
        $total = new CalculateOrderAmount();
        $total = $total->execute($data['items'], 2, 3);

        /**
         * Process the payment with credit card
         */

        /*if ($data['credit_card']['flag']) {
            $payment = new ProcessPayment();
            $payment = $payment->execute($data['credit_card'], $person, $code, $total, $data['currency_id']);
        }*/

        $order = new GenerateNewOrder($this->orderRepository);
        $order = $order->execute($data['shipping'], $data['account_id'], $code, $total, $person->id, $data['items'], $data['currency_id'], 3 , $userId);

        $company = $this->companyRepository->completeData($order->company_id);

        $emailData = [
            'fullName' => $company->legal_name,
            'emisor' => $data['person']['email'],
            'receptor' => $company->email->email,
            'uuid' => $order->uuid
        ];

        Mail::to($emailData['receptor'])->send(new KelvinOrderEmail($emailData));

        return response()->json([
            'success' => true,
            'pdfUrl' => route('public.order', ['uuid' => $order->uuid])
        ], 201);
    }

    public function storeWebOrder(Request $request)
    {
        $data = $request->all();
        $userId = null;
        if ($request->user()) {
            $userId = $request->user()->id;
        }

        $person = $this->personRepository->findByIdentityDocument($data['person']['identity_document']);

        if ($person) {
            $p = new UpdateCustomerData($this->personRepository);
            $person = $p->execute($person->id, $data['person']);
        } else {
            $p = new RegisterNewCustomer($this->personRepository);
            $person = $p->execute($data['person']);
        }

        /**
         * In CalculateOrderCode and CalculateOrderAmount execute passes as parameter
         * 1 = Venta
         * 2 = Pedido
         * 3 = Cotización
         */
        $code = new CalculateOrderCode($this->orderRepository);
        $code = $code->execute(2);
        /**
         * In CalculateOrderAmount execute passes as parameter
         * 1 = Web
         * 2 = Shop
         * 3 = Wholesale
         */
        $total = new CalculateOrderAmount();
        $total = $total->execute($data['items'], 2, 1);

        /**
         * Process the payment with credit card
         */
        /*if ($data['credit_card']['flag']) {
            $payment = new ProcessPayment();
            $payment = $payment->execute($data['credit_card'], $person, $code, $total, $data['currency_id']);
        }*/

        $order = new GenerateNewOrder($this->orderRepository);
        $order = $order->execute($data['shipping'], $data['account_id'], $code, $total, $person->id, $data['items'], $data['currency_id'], 1, $userId);

        $company = $this->companyRepository->completeData($order->company_id);
        /*
        $emailData = [
            'fullName' => $data['person']['first_name'].' '.$data['person']['last_name'],
            'emisor' => $company->email->email,
            'receptor' => $data['person']['email'],
            'uuid' => $order->uuid
        ];

        Mail::to($emailData['receptor'])->send(new WebOrderEmail($emailData));*/

        $emailData = [
            'fullName' => $company->legal_name,
            'emisor' => $data['person']['email'],
            'receptor' => $company->email->email,
            'uuid' => $order->uuid
        ];

        Mail::to($emailData['receptor'])->send(new KelvinOrderEmail($emailData));

        return response()->json([
            'success' => true,
            'pdfUrl' => route('public.order', ['uuid' => $order->uuid])
        ], 201);
    }

    public function viewReport($uuid)
    {
        $order = $this->orderRepository->toPdfReport($uuid);
        $person = $this->personRepository->completeData($order->person_id);
        $data = new OrderReportTransformer();

        return response()->json($data->transform($order, $person), 200);
    }
}
