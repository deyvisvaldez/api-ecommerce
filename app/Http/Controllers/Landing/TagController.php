<?php

namespace App\Http\Controllers\Landing;

use App\Entities\Tag;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function index()
    {
        $tags = Tag::orderBy('order', 'asc')->has('presentations')->get();
        $fT = [];
        foreach ($tags as $key => $tag) {
            $fT[] = [
                'id' => (int) $tag->id,
                'name' => $tag->name,
                'order' => (int) $tag->order
            ];
        }
        return response()->json(['data' => $fT], 200);
    }
}

