<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\CategoryRepository;
use App\Transformers\CategoriesMenuTransformer;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $categories = $this->categoryRepository->publicMenu();
        $data = new CategoriesMenuTransformer();
        return response()->json($data->transform($categories), 200);
    }
}

