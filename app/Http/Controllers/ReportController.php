<?php

namespace App\Http\Controllers;

use App\Entities\Payment;
use App\Repositories\CompanyRepository;
use App\Repositories\CurrencyRepository;
use App\Repositories\InventoryRepository;
use App\Repositories\ItemRepository;
use App\Repositories\LocationRepository;
use App\Repositories\OrderRepository;
use App\Repositories\PersonRepository;
use App\Repositories\SizeRepository;
use App\Services\GetItemSizes;
use App\Transformers\GeneralExcelTransformer;
use App\Transformers\ItemExcelTransformer;
use App\Transformers\OrderCartPdfTransformer;
use App\Transformers\OrderPdfTransformer;
use App\Transformers\OrderReportTransformer;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    protected $orderRepository;
    protected $personRepository;
    protected $itemRepository;
    protected $inventoryRepository;
    protected $companyRepository;
    protected $currencyRepository;
    protected $locationRepository;
    protected $sizeRepository;

    public function __construct(OrderRepository $orderRepository, PersonRepository $personRepository,
                                ItemRepository $itemRepository, InventoryRepository $inventoryRepository,
                                CompanyRepository $companyRepository, CurrencyRepository $currencyRepository,
                                LocationRepository $locationRepository, SizeRepository $sizeRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->personRepository = $personRepository;
        $this->itemRepository = $itemRepository;
        $this->inventoryRepository = $inventoryRepository;
        $this->companyRepository = $companyRepository;
        $this->currencyRepository = $currencyRepository;
        $this->locationRepository = $locationRepository;
        $this->sizeRepository = $sizeRepository;
    }

    public function viewReport($uuid)
    {
        $order = $this->orderRepository->toPdfReport($uuid);
        $person = $this->personRepository->completeData($order->person_id);
        $data = new OrderReportTransformer();

        return response()->json($data->transform($order, $person), 200);
    }

    public function viewReportCart($uuid)
    {
        $order = $this->orderRepository->toPdfReport($uuid);
        $data = new OrderCartPdfTransformer();

        return response()->json($data->transform($order), 200);
    }


    public function show($uuid)
    {
        //$order = $this->orderRepository->findByUuid($uuid);
        $order = $this->orderRepository->toPdfReport($uuid);

        $person = $this->personRepository->completeData($order->person_id);

        $company = $this->companyRepository->completeData($person->company_id);

        $data = new OrderPdfTransformer();
        $orderData = $data->transform($order);
        $order = $orderData['order'];

        $mpdf = new \mPDF();

        $view = view()->make('reports.pdf', compact(['order', 'company', 'person']))->render();
        $mpdf->WriteHTML($view);
        $mpdf->Output();
    }

    public function showView($uuid)
    {
        $order = $this->orderRepository->findByUuid($uuid);

        $person = $this->personRepository->completeData($order->person_id);

        $company = $this->companyRepository->completeData($person->company_id);

        //$currency = $this->currencyRepository

        $details = \DB::table('inventory_order')
        ->select('inventory_order.order_id as id', 'items.name as producto', 'sizes.name as talla', 'colors.name as color', 'inventory_order.quantity as cantidad', 'inventory_order.price as precio')
        ->join('inventories', 'inventory_order.inventory_id','=','inventories.id')
        ->join('colors', 'inventories.color_id','=','colors.id')
        ->join('sizes', 'inventories.size_id','=','sizes.id')
        ->join('items', 'inventories.item_id','=','items.id')
        ->where('inventory_order.order_id', '=', $order->id)
        ->get();

        $products = \DB::table('inventory_order')
        ->selectRaw('items.name as producto, count(items.name) as cantidad, items.description as description')
        ->join('inventories', 'inventory_order.inventory_id','=','inventories.id')
        ->join('colors', 'inventories.color_id','=','colors.id')
        ->join('sizes', 'inventories.size_id','=','sizes.id')
        ->join('items', 'inventories.item_id','=','items.id')
        ->where('inventory_order.order_id', '=', $order->id)
        ->groupBy('items.name')
        ->groupBy('items.description')
        ->get();

        return view()->make('reports.generic', compact(['order', 'details', 'products', 'company', 'person']));
    }

    public function generalExcel(Request $request)
    {
        $filters = $request->all();
        $size = $request->input('size');

        $items = $this->itemRepository->generalMatrix($filters);

        if ($size) {
            if ($size == 'all') {
                $sizes = $this->sizeRepository->all();
                foreach ($sizes as $key => $size) {
                    $allSizes[] = [
                        'id' => (int) $size->id,
                        'size' => $size->name,
                    ];
                }
            } else {
                $sizes = new GetItemSizes();
                $allSizes = $sizes->execute($items);
            }
        } else {
            $sizes = $this->sizeRepository->all();
            foreach ($sizes as $key => $size) {
                $allSizes[] = [
                    'id' => (int) $size->id,
                    'size' => $size->name,
                ];
            }
        }

        $data = new GeneralExcelTransformer();

        $excelData = $data->transform($items, $allSizes);
        $excelData = $excelData['data'];

        $shop = $this->locationRepository->find($filters['locationId']);

        $now = Carbon::now('America/Lima');
        $date = $now->format('Y-m-d g:i');
        $name  = "Stock en $shop->name $date Hrs";

        Excel::create($name, function($excel) use ($name, $excelData){

            $excel->sheet('Stock por Talla', function($sheet) use ($excelData) {
                $sheet->setOrientation('landscape');
                $sheet->fromArray($excelData['headers']);
                foreach ($excelData['inventories'] as $key => $inventory) {
                    $sheet->row($key+2, $inventory);
                }
            });
        })->export('xls');

    }

    public function itemExcel(Request $request, $itemSlug)
    {
        $locationId = $request->input('locationId');
        $size = $request->input('size');

        if ($locationId) {
            $item = $this->itemRepository->profileByLocation($itemSlug, $locationId);
        } else {
            $item = $this->itemRepository->profile($itemSlug);
        }

        if ($size) {
            if ($size == 'all') {
                $sizes = $this->sizeRepository->all();
                foreach ($sizes as $key => $size) {
                    $allSizes[] = [
                        'id' => (int) $size->id,
                        'size' => $size->name,
                    ];
                }
            } else {
                $sizes = new GetItemSizes();
                $allSizes = $sizes->execute($item);
            }
        } else {
            $sizes = $this->sizeRepository->all();
            foreach ($sizes as $key => $size) {
                $allSizes[] = [
                    'id' => (int) $size->id,
                    'size' => $size->name,
                ];
            }
        }

        $data = new ItemExcelTransformer();

        $excelData = $data->transform($item, $allSizes);
        $excelData = $excelData['data'];

        $shop = $this->locationRepository->find($locationId);

        $now = Carbon::now('America/Lima');
        $date = $now->format('Y-m-d g:i');
        $name  = "Stock en $shop->name $date Hrs";

        Excel::create($name, function($excel) use ($name, $excelData){

            $excel->sheet('Stock por Talla', function($sheet) use ($excelData) {
                $sheet->setOrientation('landscape');
                $sheet->fromArray($excelData['headers']);
                foreach ($excelData['inventories'] as $key => $inventory) {
                    $sheet->row($key+2, $inventory);
                }
            });
        })->export('xls');
    }

    public function paymentsExcel(Request $request)
    {
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');

        $payments = Payment::whereStatus(true);

        if ($startDate && $endDate) {
            $payments = $payments->whereBetween('created_at', [$startDate, $endDate])->get();
        } elseif ($startDate) {
            $payments = $payments->where('created_at', '>=', $startDate)->get();
        } elseif ($endDate) {
            $payments = $payments->where('created_at', '<=', $endDate)->get();
        } else {
            $payments = $payments->get();
        }

        return response()->json($payments);
        $items = $this->itemRepository->generalMatrix($filters);

        $now = Carbon::now('America/Lima');
        $date = $now->format('Y-m-d g:i');
        $name  = "Stock en $shop->name $date Hrs";

        Excel::create($name, function($excel) use ($name, $excelData){
            $excel->sheet('Stock por Talla', function($sheet) use ($excelData) {
                $sheet->setOrientation('landscape');
                $sheet->fromArray($excelData['headers']);
                foreach ($excelData['inventories'] as $key => $inventory) {
                    $sheet->row($key+2, $inventory);
                }
            });
        })->export('xls');
    }
}
