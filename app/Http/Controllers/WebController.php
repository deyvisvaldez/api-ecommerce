<?php
namespace App\Http\Controllers;

use App\Entities\Item;
use App\Repositories\CompanyRepository;
use App\Repositories\ItemRepository;
use Illuminate\Http\Request;
use stdClass;

class WebController extends Controller
{
    protected $companyRepository;
    protected $itemRepository;

    public function __construct(CompanyRepository $companyRepository, ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
        $this->companyRepository = $companyRepository;
    }

    public function getIndex ()
    {
        $company = $this->companyRepository->first();

        $head = new stdClass;
        // Meta Company
        $head->description = $company->who_we_are;
        $head->imageUrl = $company->color_logotype;
        $head->nameCompany = $company->legal_name;
        $head->title = $company->legal_name;
        $head->url = route('home');
        // End Meta Company

        return view('landing.index', compact('head'));
    }

    public function getIndexItem ($itemSlug = null)
    {
        $company = $this->companyRepository->first();

        $head = new stdClass;
        // Meta Company
        $head->description = $company->who_we_are;
        $head->imageUrl = $company->color_logotype;
        $head->nameCompany = $company->legal_name;
        $head->title = $company->legal_name;
        $head->url = route('home');
        // End Meta Company

        $item = Item::with('random_presentation.random_photo')->whereSlug($itemSlug)->first();

        $head = new stdClass;

        // Meta Item
        $head->description = $item->description;
        $head->imageUrl = ($item->random_presentation) ? $item->random_presentation->random_photo->resource_thumb : asset('defaults/product.png');
        $head->nameCompany = $item->name;
        $head->title = $item->name;
        $head->url = route('item', $itemSlug);
        // End Meta Item

        return view('landing.index', compact('head'));
    }


    public function getIndexNews ($newsSlug = null)
    {
      $COMPANY_DATA = [];
      $head = new stdClass;

      // Meta News
      $head->description = 'Meta Description';
      $head->imageUrl = 'https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg';
      $head->nameCompany = 'Company';
      $head->title = 'Meta Title';
      $head->url = route('news', $newsSlug);
      // End Meta News

      return view('landing.index', compact('head'));
    }
}
