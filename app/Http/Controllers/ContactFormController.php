<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\CompanyRepository;
use App\Services\SendContactForm;

class ContactFormController extends Controller
{
    protected $companyRepository;

    public function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

    public function sendContactForm(Request $request)
    {
        $data = $request->all();
        $useCase = new SendContactForm($this->companyRepository);
        $useCase->execute($data);

        return response()->json(['success' => true], 201);
    }
}
