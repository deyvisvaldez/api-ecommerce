<?php

namespace App\Payments;

use App\Exceptions\PaymentNotApprovedException;
use App\Exceptions\PaymentTransactionFailedException;
use Environment;
use Exception;
use PayU;
use PayUCountries;
use PayUParameters;
use PayUPayments;
use SupportedLanguages;

//require_once __DIR__ . '/../payu-php-sdk/lib/PayU.php';

/*require_once base_path('payu-php-sdk/lib/PayU.php');
require_once base_path('payu-php-sdk/lib/PayU/api/SupportedLanguages.php');*/

class PayUProcessor
{
    public function __construct()
    {
        PayU::$language = SupportedLanguages::ES; //Seleccione el idioma.
        PayU::$isTest = (boolean) env('PAYU_TEST'); //Dejarlo True cuando sean pruebas.
        PayU::$apiLogin = env('PAYU_API_LOGIN'); //apiLogin de prueba/CUENTA.
        PayU::$apiKey = env('PAYU_API_KEY'); //apiKey de prueba/CUENTA.
        PayU::$merchantId = env('PAYU_MERCHANT_ID'); //Id de Comercio de prueba/CUENTA.

        // URL de Pagos
        Environment::setPaymentsCustomUrl(env('PAYU_PAYMENTS_URL'));
        // URL de Consultas
        Environment::setReportsCustomUrl(env('PAYU_REPORTS_URL'));
        // URL de Suscripciones para Pagos Recurrentes
        Environment::setSubscriptionsCustomUrl(env('PAYU_SUBSCRIPTIONS_URL'));
    }

    public function cardPayment($card, $payer, $order)
    {
        $parameters = array(
            //Ingrese aquí el número de cuotas.
            PayUParameters::INSTALLMENTS_NUMBER => "1",
            //Ingrese aquí­ el nombre del pais.
            PayUParameters::COUNTRY => PayUCountries::PE,
            //Ingrese aquí­ el identificador de prueba/CUENTA.
            PayUParameters::ACCOUNT_ID => env('PAYU_ACCOUNT_ID'), //evn
            //Cookie de la sesión actual.
            PayUParameters::PAYER_COOKIE => "cookie_" . time(),
            //Ingrese aquí­ la moneda.
            //PayUParameters::CURRENCY => $order['currency_code'],
            PayUParameters::CURRENCY => 'PEN',
            //Se ingresa el id de usuario, una referencia del sistema
            PayUParameters::PAYER_ID => $payer['id'],
            //Ingrese aquí­ el código de referencia.
            PayUParameters::REFERENCE_CODE => $order['code'],
            //Ingrese aquí­ la descripción.
            PayUParameters::DESCRIPTION => "Pago online",
            //Ingrese aquí­ el valor o monto a pagar.
            PayUParameters::VALUE => $order['total'],
            //Ingrese aquí­ su firma. â€œ{APIKEY}~{MERCHANTID}~{REFERENCE_CODE}~{VALUE}~{CURRENCY}â€?
            //PayUParameters::SIGNATURE => md5(PayU::$apiKey . "~" . PayU::$merchantId . "~" . $order['code'] . "~" . $order['total'] . "~" .$order['currency_code']),
            PayUParameters::SIGNATURE => md5(PayU::$apiKey . "~" . PayU::$merchantId . "~" . $order['code'] . "~" . $order['total'] . "~" ."PEN"),
        );

        /* Recibimos por POST los datos de la tarjeta de credito */
        $parameters[PayUParameters::PAYER_NAME] = $payer['full_name'];
        $parameters[PayUParameters::PAYER_EMAIL] = $payer['email'];
        $parameters[PayUParameters::PAYER_DNI] = $payer['dni'];
        $parameters[PayUParameters::CREDIT_CARD_NUMBER] = $card['card_number'];
        //$parameters[PayUParameters::CREDIT_CARD_EXPIRATION_DATE] = $card['year_expiration']. "/" . $card['month_expiration'];
        $parameters[PayUParameters::CREDIT_CARD_EXPIRATION_DATE] = $card['expiration_date'];
        $parameters[PayUParameters::CREDIT_CARD_SECURITY_CODE] = $card['cvv'];
        $parameters[PayUParameters::PROCESS_WITHOUT_CVV2] = false;
        //VISA,MASTERCARD,ETC
        $parameters[PayUParameters::PAYMENT_METHOD] = 'VISA';

        try {
            $payu_response = PayUPayments::doAuthorizationAndCapture($parameters);
            if ($payu_response->code == "SUCCESS") {
                //dd($payu_response); //NO FUNCIONA EN MODO TEST
                if ($payu_response->transactionResponse->state == "APPROVED") {
                    return $payu_response;
                } else {
                    throw new PaymentNotApprovedException($payu_response->transactionResponse->responseCode, 1);
                }
            } else {
                throw new PaymentTransactionFailedException($payu_response->code, 1);
            }
        } catch (Exception $e) {
            throw new PaymentTransactionFailedException($e->getMessage(), 1);
        }
    }
}