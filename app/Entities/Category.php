<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'categories';

    protected $fillable = ['uuid', 'name', 'slug', 'image', 'image_path', 'image_thumb', 'image_thumb_path', 'description', 'published', 'type', 'order'];

    public function subcategories()
    {
        return $this->belongsToMany('App\Entities\Subcategory')->withPivot('id', 'active', 'order')->withTimestamps();
    }

    public function items()
    {
        return $this->hasMany('App\Entities\Item');
    }

    public function inventories()
    {
        return $this->hasMany('App\Entities\Inventory');
    }

    public function settings()
    {
        return $this->hasMany('App\Entities\AttributeSetting', 'model_id');
    }
}
