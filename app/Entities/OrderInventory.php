<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderInventory extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'order_inventories';

    protected $fillable = ['order_id', 'inventory_id', 'quantity', 'price'];

    public function inventory()
    {
        return $this->belongsTo('App\Entities\Inventory', 'inventory_id');
    }
}
