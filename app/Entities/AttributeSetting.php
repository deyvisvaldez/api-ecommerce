<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AttributeSetting extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'attribute_settings';

    protected $fillable = ['model_id', 'model_type', 'company_id', 'order'];
}
