<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Translation extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'translations';

    protected $fillable = ['uuid', 'name', 'value', 'slug', 'flag_image', 'flag_image_path', 'flag_image_thumb', 'flag_image_thumb_path', 'published'];
}
