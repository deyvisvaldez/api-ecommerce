<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Price extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'prices';

    protected $fillable = ['uuid', 'name', 'type', 'price', 'currency_id', 'content', 'promotional_price', 'available_promotion', 'active', 'user_id', 'presentation_id', 'company_id'];

    public function price_rules()
    {
        return $this->belongsToMany('App\Entities\PriceRule')->withTimestamps();
    }

    public function presentation()
    {
        return $this->belongsTo('App\Entities\Presentation');
    }
}
