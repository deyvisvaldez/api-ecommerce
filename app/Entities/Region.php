<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Region extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'regions';

    protected $fillable = ['name', 'country_id'];

    public function transports()
    {
        return $this->belongsToMany('App\Entities\Transport')->withPivot('address', 'duration')->withTimestamps();
    }

    public function country()
    {
        return $this->belongsTo('App\Entities\Country');
    }

    public function branch_offices()
    {
        return $this->hasMany('App\Entities\BranchOffice');
    }

    public function branch_office()
    {
        return $this->hasOne('App\Entities\BranchOffice');
    }

    public function costs()
    {
        return $this->hasManyThrough('App\Entities\Cost', 'App\Entities\BranchOffice', 'region_id', 'region_transport_id', 'id');
    }
}
