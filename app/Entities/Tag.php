<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'tags';

    protected $fillable = [
        'name', 'slug', 'order', 'published'
    ];

    public function presentations()
    {
        return $this->belongsToMany('App\Entities\Presentation');
    }
}