<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transport extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'transports';

    protected $fillable = ['name', 'description', 'price', 'duration'];

    public function regions()
    {
        return $this->belongsToMany('App\Entities\Region')->withTimestamps();
    }

    public function branch_offices()
    {
        return $this->hasMany('App\Entities\BranchOffice');
    }

    public function costs()
    {
        return $this->hasManyThrough('App\Entities\Cost', 'App\Entities\BranchOffice', 'transport_id', 'region_transport_id', 'id');
    }
}
