<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'orders';

    protected $fillable = ['uuid', 'doc_code', 'code', 'description', 'order_type', 'total', 'person_id', 'user_id', 'status', 'shipping_address', /*'account_id', */'currency_id', 'company_id', 'location_id', 'origin', 'discount', 'shipping_id'];

    public function presentations()
    {
        return $this->belongsToMany('App\Entities\Presentation')->withPivot('price', 'quantity')->withTimestamps();
    }

    public function person()
    {
        return $this->belongsTo('App\Entities\Person');
    }

    public function user()
    {
        return $this->belongsTo('App\Entities\User');
    }

    /**
     * Wiltex
     */

    public function order_type()
    {
        return $this->belongsTo('App\Entities\OrderType', 'order_type');
    }

    public function order_inventories()
    {
        return $this->hasMany('App\Entities\OrderInventory');
    }





    public function location()
    {
        return $this->belongsTo('App\Entities\Location');
    }

    public function payments()
    {
        return $this->hasMany('App\Entities\Payment');
    }

    public function registerPayment(Payment $payment)
    {
        return $this->payments()->save($payment);
    }

    public function extra_cost()
    {
        return $this->hasOne('App\Entities\ExtraCost');
    }

    /*public function extra_costs()
    {
        return $this->hasMany('App\Entities\ExtraCost');
    }*/
}
