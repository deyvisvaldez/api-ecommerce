<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cellphone extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'cellphones';

    protected $fillable = ['cellphone', 'main', 'model_id', 'model_type'];

    public function person()
    {
        return $this->belongsTo('App\Entities\Person', 'model_id');
    }
}
