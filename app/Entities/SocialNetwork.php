<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SocialNetwork extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'social_networks';

    protected $fillable = ['uuid', 'name', 'url', 'person_id', 'social_id', 'access_token'];

}
