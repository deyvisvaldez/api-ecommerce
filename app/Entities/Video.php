<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'videos';

    protected $fillable = [
        'title', 'url', 'published', 'model_id', 'model_type'
    ];
}
