<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PriceType extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'price_types';

    protected $fillable = ['name', 'available'];

}
