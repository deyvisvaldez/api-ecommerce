<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'companies';

    protected $fillable = [
        'uuid', 'company_registration', 'business_name', 'slug', 'legal_name', 'slogan_title', 'slogan_subtitle', 'representative', 'schedules','dropbox_path','color_logotype', 'color_logotype_path', 'color_logotype_thumb', 'color_logotype_thumb_path', 'white_logotype', 'white_logotype_path', 'icon', 'icon_path', 'who_we_are', 'what_we_do', 'proposal', 'anniversary', 'meta_keywords', 'ga_code', 'mailchimp_list_id', 'country_id', 'region_id', 'province_id', 'city_id', 'district_id', 'sector_id', 'active', 'facebook', 'twitter', 'google_plus', 'linkedin', 'youtube', 'website', 'return_policies', 'shipping_policies', 'instagram'
    ];

    public function manager()
    {
        return $this->hasOne('App\Entities\Person')->wherePersonType(2);
    }

    public function users()
    {
        return $this->hasMany('App\Entities\User');
    }

    public function photos()
    {
        return $this->hasMany('App\Entities\Content', 'model_id')->whereModelType(1);
    }

    public function items()
    {
        return $this->hasMany('App\Entities\Item');
    }

    public function email()
    {
        return $this->hasOne('App\Entities\Email', 'model_id')->whereMain(true);
    }

    public function address()
    {
        return $this->hasOne('App\Entities\Address', 'model_id')->whereMain(true);
    }

    public function whatsapp()
    {
        return $this->hasOne('App\Entities\Whatsapp', 'model_id')->whereMain(true);
    }

    public function emails()
    {
        return $this->hasMany('App\Entities\Email', 'model_id');
    }

    public function addresses()
    {
        return $this->hasMany('App\Entities\Address', 'model_id');
    }

    public function cellphones()
    {
        return $this->hasMany('App\Entities\Cellphone', 'model_id');
    }

    public function cellphone()
    {
        return $this->hasOne('App\Entities\Cellphone', 'model_id')->whereMain(true);
    }

    public function phones()
    {
        return $this->hasMany('App\Entities\Phone', 'model_id');
    }

    public function phone()
    {
        return $this->hasOne('App\Entities\Phone', 'model_id')->whereMain(true);
    }

    public function whatsapps()
    {
        return $this->hasMany('App\Entities\Whatsapp', 'model_id');
    }

    public function accounts()
    {
        return $this->hasMany('App\Entities\BillingCard', 'company_id');
    }

    public function business_categories()
    {
        return $this->hasMany('App\Entities\AttributeSetting', 'company_id')->whereModelType(1);
    }

    public function categories()
    {
        return $this->hasMany('App\Entities\AttributeSetting', 'company_id')->whereModelType(2);
    }

    public function attributes()
    {
        return $this->hasMany('App\Entities\AttributeSetting', 'company_id')->whereModelType(3);
    }
}
