<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'countries';

    protected $fillable = ['name'];

    public function regions()
    {
        return $this->hasMany('App\Entities\Region');
    }
}
