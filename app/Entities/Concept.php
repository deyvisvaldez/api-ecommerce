<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Concept extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'concepts';

    protected $fillable = ['name', 'description', 'type', 'code'];

    public function order_type()
    {
        return $this->belongsTo('App\Entities\OrderType');
    }
}
