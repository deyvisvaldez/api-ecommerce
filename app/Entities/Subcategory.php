<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subcategory extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'subcategories';

    protected $fillable = ['uuid', 'name', 'slug', 'image', 'image_path', 'image_thumb', 'image_thumb_path', 'description', 'published', 'type'];

    public function categories()
    {
        return $this->belongsToMany('App\Entities\Category')->withPivot('id', 'active', 'order')->withTimestamps();
    }

    public function business_category()
    {
        return $this->belongsTo('App\Entities\Category');
    }

    public function inventories()
    {
        return $this->hasMany('App\Entities\Inventory', 'subcategory_id');
    }

    public function attributes()
    {
        return $this->hasMany('App\Entities\AttributeSetting', 'model_id')->whereModelType(3);
    }
}
