<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'contents';

    protected $fillable = [
        'uuid', 'description', 'resource', 'resource_path', 'resource_thumb', 'resource_thumb_path', 'model_id', 'model_type', 'title', 'published'
        ];

    public function company()
    {
        return $this->belongsTo('App\Entities\Company', 'company_id');
    }

    public function inventory()
    {
        return $this->belongsTo('App\Entities\Inventory', 'model_id');
    }
}
