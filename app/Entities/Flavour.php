<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Flavour extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'flavours';

    protected $fillable = ['uuid', 'name', 'description', 'image', 'image_path', 'image_thumb', 'image_thumb_path', 'attribute_id', 'attribute_model', 'company_id', 'active'];
}
