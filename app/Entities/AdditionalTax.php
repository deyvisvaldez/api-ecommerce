<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdditionalTax extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'additional_taxes';

    protected $fillable = ['uuid', 'name', 'value', 'active', 'company_id', 'country_id'];
}
