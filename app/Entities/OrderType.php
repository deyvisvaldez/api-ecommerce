<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderType extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'order_type';

    protected $fillable = [
        'name', 'description', 'code'
    ];
}
