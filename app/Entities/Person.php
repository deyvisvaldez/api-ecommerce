<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Person extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'people';

    protected $fillable = ['uuid', 'identity_document', 'first_name', 'last_name', 'avatar', 'avatar_path', 'avatar_thumb', 'avatar_thumb_path', 'gender', 'birthdate', 'country_id', 'region_id', 'person_type', 'company_id'];

    public function user()
    {
        return $this->hasOne('App\Entities\User');
    }

    public function createUser($user)
    {
        $user = new User($user);
        return $this->user()->save($user);
    }

    public function email()
    {
        return $this->hasOne('App\Entities\Email', 'model_id', 'id')->whereModelType(1)->whereMain(true);
    }

    public function emails()
    {
        return $this->hasMany('App\Entities\Email', 'model_id', 'id')->whereModelType(1);
    }

    public function addEmail($email)
    {
        $email = new Email($email);
        return $this->email()->save($email);
    }

    public function address()
    {
        return $this->hasOne('App\Entities\Address', 'model_id', 'id')->whereModelType(1)->whereMain(true);
    }

    public function addresses()
    {
        return $this->hasMany('App\Entities\Address', 'model_id', 'id')->whereModelType(1);
    }

    public function addAddress($address)
    {
        $address = new Address($address);
        return $this->address()->save($address);
    }

    public function cellphones()
    {
        return $this->hasMany('App\Entities\Cellphone', 'model_id', 'id')->whereModelType(1);
    }

    public function cellphone()
    {
        return $this->hasOne('App\Entities\Cellphone', 'model_id', 'id')->whereModelType(1)->whereMain(true);
    }

    public function addCellphone($cellphone)
    {
        $cellphone = new Cellphone($cellphone);
        return $this->cellphone()->save($cellphone);
    }

    public function phones()
    {
        return $this->hasMany('App\Entities\Phone', 'model_id', 'id')->whereModelType(1);
    }

    public function phone()
    {
        return $this->hasOne('App\Entities\Phone', 'model_id', 'id')->whereModelType(1)->whereMain(true);
    }

    public function addPhone($phone)
    {
        $phone = new Phone($phone);
        return $this->phone()->save($phone);
    }

    public function whatsapps()
    {
        return $this->hasMany('App\Entities\Whatsapp', 'model_id', 'id')->whereModelType(1);
    }

    public function whatsapp()
    {
        return $this->hasOne('App\Entities\Whatsapp', 'model_id', 'id')->whereModelType(1)->whereMain(true);
    }

    public function addWhatsapp($whatsapp)
    {
        $whatsapp = new Whatsapp($whatsapp);
        return $this->whatsapp()->save($whatsapp);
    }

    public function country()
    {
        return $this->belongsTo('App\Entities\Country');
    }

    public function region()
    {
        return $this->belongsTo('App\Entities\Region');
    }

    public static function publish($personId)
    {
        $person = static::with('user')->find($personId);
        $person->user->activated = ! $person->user->activated;
        $person->user->save();
    }
}
