<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Whatsapp extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'whatsapps';

    protected $fillable = ['whatsapp', 'main', 'model_id', 'model_type'];

    public function person()
    {
        return $this->belongsTo('App\Entities\Person', 'person_id');
    }
}
