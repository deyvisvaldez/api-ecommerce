<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recommendation extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'recommendations';

    protected $fillable = ['image', 'image_path', 'item_id'];

    public function item()
    {
        return $this->belongsTo('App\Entities\Item');
    }

}
