<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Color extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'colors';

    protected $fillable = ['uuid', 'name', 'image', 'image_path', 'image_thumb', 'image_thumb_path', 'active', 'color_code', 'order'];
}
