<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BranchOffice extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'region_transport';

    protected $fillable = ['region_id', 'transport_id', 'address', 'duration'];

    public function costs()
    {
        return $this->hasMany('App\Entities\Cost', 'region_transport_id');
    }

    public function transport()
    {
        return $this->belongsTo('App\Entities\Transport');
    }
}
