<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Presentation extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'presentations';

    protected $fillable = [
        'uuid', 'bar_code', 'item_id', 'size_id', 'color_id', 'published', 'available'
    ];

    public function item()
    {
        return $this->belongsTo('App\Entities\Item', 'item_id');
    }

    public function size()
    {
        return $this->belongsTo('App\Entities\Size', 'size_id');
    }

    public function color()
    {
        return $this->belongsTo('App\Entities\Color', 'color_id');
    }

    public function random_price()
    {
        return $this->hasOne('App\Entities\Price')->whereType(1)->inRandomOrder();
    }

    public function web_prices()
    {
        return $this->hasMany('App\Entities\Price')->whereType(1);
    }

    public function shop_prices()
    {
        return $this->hasMany('App\Entities\Price')->whereType(2);
    }

    public function wholesale_prices()
    {
        return $this->hasMany('App\Entities\Price')->whereType(3);
    }

    public function photos()
    {
        return $this->hasMany('App\Entities\Content', 'model_id')->whereModelType(2);
    }

    public function random_photo()
    {
        return $this->hasOne('App\Entities\Content', 'model_id')->whereModelType(2)->inRandomOrder();
    }

    public function random_photos()
    {
        return $this->hasMany('App\Entities\Content', 'model_id')->whereModelType(2)->take(2)->inRandomOrder();
    }

    public function inventories()
    {
        return $this->hasMany('App\Entities\Inventory');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Entities\Tag');
    }

    // para facilitar la consulta
    public function inventory()
    {
        return $this->hasOne('App\Entities\Inventory');
    }
    /**
     * WILTEX
     */

    public function prices()
    {
        return $this->hasMany('App\Entities\Price');
    }

    public function web_price()
    {
        return $this->hasOne('App\Entities\Price')->whereType(1);
    }



    public function shop_price()
    {
        return $this->hasOne('App\Entities\Price')->whereType(2);
    }



    public function wholesale_price()
    {
        return $this->hasOne('App\Entities\Price')->whereType(3);
    }



    public function addPrice(Price $price)
    {
        return $this->prices()->save($price);
    }



    public function addPhoto(Content $photo)
    {
        return $this->photos()->save($photo);
    }

}
