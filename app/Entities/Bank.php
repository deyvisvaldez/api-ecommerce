<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bank extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'banks';

    protected $fillable = ['name', 'bank_image', 'bank_image_path', 'bank_image_thump', 'bank_image_thump_path', 'type'];

}
