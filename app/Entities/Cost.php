<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cost extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'costs';

    protected $fillable = ['region_transport_id', 'rule_id', 'cost'];

    public function rule()
    {
        return $this->belongsTo('App\Entities\Rule');
    }

    public function branch_office()
    {
        return $this->belongsTo('App\Entities\BranchOffice', 'region_transport_id');
    }
}
