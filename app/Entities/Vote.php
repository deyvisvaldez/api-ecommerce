<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vote extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'votes';

    protected $fillable = ['vote', 'user_id', 'checked', 'model_id', 'model_type'];
}
