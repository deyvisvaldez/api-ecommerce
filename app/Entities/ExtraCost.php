<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExtraCost extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'extra_costs';

    protected $fillable = ['order_id', 'transport_id', 'region_id', 'transport_cost'];

    public function order()
    {
        return $this->belongsTo('App\Entities\Order');
    }
}
