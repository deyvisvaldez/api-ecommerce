<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'currencies';

    protected $fillable = ['uuid', 'name', 'code', 'symbol', 'description', 'flag_image', 'flag_image_path', 'flag_image_thumb', 'flag_image_thumb_path', 'default_currency', 'company_id', 'country_id', 'active', 'decimal'];

    public function exchange_rate()
    {
        return $this->hasOne('App\Entities\ExchangeRate')->whereActive(true);
    }
}
