<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderPresentation extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'order_presentation';

    protected $fillable = ['order_id', 'presentation_id', 'quantity', 'price'];

    public function presentation()
    {
        return $this->belongsTo('App\Entities\Presentation');
    }

    public function order()
    {
        return $this->belongsTo('App\Entities\Order');
    }
}
