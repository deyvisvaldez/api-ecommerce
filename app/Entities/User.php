<?php

namespace App\Entities;

use Hash;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HasApiTokens;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid', 'username', 'password', 'role', 'activated', 'confirmation_code', 'person_id', 'company_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($value)
    {
        if (! empty($value))
        {
            $this->attributes['password'] = Hash::make($value);
        }
    }

    /**
     * Change email to username for password grant type in Passport
     */
    public function findForPassport($username)
    {
        return $this->where('username', $username)->first();
    }

    public function person()
    {
        return $this->belongsTo('App\Entities\Person');
    }

    public static function getRandomPassword($length = 10)
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyz234567';

        if ($length < 1) {
            throw new InvalidArgumentException('Length must be a positive integer');
        }
        $str = '';
        $alphamax = strlen($alphabet) - 1;
        if ($alphamax < 1) {
            throw new InvalidArgumentException('Invalid alphabet');
        }
        for ($i = 0; $i < $length; ++$i) {
            $str .= $alphabet[random_int(0, $alphamax)]; // PARA PHP7
        }
        return $str;
    }
}
