<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExchangeRate extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'exchange_rates';

    protected $fillable = ['uuid', 'sales_exchange', 'currency_id', 'active'];
}
