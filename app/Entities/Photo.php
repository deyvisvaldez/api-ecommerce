<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Photo extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'contents';

    protected $fillable = [
        'uuid', 'description', 'resource', 'resource_path', 'resource_thumb', 'resource_thumb_path', 'model_id', 'model_type', 'title', 'published'
    ];
}
