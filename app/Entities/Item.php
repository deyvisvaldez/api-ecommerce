<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'items';

    protected $fillable = [
        'uuid', 'name', 'business_code', 'slug', 'description', 'published', 'distributor_published', 'user_id', 'ga_code', 'company_id', 'category_id', 'subcategory_id', 'type', 'features', 'referencial_price', 'referencial_photo', 'referencial_photo_path', 'referencial_photo_thumb', 'referencial_photo_thumb_path', 'measurement_table', 'measurement_table_path'
    ];

    public function company()
    {
        return $this->belongsTo('App\Entities\Company', 'company_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Entities\Category');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Entities\Subcategory');
    }

    public function presentations()
    {
        return $this->hasMany('App\Entities\Presentation');
    }
    public function random_presentation()
    {
        return $this->hasOne('App\Entities\Presentation')->has('photos')->inRandomOrder();
    }

    /**
     * WILTEX
     */

    public function inventories()
    {
        return $this->hasMany('App\Entities\Inventory');
    }

    public function recommendations()
    {
        return $this->hasMany('App\Entities\Recommendation');
    }



}
