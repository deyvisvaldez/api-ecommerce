<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PriceRule extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'price_rules';

    protected $fillable = ['name', 'quantity'];

    public function prices()
    {
        return $this->belongsToMany('App\Entities\Price')->withTimestamps();
    }

    public function price()
    {
        return $this->hasOne('App\Entities\Price');
    }
}
