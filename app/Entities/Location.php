<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'locations';

    protected $fillable = ['uuid', 'name', 'description', 'address', 'type', 'company_id', 'main'];
}
