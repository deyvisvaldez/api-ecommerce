<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movement extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'movements';

    protected $fillable = ['type', 'inventory_id', 'quantity', 'description', 'user_id', 'origin_destiny'];

    public function user()
    {
        return $this->belongsTo('App\Entities\User');
    }

    public function location()
    {
        return $this->belongsTo('App\Entities\Location');
    }

    public function inventory()
    {
        return $this->belongsTo('App\Entities\Inventory');
    }
}
