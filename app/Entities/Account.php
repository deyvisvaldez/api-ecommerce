<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'accounts';

    protected $fillable = ['uuid', 'account_name', 'account_number', 'bank_id', 'account_holder', 'document_holder', 'account_type', 'currency_id', 'published', 'region_id', 'company_id', 'description', 'country_id', 'email', 'address', 'bank_account'];

    public function bank()
    {
        return $this->belongsTo('App\Entities\Bank', 'bank_id');
    }

    public function currency()
    {
        return $this->belongsTo('App\Entities\Currency', 'currency_id');
    }

    public function region()
    {
        return $this->belongsTo('App\Entities\Region', 'region_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Entities\Country', 'country_id');
    }

    public static function publish($published)
    {
        return $this->update(['published' => true]);
    }
}
