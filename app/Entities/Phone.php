<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Phone extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'phones';

    protected $fillable = ['phone', 'main', 'model_id', 'model_type'];
}
