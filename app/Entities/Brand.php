<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'brands';

    protected $fillable = ['uuid', 'name', 'description', 'image', 'image_path', 'image_thumb', 'image_thumb_path', 'company_id', 'active'];

}
