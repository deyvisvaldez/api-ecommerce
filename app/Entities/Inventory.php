<?php

namespace App\Entities;

use App\Entities\Price;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventory extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'inventories';

    protected $fillable = [
        'uuid', 'bar_code', 'location_id', 'located', 'available_stock', 'max_stock', 'min_stock', 'user_id', 'company_id', 'presentation_id'
    ];

    public function location()
    {
        return $this->belongsTo('App\Entities\Location', 'location_id');
    }

    public function movements()
    {
        return $this->hasMany('App\Entities\Movement');
    }

    /**
     * wiltex
     */

    public function item()
    {
        return $this->belongsTo('App\Entities\Item', 'item_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Entities\Category', 'category_id');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Entities\Subcategory', 'subcategory_id');
    }

    public function prices()
    {
        return $this->hasMany('App\Entities\Price');
    }

    public function web_price()
    {
        return $this->hasOne('App\Entities\Price')->whereType(1);
    }

    public function web_prices()
    {
        return $this->hasMany('App\Entities\Price')->whereType(1);
    }

    public function shop_price()
    {
        return $this->hasOne('App\Entities\Price')->whereType(2);
    }

    public function shop_prices()
    {
        return $this->hasMany('App\Entities\Price')->whereType(2);
    }

    public function wholesale_price()
    {
        return $this->hasOne('App\Entities\Price')->whereType(3);
    }

    public function wholesale_prices()
    {
        return $this->hasMany('App\Entities\Price')->whereType(3);
    }

    public function addPrice(Price $price)
    {
        return $this->prices()->save($price);
    }

    public function photos()
    {
        return $this->hasMany('App\Entities\Content', 'model_id')->whereModelType(2);
    }

    public function addPhoto(Content $photo)
    {
        return $this->photos()->save($photo);
    }

    public function random_photo()
    {
        return $this->hasOne('App\Entities\Content', 'model_id')->whereModelType(2);
    }

    public function brand()
    {
        return $this->belongsTo('App\Entities\Brand', 'brand_id');
    }

    public function presentation()
    {
        return $this->belongsTo('App\Entities\Presentation', 'presentation_id');
    }

    public function flavour()
    {
        return $this->belongsTo('App\Entities\Flavour', 'flavour_id');
    }

    public function size()
    {
        return $this->belongsTo('App\Entities\Size', 'size_id');
    }

    public function color()
    {
        return $this->belongsTo('App\Entities\Color', 'color_id');
    }

    public function quality()
    {
        return $this->belongsTo('App\Entities\Quality', 'quality_id');
    }

    public function property()
    {
        return $this->belongsTo('App\Entities\Property', 'property_id');
    }


}
