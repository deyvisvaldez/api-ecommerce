<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'payments';

    protected $fillable = ['order_id', 'description', 'amount', 'payment_type', 'payment_way', 'user_id', 'status', 'account_id', 'concept_id', 'receipt_code', 'order_type_id'];
}
