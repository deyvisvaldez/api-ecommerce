<?php

namespace App\Uploaders;

use League\Flysystem\Filesystem;
use Spatie\Dropbox\Client;
use Spatie\FlysystemDropbox\DropboxAdapter as Adapter;

abstract class DropboxUploader {

    protected $dropboxClient;

    protected $dropboxPath;
    protected $dropboxUrl;

	function __construct()
	{
        $this->dropboxClient = new Client(env('DROPBOX_TOKEN'));
	}

	abstract protected function process($img, $fileType, $thumb = null);

	public function upload($path, $file, $fileType, $thumb = null)
    {
        $file = $this->process($file, $fileType, $thumb);

        $uploader = new Filesystem(new Adapter($this->dropboxClient, $path));

        $dropboxFileName = time()."-".time().".".$fileType;
        $this->dropboxPath = $path.'/'.$dropboxFileName;

        try {
            $uploadFile = $uploader->put($dropboxFileName, (string) $file);
            $urlObject = $this->dropboxClient->createSharedLinkWithSettings($this->dropboxPath, ["requested_visibility" => "public"]);

            $this->dropboxUrl = str_replace( "www.dropbox.com" , "dl.dropboxusercontent.com" , $urlObject['url']);

            return true;
        }
        catch (Exception $e) {
            return false;
        }
    }

	public function getDropboxPath()
    {
        return $this->dropboxPath;
    }

    public function getDropboxUrl()
    {
        return $this->dropboxUrl;
    }

    public function delete($path, $url)
    {
        if (! empty($url)) {
            if (! is_null($url)) {
                $ok = get_headers($url);
                if ($ok[0] == 'HTTP/1.1 200 OK') {
                    if ($path) {
                        $delete = $this->dropboxClient->delete($path);
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
