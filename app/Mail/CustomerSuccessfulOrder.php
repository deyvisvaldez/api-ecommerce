<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerSuccessfulOrder extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $companyEmail)
    {
        $this->data = $data;
        $this->companyEmail = $companyEmail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->companyEmail, $this->data['company']['name'])
            ->subject('Has realizado un pedido')
            ->view('emails.orders.customerSuccessfulOrder')
            ->with($this->data);
    }
}
