<?php

namespace App\Managers;

use App\Entities\Email;

class EmailManager extends BaseManager
{
    public function prepareData(array $data)
    {
        $count = Email::whereModelId($data['model_id'])->whereModelType($data['model_type'])->count();
        //$count = Email::whereModelId($data['model_id'])->whereModelType(1)->count();

        if (! $count) {
            $data['main'] = true;
        }

        return $data;
    }
}