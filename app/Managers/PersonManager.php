<?php

namespace App\Managers;

use Uuid;

class PersonManager extends BaseManager
{
    public function prepareData(array $data)
    {
        $data['uuid'] = Uuid::generate(4)->string;
        return $data;
    }
}
