<?php

namespace App\Managers;

abstract class BaseManager
{
    protected $model;
    protected $data;

    public function __construct($model, $data)
    {
        $this->model = $model;
        $this->data = $data;
        //$this->data = array_only($data, array_keys($this->getRules()));
    }

    /*public function isValid()
    {
        $rules = $this->getRules();
        $validation = \Validator::make($this->data, $rules);
        if ($validation->fails()) {
            throw new ValidationException('Validation failed', $validation->messages());
        }
    }*/

    public function prepareData(array $data)
    {
        return $data;
    }

    public function store()
    {
        $this->model->fill($this->prepareData($this->data));
        $this->model->save();
        return $this->model;
    }

    public function updateData(array $data)
    {
        return $data;
    }

    public function update()
    {
        $this->model->fill($this->updateData($this->data));
        $this->model->save();
        return $this->model;
    }

    public function delete($model)
    {
        /*if (is_numeric($model))
        {
            $model = $this->find($model);
        }
        $model->delete();
        return $model;*/
    }
}

