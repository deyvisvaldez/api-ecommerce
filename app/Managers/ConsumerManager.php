<?php

namespace App\Managers;

use Uuid;

class ConsumerManager extends BaseManager
{
    public function prepareData(array $data)
    {
        $data['uuid'] = Uuid::generate(4)->string;
        $data['person_type'] = 4;
        $data['company_id'] = 1;
        $data['gender'] = true;

        return $data;
    }
}
