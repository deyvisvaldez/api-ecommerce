<?php

namespace ComproenTacna\Managers;

use Uuid;

class OrderManager extends BaseManager
{
    public function prepareData(array $data)
    {
        $data['order_type'] = 1;
        $data['status'] = 2;
        $data['uuid'] = Uuid::generate(4)->string;

        return $data;
    }
}