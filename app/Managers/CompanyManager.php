<?php

namespace App\Managers;

use Uuid;

class CompanyManager extends BaseManager
{
    public function prepareData(array $data)
    {
        $data['slug'] = str_slug($data['legal_name'], '-');
        $data['representative'] = $data['first_name'].' '.$data['last_name'];
        $data['uuid'] = Uuid::generate(4)->string;
        return $data;
    }
}
