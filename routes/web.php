<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


/**
 * Web Routes
 */

Route::get('/', 'WebController@getIndex')->name('home');
Route::get('/c/{categorySlug?}/{subCategorySlug?}', 'WebController@getIndex');

Route::get('ordenes/web/{uuid}', 'WebController@getIndex')->name('public.order');

Route::get('/distribuidoras/{categorySlug?}/{subCategorySlug?}', 'WebController@getIndex');
Route::get('/ingresar', 'WebController@getIndex');

Route::get('/nosotros', 'WebController@getIndex');
Route::get('/orden', 'WebController@getIndex');

Route::get('/productos/{itemSlug?}', 'WebController@getIndexItem')->name('item');
Route::get('/noticias/{newsSlug?}', 'WebController@getIndexNews')->name('news');

Route::get('/plataforma', 'WebController@getIndex');

Route::get('/admin', 'WebController@getIndex');
// Route::get('/admin/company', 'WebController@getIndex');
Route::get('/admin/plataforma', 'WebController@getIndex');
Route::get('/admin/logistica', 'WebController@getIndex');
Route::get('/admin/caja', 'WebController@getIndex');
Route::get('/admin/elegir-ubicacion', 'WebController@getIndex');

Route::get('/admin/empresa', 'WebController@getIndex');
Route::get('/admin/empresa/usuarios', 'WebController@getIndex');
Route::get('/admin/empresa/clientes', 'WebController@getIndex');
Route::get('/admin/empresa/cuentas-bancarias', 'WebController@getIndex');
Route::get('/admin/empresa/galeria', 'WebController@getIndex');
Route::get('/admin/empresa/testimonios', 'WebController@getIndex');

Route::get('/admin/inventario', 'WebController@getIndex');
Route::get('/admin/inventario/{slug}', 'WebController@getIndex');

Route::get('/admin/operaciones', 'WebController@getIndex');
Route::get('/admin/operaciones/ventas', 'WebController@getIndex');
Route::get('/admin/operaciones/ventas/{code}', 'WebController@getIndex');

Route::get('/admin/presentaciones/{itemSlug}', 'WebController@getIndex');
Route::get('/admin/productos', 'WebController@getIndex');
Route::get('/admin/productos/{categorySlug?}/{subCategorySlug?}', 'WebController@getIndex');

Route::get('/404', 'WebController@getIndex');


Route::post('contact-form', 'ContactFormController@sendContactForm');
/**
 * Public route to pdf reports
 */

Route::get('ordenes/pdf/{uuid}', ['as' => 'pdf.order', 'uses' => 'ReportController@show']);
//Route::get('ordenes/web/{uuid}', ['as' => 'public.order', 'uses' => 'ReportController@showView']);


/**
 * This route is for redirect response on Authentication Exception
 */
Route::get('/login', function (Request $request) {
    return response()->json([
        "error" => "invalid_credentials",
        "message" => "The user credentials were incorrect."
    ], 401);
});


// Catalogo Landing
Route::get('/{categorySlug?}/{subCategorySlug?}', 'WebController@getIndex')->name('catalog');
