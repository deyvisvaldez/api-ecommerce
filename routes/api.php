<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('/fotos', 'DatabaseController@dropbox');

Route::group(['prefix' => 'wiltex'], function () {
    Route::get('/locations', 'DatabaseController@locations');
    Route::get('/attributes', 'DatabaseController@attributes');
    Route::get('/categories', 'DatabaseController@categories');
    Route::get('/items', 'DatabaseController@items');
    Route::get('/photos', 'DatabaseController@photos');
});

/*Route::group(['prefix' => 'fixes'], function () {
    Route::get('/', 'DatabaseController@testo');
    Route::get('/prices', 'DatabaseController@fix_prices');
    Route::get('/photos', 'DatabaseController@fix_photos');
    Route::get('/clinic-pivot', 'DatabaseController@clinic_pivot');
});*/
