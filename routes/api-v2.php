<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes Version 2. Default => api/v1/
|--------------------------------------------------------------------------
|
| Aqui van la rutas de la Versión 2
| de la API de Company.
|
*/
Route::group(['namespace' => 'Landing'], function () {

    Route::post('reset' , 'UserController@resetPassword');

    Route::get('last_items' , 'CategoryController@getCatalog');

    Route::get('company', 'CompanyController@first');

    Route::get('currencies', 'CurrencyController@index');

    Route::get('countries', 'CountryController@getCountries');

    Route::get('destinations', 'RegionController@index');

    Route::get('accounts', 'AccountController@index');

    Route::get('transports', 'TransportController@index');

    Route::group(['prefix' => 'regions'], function () {
        Route::get('/', 'RegionController@all');
        Route::get('/{regionId}', 'RegionController@show');
    });

    Route::group(['prefix' => 'web'], function () {
        Route::group(['prefix' => 'items'], function () {
            Route::get('/', 'ItemController@index');
            Route::get('/cart', 'PresentationController@webCart');
            Route::get('/last', 'ItemController@last');
            Route::get('/{itemSlug}', 'ItemController@show');
            Route::get('/{itemSlug}/related', 'ItemController@related');
        });
        Route::post('/orders', 'OrderController@storeWebOrder');
    });

    Route::group(['prefix' => 'distributor'], function () {
        Route::group(['prefix' => 'items'], function () {
            Route::get('/', 'ItemController@distributorIndex');
            Route::get('/cart', 'PresentationController@distributorCart');
            Route::get('/{itemSlug}', 'ItemController@showDistributor');
        });
    });

    Route::group(['prefix' => 'colors'], function () {
        Route::get('/', 'ColorController@index');
        Route::get('/{colorId}', 'ColorController@show');
    });

    Route::group(['prefix' => 'sizes'], function () {
        Route::get('/', 'SizeController@index');
        Route::get('/{colorId}', 'SizeController@show');
    });

    Route::get('categories', 'CategoryController@index');
    Route::get('tags', 'TagController@index');
    Route::get('collections', 'SubcategoryController@collections');

    Route::group(['prefix' => 'orders'], function () {
        Route::get('/{uuid}', 'OrderController@viewReport');
        Route::post('/', 'OrderController@store');
    });

    Route::group(['prefix' => 'customers'], function () {
        Route::get('/{identityDocument}', 'PersonController@show');
    });


    Route::group(['middleware' => ['auth:api']], function () {
        Route::post('/distributor/orders/', 'OrderController@store');
    });
});

/**
 * Routes for Authenticated Users
 */
Route::group(['middleware' => ['auth:api']], function () {

    /**
     * Routes for Admin Panel
     */
    Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {

        Route::group(['prefix' => 'company'], function(){
            //Company Info
            Route::get('/', 'CompanyController@showFirst');
            Route::put('/', 'CompanyController@updateFirst');

            Route::group(['prefix' => 'emails'], function(){
                Route::get('/', 'CompanyController@showEmail');
                Route::put('/', 'CompanyController@updateEmail');
                Route::delete('/{id}', 'EmailController@delete');
            });

            Route::group(['prefix' => 'addresses'], function(){
                Route::get('/', 'CompanyController@showAddress');
                Route::put('/', 'CompanyController@updateAddress');
                Route::delete('/{id}', 'AddressController@delete');
            });

            Route::group(['prefix' => 'phones'], function(){
                Route::get('/', 'CompanyController@showPhone');
                Route::put('/', 'CompanyController@updatePhone');
                Route::delete('/{id}', 'PhoneController@delete');
            });

            Route::group(['prefix' => 'cellphones'], function(){
                Route::get('/', 'CompanyController@showCellphone');
                Route::put('/', 'CompanyController@updateCellphone');
                Route::delete('/{id}', 'CellphoneController@delete');
            });

            Route::group(['prefix' => 'whatsapps'], function(){
                Route::get('/', 'CompanyController@showWhatsapp');
                Route::put('/', 'CompanyController@updateWhatsapp');
                Route::delete('/{id}', 'WhatsappController@delete');
            });

            Route::group(['prefix' => 'photos'], function(){
                Route::get('/', 'PhotoController@companyPhotos');
                Route::get('/{photoId}', 'PhotoController@show');
                Route::post('/', 'PhotoController@store');
                Route::put('/{photoId}', 'PhotoController@update');
                Route::put('/{photoId}/published', 'PhotoController@publish');
                Route::delete('/{photoId}', 'PhotoController@delete');
            });

            Route::group(['prefix' => 'videos'], function(){
                Route::get('/', 'VideoController@companyVideos');
                Route::get('/{videoId}', 'VideoController@show');
                Route::post('/', 'VideoController@store');
                Route::put('/{videoId}', 'VideoController@update');
                Route::put('/{videoId}/published', 'VideoController@publish');
                Route::delete('/{videoId}', 'VideoController@delete');
            });
        });

        Route::get('user', 'UserController@CurrentUser');
        Route::get('profile', 'UserController@CurrentUser');

        Route::group(['prefix' => 'users'], function() {
            Route::get('/', 'PersonController@index');
            Route::get('/{id}', 'PersonController@show');
            Route::get('/{id}/phones', 'PersonController@getPhones');
            Route::get('/{id}/cellphones', 'PersonController@getCellphones');
            Route::get('/{id}/emails', 'PersonController@getEmails');
            Route::get('/{id}/addresses', 'PersonController@getAddresses');
            Route::get('/{id}/whatsapps', 'PersonController@getWhatsapps');
            Route::post('/', 'PersonController@store');
            Route::put('/{id}', 'PersonController@update');
            Route::put('/{id}/phones', 'PersonController@updatePhone');
            Route::put('/{id}/cellphones', 'PersonController@updateCellphone');
            Route::put('/{id}/emails', 'PersonController@updateEmail');
            Route::put('/{id}/addresses', 'PersonController@updateAddress');
            Route::put('/{id}/whatsapps', 'PersonController@updateWhatsapp');
            Route::put('/{id}/publish', 'UserController@publish');
            Route::put('/{id}/password', 'UserController@password');
        });

        Route::group(['prefix' => 'people'], function() {
            Route::post('/', 'PersonController@store');
        });

        Route::group(['prefix' => 'price_rules'], function () {
            Route::get('/{priceRuleId}', 'PriceRuleController@show');
            Route::put('/{priceRuleId}', 'PriceRuleController@update');
        });

        Route::group(['prefix' => 'price_types'], function () {
            Route::get('/', 'PriceTypeController@index');
        });

        Route::group(['prefix' => 'prices'], function () {
            Route::get('/{priceId}', 'PriceController@show');
            Route::put('/{priceId}', 'PriceController@update');
        });

        Route::group(['prefix' => 'items'], function () {
            Route::get('/', 'ItemController@index');
            Route::get('/{itemSlug}/inventories', 'ItemController@inventories');
            Route::get('/{itemId}/price_rules', 'PriceRuleController@index');
            Route::get('/{itemId}/tags', 'ItemController@getTags');
            Route::get('/{itemId}/modal_inventories', 'ItemController@modalInventories');
            Route::get('/{itemSlug}', 'ItemController@show');
            Route::get('/{itemId}/colors/{colorId}', 'PresentationController@stocks');
            Route::get('/{itemId}/colors/{colorId}/photos', 'PresentationController@getPhotos');

            Route::post('/', 'ItemController@store');
            Route::post('/{itemId}/sizes/{sizeId}', 'ItemController@storeModalSize');
            Route::post('/{itemId}/colors/{colorId}', 'ItemController@storeModalColor');
            Route::post('/{itemId}/colors/{colorId}/photos', 'PresentationController@uploadPhotos');
            Route::post('/{itemId}/inventories', 'InventoryController@store');
            Route::post('/{itemId}/rules', 'PriceRuleController@store');

            Route::put('/{itemId}', 'ItemController@update');
            Route::put('/{itemId}/prices', 'PresentationController@updatePrices');
            Route::put('/{itemId}/inventories-availability', 'ItemController@updateInvetoriesAvailability');
            //Route::put('/{itemId}/rules', 'PriceRuleController@update');

            Route::delete('/{itemId}', 'ItemController@delete');
            Route::delete('/{itemId}/colors/{colorId}', 'PresentationController@deleteSameColor');
            Route::delete('/{itemId}/sizes/{sizeId}', 'PresentationController@deleteSameSize');
        });

        Route::group(['prefix' => 'tags'], function () {
            Route::get('/', 'TagController@index');
            Route::get('/{tagId}', 'TagController@show');
            Route::post('/', 'TagController@store');
            Route::put('/{tagId}', 'TagController@update');
            Route::delete('/{tagId}', 'TagController@destroy');
        });

        Route::group(['prefix' => 'presentations'], function () {
            Route::post('/{presentationId}/tags', 'PresentationController@addTag');
            Route::delete('/{presentationId}/tags/{tagId}', 'PresentationController@removeTag');
        });

        Route::group(['prefix' => 'inventories'], function () {
            Route::get('/',  'PresentationController@index');
            Route::get('/profile', 'PresentationController@profile');
            Route::get('/search',  'InventoryController@searchUpdate');
            Route::get('/{presentationId}/movements', 'InventoryController@movements');
            Route::get('/{presentationId}/stocks', 'InventoryController@otherStocks');

            Route::post('/stock', 'InventoryController@stockMovement');

            Route::put('/adjustment', 'InventoryController@stockAdjustment');
            Route::put('/prices', 'PresentationController@updateMatrixPrices');
            Route::put('/promotions', 'PresentationController@updateWebPromotion');
            Route::put('/no-promotions', 'PresentationController@disableWebPromotion');
            Route::put('/published', 'PresentationController@published');
            Route::put('/production', 'InventoryController@production');

            Route::put('/delete', 'PresentationController@delete');
        });

        Route::group(['prefix' => 'orders'], function () {
            Route::get('/', 'OrderController@index');
            Route::get('/{orderCode}', 'OrderController@show');
            Route::get('/{orderCode}/cart', 'OrderController@orderDetail');

            Route::put('/{orderId}', 'OrderController@updateOrder');
            Route::put('/{orderCode}/cancel', 'OrderController@cancel');
            Route::put('/{orderCode}/confirm', 'OrderController@confirm');
            Route::put('/{orderCode}/deliver', 'OrderController@deliver');
        });

        Route::group(['prefix' => 'images'], function () {
            Route::delete('/{uuid}', 'ImageController@delete');
        });

        Route::group(['prefix' => 'categories'], function () {
            Route::get('/', 'CategoryController@admin');
            Route::post('/', 'CategoryController@store');
            Route::post('/{categoryId}/subcategories', 'SubcategoryController@store');
            Route::get('/{categoryId}/subcategories/{subcategoryId}', 'CategoryController@showSubcategory');
            Route::get('/{categoryId}', 'CategoryController@show');
            //Route::get('/{categorySlug}', 'CategoryController@show');
            Route::put('/{categoryId}', 'CategoryController@update');
        });

        Route::group(['prefix' => 'subcategories'], function () {
            Route::get('/', 'SubcategoryController@index');
            Route::post('/', 'SubcategoryController@store');
            Route::get('/{subcategorySlug}', 'SubcategoryController@show');
            Route::put('/{subcategoryId}', 'SubcategoryController@update');
        });

        Route::group(['prefix' => 'colors'], function () {
            Route::post('/', 'ColorController@store');
            Route::put('/{colorId}', 'ColorController@update');
        });

        Route::group(['prefix' => 'sizes'], function () {
            Route::post('/', 'SizeController@store');
            Route::put('/{sizeId}', 'SizeController@update');
        });

        Route::group(['prefix' => 'locations'], function () {
            Route::get('/', 'LocationController@index');
            Route::get('/user', 'LocationController@user');
            Route::get('/{locationSlug}', 'LocationController@show');
            Route::put('/{locationId}', 'LocationController@update');
            Route::post('/', 'LocationController@store');
        });

        Route::group(['prefix' => 'regions'], function () {
            Route::post('/', 'RegionController@store');
            Route::get('/prices', 'RegionController@prices');
            Route::post('/prices', 'RegionController@storePrices');
            Route::get('/{regionId}', 'RegionController@show');
            Route::put('/{regionId}', 'RegionController@update');
            Route::get('/{regionId}/destinations', 'RegionController@destinations');
            Route::post('/{regionId}/destinations', 'RegionController@storeDestinations');
        });

        Route::group(['prefix' => 'accounts'], function () {
            Route::get('/', 'AccountController@adminIndex');
            Route::get('/{id}', 'AccountController@show');
            Route::post('/', 'AccountController@store');
            Route::put('/{id}', 'AccountController@update');
            Route::put('/{id}/publish', 'AccountController@publish');
            Route::delete('/{id}', 'AccountController@destroy');
        });

        Route::group(['prefix' => 'transports'], function () {
            Route::get('/{transportId}', 'TransportController@show');
            Route::post('/', 'TransportController@store');
            Route::put('/{transportId}', 'TransportController@update');
        });

        Route::group(['prefix' => 'countries'], function () {
            Route::get('/{countryId}', 'CountryController@show');
            Route::post('/', 'CountryController@store');
            Route::put('/{countryId}', 'CountryController@update');
        });
    });
});
