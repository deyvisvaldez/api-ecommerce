<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes Version 1. Default => api/v1/
|--------------------------------------------------------------------------
|
| Aqui van la rutas de la Versión 1
| de la API de Company.
|
*/

Route::get('inventories/excel',  'ReportController@generalExcel');
Route::get('{itemSlug}/excel',  'ReportController@itemExcel');

Route::group(['prefix' => 'customers'], function () {
    Route::get('/', 'PersonController@index');
});

Route::get('search', 'ItemController@getSearch');

//Punto de Venta
Route::group(['prefix' => 'pos'], function () {
    Route::group(['prefix' => 'items'], function () {
        Route::get('/', 'ItemController@indexPos');
        Route::get('/cart', 'InventoryController@cartPointOfSale');
        Route::get('/{itemId}', 'ItemController@showPos');
    });
});

/**
 * Routes for Authenticated Users
 */
Route::group(['middleware' => ['auth:api']], function () {

    /**
     * Routes for Admin Panel
     */
    Route::group(['prefix' => 'admin'], function () {

        Route::group(['prefix' => 'banks'], function() {
            Route::get('/', 'BankController@index');
            Route::get('/{id}', 'BankController@show');
            Route::post('/', 'BankController@store');
            Route::post('/{id}', 'BankController@update');
        });

        Route::group(['prefix' => 'costs'], function () {
            Route::post('/', 'CostController@store');
            Route::put('/', 'CostController@update');
        });

        Route::group(['prefix' => 'concepts'], function () {
            Route::post('/', 'ConceptController@store');
        });

        //Esta ruta no existe en kelvin, es de la matriz de precios de transportes
        //Route::delete('/destinations/{destinationId}', 'RegionController@deleteDestinations');

        Route::group(['prefix' => 'rules'], function () {
            Route::post('/', 'RuleController@store');
            Route::put('/{ruleId}', 'RuleController@update');
        });

        /*Route::group(['prefix' => 'items'], function () {

            //Route::get('/search', 'ItemController@pointOfSaleSearch');

            Route::put('/{itemId}/inventories-prices', 'ItemController@updateInvetoriesWebPrice');

            Route::post('/prices', 'InventoryController@storePrices');

        });*/

        /*Route::group(['prefix' => 'inventories'], function () {

            Route::get('/excel',  'ReportController@generalExcel');
            Route::get('/prices', 'InventoryController@prices');
            Route::get('/stock', 'InventoryController@stock');

        });*/

        Route::group(['prefix' => 'payments'], function () {
            Route::get('/excel', 'ReportController@paymentsExcel');
        });

        Route::group(['prefix' => 'orders'], function () {
            Route::post('/', 'OrderController@adminStore');
            Route::put('/{orderId}/payments', 'OrderController@updatePayments');
        });

        Route::group(['prefix' => 'expenditures'], function () {
            Route::get('/', 'ExpenditureController@index');
            Route::post('/', 'ExpenditureController@store');
        });

        Route::group(['prefix' => 'incomes'], function () {
            Route::get('/', 'IncomeController@index');
            Route::post('/', 'IncomeController@store');
        });

    });
});
