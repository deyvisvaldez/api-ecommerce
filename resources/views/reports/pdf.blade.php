<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Reporte</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/genericreport.css') }}">
</head>
<body>
    <main>
        <header class="clearfix">
            <div id="logo">
                <img src="{{ asset('static/images/logo.png') }}" width="150">
            </div>

            <h1>Código de Pedido: {{ $order['code'] }}</h1>

            <div id="company" class="clearfix">
                <div>{{ $company->legal_name }}</div>
                <div>{{ $company->business_name }}</div>
                <div>{{ $company->email->email }}</div>
                <div>{{ $company->whatsapp->whatsapp }}</div>
                <div>{{ $company->address->address }}</div>
            </div>

            <div id="project">
                <div>
                    <span>Operación</span>
                    @if ($order['order_type'] == 1)
                        {{"Venta"}}
                    @elseif ($order['order_type'] == 2)
                        {{"Pedido"}}
                    @elseif ($order['order_type'] == 3)
                        {{"Cotización"}}
                    @endif
                </div>
                <div><span>Cliente</span> {{ $person->first_name }} {{ $person->last_name }}</div>
                <div><span>E-mail</span> {{ $person->email->email }}</div>
                <div><span>Célular</span> {{ $person->whatsapp->whatsapp }}</div>
                <div><span>Región</span> {{ $person->region->name }}</div>
                <div><span>Dirección</span> {{ $person->address->address }}</div>

                <div>
                    <span>Registrado</span>
                    {{ $order['date'] }}
                </div>

                {{-- <div>
                    <span>Registrado</span>
                    @if ($order->order_type == 1)
                        {{ $order->date }}
                    @else
                        {{ $order->created_at->format('Y-d-m') }}
                    @endif
                </div> --}}
                {{-- @if ($order->order_type == 6)
                    <div>
                        <span>Entrega</span>
                        {{ $order->date }}
                    </div>
                @endif --}}
            </div>

        </header>

        <div id="table-container">
            <table class="table" width="100%">
                <thead>
                    <tr>
                        <th width="34%" align="center" valign="middle">
                            Nombre del Producto
                        </th>
                        <th width="12%" align="center" valign="middle">
                            Color
                        </th>
                        <th width="12%" align="center" valign="middle">
                            Talla
                        </th>
                        <th width="12%" align="center" valign="middle">
                            Precio
                        </th>
                        <th width="13%" align="center" valign="middle">
                            Cantidad
                        </th>
                        <th width="17%" align="center" valign="middle">
                            Subtotal
                        </th>
                    </tr>
                </thead>
                    @foreach ($order['items'] as $item)
                        <tbody style="border-bottom: 1px solid #C1CED9 !important">
                        <tr style="border-bottom: 1px solid #C1CED9 !important">
                            <td rowspan="{{ count($item['inventories']) }}" align="center" style="vertical-align: top !important">{{ $item['name'] }}: {{$item['description']}}</td>
                            <td align="center" valign="middle">{{ $item['inventories'][0]['color'] }}</td>
                            <td align="center" valign="middle">{{ $item['inventories'][0]['size'] }}</td>
                            <td align="center" valign="middle">{{ $item['inventories'][0]['price'] }}</td>
                            <td align="center" valign="middle">{{ $item['inventories'][0]['quantity'] }}</td>
                            <td align="center" valign="middle">{{ $item['inventories'][0]['subtotal'] }}</td>
                        </tr>

                        @foreach ($item['inventories'] as $key => $inventory)
                            @if ($key != 0)
                                <tr>
                                <td align="center" valign="middle">{{ $inventory['color'] }}</td>
                                <td align="center" valign="middle">{{ $inventory['size'] }}</td>
                                <td align="center" valign="middle">{{ $inventory['price'] }}</td>
                                <td align="center" valign="middle">{{ $inventory['quantity'] }}</td>
                                <td align="center" valign="middle">{{ $inventory['subtotal'] }}</td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    @endforeach

                    <tfoot>
                        <tr>
                            <td colspan="5" class="grand total"> TOTAL</td>
                            <td class="grand total  ">S/.{{ $order['total'] }}</td>
                        </tr>
                    </tfoot>
            </table>
        </div>

        <br>
        <br>

    </main>
    <footer>
        {{ $company->legal_name }}, {{ $company->whatsapp->whatsapp }}, {{ $company->address->address }}.
    </footer>
</body>
</html>