{{-- <!DOCTYPE html>
<html lang='es'>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"/>
    <meta name="description" content="{{ $head->description }}"/>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="twitter:card" content="summary">

    <meta property="og:locale" content="es_ES" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="{{ $head->description }}" />
    <meta property="og:image" content="{{ $head->imageUrl }}" />
    <meta property="og:title" content="{{ $head->title }}" />
    <meta property="og:site_name" content="{{ $head->url }}" />
    <meta property="og:url" content="{{ $head->url }}" />

    <meta name="twitter:site" content="{{ '@'.$head->pageTwitter }}">
    <title>{{ $head->nameCompany }}</title>

    @if(env('APP_ENV') !== 'local')
      <link rel="stylesheet" href="{{ asset('static/css/app.css') }}"/>
    @endif

    <script src="https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit"></script>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-78749646-1', 'auto');
      ga('send', 'pageview');
    </script>
  </head>

  <body>
    <div id="root"></div>

    <script>
      window.OAUTH_SECRET = '{{env('OAUTH_CLIENT_SECRET')}}';
      window.OAUTH_ID = '{{env('OAUTH_CLIENT_ID')}}';
      window.API_URL = '{{ url('/api/v1') }}';
    </script>

    @if(env('APP_ENV') === 'local')
      <script src="http://192.168.1.111:8080/app.js"></script>
    @else
      <script src="{{ asset('static/js/manifest.js') }}"></script>
      <script src="{{ asset('static/js/vendor.js') }}"></script>
      <script src="{{ asset('static/js/app.js') }}"></script>
    @endif
  </body>
</html> --}}
