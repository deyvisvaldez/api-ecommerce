<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <title>Nueva Orden</title>
</head>
<body>

  <h1>Hola {{ $fullName }}</h1>
  <h3>Usted ah recibido un pedido web desde su plataforma</h3>

  <p>Acceda al siguiente enlace para ver el detalle la orden: <a href="{{ route('public.order', ['uuid' => $uuid]) }}">{{ route('public.order', ['uuid' => $uuid]) }}</a></p>

</body>
</html>