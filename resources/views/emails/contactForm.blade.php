<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <article style="color: #828286; display: block; font-size: 18px; margin: 0 auto; max-width: 650px; text-align: center" class="email">
    <img style="display: inline-block; margin-bottom: 2em" class="email__logo" src="{{$company['logotype']}}" alt="{{ $company['name'] }}">
    <span style="font-size: 2em; font-weight: 300" class="email__title">¡Hola!</span>
    <p style="margin-bottom: 2rem" class="mb4">Tienes <strong style="color: #f00" class="primary">un nuevo mensaje</strong> para atender</p>

    <hr>
    <p style="undefined;margin-top: 1rem; margin-bottom: 1rem" class="email__sub-title my3">Datos de consultante</p>
    <table style="border-collapse: collapse; border-spacing: 0; width: 100%;text-align: left" class="table left-align">
      <tbody style="border-top: 1px dashed #828286; border-bottom: 1px dashed #828286" class="table__t">
        <tr>
          <th style="padding-top: 1rem" class="pt3">Nombres y Apellidos:  </th>
          <td style="padding-top: 1rem" class="pt3">{{ $content['fullName'] }}</td>
        </tr>
        <tr>
          <th>Email: </th>
          <td>{{ $content['email'] }}</td>
        </tr>
        <tr>
          <th>Whatsapp o Número Telefónico: </th>
          <td> {{ $content['cellphone'] }} </td>
        </tr>
        <tr>
          <th>Tema a tratar: </th>
          <td>{{ $content['subject'] }}</td>
        </tr>
        <tr>
          <th style="padding-bottom: 1rem" class="pb3">Mensaje: </th>
          <td style="padding-bottom: 1rem" class="pb3">{{ $content['message'] }}</td>
        </tr>
      </tbody>
    </table>
  </article>
</body>
</html>
