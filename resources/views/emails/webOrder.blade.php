<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <title>Nueva Orden</title>
</head>
<body>

  <h1>Hola {{ $fullName }}</h1>
  <h3>Usted ah realizado un pedido web desde la tienda virtual.</h3>

  @if (isset($text))
    <p>{{ $text }}</p>
  @endif

  <p>Acceda al siguiente enlace para ver el detalle de la orden: <a href="{{ route('public.order', ['uuid' => $uuid]) }}">{{ route('public.order', ['uuid' => $uuid]) }}</a></p>

</body>
</html>