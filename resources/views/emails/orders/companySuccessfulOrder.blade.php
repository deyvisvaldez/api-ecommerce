<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <article style="color: #828286; display: block; font-size: 18px; margin: 0 auto; max-width: 650px; text-align: center" class="email">
    <img style="display: inline-block; margin-bottom: 2em" class="email__logo" src="{{$company['logotype']}}" alt="{{ $company['name'] }}">
    <span style="font-size: 2em; font-weight: 300" class="email__title">¡Hola!</span>
    <p style="margin-bottom: 2rem" class="mb4">Tienes <strong style="color: #f00" class="primary">nuevo pedido</strong> de tu <a style="color: #0074d9; text-decoration: none" class="link" href="">plataforma web</a> para atender</p>
    <table style="border-collapse: collapse; border-spacing: 0; width: 100%" class="table">
      <thead style="border-top: 1px dashed #828286; border-bottom: 1px dashed #828286" class="table__t">
        <tr>
          <th style="padding-bottom: 1rem; padding-top: 1rem" class="py3">Producto</th>
          @foreach ($order['itemAttributes'] as $itemAttribute)
            <th style="padding-bottom: 1rem; padding-top: 1rem" class="py3">{{$itemAttribute}}</th>
          @endforeach
          <th style="padding-bottom: 1rem; padding-top: 1rem" class="py3">Precio unit.</th>
          <th style="padding-bottom: 1rem; padding-top: 1rem" class="py3">Cantidad</th>
          <th style="padding-bottom: 1rem; padding-top: 1rem" class="py3">Subtotal</th>
        </tr>
      </thead>
      <tbody style="border-top: 1px dashed #828286; border-bottom: 1px dashed #828286" class="table__t">
        @foreach ($order['itemsName'] as $name => $quantity)
          <?php $count = 0;?>
          @foreach($order['items'] as $key => $item)
            @if ($name == $item['item'])
              @if ($count == 0)
                <tr>
                  <td rowspan="{{ $quantity }}" style="padding-top: 1rem" class="pt3">{{$item['item']}}</td>
                  @foreach ($order['itemAttributes'] as $itemAttribute)
                    <?php $flag = 0;?>
                    @foreach ($item['attributes'] as $attribute => $value)
                      @if ($itemAttribute == $attribute)
                        <td style="padding-top: 1rem" class="pt3">{{$value}}</td>
                        <?php $flag = 1;?>
                        <?php break;?>
                      @endif
                    @endforeach
                    @if ($flag == 0)
                      <td style="padding-top: 1rem" class="pt3"> - </td>
                    @endif
                  @endforeach
                  <td style="padding-top: 1rem" class="pt3">{{$item['price']}}</td>
                  <td style="padding-top: 1rem" class="pt3">{{$item['quantity']}}</td>
                  <td style="padding-top: 1rem" class="pt3">{{$item['subtotal']}}</td>
                </tr>
                <?php $count = 1;?>
              @else
                <tr>
                  @foreach ($order['itemAttributes'] as $itemAttribute)
                    <?php $flag = 0;?>
                    @foreach ($item['attributes'] as $attribute => $value)
                      @if ($itemAttribute == $attribute)
                        <td style="padding-top: 1rem" class="pt3">{{$value}}</td>
                        <?php $flag = 1;?>
                        <?php break;?>
                      @endif
                    @endforeach
                    @if ($flag == 0)
                      <td style="padding-top: 1rem" class="pt3"> - </td>
                    @endif
                  @endforeach
                  <td style="padding-top: 1rem" class="pt3">{{$item['price']}}</td>
                  <td style="padding-top: 1rem" class="pt3">{{$item['quantity']}}</td>
                  <td style="padding-top: 1rem" class="pt3">{{$item['subtotal']}}</td>
                </tr>
              @endif
            @endif
          @endforeach
        @endforeach
      </tbody>
    </table>
    <p style="margin-bottom: 0" class="mb0">TOTAL</p>
    <p style="font-size: 2em; font-weight: 300;color: #f00;margin-top: 0;margin-bottom: 1rem" class="email__title primary mt0 mb3"><strong>s./{{ $order['total'] }}</strong></p>
    <hr>
    <p style="margin: .6em 0; align-items: center; display: flex; flex-direction: row; flex-wrap: wrap; justify-content: space-around" class="email__detail">
      <span>Fecha: {{ $order['date'] }}</span>
      <span>Hora: {{ $order['time'] }}</span>
    </p>
    <hr>
    <p style="undefined;margin-top: 1rem; margin-bottom: 1rem" class="email__sub-title my3">Datos del comprador</p>
    <table style="border-collapse: collapse; border-spacing: 0; width: 100%;text-align: left" class="table left-align">
      <tbody style="border-top: 1px dashed #828286; border-bottom: 1px dashed #828286" class="table__t">
        <tr>
          <th style="padding-top: 1rem" class="pt3">Correo:  </th>
          <td style="padding-top: 1rem" class="pt3">
            @foreach ($customer['emails'] as $key => $email)
              @if ($key != 0)
                |
              @endif
              {{$email}}
            @endforeach
          </td>
        </tr>
        <tr>
          <th>De: </th>
          <td>{{$customer['firstName'].' '.$customer['lastName']}}</td>
        </tr>
        <tr>
          <th>Contacto: </th>
          <td>
            @foreach ($customer['whatsapps'] as $key => $whatsapp)
              @if ($key != 0)
                |
              @endif
              {{$whatsapp}}
            @endforeach
          </td>
        </tr>
        <tr>
          <th>Procedencia: </th>
          <td>{{$customer['country']}} - {{$customer['region']}}</td>
        </tr>
        {{-- <tr>
          <th style="padding-bottom: 1rem" class="pb3">Mensaje: </th>
          <td style="padding-bottom: 1rem" class="pb3">{{ $order['description'] }}</td>
        </tr> --}}
      </tbody>
    </table>
  </article>
</body>
</html>
