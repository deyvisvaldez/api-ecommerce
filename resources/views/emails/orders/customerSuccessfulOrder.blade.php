<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <article style="color: #828286; display: block; font-size: 18px; margin: 0 auto; max-width: 650px; text-align: center" class="email">
    <img style="display: inline-block; margin-bottom: 2em" class="email__logo" src="{{$company['logotype']}}" alt="{{ $company['name'] }}">
    <span style="font-size: 2em; font-weight: 300" class="email__title">¡Hola {{ $customer['firstName'].' '.$customer['lastName'] }}!</span>
    <p style="margin-bottom: 2rem" class="mb4">Esta es la lista de productos que elegiste</p>
    <table style="border-collapse: collapse; border-spacing: 0; width: 100%" class="table">
      <thead style="border-top: 1px dashed #828286; border-bottom: 1px dashed #828286" class="table__t">
        <tr>
          <th style="padding-bottom: 1rem; padding-top: 1rem" class="py3">Producto</th>
          @foreach ($order['itemAttributes'] as $itemAttribute)
            <th style="padding-bottom: 1rem; padding-top: 1rem" class="py3">{{$itemAttribute}}</th>
          @endforeach
          <th style="padding-bottom: 1rem; padding-top: 1rem" class="py3">Precio unit.</th>
          <th style="padding-bottom: 1rem; padding-top: 1rem" class="py3">Cantidad</th>
          <th style="padding-bottom: 1rem; padding-top: 1rem" class="py3">Subtotal</th>
        </tr>
      </thead>
      <tbody style="border-top: 1px dashed #828286; border-bottom: 1px dashed #828286" class="table__t">
  		  @foreach ($order['itemsName'] as $name => $quantity)
          <?php $count = 0;?>
          @foreach($order['items'] as $key => $item)
            @if ($name == $item['item'])
              @if ($count == 0)
                <tr>
                  <td rowspan="{{ $quantity }}" style="padding-top: 1rem" class="pt3">{{$item['item']}}</td>
                  @foreach ($order['itemAttributes'] as $itemAttribute)
                    <?php $flag = 0;?>
                    @foreach ($item['attributes'] as $attribute => $value)
                      @if ($itemAttribute == $attribute)
                        <td style="padding-top: 1rem" class="pt3">{{$value}}</td>
                        <?php $flag = 1;?>
                        <?php break;?>
                      @endif
                    @endforeach
                    @if ($flag == 0)
                      <td style="padding-top: 1rem" class="pt3"> - </td>
                    @endif
                  @endforeach
                  <td style="padding-top: 1rem" class="pt3">{{$item['price']}}</td>
                  <td style="padding-top: 1rem" class="pt3">{{$item['quantity']}}</td>
                  <td style="padding-top: 1rem" class="pt3">{{$item['subtotal']}}</td>
                </tr>
                <?php $count = 1;?>
              @else
                <tr>
                  @foreach ($order['itemAttributes'] as $itemAttribute)
                    <?php $flag = 0;?>
                    @foreach ($item['attributes'] as $attribute => $value)
                      @if ($itemAttribute == $attribute)
                        <td style="padding-top: 1rem" class="pt3">{{$value}}</td>
                        <?php $flag = 1;?>
                        <?php break;?>
                      @endif
                    @endforeach
                    @if ($flag == 0)
                      <td style="padding-top: 1rem" class="pt3"> - </td>
                    @endif
                  @endforeach
                  <td style="padding-top: 1rem" class="pt3">{{$item['price']}}</td>
                  <td style="padding-top: 1rem" class="pt3">{{$item['quantity']}}</td>
                  <td style="padding-top: 1rem" class="pt3">{{$item['subtotal']}}</td>
                </tr>
              @endif
            @endif
          @endforeach
        @endforeach
      </tbody>
    </table>
    <p style="margin-bottom: 0" class="mb0">TOTAL</p>
    <p style="font-size: 2em; font-weight: 300;color: #f00;margin-top: 0;margin-bottom: 1rem" class="email__title primary mt0 mb3"><strong>s./{{ $order['total'] }}</strong></p>
    <hr>
    <p style="margin: .6em 0; align-items: center; display: flex; flex-direction: row; flex-wrap: wrap; justify-content: space-around" class="email__detail">
      <span>Fecha: {{ $order['date'] }}</span>
      <span>Hora: {{ $order['time'] }}</span>
    </p>
    <hr>
    <p style="font-size: 1.3em; text-transform: uppercase;margin: 0" class="email__sub-title m0">Gracias por realizar tu pedido</p>
    <p style="font-size: 0.98em; text-transform: uppercase;margin: 0" class="email__sub-sub-title m0">- Pronto nos comunicaremos contigo -</p>
    <hr>
    <footer style="font-size: 0.9em;padding-top: 1rem;padding-bottom: 1rem" class="footer pt3 pb3">
      <span style="font-size: 1.3em; text-transform: uppercase" class="email__sub-title">{{$company['name']}}</span>
      <p style="margin: 0" class="m0">RUC: {{$company['ruc']}}</p>
      <p style="margin: 0" class="m0">Whatsapp:
        @if (count($company['whatsapps']))
          @foreach ($company['whatsapps'] as $key =>$whatsapp)
            @if ($key != 0)
              |
            @endif
            {{$whatsapp}}
          @endforeach
        @endif
      </p>
      <p style="margin: 0" class="m0">Teléfono:
        @if (count($company['phones']))
          @foreach ($company['phones'] as $key =>$phone)
            @if ($key != 0)
              |
            @endif
            {{$phone}}
          @endforeach
        @endif
      </p>
      <p style="margin: 0" class="m0">Correo:
        @if (count($company['emails']))
          @foreach ($company['emails'] as $key =>$email)
            @if ($key != 0)
              |
            @endif
            {{$email}}
          @endforeach
        @endif
      </p>
      <p style="margin: 0" class="m0">Direccion:
        @if (count($company['addresses']))
          @foreach ($company['addresses'] as $key =>$address)
            @if ($key != 0)
              |
            @endif
            {{$address}}
          @endforeach
        @endif
      </p>
    </footer>
    <hr>
	@if(count($company['billing_cards']))
	    <p style=";font-size: 1.3em; text-transform: uppercase;margin-bottom: 1rem" class="email__sub-title mb3">- Deposite a nuestras cuentas -</p>
		@foreach($company['builling_cards'] as $key => $billing_card)
			<ul style=";margin: 0;padding: 0" class="m0 p0">
			  <li style=";list-style: none; margin-bottom: .25rem" class="check-item">
				<p style=";margin: 0">{{ $billing_card['account_name'] }}</p>
				<div>
				  <img style=";max-height: 30px; vertical-align: middle" src="{{$billing_card['bank_image_thumb']}}">
				  <span style=";font-weight: 700">{{ $billing_card['account_number'] }}</span>
				</div>
			  </li>
			</ul>
		@endforeach
	@endif
    <hr>
  </article>
</body>
</html>
